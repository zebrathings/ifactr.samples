﻿using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Webkit;
using System.Web.Mvc;
using System.Web.Routing;
using iFactr.UI;

namespace Webkit.Container
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",                                              // Route name
                "{*url}",                                               // URL with parameters
                new { controller = "App", action = "Render", id = "" },  // Parameter defaults
                new string[] { "Webkit.Container.Controllers" }
            );

        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            //initialize webkit factory

            iApp.OnLayerLoadComplete += WebkitFactory.Instance.OutputLayer;
        }
        protected void Session_Start()
        {
            // initialize iFactr application
            WebkitFactory.TheApp = new KitchenSink.App();
        }
    }
}