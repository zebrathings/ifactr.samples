﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;


using MonoCross.Navigation;

using iFactr.Core;
using iFactr.UI;
using iFactr.Utilities;
using iFactr.Webkit;

namespace iFactr.Webkit.SignalR.NavHub
{
    [HubName("NavHub")]
    public class NavHub: Hub
    {
        public void Nav(string url)
        {
            iApp.Navigate(url);
        }

        public void NavBack()
        {
            
            
        }

        public void NavForward()
        { 
           ///if Navback load last view from pane manager
        }
    }
}

