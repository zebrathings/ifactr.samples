﻿using iFactr.Core.Utilities;
using iFactr.Webkit;
using System.Threading;
using System.Web.Mvc;

namespace Webkit.Container.Controllers
{
    /// <summary>
    /// App Controller for Webkit
    /// </summary>
    public class AppController : Controller
    {
        private ManualResetEvent _loading = new ManualResetEvent(true);

        // GET: /Render/
        public void Render(string url, string args)
        {
            _loading.Reset();
            WebkitFactory.OnOutputLayerComplete += OutputLayerComplete;

            // navigate to url
            if (url == null)
            {
                url = WebkitFactory.TheApp.NavigateOnLoad;
            }

            // Initiates navigation to the specified layer & performs any validation on POST.
            WebkitFactory.Instance.Navigate(url, this.Request);

            // wait for web factory to output the layer
            if (!_loading.WaitOne())
                Device.Log.Info("AppController.Render() failed on WaitOne() for the OutputLayerComplete() event");

            WebkitFactory.OnOutputLayerComplete -= OutputLayerComplete;
        }

        private void OutputLayerComplete()
        {
            _loading.Set();
        }
    }
}