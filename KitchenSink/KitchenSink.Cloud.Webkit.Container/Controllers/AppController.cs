using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Webkit;

namespace KitchenSink.Cloud.Webkit.Container.Controllers
{
    public class AppController : Controller
    {

        private bool loading = true;

        // GET: /Render/
        public void Render( string url, string args )
        {
            // set callback handler
            WebkitFactory.OnOutputLayerComplete += () =>
            {
                this.OutputLayerComplete();
            };

            // navigate to url
            url = url == null ? WebkitFactory.TheApp.NavigateOnLoad : url;

            // iApp.Navigate(url, WebkitFactory.Instance.GetParameters(this.Request));
            // Initiates navigation to the specified layer & performs any validation on POST.
            WebkitFactory.Instance.Navigate(url, this.Request);            

            // wait for web factory to output the layer
            while ( loading )
                System.Threading.Thread.Sleep( 100 );
        }

        private void OutputLayerComplete()
        {
            loading = false;
        }
    }
}
