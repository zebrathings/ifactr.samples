﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Styles;
using MonoCross.Utilities;
using iFactr.UI;
using iFactr.UI.Controls;
using ActionType = iFactr.UI.ActionType;
using Link = iFactr.UI.Link;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    class StyleView : ListView<Style>
    {
        private List<SelectListFieldItem> _colorDefs;

        public StyleView()
        {
            Rendering += (sender, args) =>
            {
                _colorDefs = new List<SelectListFieldItem>(StyleController.GetColors(Model).Select(s => new SelectListFieldItem(s.Key, s.Value)));
            };
        }

        protected override void OnRender()
        {
            Title = "Style";

            TitleColor = iApp.Instance.Style.HeaderTextColor;
            HeaderColor = iApp.Instance.Style.HeaderColor;
            SeparatorColor = iApp.Instance.Style.SeparatorColor;

            if (iApp.Instance.Style.LayerBackgroundImage != null)
            {
                SetBackground(iApp.Instance.Style.LayerBackgroundImage, ContentStretch.Fill);
            }
            else
            {
                SetBackground(iApp.Instance.Style.LayerBackgroundColor);
            }

            Sections.Clear();
            Sections.Add(new Section(StyleController.GetColorProperties().Count(), "Choose colors"));

            var filename = iApp.Session.GetValueOrDefault("CurrentStyle", string.Empty).ToString();
            filename = filename == string.Empty ? filename : filename.Remove(filename.LastIndexOf('.')).Substring(filename.LastIndexOf(Device.DirectorySeparatorChar) + 1);

            var fields = new Field[]
            {
                new TextField("filename") { Label = "Theme Name", Text = filename, },
                new ButtonField(null, "Style/Select") { Text = "Save Theme", Link = { Parameters = { { "Save", null }, }, }, },
            };

            Sections.Add(new Section(2)
            {
                ItemIdRequested = i => 50,
                CellRequested = (index, cell) => fields[index].Convert(iApp.Instance.Style, this, cell),
            });

            Menu = new Menu(new MenuButton(iApp.Factory.GetResourceString("Submit")) { NavigationLink = new Link("Style/Select") { Action = ActionType.Submit, } });
        }

        protected override int OnItemIdRequested(int section, int index)
        {
            return 100 + index;
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var prop = StyleController.GetColorProperties().ElementAt(index);
            var cell = recycledCell as HeaderedControlCell ?? new HeaderedControlCell(prop.Name);
            var color = (Color)StyleController.GetColorProperties().First(propt => propt.Name == prop.Name).GetValue(Model, null);
            var colorCode = "#" + color.GetHashCode().ToString("x8").ToUpper();

            ISelectList selectList = null;
            ITextBox textBox = null;

            if (recycledCell != null)
            {
                textBox = cell.GetChild<ITextBox>();
                selectList = cell.GetChild<ISelectList>();
                var header = cell.Header;
                header.Text = prop.Name;
            }

            if (textBox == null || selectList == null)
            {
                foreach (var child in cell.Children.Where(c => !c.Equals(cell.Header)))
                {
                    cell.RemoveControl(child as IControl);
                }

                selectList = new SelectList();

                var defaults = new List<SelectListFieldItem>(_colorDefs);
                defaults.Insert(0, new SelectListFieldItem(colorCode, color.HexCode));
                selectList.Items = defaults;

                selectList.SelectionChanged += SelectList_SelectionChanged;
                cell.AddControl(selectList);

                textBox = new TextBox
                {
                    Expression = "^#?[A-Fa-f0-9]{0,8}$",
                };
                textBox.TextChanged += TextBox_TextChanged;
                cell.AddControl(textBox);
            }

            if (selectList.SubmitKey != prop.Name)
            {
                selectList.SubmitKey = prop.Name;
                textBox.SubmitKey = prop.Name + ".Key";
                textBox.Text = colorCode;
            }
            cell.SelectionStyle = SelectionStyle.None;
            return cell;
        }

        private void TextBox_TextChanged(object sender, ValueChangedEventArgs<string> args)
        {
            var textBox = (ITextBox)sender;
            var c = new Color(args.NewValue);
            if (c == Color.Black && !args.NewValue.EndsWith("000")) return;
            var propName = textBox.SubmitKey.Remove(textBox.SubmitKey.LastIndexOf('.'));
            var prop = Device.Reflector.GetProperty(ModelType, propName);
            prop.SetValue(Model, c, null);
            textBox.BackgroundColor = c.IsDefaultColor ? Color.Gray : c;
            var items = new List<SelectListFieldItem>(_colorDefs);
            items.Insert(0, new SelectListFieldItem("#" + c.GetHashCode().ToString("x8").ToUpper(), c.HexCode));
            var selectList = ((IElementHost)textBox.Parent).GetChild<ISelectList>();
            selectList.Items = items;
            selectList.SelectedIndex = 0;
            textBox.ForegroundColor = c.IsDefaultColor ? Color.White : new Color((byte)(255 - c.R), (byte)(255 - c.G), (byte)(255 - c.B));
        }

        private void SelectList_SelectionChanged(object sender, ValueChangedEventArgs<object> args)
        {
            if (args.NewValue == null) return;
            var value = ((SelectListFieldItem)args.NewValue).Key;
            var c = new Color(value);
            var textBox = ((IElementHost)((ISelectList)sender).Parent).GetChild<ITextBox>();
            if (c != textBox.BackgroundColor)
            {
                textBox.Text = value;
            }
        }
    }
}