﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using iFactr.Core;
using iFactr.Integrations;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink
{
    public class GpsView : ListView<iFactr.Integrations.GeoLocation>, INotifyPropertyChanged
    {
        public GpsView() :
            base((ListViewStyle)Math.Abs((int)App.Layout - 1))
        { }

        private readonly Queue<GeoLocationData> _data = new Queue<GeoLocationData>();

        private GeoLocationData Location
        {
            get { return _data.Any() ? new GeoLocationData(_data.Average(d => d.Latitude), _data.Average(d => d.Longitude)) : new GeoLocationData(); }
        }

        public string Latitude
        {
            get { return Location.Latitude.ToString(); }
        }

        public string Longitude
        {
            get { return Location.Longitude.ToString(); }
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var cell = recycledCell as GridCell ?? new GridCell();

            var latHeader = cell.GetOrCreateChild<ILabel>("latHeader");
            latHeader.Font = Font.PreferredHeaderFont;
            latHeader.Text = "Latitude";

            var lat = cell.GetOrCreateChild<ILabel>("lat");
            lat.SetBinding(new Binding("Text", "Latitude") { Source = this, });

            var lonHeader = cell.GetOrCreateChild<ILabel>("lonHeader");
            lonHeader.Font = Font.PreferredHeaderFont;
            lonHeader.Text = "Longitude";

            var lon = cell.GetOrCreateChild<ILabel>("lon");
            lon.SetBinding(new Binding("Text", "Longitude") { Source = this, });

            return cell;
        }

        protected override void OnRender()
        {
            Title = "Your Location";

            if (iApp.Session.ContainsKey("Lat"))
            {
                _data.Enqueue(new GeoLocationData(Convert.ToDouble(iApp.Session["Lat"]), Convert.ToDouble(iApp.Session["Lon"])));
                OnPropertyChanged("Latitude");
                OnPropertyChanged("Longitude");
                iApp.Session.Remove("Lat");
                iApp.Session.Remove("Lon");
            }
            Menu = new Menu(new MenuButton("Reload") { NavigationLink = new Link("compass://?callback=Integrations/Geo") });
            Model.LocationUpdated += (o, e) =>
            {
                _data.Enqueue(e.Data);
                while (_data.Count > 10)
                {
                    _data.Dequeue();
                }
                OnPropertyChanged("Latitude");
                OnPropertyChanged("Longitude");
            };

            Activated += (o, e) => Model.Start();
            Deactivated += (o, e) => Model.Stop();

            Sections.Add(1, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}