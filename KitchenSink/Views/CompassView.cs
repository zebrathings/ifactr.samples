﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink
{
    public class CompassView : ListView<iFactr.Integrations.Compass>, INotifyPropertyChanged, IValueConverter
    {
        public CompassView() :
            base((ListViewStyle)Math.Abs((int)App.Layout - 1))
        { }

        private readonly Queue<double> _data = new Queue<double>();

        public string Bearing
        {
            get { return _bearing == null ? null : (_bearing + "°"); }
            set
            {
                if (_bearing == value) return;
                _bearing = value;
                OnPropertyChanged("Bearing");
            }
        }
        private string _bearing;

        public string BearingHeader
        {
            get { return _bearingHeader; }
            set
            {
                if (_bearingHeader == value) return;
                _bearingHeader = value;
                OnPropertyChanged("BearingHeader");
            }
        }
        private string _bearingHeader;

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var cell = recycledCell as GridCell ?? new GridCell();
            cell.Columns.Clear();
            cell.Columns.Add(Column.OneStar);
            cell.Columns.Add(Column.AutoSized);

            var header = cell.GetOrCreateChild<ILabel>("header");
            header.ColumnSpan = 2;
            header.Font = Font.PreferredHeaderFont;
            header.SetBinding(new Binding("Text", "BearingHeader") { Source = this, });

            var value = cell.GetOrCreateChild<ILabel>("value");
            value.SetBinding(new Binding("Text", "Bearing") { Source = this, });

            var convert = cell.GetOrCreateChild<ILabel>("convert");
            convert.SetBinding(new Binding("Text", "Bearing") { Source = this, ValueConverter = this });

            return cell;
        }

        protected override void OnRender()
        {
            Title = "Bearing";

            if (iApp.Session.ContainsKey("Bearing"))
            {
                BearingHeader = "Compass bearing";
                Bearing = iApp.Session["Bearing"].ToString();
                iApp.Session.Remove("Bearing");
            }
            else
            {
                BearingHeader = "There was a problem retrieving your bearing.";
                Bearing = null;
            }
            Menu = new Menu(new MenuButton("Reload") { NavigationLink = new Link("compass://?callback=Integrations/Compass") });
            Model.HeadingUpdated += (o, e) =>
            {
                _data.Enqueue(e.Data.TrueHeading);
                while (_data.Count > 10)
                {
                    _data.Dequeue();
                }
                Bearing = ((((int)Math.Round(_data.Average())) / 2) * 2).ToString("###");
            };

            Activated += (o, e) => Model.Start();
            Deactivated += (o, e) => Model.Stop();

            Sections.Add(1, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public object Convert(object value, Type targetType, object parameter)
        {
            var bearing = System.Convert.ToDouble(_bearing);
            if (bearing < 11.25)
                return "N";
            if (bearing < 33.75)
                return "NNE";
            if (bearing < 56.25)
                return "NE";
            if (bearing < 78.75)
                return "ENE";
            if (bearing < 101.25)
                return "E";
            if (bearing < 123.75)
                return "ESE";
            if (bearing < 146.25)
                return "SE";
            if (bearing < 168.75)
                return "SSE";
            if (bearing < 191.25)
                return "S";
            if (bearing < 213.75)
                return "SSW";
            if (bearing < 236.25)
                return "SW";
            if (bearing < 258.75)
                return "WSW";
            if (bearing < 281.25)
                return "W";
            if (bearing < 303.75)
                return "WNW";
            if (bearing < 326.25)
                return "NW";
            if (bearing < 348.75)
                return "NNW";
            return "N";
        }

        public object ConvertBack(object value, Type targetType, object parameter)
        {
            throw new NotImplementedException();
        }
    }
}