﻿using System;
using System.Collections.Generic;
using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink
{
    public class ClickMeView : GridView<string>
    {
        public const string RowKey = "SessionRowKey";
        public const string ColumnKey = "SessionColumnKey";
        protected override void OnRender()
        {
            Title = Model;  // Take title from View Model (string passed by controller)

            // 3 columns in the cell to accommodate "Click Me" circle
            Columns.Add(Column.OneStar);
            Columns.Add(Column.OneStar);
            Columns.Add(Column.OneStar);

            Rows.Add(new Row(Cell.StandardCellHeight, LayoutUnitType.Absolute));
            Rows.Add(new Row(Cell.StandardCellHeight, LayoutUnitType.Absolute));
            Rows.Add(new Row(Cell.StandardCellHeight, LayoutUnitType.Absolute));

            ILabel label = new Label
            {
                ColumnIndex = (int)iApp.Session.GetValueOrDefault(ColumnKey, 0),
                RowIndex = (int)iApp.Session.GetValueOrDefault(RowKey, 0),
                Text = "Cycle",
                ForegroundColor = Color.Purple,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
            };

            IButton button = new Button
            {
                Title = "Click me!",
                BackgroundColor = Color.Gray,
                ForegroundColor = Color.White,
                ColumnIndex = 1,
                RowIndex = 1,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Center,
            };

            button.Clicked += (sender, e) =>
            {
                if (label.RowIndex == 0)
                {
                    if (label.ColumnIndex < 2)
                        label.ColumnIndex++;
                    else
                        label.RowIndex++;
                }
                else if (label.RowIndex == 1)
                {
                    if (label.ColumnIndex == 2)
                        label.RowIndex++;
                    else
                        label.RowIndex--;
                }
                else if (label.RowIndex == 2)
                {
                    if (label.ColumnIndex > 0)
                        label.ColumnIndex--;
                    else
                        label.RowIndex--;
                }
                else { throw new Exception("Unexpected row/column configuration"); }
                iApp.Session[RowKey] = label.RowIndex;
                iApp.Session[ColumnKey] = label.ColumnIndex;

                this.PerformLayout(new Size(), new Size(Width, Height));
            };

            AddChild(label);
            AddChild(button);
        }
    }
}