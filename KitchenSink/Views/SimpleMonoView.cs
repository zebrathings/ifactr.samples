﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MonoCross.Utilities;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink.Views
{
    //[PreferredPane(Pane.Popover)]
    //[PreferredPane(Pane.Master)]
    //[PreferredPane(Pane.Detail)]
    public class SimpleMonoView : ListView<string>
    {
        private readonly Dictionary<string, Type> _types = Device.Reflector.GetTypes(Device.Reflector.GetAssembly(typeof(Switch)))
            .Where(t => t.Namespace.StartsWith("iFactr.UI.Controls") &&
                        !Device.Reflector.IsAbstract(t) &&
                        !t.Name.Contains("<>c"))
            .ToDictionary(t => Regex.Replace(t.Name, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0"));

        protected override void OnRender()
        {
            Title = Model;  // Take title from View Model (string passed by controller)
            Sections[0] = new Section(_types.Count);
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var typeEntry = _types.ElementAtOrDefault(index);
            var type = typeEntry.Value;
            var controlName = typeEntry.Key;

            var controls = recycledCell as HeaderedControlCell ?? new HeaderedControlCell(controlName);
            var control = (IControl)(controls.GetChild("control") ?? Activator.CreateInstance(type));
            control.ID = "control";

            new TypeSwitch(control)
                .Case<ITextBox>(t =>
                {
                    t.Text = controlName;
                })
                .Case<ILabel>(l =>
                {
                    l.Text = controlName;
                })
                .Case<ITextArea>(t =>
                {
                    t.Text = controlName;
                })
                .Case<IPasswordBox>(p =>
                {
                    p.Password = controlName;
                })
                .Case<ISelectList>(s =>
                {
                    s.Items = _types.Keys;
                })
                .Case<IButton>(b =>
                {
                    b.Title = controlName;
                    b.HorizontalAlignment = HorizontalAlignment.Stretch;
                })
                .Case<IImage>(i =>
                {
                    i.FilePath = "LoginTestBG.jpg";
                });

            if (!controls.Children.Contains(control))
                controls.AddControl(control);
            return controls;
        }

        protected override int OnItemIdRequested(int section, int index)
        {
            return index;
        }
    }
}