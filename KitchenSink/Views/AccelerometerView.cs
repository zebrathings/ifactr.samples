﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using iFactr.Core;
using iFactr.Integrations;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink
{
    public class AccelerometerView : ListView<iFactr.Integrations.Accelerometer>, INotifyPropertyChanged
    {
        public AccelerometerView() :
            base((ListViewStyle)Math.Abs((int)App.Layout - 1))
        { }

        private AccelerometerData _data = default(AccelerometerData);

        public string X
        {
            get { return _data.X + " m/s²"; }
        }

        public string Y
        {
            get { return _data.Y + " m/s²"; }
        }

        public string Z
        {
            get { return _data.Z + " m/s²"; }
        }

        public string AccelerationHeader
        {
            get { return _accelerationHeader; }
            set
            {
                if (_accelerationHeader == value) return;
                _accelerationHeader = value;
                OnPropertyChanged("AccelerationHeader");
            }
        }
        private string _accelerationHeader;

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var cell = recycledCell as GridCell ?? new GridCell();
            cell.Columns.Clear();
            cell.Columns.Add(Column.AutoSized);
            cell.Columns.Add(Column.OneStar);

            var header = cell.GetOrCreateChild<ILabel>("header");
            header.ColumnSpan = 2;
            header.Font = Font.PreferredHeaderFont;
            header.SetBinding(new Binding("Text", "AccelerationHeader") { Source = this, });

            var xHeader = cell.GetOrCreateChild<ILabel>("xHeader");
            xHeader.VerticalAlignment = VerticalAlignment.Bottom;
            xHeader.Font = Font.PreferredHeaderFont;
            xHeader.Text = "X: ";

            var xValue = cell.GetOrCreateChild<ILabel>("x");
            xValue.VerticalAlignment = VerticalAlignment.Bottom;
            xValue.SetBinding(new Binding("Text", "X") { Source = this, });

            var yHeader = cell.GetOrCreateChild<ILabel>("yHeader");
            xHeader.VerticalAlignment = VerticalAlignment.Bottom;
            yHeader.Font = Font.PreferredHeaderFont;
            yHeader.Text = "Y: ";

            var yValue = cell.GetOrCreateChild<ILabel>("y");
            yValue.VerticalAlignment = VerticalAlignment.Bottom;
            yValue.SetBinding(new Binding("Text", "Y") { Source = this, });

            var zHeader = cell.GetOrCreateChild<ILabel>("zHeader");
            zHeader.VerticalAlignment = VerticalAlignment.Bottom;
            zHeader.Font = Font.PreferredHeaderFont;
            zHeader.Text = "Z: ";

            var zValue = cell.GetOrCreateChild<ILabel>("z");
            zValue.VerticalAlignment = VerticalAlignment.Bottom;
            zValue.SetBinding(new Binding("Text", "Z") { Source = this, });

            return cell;
        }

        protected override void OnRender()
        {
            Title = "Acceleration";

            if (iApp.Session.ContainsKey("Z"))
            {
                Sections[0].Header = new SectionHeader("Values");
                _data = new AccelerometerData(
                    Convert.ToDouble(iApp.Session["X"]),
                    Convert.ToDouble(iApp.Session["Y"]),
                    Convert.ToDouble(iApp.Session["Z"]));
                OnPropertyChanged("X");
                OnPropertyChanged("Y");
                OnPropertyChanged("Z");
                iApp.Session.Remove("X");
                iApp.Session.Remove("Y");
                iApp.Session.Remove("Z");
            }
            else
            {
                Sections[0].Header = new SectionHeader("There was a problem retrieving accelerometer values.");
            }
            Menu = new Menu(new MenuButton("Reload")
            {
                NavigationLink = new Link("compass://?callback=Integrations/Accel")
            });
            Model.ValuesUpdated += (o, e) =>
            {
                _data = e.Data;
                OnPropertyChanged("X");
                OnPropertyChanged("Y");
                OnPropertyChanged("Z");
            };

            Activated += (o, e) => Model.Start();
            Deactivated += (o, e) => Model.Stop();

            Sections.Add(1, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}