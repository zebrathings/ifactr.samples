﻿using System;
using System.Collections.Generic;
using System.Linq;

using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;

namespace KitchenSink
{
    class BrowserPane : BrowserView<string>
    {
        /// <summary>
        /// Called when the view is ready to be rendered.
        /// </summary>
        protected override void OnRender()
        {
            Title = Model;  // Take title from View Model (string passed by controller)

            if (Title.StartsWith("Internal"))
            {
                // Test for LoadFinished event (which only applies to INTERNAL browser; 
                // will NOT fire for EXTERNAL browser launch)  See LH#2255
                LoadFinished += BrowserPane_LoadFinished;
                Load("http://www.zebra.com");
            }
            else
            {
                Activated += BrowserPane_Activated;
                // The following launch works fine, but does NOT fire the LoadFinished event
                // Note also that the http:// protocol prefix is not required.
                LaunchExternal("http://www.ibm.com");
                // On WinPhone, you need the "http://" prefix!
            }
        }

        private void BrowserPane_Activated(object sender, EventArgs e)
        {
            PaneManager.Instance.FromNavContext(OutputPane).PopView();
            Activated -= BrowserPane_Activated;
        }

        void BrowserPane_LoadFinished(object sender, LoadFinishedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("BrowserPane's LoadFinished Event fired with args:" + e +
            " navigated to:" + e.Url);
        }
    }
}
