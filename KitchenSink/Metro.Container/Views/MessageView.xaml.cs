﻿using System.Linq;
using iFactr.Core;
using iFactr.UI;
using Metro.Container.Common;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234233
using MonoCross.Navigation;

namespace Metro.Container
{
    /// <summary>
    /// A page that displays a message. 
    /// </summary>
    public sealed partial class MessageView : Page, IMXView, IHistoryEntry
    {
        private readonly NavigationHelper _navigationHelper;

        /// <summary>
        /// The strongly typed view model.
        /// </summary>
        public string DefaultViewModel
        {
            get { return _model; }
        }

        public MessageView()
        {
            InitializeComponent();
            _navigationHelper = new NavigationHelper(this);
            _navigationHelper.LoadState += navigationHelper_LoadState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            _model = "Hello world!";
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private RelayCommand _goBackCommand;
        public RelayCommand GoBackCommand
        {
            get
            {
                return _goBackCommand ?? (_goBackCommand = new RelayCommand(
                () =>
                {
                    if (BackLink != null && BackLink.Address == null)
                        return;

                    var stack = Stack;
                    if (stack != null)
                    {
                        stack.HandleBackLink(BackLink, Pane.Master);
                    }
                },
                () => Stack.CanGoBack()));
            }
        }

        #region IMXView and IHistoryEntry implementation

        private string _model;

        public Type ModelType
        {
            get { return _model.GetType(); }
        }

        public object GetModel()
        {
            return _model;
        }

        public void SetModel(object model)
        {
            _model = model.ToString();
        }

        public void Render()
        {
            pageTitle.Text = _model;
        }

        public Link BackLink { get; set; }

        public string StackID { get; set; }

        public Pane OutputPane { get; set; }

        public PopoverPresentationStyle PopoverPresentationStyle { get; set; }

        public ShouldNavigateDelegate ShouldNavigate { get; set; }

        public IHistoryStack Stack
        {
            get
            {
                return PaneManager.Instance.FirstOrDefault(s => s.CurrentView == this);
            }
        }

        public event EventHandler Activated;
        public event EventHandler Deactivated;

        #endregion
    }
}