﻿using System;
using MonoCross.Navigation;

namespace Console.Container.Views
{
    public class MessageView : IMXView<string>
    {
        private string _model;

        public Type ModelType
        {
            get { return typeof(string); }
        }

        public object GetModel()
        {
            return Model;
        }

        public void SetModel(object model)
        {
            Model = (string)model;
        }

        public void Render()
        {
            System.Console.WriteLine(Model);
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
    }
}