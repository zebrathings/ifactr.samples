using iFactr.Core;
using iFactr.Console;
using iFactr.Core.Targets;
using MonoCross.Navigation;

/// <summary>
/// This namespace contains the Console container for the KitchenSink sample
/// </summary>
namespace Console.Container
{
    /// <summary>
    /// This is the Console container for the KitchenSink Sample
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleFactory.Initialize();
            iApp.OnLayerLoadComplete += ConsoleFactory.Instance.OutputLayer;
            TargetFactory.TheApp = new KitchenSink.App();
            MXContainer.AddView<string>(typeof(Views.MessageView));
            iApp.Navigate(TargetFactory.TheApp.NavigateOnLoad);
        }
    }
}