using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Data;
using iFactr.Reflection;

namespace KitchenSink.Data
{
    [NavigationList(Name="BookList", Title = "{Category}")]
    public class BookList : RestfulList<Book>, IRestfulObject { }
}
