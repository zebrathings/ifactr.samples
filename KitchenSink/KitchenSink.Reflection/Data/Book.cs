using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Data;
using iFactr.Reflection;

namespace KitchenSink.Data
{
    [ListItem]
    public class Book : RestfulObject
    {
        public Category Category { get; set; }
        [LinkAddress] public override string UriEndpoint { get { return base.UriEndpoint; } set { base.UriEndpoint = value; } }
        public string ISBN10 { get; set; }
        [ListText] public string Title { get; set; }

        [IconAddress]
        [HtmlBlock]
        [HtmlBlockImage(Float = ImageFloat.Left)]
        public string ImageLink { get { return String.Format("http://images.amazon.com/images/P/{0}.01.ZTZZZZZZ.jpg", ISBN10); } }
        
        [HtmlBlockText]
        public string Contributor { get; set; }
        [HtmlBlockText(Label = "Publisher")]
        public string Publisher { get; set; }
        [HtmlBlockText]
        public string Description { get; set; }
        [HtmlBlockText(Label = "List Price")]
        public string Price { get; set; }
        
        public string Author { get; set; }

        [NavigationList(Name = "BookActions")]
        public List<BookActionLink> ActionLinks { get; set; }
    }

    [ListItem]
    public class BookActionLink
    {
        [ListText]public string Title { get; set; }
        [LinkAddress]public string Link { get; set; }
    }
}
