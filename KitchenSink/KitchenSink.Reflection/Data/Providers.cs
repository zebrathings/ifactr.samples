using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Data;

namespace KitchenSink.Data
{
    public class Providers : ProviderRegistry
    {
        public Providers()
        {
            Register(new CategoryListProvider());
            Register(new CategoryProvider());
            Register(new BookListProvider());
            Register(new BookProvider());
        }
        public static CategoryListProvider Categories { get { return (CategoryListProvider)iApp.Instance.Providers().GetProvider(typeof(CategoryList)); } }
        public static BookListProvider BookLists { get { return (BookListProvider)iApp.Instance.Providers().GetProvider(typeof(BookList)); } }
        public static BookProvider Books { get { return (BookProvider)iApp.Instance.Providers().GetProvider(typeof(Book)); } }
    }
}
