using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using iFactr.Data;

namespace KitchenSink.Data
{
    public class CategoryListProvider : Provider<CategoryList>, IDataProvider, IDataProvider<CategoryList>
    {
        public CategoryListProvider() : base(string.Empty, string.Empty, string.Empty) { }
        public override CategoryList Get(Dictionary<string, string> parameters)
        {
            CategoryList list = new CategoryList();
            XDocument loaded = XDocument.Load("http://api.nytimes.com/svc/books/v2/lists/names.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697");
            var categories = from item in loaded.Descendants("result")
                             select new
                             {
                                 name = (string)item.Element("list_name"),
                             };
            foreach (var category in categories)
            {
                list.Add(new Category() { UriEndpoint = category.name, Name = category.name });
            }
            return list;
        }
    }
}
