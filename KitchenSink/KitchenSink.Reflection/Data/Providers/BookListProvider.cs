using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using iFactr.Data;

namespace KitchenSink.Data
{
    public class BookListProvider : Provider<BookList>, IDataProvider, IDataProvider<BookList>
    {
        public BookListProvider() : base("http://api.nytimes.com/svc/books/v2/lists/{Category}.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697", string.Empty, string.Empty) { }
        public override BookList Get(Dictionary<string, string> parameters)
        {
            string uri = GetObjectUri(parameters).Replace(" ", "-");
            BookList retval = cache.ContainsKey(uri) ? cache[uri] : null;
            if (retval == null)
            {
                retval = new BookList();
                XDocument loaded = XDocument.Load(uri);
                if (loaded != null)
                {
                    var books = from item in loaded.Descendants("book")
                                where item.Descendants().Count() > 9
                                select new
                                {
                                    title = (string)item.Descendants("book_detail").Elements("title").First(),
                                    contributor = (string)item.Descendants("book_detail").Elements("contributor").First(),
                                    author = (string)item.Descendants("book_detail").Elements("author").First(),
                                    isbn10 = (string)item.Descendants("isbn").Elements("isbn10").First(),
                                };

                    foreach (var book in books)
                    {
                        Book thisBook = new Book()
                        {
                            UriEndpoint = string.Format("{0}/{1}", parameters["Category"], book.isbn10),
                            Author = book.author,
                            Contributor = book.contributor,
                            ISBN10 = book.isbn10,
                            Title = book.title
                        };
                        if (!retval.Contains(thisBook))
                            retval.Add(thisBook);
                    }
                    if (cache.ContainsKey(uri))
                        cache.Remove(uri);
                    cache.Add(uri, retval);
                }
            }
            return retval;
        }
    }
}
