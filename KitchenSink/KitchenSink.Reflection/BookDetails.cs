using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace KitchenSink
{
    /// <summary>
    /// This sample class demonstrates how a simple RESTful request can be parsed via
    /// LINQ to create a block of content.
    /// </summary>
    /// <includesource>yes</includesource>
    class BookDetails : iLayer, IDetailLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string category = parameters["Category"];
            string book = parameters["Book"];

            Title = category;

            iBlock block = new iBlock();

            Data.Book bookDetails = Data.Providers.Books.Get(parameters);

            block.InsertImageFloatLeft(String.Format("http://images.amazon.com/images/P/{0}.01.ZTZZZZZZ.jpg", bookDetails.ISBN10));
            block.AppendLine(bookDetails.Contributor);
            block.AppendLabeledTextLine("Publisher", bookDetails.Publisher);
            block.AppendLine(bookDetails.Description);
            block.AppendLabeledTextLine("List Price", "$" + bookDetails.Price);
            block.AppendLine();
            
            Items.Add(block);

            //if (bookDetails.BookReviewLink != null && bookDetails.BookReviewLink != "")
            //{
            //    Items.Add(new iMenu {
            //        Items = { new iMenuItem(bookDetails.BookReviewLink, "View Book Review ...", false) },
            //    }); 
            //}

            //if (bookDetails.FirstChapterLink != null && bookDetails.FirstChapterLink != "")
            //{
            //    Items.Add(new iMenu {
            //        Items = { 
            //            new iMenuItem(bookDetails.FirstChapterLink, "View First Chapter ...", false),
            //        }
            //    }); 
            //}

            //if (bookDetails.SundayReviewLink != null && bookDetails.SundayReviewLink != "")
            //{
            //    Items.Add(new iMenu {
            //        Items = { 
            //            new iMenuItem(bookDetails.SundayReviewLink, "View Sunday Review ...", false),
            //        }
            //    }); 
            //}

            //if (bookDetails.ArticleChapterLink != null && bookDetails.ArticleChapterLink != "")
            //{
            //    Items.Add(new iMenu {
            //        Items = { 
            //            new iMenuItem(bookDetails.ArticleChapterLink, "View Article Chapter ...", false),
            //        }
            //    }); 
            //}
			
			
        }
    }
}
