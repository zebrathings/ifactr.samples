using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

using iFactr.Touch;
using iFactr.Core;
using iFactr.Core.Layers;
using CoreGraphics;

namespace KitchenSink
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
            TouchFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => { TouchFactory.Instance.OutputLayer(layer); };
            TouchFactory.TheApp = new KitchenSink.App();
            
            iApp.Navigate(App.Instance.NavigateOnLoad);

			return true;
		}

		public override void OnActivated (UIApplication application)
		{
		}
		
		public static UITableViewCell WorkOrderCustomItem(CustomControl item, UITableViewCell cell, UITableView tableView)
	    {
			var control = item as CustomControl;
			if (control != null)
			{
				cell = tableView.DequeueReusableCell("custom");
				if (cell == null)
				{
					cell = new UITableViewCell(UITableViewCellStyle.Default, "custom");
					cell.ContentView.Add(new UILabel(new CGRect(8, 156, 92, 18)) { Text = "Status:", Font = UIFont.BoldSystemFontOfSize(16) });
					cell.ContentView.Add(new UILabel(new CGRect(8, 176, 92, 18)) { Text = "Priority:", Font = UIFont.BoldSystemFontOfSize(16) });
					cell.ContentView.Add(new UILabel(new CGRect(8, 196, 92, 18)) { Text = "Type:", Font = UIFont.BoldSystemFontOfSize(16) });
				}
	
				var titleLabel = cell.ViewWithTag(1) as UILabel;
				if (titleLabel == null)
				{
					titleLabel = new UILabel(new CGRect(8, 0, 312, 22));
					titleLabel.Font = UIFont.BoldSystemFontOfSize(20);
					titleLabel.Tag = 1;
	
					cell.ContentView.Add(titleLabel);
				}
	
				var descLabel = cell.ViewWithTag(2) as UILabel;
				if (descLabel == null)
				{
					descLabel = new UILabel(new CGRect(8, 22, 312, 132));
					descLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
					descLabel.LineBreakMode = UILineBreakMode.WordWrap;
					descLabel.Lines = 0;
					descLabel.Tag = 2;
	
					cell.ContentView.Add(descLabel);
				}
	
				var statusLabel = cell.ViewWithTag(3) as UILabel;
				if (statusLabel == null)
				{
					statusLabel = new UILabel(new CGRect(100, 156, 212, 18));
					statusLabel.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
					statusLabel.Tag = 3;
	
					cell.ContentView.Add(statusLabel);
				}
	
				var priorityLabel = cell.ViewWithTag(4) as UILabel;
				if (priorityLabel == null)
				{
					priorityLabel = new UILabel(new CGRect(100, 176, 212, 18));
					priorityLabel.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
					priorityLabel.Tag = 4;
	
					cell.ContentView.Add(priorityLabel);
				}
	
				var typeLabel = cell.ViewWithTag(5) as UILabel;
				if (typeLabel == null)
				{
					typeLabel = new UILabel(new CGRect(100, 196, 212, 18));
					typeLabel.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
					typeLabel.Tag = 5;
	
					cell.ContentView.Add(typeLabel);
				}
	
				titleLabel.Text = control.Title;
				descLabel.Text = control.Description;
				statusLabel.Text = control.Status;
				priorityLabel.Text = control.Priority;
				typeLabel.Text = control.Type;
	
				var button1 = cell.ViewWithTag(6) as UIButton;
				if (button1 == null)
				{
					button1 = new UIButton(new CGRect(8, 228, 70, 70));
					button1.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;
					button1.BackgroundColor = new UIColor(0, 0.65f, 0.8f, 1);
					button1.Font = UIFont.SystemFontOfSize(14);
					button1.SetTitle("Start/Stop", UIControlState.Normal);
					button1.SetTitleColor(UIColor.White, UIControlState.Normal);
					button1.TouchUpInside += (sender, e) => { /* Do button logic here */ };
					button1.Tag = 6;
	
					cell.ContentView.Add(button1);
				}
	
				var button2 = cell.ViewWithTag(7) as UIButton;
				if (button2 == null)
				{
					button2 = new UIButton(new CGRect(86, 228, 70, 70));
					button2.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;
					button2.BackgroundColor = new UIColor(0, 0.65f, 0.8f, 1);
					button2.Font = UIFont.SystemFontOfSize(14);
					button2.SetTitle("Complete", UIControlState.Normal);
					button2.SetTitleColor(UIColor.White, UIControlState.Normal);
					button2.TouchUpInside += (sender, e) => { /* Do button logic here */ };
					button2.Tag = 7;
	
					cell.ContentView.Add(button2);
				}
	
				var button3 = cell.ViewWithTag(8) as UIButton;
				if (button3 == null)
				{
					button3 = new UIButton(new CGRect(164, 228, 70, 70));
					button3.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;
					button3.BackgroundColor = new UIColor(0, 0.65f, 0.8f, 1);
					button3.Font = UIFont.SystemFontOfSize(14);
					button3.SetTitle("Parts", UIControlState.Normal);
					button3.SetTitleColor(UIColor.White, UIControlState.Normal);
					button3.TouchUpInside += (sender, e) => { /* Do button logic here */ };
					button3.Tag = 8;
	
					cell.ContentView.Add(button3);
				}
	
				var button4 = cell.ViewWithTag(9) as UIButton;
				if (button4 == null)
				{
					button4 = new UIButton(new CGRect(242, 228, 70, 70));
					button4.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;
					button4.BackgroundColor = new UIColor(0, 0.65f, 0.8f, 1);
					button4.Font = UIFont.SystemFontOfSize(14);
					button4.SetTitle("Time", UIControlState.Normal);
					button4.SetTitleColor(UIColor.White, UIControlState.Normal);
					button4.TouchUpInside += (sender, e) => { /* Do button logic here */ };
					button4.Tag = 9;
	
					cell.ContentView.Add(button4);
				}
			}

			return cell;
    	}		
    	const float WorkOrderCustomItemHeight = 304;
	}
}
