using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    public class AutomationSetupAndTeardown
    {
        public IApp App { get; set; }

        public string ApkFile { get; private set; }

        public string IpaFile { get; private set; }


        [TestFixtureSetUp]
        public void RunBeforeAnyTests()
        {
#if DEBUG
            ApkFile = @"..\..\..\Droid.Container\bin\Debug\com.zebra.kitchensink-Signed.apk";
            IpaFile = @"..\..\..\Touch.Container\bin\iPhone\Debug\com.zebra.kitchensink.ipa";
#else
            ApkFile = @"..\..\..\Droid.Container\bin\Release\com.zebra.kitchensink-Signed.apk";
            IpaFile = @"..\..\..\Touch.Container\bin\iPhone\Release\com.zebra.kitchensink.ipa";
#endif
        }

        protected virtual void Configure(Platform platform)
        {
            switch (platform)
            {
                case Platform.Android:
                    App = ConfigureApp.Android
                        .ApkFile(ApkFile)
                        .EnableLocalScreenshots()
                        .StartApp();
                    break;
                case Platform.iOS:
                    App = ConfigureApp.iOS
                        .AppBundle(IpaFile)
                        .EnableLocalScreenshots()
                        .StartApp();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("platform", platform, null);
            }
        }

        protected string Contains(string contains)
        {
            const string contain = "* {{text CONTAINS '{0}'}}";
            return string.Format(contain, contains);
        }

        protected async Task<bool> ScrollAndTap(Func<AppQuery, AppQuery> query, int maxScrolls = 10)
        {
            int tries = 0;
            if (!App.Query(query).Any())
            {
                await Task.Delay(1000);
            }
            while (!App.Query(query).Any())
            {
                if (maxScrolls > tries)
                {
                    App.ScrollDown();
                    await Task.Delay(200);
                    tries++;
                }
                else
                {
                    return false;
                }
            }
            try
            {
                App.Flash(query);  // show it before tapping
            }
            catch (Exception) { }
            App.Tap(query);
            return true;
        }
    }
}