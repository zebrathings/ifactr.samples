﻿using System;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    public class UnitTests : AutomationSetupAndTeardown
    {
        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public void RunAllUnitTests(Platform platform)
        {
            Configure(platform);

            var testQuery = new Func<AppQuery, AppQuery>(c => c.Text("Unit Tests"));
            int tries = 0;
            while (!App.Query(testQuery).Any() && 5 > tries++)
            {
                App.ScrollDown();
            }
            App.Tap(testQuery);
            App.Tap(c => c.Text("Run Tests"));
            App.WaitForElement(c => c.Raw(Contains("passed")), timeout: TimeSpan.FromMinutes(5));

            var failures = App.Query(c => c.Raw(Contains("failed")));
            Assert.IsEmpty(failures);
            App.ScrollDown();

            failures = App.Query(c => c.Raw(Contains("failed")));
            Assert.IsEmpty(failures);
            App.ScrollDown();

            failures = App.Query(c => c.Raw(Contains("failed")));
            Assert.IsEmpty(failures);
        }
    }
}