﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    [TestFixture]
    public class LoginTests : AutomationSetupAndTeardown
    {
        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void LoginTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("Login Layer"));

            await Task.Delay(200);

            App.Tap(c => c.TextField());
            await Task.Delay(200);
            App.EnterText("test");

            // BUG: Does not work on Android
            //_app.PressEnter();
            //_app.PressEnter();

            var results = App.WaitForElement(c => c.Text("Login"));
            var loginButton = results.Last().Rect;
            App.TapCoordinates(loginButton.CenterX, loginButton.CenterY);

            var menu = App.WaitForElement(c => c.Text("iMenu"), timeout: TimeSpan.FromSeconds(10));
            Assert.IsNotEmpty(menu);
        }
    }
}