﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    [TestFixture]
    public class NavigationTests : AutomationSetupAndTeardown
    {
        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void SearchListTest(Platform platform)
        {
            Configure(platform);
            App.Tap(c => c.Text("iList"));
            App.Tap(c => c.Text("List Type"));
            App.Tap(c => c.Text("SearchList"));
            App.Tap(c => c.Text("iItem"));
            await Task.Delay(500);
            App.Screenshot("Android SearchList");

            var searchBox = App.Query(c => c.TextField());
            Assert.IsNotEmpty(searchBox);
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void SubtextItemTest(Platform platform)
        {
            Configure(platform);
            App.Tap(c => c.Text("iList"));
            App.Tap(c => c.Text("SubtextItem"));
            await Task.Delay(500);

            var subtext = App.WaitForElement(c => c.Raw(Contains("subtext")));
            Assert.IsNotEmpty(subtext);
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public void BackButtonConfirmTest(Platform platform)
        {
            Configure(platform);
            var menuQuery = new Func<AppQuery, AppQuery>(c => c.Text("iMenu"));
            App.WaitForElement(menuQuery);
            App.Tap(menuQuery);
            App.WaitForElement(c => c.Text("iItem"));
            App.Back();

            var query = new Func<AppQuery, AppQuery>(c => c.Text("OK"));
            var okButton = App.WaitForElement(query);
            Assert.IsNotEmpty(okButton);

            App.Tap(query);

            var menu = App.WaitForElement(menuQuery);
            Assert.IsNotEmpty(menu);
        }
    }
}