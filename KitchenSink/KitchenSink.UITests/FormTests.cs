﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    [TestFixture]
    public class FormTests : AutomationSetupAndTeardown
    {
        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void BoolFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("BoolField"));
            var switches = App.WaitForElement(c => c.Switch());
            var rect = switches[2].Rect;

            App.TapCoordinates(rect.CenterX, rect.CenterY);

            Submit();
            AssertResult("off");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void ButtonFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("ButtonField"));
            await ScrollAndTap(c => c.Text("A postback button"));
            AssertResult("A postback button");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void DateFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("DateField"));
            await Task.Delay(500);
            Submit();
            AssertResult(DateTime.Today.ToShortDateString());
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void DrawingFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("DrawingField"));
            await Task.Delay(500);

            App.Tap(c => c.Text("A drawing canvas"));

            AppRect canvasRect = null;

            switch (platform)
            {
                case Platform.Android:
                    canvasRect = App.WaitForElement(c => c.Class("DrawingView"))[0].Rect;
                    break;
                case Platform.iOS:
                    //TODO: Get the AppRect for iOS's canvas
                    break;
                default:
                    throw new ArgumentOutOfRangeException("platform", platform, null);
            }

            App.DragCoordinates(canvasRect.CenterX - 70, canvasRect.CenterY - 70, canvasRect.CenterX + 70, canvasRect.CenterY + 70);
            App.DragCoordinates(canvasRect.CenterX + 70, canvasRect.CenterY - 70, canvasRect.CenterX - 70, canvasRect.CenterY + 70);

            App.Tap(c => c.Text("Done"));
            App.WaitForElement(c => c.Text("A signature field"));

            Submit();
            AssertResult("<img ");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void EmailFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("EmailField"));
            await Task.Delay(500);
            App.Tap(c => c.TextField());
            App.EnterText("itrmobile@gmail.com");
            Submit();
            AssertResult("itrmobile@gmail.com");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void LabelFieldTest(Platform platform)
        {
            Configure(Platform.Android);
            await ScrollAndTap(c => c.Text("LabelField"));
            await Task.Delay(500);
            Submit();
            AssertResult("With optional text");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void MultiLineTextFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("MultiLineTextField"));
            await Task.Delay(500);
            App.Tap(c => c.TextField());
            App.EnterText("I am \nthe \ngreatest \nat testing!!\n\n\n");
            Submit();
            AssertResult("I am the greatest at testing!!");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void NavigationFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("NavigationField"));
            await Task.Delay(500);
            App.Tap(c => c.Text("A navigation field that submits"));
            AssertResult("A navigation field that submits");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void NumericFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("NumericField"));
            await Task.Delay(500);
            App.Tap(c => c.TextField());
            App.EnterText("93");
            Submit();
            AssertResult("93");

            // TODO: check negative/decimal availability for numeric fields on iOS
            //App.Tap(c => c.TextField());
            //App.EnterText("-17.393");
            //Submit();
            //AssertResult("17.393");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void SelectListFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("SelectListField"));
            await Task.Delay(500);

            App.Tap(c => c.Text("Option 3"));
            App.Tap(c => c.Text("Option 1"));

            Submit();
            AssertResult("SelectList1: Option 1");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void SliderFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("SliderField"));

            AppRect sliderRect = null;

            switch (platform)
            {
                case Platform.Android:
                    sliderRect = App.WaitForElement(c => c.Class("android.widget.SeekBar"))[0].Rect;
                    break;
                case Platform.iOS:
                    //TODO: Get the AppRect for iOS's slider
                    break;
                default:
                    throw new ArgumentOutOfRangeException("platform", platform, null);
            }

            App.DragCoordinates(sliderRect.X + 10, sliderRect.CenterY, sliderRect.X + sliderRect.Width, sliderRect.CenterY);

            Submit();
            AssertResult("100");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void TextFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("TextField"));
            await Task.Delay(500);
            if (platform == Platform.Android)
                App.Back();
            App.ScrollUp();
            App.Flash(c => c.Text("A text entry field"));
            App.Tap(c => c.TextField());  // taps the first text field (id "Text1"), giving it focus.

            App.EnterText("test");
            if (platform == Platform.Android)
                App.Back();

            Submit();

            await Task.Delay(500);
            if (platform == Platform.Android)
                App.Back();
            App.ScrollUp();
            await Task.Delay(1500);
            AssertResult("Text1: test");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void TextFieldFocusedTest(Platform platform)
        {
            // NOTE: in KitchenSink/Forms/TextField, the default focused text element is
            //   "A PIN entry field" which is submitted as id "Password2"
            //   This test assumes that default focus.
            Configure(platform);
            await ScrollAndTap(c => c.Text("TextField"));

            await Task.Delay(500);

            // Enter straight into the textfield that currently has focus, namely: Password2 which is a 4-digit PIN
            App.Flash(c => c.Text("A PIN entry field"));  // flash it before entering text
            App.EnterText("9393");
            if (platform == Platform.Android)
                App.Back();

            await Task.Delay(500);
            Submit();

            await Task.Delay(500);
            if (platform == Platform.Android)
                App.Back();
            App.ScrollUp();

            await Task.Delay(1500);
            AssertResult("Password2: 9393");
        }

        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public async void CustomFieldTest(Platform platform)
        {
            Configure(platform);
            await ScrollAndTap(c => c.Text("CustomField"));
            await Task.Delay(500);
            App.Tap(c => c.TextField());
            App.EnterText("(651) 757-4500");
            Submit();
            AssertResult("(651) 757-4500");
        }

        protected override void Configure(Platform platform)
        {
            base.Configure(platform);

            var local = App.WaitForElement(c => c.Text("Localization"));
            ActionLocation = local[0].Rect;

            if (platform == Platform.Android)
                App.Tap(c => c.Text("Elements"));

            App.Tap(c => c.Text("Forms"));
        }

        protected void Submit()
        {
            var submit = App.Query(c => c.Text("Submit"));
            if (!submit.Any())
            {
                App.TapCoordinates(ActionLocation.X + ActionLocation.Width - 10, ActionLocation.CenterY);
            }

            App.WaitForElement(c => c.Text("Submit"));
            App.Tap(c => c.Text("Submit"));
            App.WaitForElement(c => c.Text("OK"));
            App.Tap(c => c.Text("OK"));
        }

        protected void AssertResult(string query)
        {
            var result = App.WaitForElement(c => c.Css("body"))[0];
            Assert.That(result.TextContent.Contains(query) || result.Html.Contains(query));
        }

        private AppRect ActionLocation { get; set; }
    }
}