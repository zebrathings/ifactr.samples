﻿using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class TabTests : AutomationSetupAndTeardown
    {
        [TestCase(Platform.Android)]
        //[TestCase(Platform.iOS)]
        public void SwitchTab(Platform platform)
        {
            Configure(platform);

            if (platform == Platform.Android)
                App.Tap(c => c.Text("Elements"));

            App.Tap(c => c.Text("Forms"));

            var list = App.WaitForElement(c => c.Text("BoolField"));
            Assert.IsNotEmpty(list);

            if (platform == Platform.Android)
                App.Tap(c => c.Text("Forms"));

            App.Tap(c => c.Text("Style"));

            list = App.WaitForElement(c => c.Text("Choose new colors"));
            Assert.IsNotEmpty(list);

            if (platform == Platform.Android)
                App.Tap(c => c.Text("Style"));

            App.Tap(c => c.Text("Integrations"));

            list = App.WaitForElement(c => c.Text("Choose an image"));
            Assert.IsNotEmpty(list);

            if (platform == Platform.Android)
                App.Tap(c => c.Text("Integrations"));

            App.Tap(c => c.Text("Elements"));

            list = App.WaitForElement(c => c.Text("iList"));
            Assert.IsNotEmpty(list);
        }
    }
}