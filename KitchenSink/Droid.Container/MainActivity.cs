using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Droid;
using Java.Lang;
using KitchenSink;
using System;
using System.Linq;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        private const int PermissionRequestCode = 42;

        protected override void OnCreate(Bundle bundle)
        {
            if (DroidFactory.TheApp != null)
            {
                base.OnCreate(bundle);
                return;
            }

            DroidFactory.MainActivity = this;
            DroidFactory.TheApp = new App();
            base.OnCreate(bundle);

            App.ChangeTabMode = () =>
            {
                DroidFactory.Instance.TabMode =
                    DroidFactory.Instance.TabMode == TabMode.Dropdown ?
                    TabMode.SlidingTabs : TabMode.Dropdown;
            };

            DroidFactory.AddView<string>(typeof(MessageFragment));

            // Test harness
            DroidFactory.AddView<Type>(typeof(ActivityForwarder));
            iApp.Instance.NavigationMap.Add("TestRunner", new TestController());

            iApp.OnLayerLoadComplete += layer =>
            {
                var scanLayer = layer as ScanLayer;
                if (scanLayer != null)
                {
                    PerformScan(scanLayer);
                    return;
                }
                DroidFactory.Instance.OutputLayer(layer);
            };

            DroidFactory.Instance.CustomItemRequested = CustomItemRequested;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                RequestPermissions(new[] {
                        Manifest.Permission.Camera,
                        Manifest.Permission.AccessFineLocation,
                    }, PermissionRequestCode);
            }
            else
            {
                iApp.Navigate(iApp.Instance.NavigateOnLoad);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (grantResults.All(r => r == Permission.Granted))
            {
                iApp.Navigate(iApp.Instance.NavigateOnLoad);
            }
            else
            {
                JavaSystem.Exit(0);
            }
        }

        private View CustomItemRequested(ICustomItem customItem, View convertView, iLayer layer)
        {
            var item = customItem as CustomNumericField;
            if (item != null)
            {
                TextView label;
                if (convertView == null)
                {
                    convertView = LayoutInflater.Inflate(Resource.Layout.AndroidNumericField, null);
                    var textBox = convertView.FindViewById<TextBox>(Resource.Id.editText1);
                    textBox.AddTextChangedListener(new PhoneNumberFormattingTextWatcher());
                    textBox.TextChanged += (o, e) => item.Text = e.NewValue;
                    textBox.Text = item.Text;
                    label = convertView.FindViewById<Label>(Resource.Id.textView1);
                    label.Click += (o, e) => label.Animate().RotationXBy(720).SetDuration(600);
                }
                else
                {
                    label = convertView.FindViewById<TextView>(Resource.Id.textView1);
                }
                label.Text = item.Label;
            }
            else if (customItem is CustomControl)
            {
                var textView = convertView as TextView;
                if (textView == null)
                {
                    convertView = textView = new TextView(this);
                }
                textView.Text = "Custom!";
            }
            return convertView;
        }

        private async void PerformScan(ScanLayer scanLayer)
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan();

            if (result == null)
            {
                // Scan cancelled.
                DroidFactory.Instance.StopBlockingUserInput();
                return;
            }

            //Results for single scan
            var resultString = result.Text;

            ////Results for multiple scans
            //var multi = (bool)iApp.Session.GetValueOrDefault("MultiScan", true);
            //var resultString = string.Empty;
            //do
            //{
            //    resultString += result.Text + ScanLayer.BarcodeSeparatorChar;
            //} while (multi && (result = await scanner.Scan()) != null);
            //resultString = resultString.Remove(resultString.Length - ScanLayer.BarcodeSeparatorChar.Length);

            scanLayer.Callback.Parameters[ScanLayer.BarcodeKey] = resultString;
            DroidFactory.Navigate(scanLayer.Callback, scanLayer.NavContext.OutputOnPane);
        }
    }
}