﻿using MonoCross.Navigation;
using System;
using System.Collections.Generic;

namespace Droid.Container
{
    public class TestController : MXController<Type>
    {
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            Model = typeof(TestActivity);
            return ViewPerspective.Default;
        }
    }
}