using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MonoCross.Navigation;
using View = Android.Views.View;

namespace Droid.Container
{
    public class MessageFragment : Fragment, IMXView<string>
    {
        private TextView _text;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.Message, container, false);
            _text = view.FindViewById<TextView>(Resource.Id.message);
            Render();
            return view;
        }

        public  void Render()
        {
            //Title = "Monocross Message";
            if (_text != null && Model != null)
            {
                _text.Text = Model;
            }
            //base.Render();
        }

        public string Model { get; set; }

        public Type ModelType
        {
            get { return typeof(string); }
        }

        object IMXView.GetModel()
        {
            return Model ?? "Hello world!";
        }

        void IMXView.SetModel(object model)
        {
            Model = model.ToString();
        }
    }
}