using System;
using MonoCross.Navigation;
using iFactr.Droid;

namespace Droid.Container
{
    public class ActivityForwarder : IMXView<Type>
    {
        private Type _model;

        public Type Model
        {
            get { return (Type)GetModel(); }
            set { SetModel(value); }
        }

        public Type ModelType
        {
            get { return typeof(Type); }
        }

        public object GetModel()
        {
            return _model;
        }

        public void Render()
        {
            DroidFactory.MainActivity.StartActivity(_model);
        }

        public void SetModel(object model)
        {
            _model = (Type)model;
        }
    }
}