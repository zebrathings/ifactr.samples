﻿using System.Reflection;
using Android.App;
using Android.OS;
using Xamarin.Android.NUnitLite;

namespace Droid.Container
{
    [Activity(Label = "TestRunner.Android")]
    public class TestActivity : TestSuiteActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            // Android test output appears in the Visual Studio "Debug" Window.
            // Each UnitTest Assembly must be added to this program in order to find the tests
            AddTest(typeof(UnitTests.Utils.Extensions.DictionaryTests).GetTypeInfo().Assembly);
            // (NOTE: the following UI reference does NOT WORK on Xamarin Studio!)
            AddTest(typeof(UnitTests.UI.Block.iBlockTests).GetTypeInfo().Assembly);
            AddTest(typeof(UnitTests.Data.QueueExceptionTests).GetTypeInfo().Assembly);
            AddTest(typeof(UnitTests.Data.OTW.ProviderTimeoutTests).GetTypeInfo().Assembly);
            AddTest(typeof(UnitTests.LongRunningThreadTests.IdleQueueThreadTests).GetTypeInfo().Assembly);

            // Once you called base.OnCreate(), you cannot add more assemblies.
            base.OnCreate(bundle);
        }
    }
}