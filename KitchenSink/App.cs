using System;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Styles;
using MonoCross.Utilities.Logging;
using iFactr.UI;
using KitchenSink.Controllers;
using KitchenSink.Views;
using MonoCross.Navigation;

namespace KitchenSink
{
    public class App : iApp
    {
        /// <summary>
        /// Called when the application instance is loaded. This event is meant to be overridden in consuming applications
        /// for application-level initialization code.
        /// </summary>
        public override void OnAppLoad()
        {
            Title = "Kitchen Sink";
            FormFactor = FormFactor.SplitView;
            VanityImagePath = "Default-Detail.png";

            Log.LoggingLevel = LoggingLevel.Info;
            Log.Info("In App.cs OnAppLoad method.");

            // Add navigation mappings
            NavigationMap.Add(string.Empty, new Tabs());

            var login = new Login();

            #region Elements tab

            var elementLayer = new Elements();
            NavigationMap.Add("Elements", elementLayer);
            NavigationMap.Add("Elements/Language", new Localization());
            NavigationMap.Add("Elements/Language/{Language}", elementLayer);

            NavigationMap.Add("Elements/Collections", new ListType());
            NavigationMap.Add("Elements/Collections/{Type}", new CollectionsList());
            NavigationMap.Add("Elements/Collections/{Type}/{Example}", new Collections());

            NavigationMap.Add("Elements/HtmlText/{Type}", new HtmlTextList());
            NavigationMap.Add("Elements/HtmlText/{Type}/{Example}", new HtmlText());

            NavigationMap.Add("Elements/CustomControlLayer", new CustomControlDisplayLayer());

            NavigationMap.Add("Elements/TabProperties", new TabProperties());
            NavigationMap.Add("Quantity/{Key}/{quantity}", new Quantity());

            NavigationMap.Add("Elements/LoginLayer", login);
            NavigationMap.Add("Elements/PopoverLayer", new Popover1());
            NavigationMap.Add("Elements/PopoverLayer2", new Popover2());

            NavigationMap.Add("Elements/DetailLayer1", new DetailLayer1());
            NavigationMap.Add("Elements/DetailLayer1/DetailLayer2", new DetailLayer2());
            NavigationMap.Add("Elements/DetailLayer2", new DetailLayer2());

            #endregion

            #region Forms tab

            NavigationMap.Add("Forms", new Forms());
            NavigationMap.Add("Forms/Aggregation", new Aggregation());
            NavigationMap.Add("Forms/Submit", new FormSubmission());
            NavigationMap.Add("Forms/{Example}", new FormFields());
            NavigationMap.Add("Forms/{Example}/LoginLayer", login);
            NavigationMap.Add("CompositeLayer", new FormSubmission());

            #endregion

            #region Style tab

            NavigationMap.Add("Style", new StyleMenu());
            var styleController = new StyleController();
            NavigationMap.Add("Style/Select", styleController);
            NavigationMap.Add("Style/Select/{Name}", styleController);

            NavigationMap.Add("Style/Theme", new ThemeSelector());

            var layerTypes = new LayerTypes();
            NavigationMap.Add("Style/Layout", layerTypes);
            NavigationMap.Add("Style/Layout/{Layout}", layerTypes);

            NavigationMap.Add("Style/LoadIndicator", new LoadIndicator());

            #endregion

            #region Integrations tab

            NavigationMap.Add("Integrations", new Integrations());
            NavigationMap.Add("Integrations/Audio", new Audio());
            NavigationMap.Add("Integrations/Images", new ImageResizer());
            NavigationMap.Add("Integrations/Video", new Video());
            NavigationMap.Add(AScanLayer.Uri, new AScanLayer("Forms/Submit"));
            NavigationMap.Add(CameraScanLayerUri, new CameraScanLayer("Forms/Submit"));

            #endregion

            #region Controllers

            NavigationMap.Add("MonoCross", typeof(MessageController));

            NavigationMap.Add("Integrations/Accel", new Accel());
            NavigationMap.Add("Integrations/Compass", new Compass());
            NavigationMap.Add("Integrations/Geo", new Geolocation());

            #endregion

            #region ViewMap Entries

            MXContainer.AddView<string>(typeof(SimpleMonoView), MessageController.MonoViewPerspective);
            MXContainer.AddView<string>(typeof(ClickMeView), MessageController.ClickViewPerspective);
            MXContainer.AddView<string>(typeof(BrowserPane), MessageController.InternalBrowserPerspective);
            MXContainer.AddView<string>(typeof(BrowserPane), MessageController.ExternalBrowserPerspective);

            MXContainer.AddView<Style>(typeof(StyleView));

            MXContainer.AddView<iFactr.Integrations.Compass>(typeof(CompassView));
            MXContainer.AddView<iFactr.Integrations.Accelerometer>(typeof(AccelerometerView));
            MXContainer.AddView<iFactr.Integrations.GeoLocation>(typeof(GpsView));

            #endregion

            #region App Style

            if (Factory.Target != MobileTarget.WinPhone)
            {
                // Layer styles set here in App are inherited by each Layer
                Style.HeaderColor = new Color("#094A84");
                Style.HeaderTextColor = Color.White;
                Style.PopoverTransitionAnimation = Transition.Reveal;
                Style.PopoverTransitionDirection = TransitionDirection.Up;
            }
            #endregion

            // Set default navigation URI
            NavigateOnLoad = string.Empty;
            //NavigateOnLoad = "Elements";
            //NavigateOnLoad = "Elements/LoginLayer";
        }

        public static LayerLayout Layout
        {
            get
            {
                if (!Session.ContainsKey("Layout"))
                    Session["Layout"] = default(LayerLayout);
                return (LayerLayout)Session["Layout"];
            }
            set
            {
                Session["Layout"] = value;
            }
        }

        public const string CameraScanLayerUri = "ToTheCameraScanTestLayerUri";

        public static Action ChangeTabMode { get; set; }
    }
}