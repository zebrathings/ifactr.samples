﻿using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    public class Integrations : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Integrations";
            Layout = App.Layout;

            Items.Add(new iMenu("Hardware Integrations")
            {
                new iItem("image://?callback=Integrations/Images", "Choose an image"),
                new iItem("image://?callback=Integrations/Images&gallery=false", "Camera only"),
                new iItem("image://?callback=Integrations/Images&camera=false", "Gallery only"),
                new iItem("voicerecording://?callback=Integrations/Audio", "Audio"),
                new iItem("videorecording://?callback=Integrations/Video", "Video"),
                new iItem("geoloc://?callback=Integrations/Geo", "Geolocation"),
                new iItem("compass://?callback=Integrations/Compass", "Compass"),
                new iItem("accel://?callback=Integrations/Accel", "Accelerometer"),
            });

            Items.Add(new iMenu("Web Integrations")
            {
                new iItem("http://support.ifactr.com/", "Internal Browser"),
                new iItem(new Link("http://support.ifactr.com/") { RequestType = RequestType.NewWindow, }, "External Browser"),
            });

            Items.Add(new iMenu("Barcode Scanning")
            {
                new iItem(AScanLayer.Uri, "HID Barcode Scanning"),
                new iItem(App.CameraScanLayerUri, "Integrated Camera Scanning"),
            });
        }
    }

    public class AScanLayer : ScanLayer
    {
        public AScanLayer() : base("Scanning...", "Submit", null) { }

        public AScanLayer(string uri) : base("Scanning...", "Submit", uri) { }

        public override void Load(Dictionary<string, string> parameters)
        {
            LayerStyle.DefaultLabelStyle.FontFamily = "Avenir-Roman";
            LayerStyle.DefaultLabelStyle.FontSize = 24;

            ActionParameters["42"] = "Will be in the Parameters";
        }

        // an example of parsing a barcode before it's displayed on screen.
        public override string ScannedBarcode(string barcode)
        {
            var parsedResult = barcode;

            if (parsedResult.Length > 1)
                parsedResult = parsedResult.Insert(1, "-");

            if (parsedResult.Length > 4)
                parsedResult = parsedResult.Insert(parsedResult.Length - 1, "-");

            return parsedResult;
        }

        public const string Uri = "AScanLayerUri";
    }
}