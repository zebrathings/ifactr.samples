using System;
using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;
using MonoCross.Utilities;

namespace KitchenSink
{
    public class ImageResizer : Layer
    {
        public override void Load(string url, Dictionary<string, string> parameters)
        {
            Title = "Photo";
            var photo = parameters.ContainsKey("PhotoImage") ? parameters["PhotoImage"] : null;
            Layout = App.Layout;

            var image = new iPanel();
            image.Append("iFactr stores photos in temporary folder, which is cleared out on app startup. " +
                         "However, it is considered best practice to delete this photo yourself as soon as you're done using it, or move it " +
                         "to a more permanent location. This keeps the app's temporary storage use down while the user is taking photos.");
            Items.Add(image);
            Items.Add(new iMenu { new iItem("image://?callback=" + url, "Take photo") });

            if (string.IsNullOrEmpty(photo)) return;

            var imagePath = iApp.Factory.RetrieveImage(photo);
            var bytes = iApp.File.Read(imagePath);
            iApp.File.Delete(imagePath);

            image.InsertImage(ImageUtility.EncodeImageToDataUri(bytes, imagePath.Substring(imagePath.LastIndexOf('.') + 1)));
            if (iApp.Factory.Target != MobileTarget.Windows)
            {
                image.Text = image.Text.Replace("max-width:100%", "max-width:100%;width:100%");
            }
        }
    }
}