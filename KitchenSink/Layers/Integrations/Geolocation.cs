﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Integrations;
using MonoCross.Navigation;

namespace KitchenSink
{
    public class Geolocation : MXController<GeoLocation>
    {
        public override string Load(string url, Dictionary<string, string> parameters)
        {
            if (parameters.ContainsKey("Lat"))
            {
                iApp.Session["Lat"] = parameters["Lat"];
                iApp.Session["Lon"] = parameters["Lon"];
            }
            Model = new GeoLocation();
            return ViewPerspective.Default;
        }
    }
}