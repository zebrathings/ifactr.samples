using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class Video : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Video";
            Layout = App.Layout;
            if (parameters.ContainsKey("VideoId"))
            {
                var path = iApp.Factory.RetrieveImage(parameters["VideoId"]);
                Items.Add(new iMenu
                {
                    Items = 
                    {
                        new iItem("video://?source=" + path, "Play Video"),
                        new iItem("videorecording://?callback=Integrations/Video", "New Recording"),
                    }
                });
            }
            else
            {
                var errorblock = new iBlock();
                errorblock.Append("There was a problem accessing your recording.");
                Items.Add(errorblock);
            }
        }
    }
}