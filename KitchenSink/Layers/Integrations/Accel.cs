using System.Collections.Generic;
using iFactr.Core;
using iFactr.Integrations;
using MonoCross.Navigation;

namespace KitchenSink
{
    public class Accel : MXController<Accelerometer>
    {
        public override string Load(string url, Dictionary<string, string> parameters)
        {
            if (parameters.ContainsKey("X") && parameters.ContainsKey("Y") && parameters.ContainsKey("Z"))
            {
                iApp.Session["X"] = parameters["X"];
                iApp.Session["Y"] = parameters["Y"];
                iApp.Session["Z"] = parameters["Z"];
            }
            Model = new Accelerometer();
            return ViewPerspective.Default;
        }
    }
}