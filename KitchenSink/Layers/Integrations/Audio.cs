using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class Audio : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Audio";
            Layout = App.Layout;
            if (parameters.ContainsKey("AudioId"))
            {
                var path = iApp.Factory.RetrieveImage(parameters["AudioId"]);
                Items.Add(new iMenu
                {
                    Items = 
                    {
                        new iItem("audio://?source=" + path, "Play from beginning"),
                        new iItem("audio://?command=pause", "Toggle pause"),
                        new iItem("audio://?command=stop", "Stop"),
                        new iItem("voicerecording://?callback=Integrations/Audio", "New Recording"),
                    }
                });
            }
            else
            {
                var errorblock = new iBlock();
                errorblock.Append("There was a problem accessing your recording.");
                Items.Add(errorblock);
            }
        }
    }
}