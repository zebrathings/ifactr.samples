using System.Collections.Generic;
using iFactr.Core;
using MonoCross.Navigation;

namespace KitchenSink
{
    public class Compass : MXController<iFactr.Integrations.Compass>
    {
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            iApp.Session["Bearing"] = parameters["Bearing"];
            Model = new iFactr.Integrations.Compass();
            return ViewPerspective.Default;
        }
    }
}