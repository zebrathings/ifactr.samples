﻿using System;
using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    public class Quantity : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Quantity";
            Layout = App.Layout;
            var field = new NumericField("quantity")
            {
                Label = "Number",
                Text = parameters.ContainsKey("quantity") ? parameters["quantity"] : null,
            };
            field.Validate += (value, errmsg, param) =>
            {
                try
                {
                    int number = int.Parse(value);
                    if (number < 0)
                        field.BrokenRules.Add("Please enter a valid number.");
                }
                catch (Exception)
                {
                    field.BrokenRules.Add("Please enter a valid number.");
                }
            };

            Items.Add(new Fieldset { field, });
            var path = parameters.ContainsKey("Key") ? iApp.Session[parameters["Key"]].ToString() : "Forms/Submit";
            ActionButtons.Add(new Button(null, path) { Action = Button.ActionType.Submit });
        }
    }
}