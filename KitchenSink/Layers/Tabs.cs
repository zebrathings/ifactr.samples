﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class Tabs : NavigationTabs
    {
        public static int Quantity
        {
            get { return iApp.Session.ContainsKey("tabs") ? (int)iApp.Session["tabs"] : 0; }
        }

        public override void Load(Dictionary<string, string> parameters)
        {
            Title = iApp.Instance.Title;
            Layout = App.Layout;
            int quantity = 4;
            if (parameters.ContainsKey("quantity"))
                try { quantity = int.Parse(parameters["quantity"]); }
                catch { }

            if (quantity > 0)
                TabItems.Add(new Tab("Elements", "Elements", "elements.png"));
            if (quantity > 1)
                TabItems.Add(new Tab("Forms", "Forms", "patterns.png"));
            if (quantity > 2)
                TabItems.Add(new Tab("Style", "Style", "bestpractices.png"));
            if (quantity > 3)
                TabItems.Add(new Tab("Integrations", "Integrations", "bestpractices.png"));

            while (TabItems.Count < quantity)
                TabItems.Add(new Tab("Tab " + (TabItems.Count + 1), "Integrations", "patterns.png"));

            iApp.Session["tabs"] = quantity;
        }
    }
}