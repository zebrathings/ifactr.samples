using System.Collections.Generic;
using System.Threading;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;
using Link = iFactr.UI.Link;
using iFactr.Core;

namespace KitchenSink
{
    public class Login : LoginLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string link = "..";
            if(iApp.Factory.Target == MobileTarget.WebKit)
            {
                link = string.Empty;
            }
            LoginLink = new Link(link);
            BrandImage = new Icon("LoginBrand.png");
            LayerStyle.LayerBackgroundImage = "LoginTestBG.jpg";
            LayerStyle.TextColor = Color.Black;
            base.Load(parameters);
        }

        public override bool LogIn(string username, string password)
        {
            // to demonstrate a long authentication process
            new ManualResetEvent(false).WaitOne(2000);

            var success = username.ToLower() == "test";
            if (!success)
                ErrorText = "Sign In Failed.  Username must be Test";

            return success;
        }
    }
}