﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Master)]
    public class Forms : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Forms";
            Layout = App.Layout;

            var numColumns = parameters.ContainsKey("quantity") ? parameters["quantity"] : "0";
            if (parameters.Count > 0)
            {
                if (int.Parse(numColumns) <= 1)  // must have >1 column for aggregate to make sense
                {
                    parameters.Remove("header");  // header is ignored for 1 column anyway
                    parameters.Remove("quantity");
                    parameters.Remove("quantity.Key");
                }
                iApp.Session["aggregate"] = parameters;
            }
            else if (iApp.Session.ContainsKey("aggregate"))
                iApp.Session.Remove("aggregate");

            FormFields.Backer = null;

            var menu = new iMenu();
            menu.Add(new iItem("Forms/BoolField", "BoolField"));
            menu.Add(new iItem("Forms/ButtonField", "ButtonField"));
            menu.Add(new iItem("Forms/DateField", "DateField"));
            menu.Add(new iItem("Forms/DrawingField", "DrawingField"));
            menu.Add(new iItem("Forms/EmailField", "EmailField"));
            menu.Add(new iItem("Forms/LabelField", "LabelField"));
            menu.Add(new iItem("Forms/MultiLineTextField", "MultiLineTextField"));
            menu.Add(new iItem("Forms/NavigationField", "NavigationField"));
            menu.Add(new iItem("Forms/NumericField", "NumericField"));
            menu.Add(new iItem("Forms/SelectListField", "SelectListField"));
            menu.Add(new iItem("Forms/SliderField", "SliderField"));
            menu.Add(new iItem("Forms/TextField", "TextField"));
            menu.Add(new iItem("Forms/CustomField", "CustomField"));
            menu.Add(new iItem("Forms/Validation", "Validation"));
            menu.Add(new iItem("Forms/Composite", "Composite Layers"));
            menu.Add(new iItem("Forms/All", "All Fields"));

            Items.Add(menu);

            ActionButtons.Add(new Button("Aggregation") { Address = "Forms/Aggregation", });
        }
    }
}
