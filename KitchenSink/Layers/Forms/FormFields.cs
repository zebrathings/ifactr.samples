using System.Collections.Generic;
using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using System;
using iFactr.UI;
using KeyboardType = iFactr.Core.Forms.KeyboardType;
using Link = iFactr.UI.Link;

namespace KitchenSink
{
    public class FormFields : Layer
    {
        int _numExtraButtons;

        public static string Backer
        {
            get { return (string)iApp.Session["Backer"]; }
            set { iApp.Session["Backer"] = value; }
        }

        public override void Load(string url, Dictionary<string, string> parameters)
        {
            var example = parameters.ContainsKey("Example") ? parameters["Example"] : "All";
            Layout = App.Layout;
            Backer = parameters.ContainsKey("PhotoImage") ? iApp.Factory.RetrieveImage(parameters.GetValueOrDefault("PhotoImage")) : null;

            if (iApp.Session.ContainsKey("aggregate"))
            {
                var aggregationParams = (Dictionary<string, string>)iApp.Session["aggregate"];
                if (BoolField.ParseValue(aggregationParams.GetValueOrDefault("complayer", "False")) && example != "Composite")
                {
                    CompositeLayerLink = new Link("CompositeLayer");
                    ActionButtons.Add(new Button("Next", CompositeLayerLink.Address));
                    CompositeLayerLayout = CompositeLayout.TwoColumns;  // side-by-side layout on WPF             
                }
                else
                {
                    CompositeLayerLink = null;
                }
                if (aggregationParams.ContainsKey("numbuttons"))
                {
                    try { _numExtraButtons = int.Parse(aggregationParams["numbuttons"]); }
                    catch { _numExtraButtons = 0; }
                    if (_numExtraButtons < 0 || _numExtraButtons > 1000) _numExtraButtons = 1;
                }
            }

            Title = CompositeLayerLink == null ? example : example + " on Composite Layer";
            var fieldset = new Fieldset();

            // When displaying Submitted values, show only those parameters ending in a digit (e.g. "BoolField1", "BoolField2"...)
            // and sort them so they display in that order.
            var formvalues = parameters.Where(pair => char.IsDigit(pair.Key.ToCharArray().Last())).OrderBy(x => x.Key).ToList();
            if (formvalues.Any())
            {
                Items.Add(FormSubmission.GetParameterBlock(formvalues));
            }

            switch (example)
            {
                case "BoolField":
                    fieldset.AddRange(BoolFields);
                    break;
                case "ButtonField":
                    fieldset.AddRange(ButtonFields);
                    break;
                case "DateField":
                    fieldset.AddRange(DateFields);
                    break;
                case "DrawingField":
                    fieldset.AddRange(DrawingFields);
                    break;
                case "EmailField":
                    fieldset.AddRange(EmailFields);
                    break;
                case "LabelField":
                    fieldset.AddRange(LabelFields);
                    break;
                case "MultiLineTextField":
                    fieldset.AddRange(MultiLineTextFields);
                    break;
                case "NavigationField":
                    fieldset.AddRange(NavigationFields);
                    break;
                case "NumericField":
                    fieldset.AddRange(NumericFields);
                    break;
                case "SelectListField":
                    fieldset.AddRange(SelectListFields);
                    break;
                case "SliderField":
                    fieldset.AddRange(SliderFields);
                    break;
                case "TextField":
                    fieldset.AddRange(TextFields);
                    FocusedItem = fieldset.Last();
                    break;
                case "CustomField":
                    fieldset.AddRange(CustomFields);
                    break;
                case "Validation":
                    fieldset.AddRange(Validation);
                    break;
                case "Composite":
                    CompositeLayerLink = new Link("CompositeLayer");
                    ActionButtons.Add(new Button("Next", CompositeLayerLink.Address));
                    fieldset.AddRange(TextFields);
                    break;
                case "All":
                    fieldset.AddRange(BoolFields);
                    fieldset.AddRange(ButtonFields);
                    fieldset.AddRange(DateFields);
                    fieldset.AddRange(DrawingFields);
                    fieldset.AddRange(EmailFields);
                    fieldset.AddRange(LabelFields);
                    fieldset.AddRange(MultiLineTextFields);
                    fieldset.AddRange(NavigationFields);
                    fieldset.AddRange(NumericFields);
                    fieldset.AddRange(SelectListFields);
                    fieldset.AddRange(SliderFields);
                    fieldset.AddRange(TextFields);
                    fieldset.AddRange(CustomFields);
                    break;
            }
            if (CompositeLayerLink == null)  // Add usual Action buttons unless CompositeLayer (which only has Submit action)
            {
                ActionButtons.AddRange(new List<Button>
                {
                    new Button(null, url) { Action = Button.ActionType.Submit, Image = new Icon("menu-submit.png"), ConfirmationText = "Are you ready to see your values?", },
                    new Button(null, url) { Action = Button.ActionType.Add, Image = new Icon("menu-add.png") },
                    new Button(null, url) { Action = Button.ActionType.Cancel, Image = new Icon("menu-cancel.png") },
                    new Button(null, url) { Action = Button.ActionType.Delete, Image = new Icon("menu-delete.png") },
                    new Button(null, url) { Action = Button.ActionType.Edit, Image = new Icon("menu-edit.png") },
                    new Button(null, url) { Action = Button.ActionType.More, Image = new Icon("menu-more.png") },
                    new Button("None", "Elements") { Action = Button.ActionType.None, },
                    new Button("Undefined", url) { Action = Button.ActionType.Undefined, },
                });
            }
            bool multiColumn = false;
            if (iApp.Session.ContainsKey("aggregate"))
            {
                var aggregationParams = (Dictionary<string, string>)iApp.Session["aggregate"];
                if (aggregationParams.ContainsKey("header"))
                {
                    if (aggregationParams.ContainsKey("quantity"))
                    {
                        multiColumn = true;
                        Items.AddRange(Fieldset.BuildColumnFieldset(aggregationParams["header"], Enumerable.Range(1, int.Parse(aggregationParams["quantity"])).Select(a => "Column " + a).ToList(), fieldset));
                    }
                    else
                    {
                        // something went wrong internally; remove the global variables
                        iApp.Session.Remove("aggregate");
                        //multiColumn = false;
                    }
                }
            }
            if (!multiColumn)
            {
                Items.Add(fieldset);
            }
        }

        public override bool ShouldNavigateFrom(Link uri, NavigationType navigationType)
        {
            if (iApp.Factory.Target == MobileTarget.WebKit || iApp.Factory.Target == MobileTarget.Web ||
                iApp.Factory.Target == MobileTarget.Wap || iApp.Factory.Target == MobileTarget.Console)
                return true;

            var values = GetFieldValues();
            foreach (var item in values)
                iApp.Log.Debug(item.Key + ": " + item.Value);

            var shouldNav = !(((navigationType == NavigationType.Forward || navigationType == NavigationType.Tab) && values.ContainsKey("Bool1") && BoolField.ParseValue(values["Bool1"]))
                || (navigationType == NavigationType.Back && values.ContainsKey("Bool2") && BoolField.ParseValue(values["Bool2"])));
            if (!shouldNav)
            {
                new Alert("Navigation cancelled by form.", iApp.Factory.GetResourceString("Error"), AlertButtons.OK).Show();
            }
            return shouldNav;
        }

        public IEnumerable<Field> BoolFields
        {
            get
            {
                return new List<Field>
                {
                    new BoolField("Bool1", "Prevent navigations"),
                    new BoolField("Bool2", "Prevent back button presses"),
                    new BoolField("Bool3", "iFactr is fun") { Value = true, },
                    new BoolField("Bool4") {
                        Label = "BoolLabel",
                        Value = true,  // First property is overwritten
                        Text = "Nada",
                    }
                };
            }
        }

        public IEnumerable<Field> ButtonFields
        {
            get
            {
                var buttonList = new List<Field>
                {
                    new ButtonField("Button1", "Forms") { Label = "A non-submitting button", Action = Button.ActionType.Undefined, },
                    new ButtonField("Button2", ".") { Label = "A postback button", },
                    new ButtonField("Button3", "Elements") { Label = "Submit values to Elements" },
                    new ButtonField("Button4", "Elements/Collections") { Label = "Submit values to Collections" },
                    new ButtonField("Button5", "./LoginLayer") { Label = "Submit values to Login screen" },
                    new ButtonField(null, null) { Label = "This does nothing and submits nothing." },
                    new ButtonField("Button7", null) {
                        Label = "This is NOT the Label",  // First property is overwritten
                        Text = "This is the Label",
                    },
                };

                for (int i = 0; i < _numExtraButtons; i++)
                {
                    string buttonID = String.Format("ButtonExtra{0}", i);
                    string buttonLabel = String.Format("ButtonExtra{0} does nothing", i);
                    buttonList.Add(new ButtonField(buttonID, null) { Label = buttonLabel });
                }

                return buttonList;
            }
        }

        public IEnumerable<Field> DateFields
        {
            get
            {
                return new List<Field>
                {
                    new DateField("Date1", DateField.DateType.Date) { Label = "A date field" },
                    new DateField("Date2", DateField.DateType.Time) { Label = "A time field", MinuteInterval = 5, },
                    new DateField("Date3", DateField.DateType.DateTime) { Label = "A date/time field" },
                };
            }
        }

        public IEnumerable<Field> DrawingFields
        {
            get
            {
                ActionButtons.Add(new Button("Select canvas image", "image://?callback=."));
                return new List<Field>
                {
                    new DrawingField("Drawing1", "A drawing canvas", DrawingType.Canvas, Backer, Backer != null),
                    new DrawingField("Drawing2", "A signature field", DrawingType.Signature, "signature.png",  Backer != null)
                };
            }
        }

        public IEnumerable<Field> EmailFields
        {
            get
            {
                return new List<Field>
                {
                    new EmailField("Email1") { Label = "An email field" },
                };
            }
        }

        public IEnumerable<Field> ImagePickerFields
        {
            get
            {
                return new List<Field>
                {
                    new ImagePickerField("ImagePicker1") { Label = "An image picker field" }
                };
            }
        }

        public IEnumerable<Field> LabelFields
        {
            get
            {
                return new List<Field>
                {
                    new LabelField("Label1") { Label = "A Label field", Text = "With optional text" },
                    new LabelField(), //Blank test
                    new LabelField("Label2") { Label = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem nisl, aliquam quis imperdiet vitae, laoreet vitae enim. Maecenas quis neque suscipit justo ultrices auctor nec at quam. Sed mollis dolor vel neque volutpat semper. Ut venenatis interdum odio non ornare. Donec eu neque nibh. Duis sapien tellus, placerat vitae accumsan scelerisque, commodo sit amet erat. Ut accumsan nisl viverra massa vestibulum ac sodales tortor sollicitudin. Pellentesque aliquet mi ornare nisl placerat fermentum. Fusce nec turpis quis ipsum imperdiet feugiat volutpat vel eros. Quisque accumsan mattis tellus, in cursus mauris semper varius. Suspendisse egestas lacus arcu, at auctor metus. Integer convallis, nibh ut posuere laoreet, libero sem ultricies sem, eget laoreet arcu neque id elit. Suspendisse potenti. Fusce sollicitudin ultrices quam, at vulputate turpis fermentum in. Vestibulum et nisi et nisl venenatis hendrerit quis nec libero."},
                };
            }
        }

        public IEnumerable<Field> MultiLineTextFields
        {
            get
            {
                return new List<Field>
                {
                    new MultiLineTextField("MutliLine1") { Label = "A multiline text entry field" },
                };
            }
        }

        public IEnumerable<Field> NavigationFields
        {
            get
            {
                return new List<Field>
                {
                    new NavigationField("Navigation1", "Forms") { Label = "A navigation field", Action = Button.ActionType.Undefined, },
                    new NavigationField("Navigation2", "Forms/Submit") { Label = "A navigation field that submits", },
                };
            }
        }

        public IEnumerable<Field> NumericFields
        {
            get
            {
                return new List<Field>
                {
                    new NumericField("Number1") { Label = "A numeric field" },
                };
            }
        }

        public IEnumerable<Field> SelectListFields
        {
            get
            {
                string[] items = { "Option 1", "Option 2", "Option 3" };
                return new List<Field>
                {
                    new SelectListField("SelectList1", items) { Label = "A select list field", SelectedIndex = 2, },
                    new SelectListField("SelectList2", items) { Label = "A select list field", SelectedIndex = 1, },
                    new SelectListField("SelectList3", items) { Label = "A select list field", SelectedIndex = 0, },
                    new SelectListField("SelectList4") { Label = "A select list field with ID only", }
                };
            }
        }

        public IEnumerable<Field> SliderFields
        {
            get
            {
                return new List<Field>
                {
                    new SliderField("Slider1") { Label = "A field with a slider", Min = 1, },
                    new SliderField("Slider2") { Label = "Step Increment 5", StepSize = 5, },
                    new SliderField("Slider3") { Label = "Start value of 25", Value = 25, },
                };
            }
        }

        public IEnumerable<Field> TextFields
        {
            get
            {
                return new List<Field>
                {
                    new TextField("Text1") { Label = "A text entry field" },
                    new TextField("Text2") { Label = "Text and placeholder", Placeholder = "Optional"},
                    new TextField("Text3"){ Label = "PIN Keyboard", KeyboardType = KeyboardType.PIN },
                    new TextField("Text4"){ Label = "Symbolic Keyboard", KeyboardType = KeyboardType.Symbolic },
                    new TextField("Text5"){ Label = "Email Keyboard", KeyboardType = KeyboardType.Email },
                    new TextField("Password1") { Label = "A password entry field", IsPassword = true, },
                    new TextField("Password2")
                    {
                        Label = "A PIN entry field",
                        IsPassword = true,
                        Placeholder = "Enter 4-digit PIN",
                        KeyboardType = KeyboardType.PIN,
                        Expression = @"^[\d]{0,4}$",
                    },
                };
            }
        }

        public IEnumerable<Field> CustomFields
        {
            get
            {
                return new List<Field>
                {
                    new CustomNumericField("Custom1") { Label = "Shows a custom keyboard" }
                };
            }
        }

        private static IEnumerable<Field> Validation
        {
            get
            {
                var valid0 = new BoolField("Valid0") { Label = "Valid bool" };
                valid0.Validate += (value, msg, args) =>
                {
                    if (!valid0.Value)
                        valid0.BrokenRules.Add("Must be on");
                };

                var valid1 = new EmailField("Valid1") { Label = "Valid email" };
                valid1.Validate += (value, msg, args) => valid1.ValidEmail(value, "Please use a valid email address.");

                var valid2 = new TextField("Valid2") { Label = "Required", IsPassword = true, };
                valid2.Validate += (value, msg, args) => valid2.TextIsRequired(value, "Please enter a password.");

                var valid3 = new MultiLineTextField("Valid3") { Label = "Send a tweet", Placeholder = "Twitter tweet" };
                valid3.Validate += (value, msg, args) => valid3.TextMaxLength(value, "Tweets are limited to 140 characters.", 140);

                var valid4 = new TextField("Valid4") { Label = "Last Name" };
                valid4.Validate += (value, msg, args) => valid4.TextMinLength(value, "Please enter your last name.", 2);

                var valid5 = new TextField("Valid5") { Label = "Custom validation", Placeholder = "iFactr rules!", };
                valid5.Validate += (value, msg, args) =>
                {
                    if (string.IsNullOrEmpty(value))
                        valid5.BrokenRules.Add("Field cannot be blank");
                    if (value != "iFactr rules!")
                        valid5.BrokenRules.Add("You must enter \"iFactr rules!\"");
                };

                // valid6: Add a DateField displaying Date/Time and give it a validation rule.
                var valid6 = new DateField("Valid6")
                {
                    Label = "Enter a 21st C. date (or greater)",
                    Type = DateField.DateType.DateTime,
                };

                valid6.Validate += (value, msg, args) =>
                {
                    if (DateTime.Parse(value) <= DateTime.Parse("31-Dec-2000"))
                        valid6.BrokenRules.Add("Date must be greater than 31-Dec-2000");
                };

                // valid7: Add a CustomNumericField (which inherits from NumericField) and give it a validation rule.
                var valid7 = new CustomNumericField("Valid7") { Label = "Enter a number containing '42' somewhere" };
                valid7.Validate += (value, msg, args) =>
                {
                    if (value == null || !value.Contains("42"))
                        valid7.BrokenRules.Add("Number must contain a 42");
                };

                var valid8 = new DrawingField("Valid8", "Draw something", DrawingType.Canvas, null, false);
                valid8.Validate += (value, msg, args) =>
                {
                    if (valid8.DrawnImageId == null)
                    {
                        valid8.BrokenRules.Add("Must draw something.");
                        valid8.BrokenRules.Add("Make it weird.");
                    }
                };

                var valid9 = new SelectListField("Valid9", "Option 1", "Option 2") { Label = "Pick something" };
                valid9.Validate += (value, msg, args) =>
                {
                    if (value == "Option 1")
                        valid9.BrokenRules.Add("Don't pick Option 1");
                };
                var valid10 = new SliderField("Valid10")
                {
                    Label = "Drag me to the end",
                };
                valid10.Validate += (value, msg, args) =>
                {
                    if (valid10.Value != 100)
                        valid10.BrokenRules.Add("Value must be 100.");
                };

                return new List<Field>
                {
                    valid0,
                    valid1,
                    valid2,
                    valid3,
                    valid4,
                    valid5,
                    valid6,
                    valid7,
                    valid8,
                    valid9,
                    valid10,
                };
            }
        }
    }
}