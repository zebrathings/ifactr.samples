using System.Collections.Generic;
using System.Text.RegularExpressions;
using iFactr.Core;
using iFactr.Core.Layers;
using MonoCross.Utilities;

namespace KitchenSink
{
    public class FormSubmission : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Form Submission";
            Layout = App.Layout;
            Items.Add(GetParameterBlock(parameters));
        }

        public static iBlock GetParameterBlock(IEnumerable<KeyValuePair<string, string>> parameters)
        {
            var block = new iBlock();
            foreach (var pair in parameters)
            {
                if (pair.Value != null && new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}(.png|.jpg){0,1}$").IsMatch(pair.Value))
                {
                    block.AppendBoldLine(pair.Key + ":");
                    block.InsertImage(iApp.Factory.RetrieveImage(pair.Value));
                }
                else if (pair.Value != null && iApp.File.Exists(pair.Value) && (pair.Value.EndsWith(".png") || pair.Value.EndsWith(".jpg")))
                {
                    block.AppendBoldLine(pair.Key + ":");
                    var bytes = iApp.File.Read(pair.Value);
                    block.InsertImage(ImageUtility.EncodeImageToDataUri(bytes, pair.Value.Substring(pair.Value.LastIndexOf('.') + 1)));
                }
                else
                {
                    block.AppendLabeledTextLine(pair.Key, pair.Value);
                }
            }
            return block;
        }
    }
}