﻿using System.Collections.Generic;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.UI;
using Button = iFactr.Core.Controls.Button;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    public class Popover1 : Layer
    {
		
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Pop1";
            // Experiment with LayerStyle at your peril:
            //LayerStyle.LayerBackgroundColor = Color.Green;
            //LayerStyle.TextColor = Color.Red;

            var tf1 = new TextField("TF1") { Label = "Text 1", };
            Items.Add(new Fieldset
            {
                tf1,
                new TextField("TF2") { Label = "Text 2", }, 
            });
            FocusedItem = tf1;
            ActionButtons.Add(new Button("Next", "Elements/PopoverLayer2"));
            BackButton = new Button("Back") { Address = "Elements", };

        }
    }

    [PreferredPane(Pane.Popover)]
    public class Popover2 : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Pop2";

            //LayerStyle.LayerBackgroundColor = Color.Purple;
            //LayerStyle.TextColor = Color.Black;

            Items.Add(new Fieldset
            {
                new LabelField(null)
                {
                    Label = "Text 1's value", 
                    Text = parameters.GetValueOrDefault("TF1", "Not found in parameters"),
                },
                new LabelField(null)
                {
                    Label = "Text 2's value",
                    Text = parameters.GetValueOrDefault("TF2", "Not found in parameters"),
                }, 
            });
            ActionButtons.Add(new Button("Finish", "Elements"));

        }
    }
}