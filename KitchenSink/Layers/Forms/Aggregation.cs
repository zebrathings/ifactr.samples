﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class Aggregation : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Items.Add(new Fieldset
            {
                new TextField("header") {Label = "Aggregated Header", Text = string.Empty, Placeholder="Displayed only if #columns>1"},
                new SelectListField("quantity", Enumerable.Range(1, 6).Select(a => a.ToString(CultureInfo.InvariantCulture))) { Label = "Number of columns", SelectedIndex = 0, },
                new BoolField("complayer", "Composite Layer (on/off)") { Value = false, },
                new NumericField("numbuttons") { Label="Extra buttons", Text="0" }, 
            });
            ActionButtons.Add(new Button(null, "Forms"));
        }
    }
}