﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class TabProperties : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            if (parameters.ContainsKey("pb"))
            {
                App.ChangeTabMode();
                CancelLoadAndNavigate(iApp.Instance.NavigateOnLoad);
                return;
            }

            Title = "Tabs";
            Layout = App.Layout;
            Items.Add(new Fieldset
            {
                new SelectListField("quantity", "0", "1", "2", "3", "4", "5", "6") { Label = "Number of tabs", SelectedIndex = Tabs.Quantity, },
            });
            if (App.ChangeTabMode != null)
            {
                Items.Add(new iMenu
                {
                    Items =
                    {
                        new iItem("Elements/TabProperties", "Change tab mode") { Link = { Parameters = { { "pb", "true" }, }, }, },
                    },
                });
            }
            ActionButtons.Add(new Button(null, string.Empty));
        }
    }
}