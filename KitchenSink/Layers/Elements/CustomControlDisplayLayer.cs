﻿using System.Collections.Generic;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class CustomControlDisplayLayer : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Multi Button Item";

            //var list = new iList();
            //list.Add(new CustomControl() { 
            //    ButtonEndPoints =  new string[] { "Elements", "Elements/HtmlText/iBlock", "Elements/HtmlText/iPanel", },
            //});
            //Items.Add(list);

            Title = "Work Orders";

            var list = new iList();
            list.Add(new CustomControl()
            {
                Title = "WO1234",
                Description =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu egestas diam, vel congue augue. Sed consequat, ligula ac fermentum pellentesque, dolor sem auctor purus, ut hendrerit nisl tellus quis neque. In ultrices odio vitae felis placerat, eu porta nibh elementum. Donec ac tempus ipsum",
                Status = "Released",
                Priority = "High",
                Type = "Corrective Maintenance",
            });
            list.Add(new CustomControl()
            {
                Title = "WO5678",
                Description =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu egestas diam, vel congue augue. Sed consequat, ligula ac fermentum pellentesque, dolor sem auctor purus, ut hendrerit nisl tellus quis neque. In ultrices odio vitae felis placerat, eu porta nibh elementum. Donec ac tempus ipsum",
                Status = "Released",
                Priority = "High",
                Type = "Corrective Maintenance",
            });
            list.Add(new CustomControl()
            {
                Title = "WO9123",
                Description =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu egestas diam, vel congue augue. Sed consequat, ligula ac fermentum pellentesque, dolor sem auctor purus, ut hendrerit nisl tellus quis neque. In ultrices odio vitae felis placerat, eu porta nibh elementum. Donec ac tempus ipsum",
                Status = "Released",
                Priority = "Medium",
                Type = "Corrective Maintenance",
            });
            list.Add(new CustomControl()
            {
                Title = "WO4567",
                Description =
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu egestas diam, vel congue augue. Sed consequat, ligula ac fermentum pellentesque, dolor sem auctor purus, ut hendrerit nisl tellus quis neque. In ultrices odio vitae felis placerat, eu porta nibh elementum. Donec ac tempus ipsum",
                Status = "Hold",
                Priority = "Low",
                Type = "Corrective Maintenance",
            });

            Items.Add(list);
        }
    }
}