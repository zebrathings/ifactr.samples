﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;
using MonoCross.Utilities;
using Link = iFactr.UI.Link;

namespace KitchenSink
{
    public class HtmlText : Layer
    {
        private const string maxWidth = "50";
        private const string maxHeight = "50";
        private const string imageSource = "blockTest.jpg";

        public override void Load(string url, Dictionary<string, string> parameters)
        {
            string example = parameters.GetValueOrDefault("Example", "Formatting");
            var quantity = parameters.ContainsKey("quantity") ? int.Parse(parameters["quantity"]) : 1;
            iApp.Session["Number"] = url;
            Layout = App.Layout;

            IHtmlText htmlText;
            switch (parameters.GetValueOrDefault("Type", "iPanel"))
            {
                case "iBlock":
                    htmlText = new iBlock { Header = "iBlock", };
                    break;
                default:
                    htmlText = new iPanel { Header = "iPanel", };
                    break;
            }

            switch (example)
            {
                case "Links":
                    Title = "Links";
                    htmlText.AppendLine("You can add some links to your HTML content via Append methods.");
                    htmlText.AppendLink(new Link("http://support.ifactr.com/") { Text = "AppendLink(link) with confirm", ConfirmationText = "Do you want to go to the iFactr support site?" });
                    htmlText.AppendLine();
                    htmlText.AppendLine("The following demonstrates AppendLink without Text, which defaults to link's address (opens in new window):");
                    htmlText.AppendLink(new Link("http://www.fedex.com/cgi-bin/tracking?tracknumbers=973922463792&action=track&language=english", RequestType.NewWindow));
                    htmlText.AppendLine();
                    htmlText.AppendLine("AppendEmail(text)");
                    htmlText.AppendEmail("support@ifactr.com");
                    htmlText.AppendLine();
                    htmlText.AppendLine("AppendTelephone(text)");
                    htmlText.AppendTelephone("(555) 555-5555");
                    break;
                case "InsertImage":
                    Title = "Images";
                    htmlText.InsertImage(imageSource);
                    htmlText.AppendLine();
                    htmlText.AppendBoldLine("Constraining size:");
                    htmlText.InsertImage(imageSource, maxHeight, maxWidth);
                    break;
                case "InsertImageFloatLeft":
                    Title = "Float Left";
                    htmlText.InsertImageFloatLeft(imageSource);
                    htmlText.AppendBoldLine("Constraining size:");
                    htmlText.InsertImageFloatLeft(imageSource, maxHeight, maxWidth);
                    htmlText.Text += "<div style='clear: both'></div>";
                    break;
                case "InsertImageFloatRight":
                    Title = "Float Right";
                    htmlText.InsertImageFloatRight(imageSource);
                    htmlText.AppendBoldLine("Constraining size:");
                    htmlText.InsertImageFloatRight(imageSource, maxHeight, maxWidth);
                    htmlText.Text += "<div style='clear: both'></div>";
                    break;
                case "InsertImageLink":
                    Title = "Image Link";
                    htmlText.InsertImageLink("http://cdn.ifactr.com/wp-content/uploads/2012/08/ifactr-logo.png", "http://support.ifactr.com/");
                    break;
                case "InsertCompositeImage":
                    Title = "Composite";

                    // The first image in list sets the size of the Composite.
                    // The last image "wins" if it is opaque.
                    // jpg does not support transparency

                    var images = new List<string>
                    {
                        iApp.Factory.ApplicationPath.AppendPath("testIcon.jpg"),
                        iApp.Factory.ApplicationPath.AppendPath("bestpractices.png"),
                    };

                    string outputFile = iApp.Factory.SessionDataPath.AppendPath("result.png");

                    htmlText.AppendBoldLine("Composite of 2 images with different sizes: testIcon.jpg and bestpractices.png");
                    iApp.Compositor.CreateCompositeImage(images, outputFile, true);
                    htmlText.InsertImage(outputFile);
                    htmlText.AppendHorizontalRule();

                    break;
                default:
                    Title = "Formatting";
                    htmlText.Text = "Text append methods";
                    htmlText.AppendLine();
                    htmlText.AppendLine("You can add content to HTML-based controls via the Append methods.");
                    htmlText.Text += "To add HTML-based content directly, use the Text property.";
                    htmlText.AppendLine();
                    htmlText.AppendLine("The Append methods include:");
                    htmlText.AppendLine("Append(text)");
                    htmlText.AppendLine("AppendLine()");
                    htmlText.AppendLine();
                    htmlText.AppendLine("AppendLine(text)");
                    htmlText.AppendBoldLine("AppendBold(text)");
                    htmlText.AppendBoldItalicLine("AppendBoldItalic(text)");
                    htmlText.AppendBoldItalicLine("AppendBoldItalicLine(text)");
                    htmlText.AppendBoldLine("AppendBoldLine(text)");
                    htmlText.AppendHeading("AppendHeading(text)");
                    htmlText.AppendSubHeading("AppendSubHeading(text)");
                    htmlText.AppendSecondarySubHeading("AppendSecondarySubHeading(text)");
                    htmlText.AppendItalicLine("AppendItalic(text)");
                    htmlText.AppendItalicLine("AppendItalicLine(text)");
                    htmlText.AppendLabeledTextLine("Label", "AppendLabeledTextLine(label, text)");
                    htmlText.Append("AppendHorizontalRule()");
                    htmlText.AppendHorizontalRule();
                    break;
            }

            for (int i = 0; i < quantity; i++)
                Items.Add(((iLayerItem)htmlText).Clone());

            ActionButtons.Add(new Button("Quantity", "Quantity/Number/" + quantity));
        }
    }
}