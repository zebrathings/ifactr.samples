using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.UI;

namespace KitchenSink
{
    //  Kenny Goer's Support ticket:
    //  http://support.ifactr.com/discussions/windows-target/165-masterdetail-bug
    //  Add an iLayer (implementing IDetailLayer) to test Master/Detail navigation problem
    [PreferredPane(Pane.Detail)]
    public class DetailLayer1 : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Detail Layer 1";
            Items.Add(new Fieldset
            {
                new ButtonField("gotoDL2", "Elements/DetailLayer1/DetailLayer2") { Label = "Next Detail long path", },
                new ButtonField("gotoDL2b", "Elements/DetailLayer2") { Label = "Next Detail Layer", }
            });
        }
    }
    [PreferredPane(Pane.Detail)]
    public class DetailLayer2 : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Detail Layer 2";
            var fieldset = new Fieldset { new BoolField("Bool1", "Second detail layer") { Value = true, }, };
            Items.Add(fieldset);
            ActionButtons.Add(new Button("Path parent", ".."));
        }
    }
}