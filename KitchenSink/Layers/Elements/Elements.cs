﻿using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.Core;
using MonoCross.Utilities;
using KitchenSink.Controllers;

namespace KitchenSink
{
    public class Elements : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            if (parameters.ContainsKey("Language"))
            {
                switch (parameters["Language"])
                {
                    case "zh-hans":
                        iApp.Factory.LanguageCode = "zh-Hans";
                        break;
                    case "zh-hant":
                        iApp.Factory.LanguageCode = "zh-Hant";
                        break;
                    case "English":
                        iApp.Factory.LanguageCode = "en-US";
                        break;
                    case "German":
                        iApp.Factory.LanguageCode = "de";
                        break;
                    case "French":
                        iApp.Factory.LanguageCode = "fr";
                        break;
                }
            }

            Title = "Elements";
            Layout = App.Layout;

            string fromDataPath = iApp.Factory.SessionDataPath.AppendPath("testIcon.jpg");
            if (iApp.File.Exists("testIcon.jpg"))
            {
                iApp.File.Copy("testIcon.jpg", fromDataPath);
            }

            string dataIcon = ImageUtility.EncodeImageToDataUri(iApp.File.Read(fromDataPath), "jpg");

            var menu = new iMenu();
            menu.Items.Add(new iItem("Elements/Collections/iList", "iList") { Icon = new Icon("testIcon.jpg"), });
            menu.Items.Add(new iItem("Elements/Collections/iMenu", "iMenu") { Icon = new Icon(fromDataPath), });
            menu.Items.Add(new iItem("Elements/HtmlText/iBlock", "iBlock")
            {
                Icon = dataIcon == null ? null : new Icon(dataIcon),
            });
            menu.Items.Add(new iItem("Elements/HtmlText/iPanel", "iPanel")
            {
                Icon = new Icon("https://raw.githubusercontent.com/Zebra/iFactr-UI/master/iFactr.png")
            });
            menu.Items.Add(new iItem("Elements/CustomControlLayer", "Custom Control Layer"));
            menu.Items.Add(new iItem("Elements/DetailLayer1", "Detail Layer"));
            menu.Items.Add(new iItem("Elements/PopoverLayer", "Popover Layer"));
            menu.Items.Add(new iItem("Elements/LoginLayer", "Login Layer"));
            menu.Items.Add(new iItem("Elements/TabProperties", "Tabs"));
            Items.Add(menu);

            menu = new iMenu { Header = "MonoCross" };
            menu.Items.Add(new iItem("MonoCross", "Message IMXView: Hello!"));
            menu.Items.Add(new iItem("MonoCross", "Message IMXView: Goodbye!")
            {
                Link = { Parameters = { { "message", "Goodbye!" } } }
            });
            menu.Items.Add(new iItem("MonoCross", "Controls Sampler") { Link = { Parameters = { { "monoview", MessageController.SamplerTitle } } } });
            menu.Items.Add(new iItem("MonoCross", "Dynamic Placement") { Link = { Parameters = { { "monoview", MessageController.ClickMeTitle } } } });
            menu.Items.Add(new iItem("MonoCross", "Internal Browser") { Link = { Parameters = { { "monoview", MessageController.InternalBrowserPerspective } } } });
            menu.Items.Add(new iItem("MonoCross", "External Browser") { Link = { Parameters = { { "monoview", MessageController.ExternalBrowserPerspective } } } });

            if (iApp.Instance.NavigationMap.GetControllerForPattern("TestRunner") != null)
            {
                menu.Items.Add(new iItem("TestRunner", "Unit Tests"));
            }

            Items.Add(menu);

            ActionButtons.Add(new Button("Localization", "Elements/Language"));
        }
    }
}