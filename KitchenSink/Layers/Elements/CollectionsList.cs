﻿using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Master)]
    public class CollectionsList : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = parameters.GetValueOrDefault("Type", "iList");
            Layout = App.Layout;
            var menu = new iMenu();
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "iItem", "iItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "ContentItem", "ContentItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "MessageItem", "MessageItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "MultiLineSubtextItem", "MultiLineSubtextItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "PhotoItem", "PhotoItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "RightSubtextItem", "RightSubtextItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "SubtextBelowAndBesideItem", "SubtextBelowAndBesideItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "SubtextItem", "SubtextItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "VariableContentItem", "VariableContentItem"));
            menu.Items.Add(new iItem("Elements/Collections/" + Title + "/" + "All", "All iItems"));
            Items.Add(menu);

            if (Title == "iMenu")
            {
                BackButton = new Button("Back", null) { ConfirmationText = "Back to Elements?" };
            }
            else
            {
                ActionButtons.Add(new Button("List Type", "Elements/Collections"));
            }
            Collections.Icon.Location = null;
        }
    }
}