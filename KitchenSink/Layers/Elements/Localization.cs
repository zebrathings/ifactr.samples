using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    public class Localization : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Localization";
            var menu = new iMenu { Header = "Choose a language" };
            menu.Add(new iItem("Elements/Language/English", "English"));
            menu.Add(new iItem("Elements/Language/German", "German"));
            menu.Add(new iItem("Elements/Language/French", "French"));
            menu.Add(new iItem("Elements/Language/zh-hant", "Chinese (Traditional)"));
            menu.Add(new iItem("Elements/Language/zh-hans", "Chinese (Simplified)"));
            Items.Add(menu);
        }
    }
}