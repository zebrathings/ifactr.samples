﻿using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Master)]
    public class HtmlTextList : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = parameters.GetValueOrDefault("Type", "iBlock");
            Layout = App.Layout;

            var formatMenu = new iMenu { Header = "Formatting" };
            formatMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/Formatting", "Basic formatting"));
            formatMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/Links", "Links"));
            Items.Add(formatMenu);

            var imageMenu = new iMenu { Header = "Showing images" };
            imageMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/InsertImage", "Inserting images into content"));
            imageMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/InsertImageFloatLeft", "Float images to the left"));
            imageMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/InsertImageFloatRight", "Float images to the right"));
            imageMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/InsertImageLink", "Image links"));
            imageMenu.Items.Add(new iItem("Elements/HtmlText/" + Title + "/InsertCompositeImage", "Composite Images"));
            Items.Add(imageMenu);

            DetailLink = formatMenu.Items.First().Link;
        }
    }
}