﻿using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using Link = iFactr.UI.Link;

namespace KitchenSink
{
    public class Collections : iLayer
    {
        private const string LongSubtext = "subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext subtext";
        private const string Filler = " filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler filler";

        private static readonly Button Button = new Button("To the Main Menu", "Elements");
        public static readonly Icon Icon = new Icon();

        private static Icon DisplayIcon
        {
            get { return (Icon)iApp.Session["Icons"]; }
            set { iApp.Session["Icons"] = value; }
        }

        public override void Load(string url, Dictionary<string, string> parameters)
        {
            DisplayIcon = null;
            var type = parameters.GetValueOrDefault("Type", "iList");
            var example = parameters.GetValueOrDefault("Example", "All");
            var pageSize = parameters.ContainsKey("quantity") ? int.Parse(parameters["quantity"]) : 8;

            if (parameters.ContainsKey("PhotoImage"))
            {
                Icon.Location = iApp.Factory.RetrieveImage(parameters["PhotoImage"]);
            }
            else if (string.IsNullOrEmpty(Icon.Location))
            {
                Icon.Location = "testIcon.jpg";
            }

            Layout = App.Layout;
            iApp.Session["Pages"] = url;

            Title = example;

            iCollection collection;
            switch (type)
            {
                case "iMenu":
                    collection = new iMenu();
                    break;
                case "Search":
                    collection = new SearchList { PageSize = pageSize };
                    break;
                case "Custom":
                    Items.Add(new iBlock { Items = { new Label(LayerStyle) { Text = "Look for things in this list!", } } });
                    Items.Add(new CustomSearchList { PageSize = pageSize });
                    collection = new SearchList { PageSize = pageSize };
                    break;
                case "Paged":
                    collection = new iPagedList { PageSize = pageSize };
                    break;
                default:
                    collection = new iList();
                    break;
            }

            collection.Header = type;

            for (int i = 0; i < 2; i++)
            {
                switch (example)
                {
                    case "ContentItem":
                        collection.AddRange(Iconify(ContentItems));
                        collection.AddRange(Linkify(ContentItems));
                        collection.AddRange(Buttonify(ContentItems));
                        break;
                    case "MessageItem":
                        collection.AddRange(Iconify(MessageItems));
                        collection.AddRange(Linkify(MessageItems));
                        collection.AddRange(Buttonify(MessageItems));
                        break;
                    case "MultiLineSubtextItem":
                        collection.AddRange(Iconify(MultiLineSubtextItems));
                        collection.AddRange(Linkify(MultiLineSubtextItems));
                        collection.AddRange(Buttonify(MultiLineSubtextItems));
                        break;
                    case "PhotoItem":
                        collection.AddRange(Iconify(PhotoItems));
                        collection.AddRange(Linkify(PhotoItems));
                        collection.AddRange(Buttonify(PhotoItems));
                        break;
                    case "RightSubtextItem":
                        collection.AddRange(Iconify(RightSubtextItems));
                        collection.AddRange(Linkify(RightSubtextItems));
                        collection.AddRange(Buttonify(RightSubtextItems));
                        break;
                    case "SubtextBelowAndBesideItem":
                        collection.AddRange(Iconify(SubtextBelowAndBesideItems));
                        collection.AddRange(Linkify(SubtextBelowAndBesideItems));
                        collection.AddRange(Buttonify(SubtextBelowAndBesideItems));
                        break;
                    case "SubtextItem":
                        collection.AddRange(Iconify(SubtextItems));
                        collection.AddRange(Linkify(SubtextItems));
                        collection.AddRange(Buttonify(SubtextItems));
                        break;
                    case "VariableContentItem":
                        collection.AddRange(Iconify(VariableContentItems));
                        collection.AddRange(Linkify(VariableContentItems));
                        collection.AddRange(Buttonify(VariableContentItems));
                        break;
                    case "All":
                        collection.AddRange(Iconify(iItems));
                        collection.AddRange(Iconify(ContentItems));
                        collection.AddRange(Linkify(ContentItems));
                        collection.AddRange(Buttonify(ContentItems));
                        collection.AddRange(Iconify(MessageItems));
                        collection.AddRange(Linkify(MessageItems));
                        collection.AddRange(Buttonify(MessageItems));
                        collection.AddRange(Iconify(MultiLineSubtextItems));
                        collection.AddRange(Linkify(MultiLineSubtextItems));
                        collection.AddRange(Buttonify(MultiLineSubtextItems));
                        collection.AddRange(Iconify(PhotoItems));
                        collection.AddRange(Linkify(PhotoItems));
                        collection.AddRange(Buttonify(PhotoItems));
                        collection.AddRange(Iconify(RightSubtextItems));
                        collection.AddRange(Linkify(RightSubtextItems));
                        collection.AddRange(Buttonify(RightSubtextItems));
                        collection.AddRange(Iconify(SubtextBelowAndBesideItems));
                        collection.AddRange(Linkify(SubtextBelowAndBesideItems));
                        collection.AddRange(Buttonify(SubtextBelowAndBesideItems));
                        collection.AddRange(Iconify(SubtextItems));
                        collection.AddRange(Linkify(SubtextItems));
                        collection.AddRange(Buttonify(SubtextItems));
                        collection.AddRange(Iconify(VariableContentItems));
                        collection.AddRange(Linkify(VariableContentItems));
                        collection.AddRange(Buttonify(VariableContentItems));
                        break;
                    default:
                        collection.AddRange(Iconify(iItems));
                        break;
                }
                DisplayIcon = Icon;
            }

            Items.Add(collection);
            if (collection is iPagedList)
            {
                ActionButtons.Add(new Button("Page Size", "Quantity/Pages/" + pageSize));
            }
            ActionButtons.Add(new Button("Icon", "image://?callback=."));
        }

        private static IEnumerable<iItem> Iconify(IEnumerable<iItem> iitems)
        {
            return iitems.Select(item =>
            {
                item.Icon = DisplayIcon;
                return item;
            });
        }

        private static IEnumerable<iItem> Linkify(IEnumerable<iItem> iitems)
        {
            return iitems.Select(item =>
            {
                item.Link = new Link(string.Empty);
                item.Icon = DisplayIcon;
                return item;
            });
        }

        private static IEnumerable<iItem> Buttonify(IEnumerable<iItem> iitems)
        {
            return iitems.Select(item =>
            {
                item.Button = Button;
                item.Link = new Link(string.Empty);
                item.Icon = DisplayIcon;
                return item;
            });
        }

        #region iItem definitions
        private static IEnumerable<iItem> ContentItems
        {
            get
            {
                return new List<iItem>
                {
                    new ContentItem(null, "An iItem that can display content."),
                    new ContentItem(null, "An iItem that can display content with long subtext.", LongSubtext),
                    new ContentItem(null, "An iItem that can display content with long text and no subtext." + Filler),
                    new ContentItem(null, "An iItem that can display content with long text and subtext." + Filler, LongSubtext),
                };
            }
        }

        public static IEnumerable<iItem> MessageItems
        {
            get
            {
                var date = DateTime.Now.ToString("d");
                return new List<iItem>
                {
                     new MessageItem(null, "An iItem meant for displaying messages."),
                     new MessageItem(null, "An iItem meant for displaying messages.", "iFactr Dev Team", date, LongSubtext),
                     new MessageItem(null, "An iItem meant for displaying messages, without subtext.", null, date, LongSubtext),
                     new MessageItem(null, "An iItem meant for displaying messages, with no date.", "iFactr Dev Team", null, LongSubtext),
                     new MessageItem(null, "An iItem meant for displaying messages, with no message.", "iFactr Dev Team", date, null),
                     new MessageItem(null, "An iItem meant for displaying messages, with no date or message.", "iFactr Dev Team"),
                     new MessageItem(null, "An iItem meant for displaying messages, without subtext or date.", null, null, LongSubtext),
                     new MessageItem(null, "An iItem meant for displaying messages, without subtext or message.", null, date, null),
                };
            }
        }

        private static IEnumerable<iItem> MultiLineSubtextItems
        {
            get
            {
                return new List<iItem>
                {
                     new MultiLineSubtextItem(null, "An iItem that can display multi-line subtext.", LongSubtext),
                };
            }
        }

        private static IEnumerable<iItem> PhotoItems
        {
            get
            {
                return new List<iItem>
                {
                    new PhotoItem(null, "An iItem for displaying photos"),
                };
            }
        }

        private static IEnumerable<iItem> RightSubtextItems
        {
            get
            {
                return new List<iItem>
                {
                     new RightSubtextItem(null, "An iItem that forces subtext to the right", "12345678"),
                };
            }
        }

        private static IEnumerable<iItem> SubtextBelowAndBesideItems
        {
            get
            {
                return new List<iItem>
                {
                     new SubtextBelowAndBesideItem(null, "An iItem that can display two subtexts", "Below the item", "To the right"),
                     new SubtextBelowAndBesideItem(null, "An iItem that can display two subtexts", null, "To the right"),
                     new SubtextBelowAndBesideItem(null, "An iItem that can display two subtexts", "Below the item", null),
                };
            }
        }

        private static IEnumerable<iItem> SubtextItems
        {
            get
            {
                return new List<iItem>
                {
                    new SubtextItem(null, "An iItem that can display subtext", LongSubtext),
                };
            }
        }

        private static IEnumerable<iItem> VariableContentItems
        {
            get
            {
                return new List<iItem>
                {
                    new VariableContentItem(null, "An iItem that can display variable content."),
                    new VariableContentItem(null, "An iItem that can display variable content with long subtext.", LongSubtext),
                    new VariableContentItem(null, "An iItem that can display variable content with long text and no subtext." + Filler),
                    new VariableContentItem(null, "An iItem that can display variable content with long text and subtext." + Filler, LongSubtext),
                };
            }
        }

        private static IEnumerable<iItem> iItems
        {
            get
            {
                return new List<iItem>
                {
                    new iItem((string)null, "This is an iItem."),
                    new iItem(null, "This is an iItem with some subtext.", "I'm longer than 7 characters!"),
                    new iItem(null, "Short subtext is different:", "1234567"),
                    new iItem(string.Empty, "This is an iItem that navigates somewhere."),
                    new iItem(string.Empty, "This is an iItem that navigates somewhere.", "It also has long subtext."),
                    new iItem(string.Empty, "This is an iItem that navigates somewhere.", "1234567"),
                    new iItem(string.Empty, "This is an iItem that navigates somewhere, with an alternate route.") { Button = Button, },
                    new iItem(string.Empty, "This is an iItem that navigates somewhere.", "With an alternate route, and subtext.") { Button = Button, },
                    new iItem(string.Empty, "This is an iItem that navigates somewhere.", "1234567") { Button = Button, },
                };
            }
        }
        #endregion
    }
}