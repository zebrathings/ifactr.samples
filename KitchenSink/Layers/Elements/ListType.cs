﻿using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    public class ListType : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "iList Types";
            Layout = App.Layout;
            Items.Add(new iMenu
            {
                Items =
                {
                    new iItem("Elements/Collections/iList", "iList"),
                    new iItem("Elements/Collections/Paged", "PagedList"),
                    new iItem("Elements/Collections/Search", "SearchList"),
                    new iItem("Elements/Collections/Custom", "Custom SearchList"),
                },
            });
        }

        public bool IsFullscreen { get; set; }
    }
}