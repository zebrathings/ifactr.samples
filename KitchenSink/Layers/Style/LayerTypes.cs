using System;
using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;

namespace KitchenSink
{
    [PreferredPane(Pane.Popover)]
    public class LayerTypes : Layer
    {
        public override void Load(string navigatedUri, Dictionary<string, string> parameters)
        {
            if (parameters.ContainsKey("Layout"))
            {
                App.Layout = (LayerLayout)Enum.Parse(typeof(LayerLayout), parameters["Layout"], true);
                CancelLoadAndNavigate("Style");
                return;
            }

            Title = "Layouts";
            Layout = App.Layout;

            var menu = new iMenu
            {
                new iItem("Style/Layout/Rounded", "Rounded"),
                new iItem("Style/Layout/EdgetoEdge", "Edge to edge"),
            };
            Items.Add(menu);
        }
    }
}