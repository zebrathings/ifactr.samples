﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using MonoCross.Utilities;
using iFactr.Core.Targets;
using iFactr.Core.Forms;
using iFactr.Core.Controls;

namespace KitchenSink
{
    public class LoadIndicator : Layer
    {
        public override void Load(string navigatedUri, Dictionary<string, string> parameters)
        {
            string loadValue = parameters.GetValueOrDefault("currentLID");
            var loadIndProp = Device.Reflector.GetProperty(typeof(TargetFactory), "DefaultLoadIndicatorDelay");

            if (loadValue != null)
            {
                loadIndProp.SetValue(TargetFactory.Instance, Convert.ToDouble(loadValue), null);
                CancelLoadAndNavigate("..");
                return;
            }
            Title = "Set Load Indicator Delay";

            var currentLoadIndDelay = (double)loadIndProp.GetValue(TargetFactory.Instance, null);

            var fieldset = new Fieldset();
            var currentLIDText = new TextField("currentLID")
            {
                Label = "Delay(ms)",
                Text = currentLoadIndDelay.ToString(),
            };

            fieldset.Add(currentLIDText);

            Items.Add(fieldset);

            var subButton = new Button() { Address = ".", };
            ActionButtons.Add(subButton);
        }
    }
}