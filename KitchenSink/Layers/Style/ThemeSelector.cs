﻿using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;
using MonoCross.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KitchenSink
{
    public class ThemeSelector : Layer
    {
        public override void Load(string navigatedUri, Dictionary<string, string> parameters)
        {
            Title = "Themes";

            string styleDir = Path.Combine(iApp.Factory.SessionDataPath, "Style");
            iApp.File.EnsureDirectoryExistsForFile(styleDir.AppendPath("fake.xml"));

            var styleResources = Path.Combine(iApp.Factory.ApplicationPath, "Style");
            var files = new List<string>(iApp.File.GetFileNames(styleDir));
            if (iApp.File.Exists(styleResources))
            {
                files.AddRange(iApp.File.GetFileNames(styleResources));
            }

            if (files.Any())
            {
                var names = files.Where(f => f.EndsWith(".xml"));
                Items.Add(new SearchList
                {
                    Items = new ItemsCollection<iItem>(names.Select(f => new iItem(new iFactr.UI.Link("Style/Select") { Parameters = { { "Name", f }, }, }, f.Remove(f.LastIndexOf('.')).Substring(f.LastIndexOf(Device.DirectorySeparatorChar) + 1)))),
                });
            }
            else
            {
                Items.Add(new iBlock
                {
                    Items =
                    {
                        new Label { Text = "Please save a theme in the style selector." },
                    },
                });
            }
        }
    }
}