﻿using System.Collections.Generic;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class StyleMenu : Layer
    {
        public override void Load(string navigatedUri, Dictionary<string, string> parameters)
        {
            Title = "Style";

            var styleMenu = new iMenu
            {
                new iItem("Style/Select", "Choose new colors"),
                new iItem("Style/Theme", "Load saved theme"),
                new iItem("Style/Layout", "Choose layout"),
                new iItem("Style/LoadIndicator", "Change Load Indicator Delay"),
            };
            Items.Add(styleMenu);
        }
    }
}