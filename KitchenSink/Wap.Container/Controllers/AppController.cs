using System.Web.Mvc;
using iFactr.Core;
using iFactr.Wap;

namespace KitchenSink.Wap.Controllers
{
    public class AppController : Controller
    {
        private bool loading = true;

        // GET: /Render/
        public void Render(string url, string args)
        {
            // set callback handler
            WapFactory.OnOutputLayerComplete += () => { this.OutputLayerComplete(); };

            // navigate to url
            url = url ?? string.Empty;
            iApp.Navigate(url, WapFactory.Instance.GetParameters(Request));

            // wait for web factory to output the layer
            while (loading)
                System.Threading.Thread.Sleep(100);
        }

        private void OutputLayerComplete()
        {
            loading = false;
        }
    }
}