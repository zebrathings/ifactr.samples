﻿using System.Collections.Generic;
using MonoCross.Navigation;

namespace KitchenSink.Controllers
{

    public class MessageController : MXController<string>
    {
        public const string MonoViewPerspective = "controlssample";
        public const string ClickViewPerspective = "clickme";
        public const string InternalBrowserPerspective = "Internal Browser";
        public const string ExternalBrowserPerspective = "External Browser";

        public const string SamplerTitle = "Controls Sampler";
        public const string ClickMeTitle = "ClickMe";

        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            string model, monoViewParm;

            if (!parameters.TryGetValue("message", out model))
            {
                model = "Hello world!";
            }
            if (parameters.TryGetValue("monoview", out monoViewParm))
            {
                Model = monoViewParm;
                switch (monoViewParm)
                {
                    case SamplerTitle:
                        return MonoViewPerspective;
                    case ClickMeTitle:
                        return ClickViewPerspective;
                    case InternalBrowserPerspective:
                    case ExternalBrowserPerspective:
                        return Model;
                    default:
                        return "Unknown Model-Title";
                }
            }

            Model = model;
            return ViewPerspective.Default;
        }
    }
}