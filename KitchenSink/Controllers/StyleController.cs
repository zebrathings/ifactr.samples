﻿using iFactr.Core;
using iFactr.Core.Styles;
using iFactr.UI;
using MonoCross.Navigation;
using MonoCross.Utilities;
using MonoCross.Utilities.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace KitchenSink
{
    public class StyleController : MXController<Style>
    {
        private static readonly ISerializer<Style> StyleSerializer = SerializerFactory.Create<Style>();

        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            if (parameters.Count > 0)
            {
                if (parameters.ContainsKey("Name"))
                {
                    iApp.Instance.Style = StyleSerializer.DeserializeObjectFromFile(parameters["Name"]);
                    iApp.Session["CurrentStyle"] = parameters["Name"];
                }
                else
                {
                    foreach (var parameter in parameters.Where(pair => pair.Key.EndsWith(".Key")))
                    {
                        iApp.Log.Info(parameter.Key + " " + parameter.Value);
                        GetColorProperties().First(prop => parameter.Key.StartsWith(prop.Name)).SetValue(iApp.Instance.Style, new Color(parameter.Value), null);
                    }
                }

                if (parameters.ContainsKey("Save"))
                {
                    var file = Path.Combine(iApp.Factory.SessionDataPath, "Style", parameters["filename"].Trim() + ".xml");
                    StyleSerializer.SerializeObjectToFile(iApp.Instance.Style, file);
                    iApp.Session["CurrentStyle"] = file;
                }
                else if (!parameters.ContainsKey("Name"))
                {
                    iApp.Session["CurrentStyle"] = string.Empty;
                }
                iApp.Navigate(string.Empty);
                return null;
            }

            Model = iApp.Instance.Style.Clone() as Style;
            return ViewPerspective.Default;
        }

        private static PropertyInfo[] _props;
        public static IEnumerable<PropertyInfo> GetColorProperties()
        {
            return _props ?? (_props = Device.Reflector.GetProperties(typeof(Style)).Where(prop => Device.Reflector.IsAssignableFrom(prop.PropertyType, typeof(Color))).ToArray());
        }

        private static KeyValuePair<string, string>[] _colors;
        public static IEnumerable<KeyValuePair<string, string>> GetColors(Style style)
        {
            return _colors ?? (_colors = Device.Reflector.GetProperties(typeof(Color))
                .Where(prop => Device.Reflector.IsAssignableFrom(prop.PropertyType, typeof(Color)))
                .Select(c => new KeyValuePair<string, string>("#" + ((Color)c.GetValue(style.TextColor, null)).GetHashCode().ToString("x8").ToUpper(), c.Name))
                .OrderBy(a => a.Value).ToArray());
        }
    }
}