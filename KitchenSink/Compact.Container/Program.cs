using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.Core.Forms;
using System.Drawing;
using MonoCross.Navigation;

namespace KitchenSink
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            iApp.OnLayerLoadComplete += ilayer =>
            {
                CompactFactory.Instance.OutputLayer(ilayer);
            };

            CompactFactory.Initialize();
            CompactFactory.TheApp = new App();
            MXContainer.AddView<string>(typeof(Views.MessageView));
            CompactFactory.Instance.CustomItemRequested = (item, style) =>
            {
                if (item is CustomNumericField)
                {
                    var field = (CustomNumericField)item;
                    var control = new TextBoxWithPrompt
                    {
                        Text = field.Text,
                        Tag = field,
                        ForeColor = Color.Crimson,
                        Placeholder = field.Label,
                    };
                    control.TextChanged += (o, e) =>
                    {
                        var sender = (TextBox)o;
                        ((Field)sender.Tag).Text = sender.Text;
                    };
                    return control;
                }
                return null;
            };

            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.RootForm);
        }
    }
}