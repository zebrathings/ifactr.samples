﻿using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.UI;
using MonoCross.Navigation;

namespace KitchenSink.Views
{
    partial class MessageView 
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblMessage.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblMessage.Location = new System.Drawing.Point(3, 15);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(227, 199);
            this.lblMessage.Text = "Hello world!";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MessageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblMessage);
            this.Name = "MessageView";
            this.Size = new System.Drawing.Size(233, 214);
            this.ResumeLayout(false);

        }

        #endregion

        #region IMXView<string> Members

        public string Model { get; set; }

        #endregion

        #region IMXView Members

        public System.Type ModelType
        {
            get { return typeof(string); }
        }

        public object GetModel()
        {
            return Model;
        }

        public void SetModel(object model)
        {
            Model = model.ToString();
        }

        public void Render()
        {
            lblMessage.Text = Model;
        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
    }
}