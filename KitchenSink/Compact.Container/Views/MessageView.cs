﻿using System;
using System.Linq;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.UI;
using MonoCross.Navigation;

namespace KitchenSink.Views
{
    public partial class MessageView : UserControl, IMXView<string>, IHistoryEntry
    {
        public MessageView()
        {
            InitializeComponent();
        }

        public event EventHandler Activated;
        public event EventHandler Deactivated;

        public Link BackLink { get; set; }

        public string StackID { get; set; }

        public Pane OutputPane { get; set; }

        public PopoverPresentationStyle PopoverPresentationStyle { get; set; }

        public ShouldNavigateDelegate ShouldNavigate { get; set; }

        public IHistoryStack Stack
        {
            get
            {
                return PaneManager.Instance.FirstOrDefault(s => s.CurrentView == this);
            }
        }
    }
}