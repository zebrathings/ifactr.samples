using System.Windows;
using System.Windows.Media;
using KitchenSink;
using KitchenSink.Windows.Container.Views;
using MonoCross.Navigation;
using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Wpf;
using System.ComponentModel;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            iApp.OnLayerLoadComplete += layer => { WpfFactory.Instance.OutputLayer(layer); };
            WpfFactory.Initialize();
            WpfFactory.TheApp = new KitchenSink.App();
            InitializeViews();
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
            Content = WpfFactory.Instance.MainWindow;

            //WpfFactory.Instance.CustomItemRequested = (item, ilayer) =>
            //{
            //    if (item is CustomNumericField)
            //    {
            //        //var control = (RadMaskedTextBox)((NumericField)item).GetControl(ilayer.LayerStyle);
            //        //control.Foreground = new SolidColorBrush(Colors.Crimson);
            //        //control.ValueChanged += (o, e) =>
            //        //{
            //        //    var text = (RadMaskedTextBox)o;
            //        //    ((Field)text.Tag).Text = text.MaskedText;
            //        //};
            //        //return control;
            //    }
            //    return null;
            //};
        }

        public void InitializeViews()
        {
            MXContainer.AddView<string>(new MessageView(), ViewPerspective.Default);
        }
        /// <summary>
        /// Prevent accidental closing by offering a pop-up confirmation message when user closes window
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            if (iApp.CurrentNavContext.ActiveLayer != null
                && !iApp.CurrentNavContext.ActiveLayer.Name.Contains("Login"))
                e.Cancel = MessageBox.Show("Do you want to exit?", null, MessageBoxButton.YesNo) == MessageBoxResult.No;
            base.OnClosing(e);
        }
    }
}