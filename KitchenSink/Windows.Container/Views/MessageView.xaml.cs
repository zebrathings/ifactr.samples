﻿using System;
using System.Windows.Controls;
using MonoCross.Navigation;

namespace KitchenSink.Windows.Container.Views
{
    /// <summary>
    /// Interaction logic for MessageView.xaml
    /// </summary>
    public partial class MessageView : UserControl, IMXView<string>
    {
        public MessageView()
        {
            InitializeComponent();
        }

        public Type ModelType { get { return typeof(string); } }

        public object GetModel()
        {
            return Model;
        }

        public void SetModel(object model)
        {
            Model = model as string;
        }

        public void Render()
        {
            ModelTextBlock.Text = Model;
        }

        public string Model { get; set; }
    }
}