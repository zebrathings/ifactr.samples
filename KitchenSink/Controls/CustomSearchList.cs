﻿using System.Linq;
using iFactr.Core.Layers;

namespace KitchenSink
{
    internal class CustomSearchList : SearchList
    {
        public CustomSearchList()
        {
            SearchText = "Two hands clap and there is a sound. What is the sound of one hand?";
            AutoFocus = true;
        }

        public override void PerformSearch(string navigationUri, string searchTerm)
        {
            Items = searchTerm.Split(' ').Where(term => !string.IsNullOrEmpty(term)).Select(term => new iItem(string.Empty, term)).ToList();
        }
    }
}