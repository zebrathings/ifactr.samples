using iFactr.Core.Layers;

namespace KitchenSink
{
    public class CustomControl : iItem, ICustomItem
    {
        public string[] ButtonEndPoints;

        public string Title;
        public string Description;
        public string Status;
        public string Priority;
        public string Type;
    }
}