using iFactr.Core.Forms;
using iFactr.Core.Layers;

namespace KitchenSink
{
    public class CustomNumericField : NumericField, ICustomItem
    {
        public CustomNumericField() { }

        public CustomNumericField(string id)
            : base(id)
        { }
    }
}