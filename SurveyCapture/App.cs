using iFactr.Core;
using iFactr.Core.Styles;

namespace SurveyCapture
{
    public class App : iApp
    {
        public override void OnAppLoad()
        {
            // Set the application title
            Title = "Surveys";

            // Set the application style
            Style.HeaderColor = Style.Color.Black;

            // Add navigation mappings
            NavigationMap.Add("Surveys", new SurveyList());
            NavigationMap.Add("Surveys/{Survey}/List", new SurveyList());
            NavigationMap.Add("Surveys/{Survey}", new Survey());

            // Set default navigation URI
            NavigateOnLoad = "Surveys";
        }
    }
}