using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.WinPhone;
using Microsoft.Phone.Controls;

namespace WinPhone.Container
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WinPhoneFactory.Instance.OutputLayer(layer); };
            WinPhoneFactory.Initialize();
            WinPhoneFactory.TheApp = new SurveyCapture.App();
            iApp.Navigate(WinPhoneFactory.TheApp.NavigateOnLoad);
        }
    }
}
