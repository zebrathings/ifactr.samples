﻿using System.Windows;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Hide();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };
            WpfFactory.Initialize();
            WpfFactory.TheApp = new SurveyCapture.App();
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
            Content = WpfFactory.Instance.MainWindow;
            WpfFactory.Instance.MainWindow.IsEnabledChanged += (o, e) =>
            {
                if ((bool)e.NewValue) this.Show();
                else this.Hide();
            };
        }
    }
}