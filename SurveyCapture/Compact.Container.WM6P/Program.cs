﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Compact;

namespace SurveyCapture 
{
    static class Program 
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main() 
        {
            iApp.OnLayerLoadComplete += (iLayer layer) => { CompactFactory.Instance.OutputLayer(layer); };

            //TODO: Instantiate your iFactr application and set the Factory App property
            CompactFactory.TheApp = new SurveyCapture.App();

            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.Form);

        }
    }
}
