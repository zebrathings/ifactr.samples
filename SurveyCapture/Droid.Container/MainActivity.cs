using Android.App;
using Android.OS;
using Android.Views;
using iFactr.Core;
using iFactr.Droid;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            if (DroidFactory.Instance.MainActivity == null)
            {
                DroidFactory.Instance.MainActivity = this;
                DroidFactory.TheApp = new SurveyCapture.App();
                iApp.OnLayerLoadComplete += DroidFactory.Instance.OutputLayer;
            }
            else DroidFactory.Instance.MainActivity = this;
            base.OnCreate(bundle);
        }
    }
}