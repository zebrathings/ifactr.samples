
using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Touch;

namespace BestSellers
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			TouchFactory.Initialize();
			iApp.OnLayerLoadComplete += (iLayer layer) => { InvokeOnMainThread(delegate { TouchFactory.Instance.OutputLayer(layer); } );  };
			TouchFactory.TheApp = new SurveyCapture.App();
			iApp.Navigate(TouchFactory.TheApp.NavigateOnLoad);
			TouchFactory.InitializeViews();
			
			return true;
		}

		public override void OnActivated (UIApplication application)
		{
		}
	}
}
