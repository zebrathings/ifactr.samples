using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.Data;

namespace SurveyCapture
{
    public class SurveyList : iLayer, IMasterLayer
    {
        private RestfulQueue<Dictionary<string, string>> _queue;

        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Surveys";

            Items.Add(new iMenu
            {
                Header = "Proposal Requests:",
                Items =
                {
                    new iItem("Surveys/Business Intelligence", "Business Intelligence"),
                    new iItem("Surveys/CRM System", "CRM System"),
                    new iItem("Surveys/ERP System", "ERP System"),
                    new iItem("Surveys/eCommerce Web Site", "eCommerce Web Site "),
                    new iItem("Surveys/Marketing Web Site", "Marketing Web Site "),
                }
            });

            if (parameters.ContainsKey("Survey"))
            {
                _queue = new RestfulQueue<Dictionary<string, string>>("http://www.postbin.org/o3c5ut", string.Empty);
                _queue.Enqueue(new RestfulObject<Dictionary<string, string>>(parameters, "http://www.postbin.org/o3c5ut"));
            }
        }
    }
}