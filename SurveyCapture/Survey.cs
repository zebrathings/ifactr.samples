using System;
using System.Collections.Generic;
using System.Globalization;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;

namespace SurveyCapture
{
    public class Survey : FormLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = parameters["Survey"];

            Fieldsets.Add(new Fieldset
            {
                Header = "Project Info",
                Fields =
                {
                    new LabelField("Project.Date") { Label = "Date", Text = DateTime.Now.ToString(CultureInfo.InvariantCulture), },
                    new SelectListField("Project.Timeframe")
                    { 
                        Label = "Timeframe",
                        Items = new List<SelectListFieldItem>
                        {
                            new SelectListFieldItem("Immediately"),
                            new SelectListFieldItem("30-60 Days"),
                            new SelectListFieldItem("60-90 Days"),
                            new SelectListFieldItem("90-120 Days"),
                            new SelectListFieldItem("120+ Days"),
                            new SelectListFieldItem("Not Decided"),
                        },
                    },
                    new BoolField("Project.BudgetApproved") { Label = "Budget Approved" },
                    new TextField("Project.Sponsor") { Placeholder = "Sponsor" },
                },
            });

            Fieldsets.Add(new Fieldset
            {
                Header = "Technology Details",
                Fields =
                {
                    new SelectListField("Technology.Language")
                    {
                        Label = "Language",
                        Items = new List<SelectListFieldItem>
                        {
                            new SelectListFieldItem("C# .NET"),
                            new SelectListFieldItem("COBOL"),
                            new SelectListFieldItem("Java/J2EE"),
                            new SelectListFieldItem("Ruby on Rails"),
                            new SelectListFieldItem("Other"),
                        },
                    },
                    new SelectListField("Technology.OperatingSystem")
                    {
                        Label = "Server OS",
                        Items = new List<SelectListFieldItem>
                        {
                            new SelectListFieldItem("Linux"),
                            new SelectListFieldItem("Mac OS X"),
                            new SelectListFieldItem("Unix"),
                            new SelectListFieldItem("Windows Server 2003"),
                            new SelectListFieldItem("Windows Server 2008"),
                            new SelectListFieldItem("Not Decided"),
                            new SelectListFieldItem("Other"),
                        },
                    },
                    new SelectListField("Technology.Database")
                    {
                        Label = "Database",
                        Items = new List<SelectListFieldItem>
                        {
                            new SelectListFieldItem("MySQL"),
                            new SelectListFieldItem("Oracle"),
                            new SelectListFieldItem("Sybase"),
                            new SelectListFieldItem("SQLite"),
                            new SelectListFieldItem("SQL Server"),
                            new SelectListFieldItem("Ultralite"),
                            new SelectListFieldItem("None"),
                            new SelectListFieldItem("Not Decided"),
                            new SelectListFieldItem("Other"),
                        },
                    },
                    new BoolField("Technology.TrunkStock") { Label = "Trunk Stock:", },
                }
            });

            Fieldsets.Add(new Fieldset
            {
                Header = "Project Management",
                Fields =
                {
                    new BoolField("Management.Iterative") { Label = "Iterative Style:", },
                    new BoolField("Management.Agile") { Label = "Agile Process:", },
                    new BoolField("Management.Scrum") { Label = "Daily Scrums:", },
                },
            });

            Fieldsets.Add(new Fieldset
            {
                Header = "Signature",
                Fields = { new DrawingField("Signature", null, DrawingType.Signature, null, false), },
            });

            ActionButton = new SubmitButton("Save", "Surveys" + "/" + Title + "/List") { ConfirmationText = "Save Survey?", };
            ActionParameters = parameters;
        }
    }
}