//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UIBuilder.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UIBuilder.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        internal static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Signing In....
        /// </summary>
        internal static string AuthLabel {
            get {
                return ResourceManager.GetString("AuthLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        internal static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose A Photo.
        /// </summary>
        internal static string ChoosePhoto {
            get {
                return ResourceManager.GetString("ChoosePhoto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose A Video.
        /// </summary>
        internal static string ChooseVideo {
            get {
                return ResourceManager.GetString("ChooseVideo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear.
        /// </summary>
        internal static string Clear {
            get {
                return ResourceManager.GetString("Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to clear the drawing?.
        /// </summary>
        internal static string ClearConfirm {
            get {
                return ResourceManager.GetString("ClearConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear?.
        /// </summary>
        internal static string ClearConfirmTitle {
            get {
                return ResourceManager.GetString("ClearConfirmTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear search.
        /// </summary>
        internal static string ClearTerm {
            get {
                return ResourceManager.GetString("ClearTerm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        internal static string Close {
            get {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        internal static string ConfirmTitle {
            get {
                return ResourceManager.GetString("ConfirmTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authentication Failed.
        /// </summary>
        internal static string DefaultError {
            get {
                return ResourceManager.GetString("DefaultError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dismiss.
        /// </summary>
        internal static string Dismiss {
            get {
                return ResourceManager.GetString("Dismiss", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        internal static string Done {
            get {
                return ResourceManager.GetString("Done", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        internal static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter search term.
        /// </summary>
        internal static string EnterTerm {
            get {
                return ResourceManager.GetString("EnterTerm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred. Please try again..
        /// </summary>
        internal static string ErrorText {
            get {
                return ResourceManager.GetString("ErrorText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The next screen doesn&apos;t exist, or isn&apos;t viewable on your device..
        /// </summary>
        internal static string FailedNavigation {
            get {
                return ResourceManager.GetString("FailedNavigation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go Back.
        /// </summary>
        internal static string GoBack {
            get {
                return ResourceManager.GetString("GoBack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go Forward.
        /// </summary>
        internal static string GoForward {
            get {
                return ResourceManager.GetString("GoForward", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid file.
        /// </summary>
        internal static string InvalidFile {
            get {
                return ResourceManager.GetString("InvalidFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Launch in Safari.
        /// </summary>
        internal static string LaunchSafari {
            get {
                return ResourceManager.GetString("LaunchSafari", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading....
        /// </summary>
        internal static string Loading {
            get {
                return ResourceManager.GetString("Loading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        internal static string LoginAction {
            get {
                return ResourceManager.GetString("LoginAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Canceling your login will close this application, and any unsaved work will be lost. Do you want to quit?.
        /// </summary>
        internal static string LoginCancel {
            get {
                return ResourceManager.GetString("LoginCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        internal static string LoginTitle {
            get {
                return ResourceManager.GetString("LoginTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menu.
        /// </summary>
        internal static string Menu {
            get {
                return ResourceManager.GetString("Menu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More.
        /// </summary>
        internal static string More {
            get {
                return ResourceManager.GetString("More", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next page.
        /// </summary>
        internal static string Next {
            get {
                return ResourceManager.GetString("Next", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        internal static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service unavailable..
        /// </summary>
        internal static string NoService {
            get {
                return ResourceManager.GetString("NoService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page {0} of {1}.
        /// </summary>
        internal static string Pager {
            get {
                return ResourceManager.GetString("Pager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password:.
        /// </summary>
        internal static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to take a photo or choose one from the gallery?.
        /// </summary>
        internal static string PhotoChoose {
            get {
                return ResourceManager.GetString("PhotoChoose", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Previous page.
        /// </summary>
        internal static string Previous {
            get {
                return ResourceManager.GetString("Previous", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Printing is currently unavailable.
        /// </summary>
        internal static string PrintError {
            get {
                return ResourceManager.GetString("PrintError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to Print.
        /// </summary>
        internal static string PrintErrorTitle {
            get {
                return ResourceManager.GetString("PrintErrorTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your document is being printed..
        /// </summary>
        internal static string Printing {
            get {
                return ResourceManager.GetString("Printing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Printing in Progress.
        /// </summary>
        internal static string PrintingTitle {
            get {
                return ResourceManager.GetString("PrintingTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot print {0}.
        /// </summary>
        internal static string PrintUrlError {
            get {
                return ResourceManager.GetString("PrintUrlError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to quit?.
        /// </summary>
        internal static string Quit {
            get {
                return ResourceManager.GetString("Quit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quit.
        /// </summary>
        internal static string QuitTitle {
            get {
                return ResourceManager.GetString("QuitTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start Recording.
        /// </summary>
        internal static string RecordAudio {
            get {
                return ResourceManager.GetString("RecordAudio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recording....
        /// </summary>
        internal static string Recording {
            get {
                return ResourceManager.GetString("Recording", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record A Video.
        /// </summary>
        internal static string RecordVideo {
            get {
                return ResourceManager.GetString("RecordVideo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refresh.
        /// </summary>
        internal static string Refresh {
            get {
                return ResourceManager.GetString("Refresh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to open this URL in Safari?.
        /// </summary>
        internal static string SafariConfirm {
            get {
                return ResourceManager.GetString("SafariConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot open the current page in Safari..
        /// </summary>
        internal static string SafariError {
            get {
                return ResourceManager.GetString("SafariError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving video....
        /// </summary>
        internal static string SaveVideo {
            get {
                return ResourceManager.GetString("SaveVideo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The URL scheme ‘{0}’ is not supported on this device..
        /// </summary>
        internal static string SchemeNotSupported {
            get {
                return ResourceManager.GetString("SchemeNotSupported", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        internal static string SearchHint {
            get {
                return ResourceManager.GetString("SearchHint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stop Recording.
        /// </summary>
        internal static string StopRecording {
            get {
                return ResourceManager.GetString("StopRecording", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        internal static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Take A Photo.
        /// </summary>
        internal static string TakePhoto {
            get {
                return ResourceManager.GetString("TakePhoto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username:.
        /// </summary>
        internal static string Username {
            get {
                return ResourceManager.GetString("Username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please correct the errors indicated in red..
        /// </summary>
        internal static string ValidationFailure {
            get {
                return ResourceManager.GetString("ValidationFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to make a new video or choose one from the gallery?.
        /// </summary>
        internal static string VideoChoose {
            get {
                return ResourceManager.GetString("VideoChoose", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There was a problem saving the video. Please check your disk usage..
        /// </summary>
        internal static string VideoError {
            get {
                return ResourceManager.GetString("VideoError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        internal static string Yes {
            get {
                return ResourceManager.GetString("Yes", resourceCulture);
            }
        }
    }
}
