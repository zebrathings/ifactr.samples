using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using UIBuilder.Data;

namespace UIBuilder
{
    public class EditLayerItem : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Name = Title = parameters["Type"];
            string id = parameters["ID"];
            int index = -1;
            string propName = null;
            if (parameters.ContainsKey("Index") && !int.TryParse(parameters["Index"], out index))
            {
                index = -1;
                propName = parameters["Index"];
            }
            var layer = LayerStore.Instance.FirstOrDefault(l => l.Name == id);
            var props = new Fieldset { Header = Title + " Properties", };
            Type type = Assembly.Load(props.GetType().AssemblyQualifiedName).GetTypes().First(t => t.Name == parameters["Type"]);
            var layerProps = type.GetProperties();

            if (iApp.Session.Keys.Count > 8 && NavContext.NavigatedUrl.Split('/').Length > 4 || iApp.Session.Keys.Count > 7 && NavContext.NavigatedUrl.Split('/').Length < 5)
            {
                var key = iApp.Session.Keys.Last();
                var item = iApp.Session[key];
                var parent = iApp.Session[Name];

                if (item is iItem)
                {
                    var itemProperties = item.GetType().GetProperties();
                    foreach (var itemProperty in itemProperties)
                    {
                        if (parameters.ContainsKey(itemProperty.Name) && itemProperty.PropertyType == typeof(string))
                            itemProperty.SetValue(item, parameters[itemProperty.Name], null);
                    }
                    ((iCollection)parent).Items.Add(item);
                }
                else if (item is SelectListFieldItem)
                {
                    var itemProperties = item.GetType().GetProperties();
                    foreach (var itemProperty in itemProperties.Where(itemProperty => parameters.ContainsKey(itemProperty.Name) && itemProperty.PropertyType == typeof(string)))
                    {
                        itemProperty.SetValue(item, parameters[itemProperty.Name], null);
                    }
                    ((SelectListField)parent).Items.Add((SelectListFieldItem)item);
                }
                else if (item is Field)
                {
                    var itemProperties = item.GetType().GetProperties();
                    foreach (var itemProperty in itemProperties.Where(itemProperty => parameters.ContainsKey(itemProperty.Name) && itemProperty.PropertyType == typeof(string)))
                    {
                        itemProperty.SetValue(item, parameters[itemProperty.Name], null);
                    }
                    var field = (Field)item;
                    field.ID = Guid.NewGuid().ToString();
                    ((Fieldset)parent).Fields.Add(field);
                }
                iApp.Session.Remove(key);
            }

            object source = iApp.Session.ContainsKey(Name) ? iApp.Session[Name] : iApp.Session[Name] = index == -1 ? Activator.CreateInstance(type) : (propName == null ? (parameters["Type"].EndsWith("Button") ? (object)layer.ActionButtons[index] : layer.Items[index]) : type.GetProperty(propName).GetValue(layer, null));

            foreach (var parameter in parameters)
            {
                var prop = layerProps.FirstOrDefault(p => p.Name == parameter.Key);
                if (prop == null) continue;
                if (prop.PropertyType == typeof(string))
                    prop.SetValue(source, parameter.Value, null);
            }


            foreach (var field in
                layerProps.Where(l => l.GetSetMethod() != null && !AbstractMap.Blacklist.Contains(l.Name) && !Attribute.IsDefined(l, typeof(ObsoleteAttribute)))
                .Select(propertyInfo => this.GetPropertyField(propertyInfo.Name, source)).Where(field => field != null))
            {
                props.Add(field);
            }

            Items.Insert(0, props);

            ActionParameters["Type"] = Title;
            ActionParameters["Index"] = parameters["Index"];

            const string submit = "../..";
            ActionButtons.Add(new SubmitButton(submit));

            if (index < 0) return;
            ActionButtons.Add(new SubmitButton("Delete", submit) { Parameters = new Dictionary<string, string> { { "Command", "Delete" }, }, });
        }
    }
}