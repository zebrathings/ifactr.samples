using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using iFactr.Touch;
using iFactr.Core;

namespace UIBuilder
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
            TouchFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => { InvokeOnMainThread(() => TouchFactory.Instance.OutputLayer(layer)); };
            TouchFactory.TheApp = new UIBuilder.App();
            ((App)TouchFactory.TheApp).SetFormFactor(NSUserDefaults.StandardUserDefaults.BoolForKey("formFactor") ? FormFactor.Accordion : FormFactor.SplitView);
            iApp.Navigate(App.Instance.NavigateOnLoad);

            TouchFactory.Instance.CustomItemRequested += (item, cell, tableView) =>
            {
                return GetCells.GetCustomField((CustomNumericField)item, cell);
            };

            TouchFactory.Instance.CustomItemHeightRequested += (item, tableView) =>
            {
                return 44;
            };

			return true;
		}

		public override void OnActivated (UIApplication application)
		{
		}
	}

    public static class GetCells
    {
        // To utilize custom items, the developer must have a basic knowledge of native controls.
        // The TouchFactory will give us the CustomItem it's creating a cell for.
        // It will also give us the cell that is going to be used to display our item.
        public static UITableViewCell GetCustomField(CustomNumericField item, UITableViewCell cell)
        {
            // In this example, we have subclassed a NumericField, so our cell is already set up for that kind of field.
            // The text field instance for NumericFields is housed within the cell with a tag of 1.  Knowing that, retrieval of the text field is easy.
            UITextField field = cell.ViewWithTag(1) as UITextField;
            if (field != null && TouchFactory.Instance.Platform == MobilePlatform.iPhone)
                // Now that we have access to the control we're looking for, changing its properties is a trivial matter.
                field.KeyboardType = UIKeyboardType.DecimalPad;

            return cell;
        }
    }
}
