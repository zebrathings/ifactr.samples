using iFactr.Core;
using iFactr.Console;
using iFactr.Core.Targets;

/// <summary>
/// This namespace contains the Console container for the UIBuilder sample
/// </summary>
namespace Console.Container
{
    /// <summary>
    /// This is the Console container for the UIBuilder Sample
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleFactory.Initialize();
            iApp.OnLayerLoadComplete += ConsoleFactory.Instance.OutputLayer;
            TargetFactory.TheApp = new UIBuilder.App();
            iApp.Navigate(TargetFactory.TheApp.NavigateOnLoad);
        }
    }
}