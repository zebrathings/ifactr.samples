using iFactr.Core;
using iFactr.Utilities.Serialization;
using UIBuilder.Data;

namespace UIBuilder
{
    public class App : iApp
    {
        /// <summary>
        /// Called when the application instance is loaded. This event is meant to be overridden in consuming applications
        /// for application-level initialization code.
        /// </summary>
        public App()
        {
            Title = "UI Builder";

            NavigationMap.Add(string.Empty, new Tabs());

            // Add navigation mappings
            NavigationMap.Add("Layers", new Layers());
            NavigationMap.Add("Layers/{ID}", new EditLayer());

            NavigationMap.Add("Layers/{ID}/iLayerItem", new AddLayerItem());
            NavigationMap.Add("Layers/{ID}/Reorder/{Index}", new OrderLayerItems());

            NavigationMap.Add("Layers/{ID}/{Type}/{Index}", new EditLayerItem());
            NavigationMap.Add("Layers/{ID}/{Type}/{Index}/Reorder/{Index}", new OrderLayerItems());
            NavigationMap.Add("Layers/{ID}/{Type}/{Index}/{ParentType}", new AddLayerItem()); //subtype chooser
            NavigationMap.Add("Layers/{ID}/{Type}/{Index}/{Type}/{Index}", new EditLayerItem()); //subtype editor
            NavigationMap.Add("Layers/{ID}/{Type}/{Index}/{Type}/{Index}/{Type}/{Index}", new EditLayerItem()); //selectlistfield editor

            NavigationMap.Add("App", new AppProperties());

            NavigationMap.Add("Images", new ImageManager());

            // Set default navigation URI
            NavigateOnLoad = string.Empty;
        }
    }
}