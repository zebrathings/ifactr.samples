using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using UIBuilder.Data;

namespace UIBuilder
{
    public class OrderLayerItems : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            var id = parameters["ID"];
            int index = int.Parse(parameters.ContainsKey("Index") ? parameters["Index"] : "-1");

            var parent = parameters.ContainsKey("Type") ? iApp.Session[parameters["Type"]] : LayerStore.Instance.FirstOrDefault(l => l.Name == id);
            var command = parameters.ContainsKey("Command") ? parameters["Command"] : null;
            var parentType = parent.GetType();

            var collection = (IList)parentType.GetProperty("Items").GetValue(parent, null);

            if (index > -1)
            {
                var item = collection[index];
                if (command == null)
                {
                    collection.Remove(item);
                    collection.Insert(index - 1, item);
                }
                else if (command == "Duplicate")
                {
                    collection.Insert(index, item is iLayerItem ? ((iLayerItem)item).Clone() :
                        item is Field ? ((Field)item).Clone() : ((iItem)item).Clone());
                }
            }

            Title = "Reorder Items";

            iLayerItem items = AbstractMap.CreateReorder(parentType.GetProperty("Items"), collection.Cast<object>().ToDictionary(item => collection.IndexOf(item).ToString(CultureInfo.InvariantCulture)));
            items.Header = "Select an item to move it up";
            Items.Add(items);
            ActionButtons.Add(new SubmitButton("Done", "../.."));
        }
    }
}