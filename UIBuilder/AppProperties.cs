using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using UIBuilder.Data;

namespace UIBuilder
{
    public class AppProperties : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "App";
            var props = new Fieldset { Header = "Layer Properties", };
            var layerProps = typeof(App).GetProperties();
            foreach (var field in layerProps.Where(l => l.Name != "ID" && !Attribute.IsDefined(l, typeof(ObsoleteAttribute))).Select(propertyInfo => this.GetPropertyField(propertyInfo.Name, iApp.Instance)).Where(field => field != null))
            {
                props.Fields.Add(field);
            }

            Items.Insert(0, props);

            ActionButtons.Add(new SubmitButton(NavContext.NavigatedUrl));
        }
    }
}