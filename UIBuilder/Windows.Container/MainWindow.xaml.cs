using System.Windows;
using iFactr.Core;
using iFactr.Wpf;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            iApp.OnLayerLoadComplete += layer => { WpfFactory.Instance.OutputLayer(layer); };
            WpfFactory.Initialize();
            WpfFactory.TheApp = new UIBuilder.App();
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
            Content = WpfFactory.Instance.MainWindow;
        }
    }
}