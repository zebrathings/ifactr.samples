﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using iFactr.Core.Layers;

namespace UIBuilder
{
    internal class AddLayerItem : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Add Item";
            var menu = new iMenu {Header = "Choose an item"};
            Items.Add(menu);
            string assemblyQualifiedName = menu.GetType().AssemblyQualifiedName ?? Assembly.GetCallingAssembly().FullName;
            Type[] types = Assembly.Load(assemblyQualifiedName).GetTypes();
            Type fieldType = parameters.ContainsKey("ParentType")
                ? types.First(type => type.Name == parameters["ParentType"])
                : typeof (iLayerItem);
            Name = fieldType.Name;
            menu.Items.AddRange(types.Where(type =>
                !type.IsAbstract && type != typeof (Tab) &&
                (fieldType.IsInterface
                    ? type.GetInterfaces().FirstOrDefault(f => f.Name == fieldType.Name) == fieldType
                    : fieldType.IsAssignableFrom(type)))
                .Select(type => new iItem(string.Format("../{0}/-1", type.Name), type.Name)));
        }
    }
}