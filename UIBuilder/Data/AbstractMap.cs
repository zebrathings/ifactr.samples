﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using iFactr.UI;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Core.Styles;

namespace UIBuilder.Data
{
    static class AbstractMap
    {
        public static Field GetPropertyField(this iLayer host, string name, object origin)
        {
            if (Blacklist.Contains(name)) return null;

            var prop = origin.GetType().GetProperties().FirstOrDefault(l => l.Name == name);
            FieldInfo field = null;
            Type item = null;
            if (prop == null)
            {
                field = origin.GetType().GetField(name);
                if (field != null)
                    item = field.FieldType;
            }
            else
            {
                item = prop.PropertyType;
            }

            if (item == null
                || item == typeof(DynamicImage)
                || item == typeof(Transition)
                || item == typeof(TransitionDirection)
                || item == typeof(Dictionary<string, ValidationInfo>)
                || item == typeof(iLayer.NavigationContext)
                || item == typeof(Shortcuts)
                || item == typeof(Dictionary<string, string>))
            {
                return null;
            }

            if (item.IsEnum)
            {
                var types = Enum.GetNames(item);
                return new SelectListField(name, types) { Label = name, Text = prop.GetValue(origin, null).ToString(), };
            }

            if (item == typeof(Color))
            {
                var colors = typeof(Color).GetProperties(BindingFlags.Public | BindingFlags.Static).Where(props => props.PropertyType.IsAssignableFrom(typeof(Color))).Select(c => new KeyValuePair<string, string>(((Style.Color)c.GetValue(new Style().TextColor, null)).GetHashCode().ToString(CultureInfo.InvariantCulture), c.Name)).OrderBy(a => a.Value).ToArray();
                var defaults = new List<KeyValuePair<string, string>>(colors);
                var color = (Color)prop.GetValue(origin, null);
                defaults.Insert(0, new KeyValuePair<string, string>(color.GetHashCode().ToString(CultureInfo.InvariantCulture), color.HexCode));
                return new SelectListField(name, defaults) { Label = name, };
            }

            if (item.IsValueType && !item.IsPrimitive)
            {
                host.Items.Insert(0, host.CreateSelector(prop, prop.GetValue(origin, null)));
                return null;
            }

            switch (item.Name)
            {
                case "ItemsCollection`1":
                case "List`1":
                    switch (item.GetGenericArguments()[0].Name)
                    {
                        case "iItem":
                            var items = ((IList<iItem>)prop.GetValue(origin, null));
                            host.Items.Insert(0, CreateCollector(prop, items.ToDictionary(it => items.IndexOf(it).ToString(CultureInfo.InvariantCulture)), null));
                            break;
                        case "Button":
                            var buttons = ((IList<Link>)prop.GetValue(origin, null));
                            host.Items.Insert(0, CreateCollector(prop, buttons.ToDictionary(it => buttons.IndexOf(it).ToString(CultureInfo.InvariantCulture)), "-1"));
                            break;
                        case "iLayerItem":
                            var i = ((IList<iLayerItem>)prop.GetValue(origin, null));
                            host.Items.Insert(0, CreateCollector(prop, i.ToDictionary(it => i.IndexOf(it).ToString(CultureInfo.InvariantCulture)), null));
                            break;
                        case "Field":
                            host.Items.Insert(0, CreateCollector(prop, ((IList<Field>)prop.GetValue(origin, null)).ToDictionary(k => Guid.NewGuid().ToString()), null));
                            break;
                        case "SelectListFieldItem":
                            host.Items.Insert(0, CreateCollector(prop, ((IList<SelectListFieldItem>)prop.GetValue(origin, null)).ToDictionary(k => k.Key), "-1"));
                            break;
                    }
                    return null;
                case "Icon":
                case "Style":
                case "LabelStyle":
                    host.Items.Insert(0, host.CreateSelector(prop, prop.GetValue(origin, null)));
                    return null;
                case "Button":
                    var link = (Link)prop.GetValue(origin, null);
                    return new ButtonField(name, "/" + item.Name + "/" + prop.Name) { Text = link == null ? (name + ": null") : (name + ": " + link.Text), Action = iFactr.Core.Controls.Button.ActionType.Submit, };
                case "String":
                    if (name.EndsWith("Image") || name.StartsWith("Image"))
                        return new SelectListField(name) { Label = name, Text = (prop.GetValue(origin, null) ?? "null").ToString(), };
                    return new TextField(name) { Label = name, Text = (prop.GetValue(origin, null) ?? string.Empty).ToString(), Placeholder = prop.GetValue(origin, null) == null ? "null" : string.Empty, };
                case "DateTime":
                    return new DateField(name) { Label = name, Text = (prop.GetValue(origin, null) ?? "null").ToString(), Type = DateField.DateType.DateTime, };
                case "DateType":
                    var dateTypes = Enum.GetNames(typeof(DateField.DateType));
                    return new SelectListField(name, dateTypes) { Label = name, Text = prop.GetValue(origin, null).ToString(), };
                case "MobilePlatform":
                case "MobileTarget":
                    return new LabelField(name) { Label = name, Text = prop.GetValue(origin, null).ToString(), };
                case "Link":
                    var layerList = new List<KeyValuePair<string, string>>(LayerStore.Instance.Select(l => new KeyValuePair<string, string>(l.Name, l.Title)));
                    layerList.Insert(0, new KeyValuePair<string, string>(string.Empty, "null"));
                    return new SelectListField(name, layerList) { Label = name, Text = (prop.GetValue(origin, null) ?? "null").ToString(), };
                case "Int32":
                case "Double":
                case "Single":
                case "UInt16":
                    return new NumericField(name) { Label = name, Text = ((prop == null ? field.GetValue(origin) : prop.GetValue(origin, null)) ?? "null").ToString(), };
                case "Boolean":
                    return new BoolField(name) { Label = name, Value = (bool)prop.GetValue(origin, null), };
                default:
                    return new TextField(name) { Label = name, Text = (prop.GetValue(origin, null) ?? string.Empty).ToString(), Placeholder = prop.GetValue(origin, null) == null ? "null" : string.Empty, };
            }
        }

        private static iLayerItem CreateSelector(this iLayer host, PropertyInfo item, object getValue)
        {
            var styleProperties = item.PropertyType.IsValueType ? item.PropertyType.GetFields().Cast<MemberInfo>().ToArray() : item.PropertyType.GetProperties().ToArray();
            var fields = new Fieldset { Header = item.Name, };
            foreach (var prop in styleProperties.Where(l => !Blacklist.Contains(l.Name) && !Attribute.IsDefined(l, typeof(ObsoleteAttribute))))
            {
                fields.Add(host.GetPropertyField(prop.Name, getValue ?? Activator.CreateInstance(item.PropertyType)));
            }
            return fields;
        }

        private static iLayerItem CreateCollector<T>(PropertyInfo item, Dictionary<string, T> source, string newId)
        {
            var list = new iList { Header = item.Name, };
            list.AddRange(source.Keys.Select(i => CreateItem(source[i], "/" + source[i].GetType().Name + "/" + i, null)));
            list.Add(new iItem(("/" + typeof(T).Name + "/" + newId).TrimEnd('/'), "Add"));
            if (source.Count > 1) list.Add(new iItem("/Reorder/-1", "Reorder"));
            return list;
        }

        public static iLayerItem CreateReorder<T>(PropertyInfo item, Dictionary<string, T> source)
        {
            var list = new iList { Header = item.Name, };
            var disclosure = new iFactr.Core.Controls.Button("Duplicate") { Parameters = new Dictionary<string, string> { { "Command", "Duplicate" } } };
            list.AddRange(source.Keys.Select(i => CreateItem(source[i], "../" + i, disclosure)));
            list[0].Link = null;
            return list;
        }

        public static iItem CreateItem(object item, string uri, iFactr.Core.Controls.Button disclosure)
        {
            var itemType = item.GetType();
            iItem iitem;
            switch (itemType.Name)
            {
                case "iPanel":
                case "iBlock":
                    var html = item as iLayerItem;
                    iitem = new ContentItem
                    {
                        Text = itemType.Name,
                        Subtext = html.Header,
                        Link = new Link(uri),
                        Button = disclosure,
                    };
                    break;
                case "iList":
                case "iMenu":
                    var collection = item as iLayerItem;
                    iitem = new SubtextBelowAndBesideItem
                    {
                        Text = itemType.Name,
                        Subtext = collection.Header,
                        BesideText = ((iCollection)collection).Count.ToString(CultureInfo.InvariantCulture),
                        Link = new Link(uri),
                        Button = disclosure,
                    };
                    break;
                default:
                    string label = null;
                    if (itemType.Name.EndsWith("Field"))
                    {
                        label = ((Field)item).Label;
                    }
                    iitem = new iItem(uri, itemType.Name) { Button = disclosure, Subtext = label, };
                    break;
            }
            return iitem;
        }

        public static readonly List<string> Blacklist = new List<string> { "Validate", "Name", "Item", "CancelLoad", "CompositeParent", "ID", };
    }
}