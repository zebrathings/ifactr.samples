﻿using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using MonoCross.Navigation;

namespace UIBuilder.Data
{
    public class LayerStore : List<iLayer>
    {
        public static LayerStore Instance
        {
            get { return _store ?? (_store = new LayerStore()); }
        }

        private static LayerStore _store;
        private LayerStore() { }

        public static iItem BindData(string baseUri, iLayer item)
        {
            return new iItem(string.Format(baseUri, item.Name), item.Title) { Button = new Button("Preview", string.Format("Preview/" + item.Name)) };
        }

        public void AddLayer(iLayer obj)
        {
            if (!Contains(obj))
                MXContainer.Instance.App.NavigationMap.Add(string.Format("Preview/{0}", obj.Name), obj);
            Add(obj);
        }
    }
}