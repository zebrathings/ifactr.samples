﻿using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;
using UIBuilder.Data;

namespace UIBuilder
{
    class Layers : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Layers";
            Items.Add(new SearchList { Items = new ItemsCollection<iItem>(LayerStore.Instance.Select(layer => LayerStore.BindData("Layers/{0}", layer))) });
            ActionButtons.Add(new Button(null, "Layers/Add") { Action = Button.ActionType.Add, });
        }
    }
}