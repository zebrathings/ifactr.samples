﻿using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;

namespace UIBuilder
{
    class Tabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = iApp.Instance.Title;
            TabItems.Add("Layers", "Layers");
            TabItems.Add("App", "App");
            TabItems.Add("Images", "Images");
        }
    }
}