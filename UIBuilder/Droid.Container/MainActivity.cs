using Android.App;
using Android.Dialog;
using Android.OS;
using Android.Views;
using UIBuilder;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Targets;
using iFactr.Droid;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            if (DroidFactory.Instance.MainActivity == null)
            {
                DroidFactory.Instance.MainActivity = this;
                TargetFactory.TheApp = new App();
                iApp.OnLayerLoadComplete += DroidFactory.Instance.OutputLayer;
            }
            else DroidFactory.Instance.MainActivity = this;
            base.OnCreate(bundle);
        }
    }
}