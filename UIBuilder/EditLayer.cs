﻿using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Core.Styles;
using UIBuilder.Data;

namespace UIBuilder
{
    class EditLayer : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Edit Layer";
            string id = parameters["ID"];

            iLayer layer;
            if (iApp.Session.ContainsKey("Layer")) layer = (iLayer)iApp.Session["Layer"];
            else if (id == "Add") iApp.Session["Layer"] = layer = new BaseLayer { Name = id = Guid.NewGuid().ToString(), };
            else iApp.Session["Layer"] = layer = LayerStore.Instance.FirstOrDefault(l => l.Name == id);

            int index = -1;
            string propName = null;
            if (parameters.ContainsKey("Index") && !int.TryParse(parameters["Index"], out index))
            {
                index = -1;
                propName = parameters["Index"];
            }

            var command = parameters.ContainsKey("Command") ? parameters["Command"] : null;
            var layerProps = layer.GetType().GetProperties();

            if (iApp.Session.Keys.Count > 6)
            {
                var key = iApp.Session.Keys.Last();
                var item = iApp.Session[key];
                var itemProperties = item.GetType().GetProperties();
                foreach (var itemProperty in itemProperties.Where(itemProperty => parameters.ContainsKey(itemProperty.Name) && itemProperty.PropertyType == typeof(string)))
                {
                    itemProperty.SetValue(item, parameters[itemProperty.Name], null);
                }
                if (item is iLayerItem)
                {
                    if (index > -1)
                    {
                        switch (command)
                        {
                            case "Delete":
                                layer.Items.Remove(item);
                                break;
                            case "Duplicate":
                                layer.Items.Insert(index + 1, ((iLayerItem)item).Clone());
                                break;
                            default:
                                layer.Items[index] = (iLayerItem)item;
                                break;
                        }
                    }
                    else
                    {
                        layer.Items.Add(item);
                    }
                }
                else if (item is Button)
                {
                    Button button = (Button)item;
                    if (propName != null)
                    {
                        layer.GetType().GetProperty(propName).SetValue(layer, button, null);
                    }
                    else if (index > -1)
                    {
                        switch (command)
                        {
                            case "Delete":
                                layer.ActionButtons.Remove(item);
                                break;
                            case "Duplicate":
                                layer.ActionButtons.Insert(index + 1, button.Clone());
                                break;
                            default:
                                layer.ActionButtons[index] = button;
                                break;
                        }
                    }
                    else
                    {
                        layer.ActionButtons.Add(item);
                    }
                }
                iApp.Session.Remove(key);
            }

            if (parameters.ContainsKey("Title"))
            {
                var colorProperties = layer.LayerStyle.GetType().GetProperties();
                foreach (var pair in parameters)
                {
                    if (pair.Key.EndsWith("Color.Key"))
                    {
                        var color = new Style.Color("#" + int.Parse(pair.Value).ToString("x8").ToUpperInvariant());
                        iApp.Log.Info(pair.Key + " " + color.HexCode);
                        colorProperties.First(prop => pair.Key.StartsWith(prop.Name)).SetValue(layer.LayerStyle, color, null);
                    }
                    else
                    {
                        var prop = layerProps.FirstOrDefault(p => p.Name == pair.Key);
                        if (prop != null)
                        {
                            if (prop.PropertyType.IsEnum)
                                prop.SetValue(layer, Enum.Parse(prop.PropertyType, pair.Value, true), null);
                            else if (prop.PropertyType == typeof(string))
                                prop.SetValue(layer, pair.Value, null);
                        }
                    }
                }
                LayerStore.Instance.AddLayer(layer);
                iApp.Session.Remove("Layer");
                iApp.SetNavigationContext(NavContext);
                CancelLoadAndNavigate("Layers");
                return;
            }

            var props = new Fieldset { Header = "Layer Properties", };
            foreach (var field in layerProps.Where(l => l.GetSetMethod() != null && !AbstractMap.Blacklist.Contains(l.Name) && !Attribute.IsDefined(l, typeof(ObsoleteAttribute))).Select(propertyInfo => this.GetPropertyField(propertyInfo.Name, layer)).Where(field => field != null))
            {
                props.Fields.Add(field);
            }

            Items.Insert(0, props);

            ActionButtons.Add(new SubmitButton(NavContext.NavigatedUrl));
            ActionButtons.Add(new Button("Preview", "Preview/" + id));
            ActionButtons.Add(new Button("Delete", "Delete/" + id));
        }
    }
}