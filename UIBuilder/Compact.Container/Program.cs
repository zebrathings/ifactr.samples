using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.Core.Forms;
using System.Drawing;

namespace UIBuilder
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
#if (NETCF)
        [MTAThread]
#else
        [STAThread]
#endif
        static void Main()
        {
            iApp.OnLayerLoadComplete += ilayer =>
            {
                CompactFactory.Instance.OutputLayer(ilayer);
            };
            CompactFactory.Initialize();
            CompactFactory.TheApp = new App();
            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);

            CompactFactory.Instance.CustomItemRequested = (item, style) =>
            {
                if (item is CustomNumericField)
                {
                    var field = (CustomNumericField)item;
                    var control = new TextBox
                    {
                        Text = field.Text,
                    };
                    control.ForeColor = Color.Crimson;
                    control.TextChanged += (o, e) =>
                    {
                        var sender = (TextBox)o;
                        ((Field)sender.Tag).Text = sender.Text;
                    };
                    return control;
                }
                return null;
            };

            Application.Run(CompactFactory.Instance.Form);
        }
    }
}