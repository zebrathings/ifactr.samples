//--------------------------------------------------------------------
// FILENAME: ReaderForm.cs
//
// Copyright(c) 2005 Symbol Technologies Inc. All rights reserved.
//
//--------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Symbol.Barcode;
using iFactr.Core.Layers;

namespace CS_ScanSample1
{
    /// <summary>
    /// CS_ScanSample1 Form class.
    /// </summary>
    public class ReaderForm : Form
    {
        private readonly ScanLayer _scanner;
        public ReaderForm(ScanLayer scanner)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            _scanner = scanner;
            lblDisplayText.Text = _scanner.DisplayText;
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            TriggerButton = new Button();
            lblDisplayText = new Label();
            SuspendLayout();
            // 
            // TriggerButton
            // 
            TriggerButton.Location = new Point(8, 144);
            TriggerButton.Name = "TriggerButton";
            TriggerButton.Size = new Size(223, 25);
            TriggerButton.TabIndex = 2;
            TriggerButton.Text = "Scan ON/OFF";
            TriggerButton.Click += new EventHandler(TriggerButton_Click);
            TriggerButton.KeyDown += new KeyEventHandler(TriggerButton_KeyDown);
            // 
            // lblDisplayText
            // 
            lblDisplayText.Location = new Point(8, 10);
            lblDisplayText.Name = "lblDisplayText";
            lblDisplayText.Size = new Size(223, 131);
            lblDisplayText.Text = "Please scan your item.";
            lblDisplayText.TextAlign = ContentAlignment.TopCenter;
            // 
            // ReaderForm
            // 
            AutoScaleMode = AutoScaleMode.Inherit;
            BackColor = Color.White;
            ClientSize = new Size(239, 179);
            Controls.Add(lblDisplayText);
            Controls.Add(TriggerButton);
            Name = "ReaderForm";
            Text = "CS_ScanSample1";
            Load += new EventHandler(ReaderForm_Load);
            ResumeLayout(false);
        }
        #endregion

        private Reader MyReader;
        private ReaderData MyReaderData;
        private EventHandler MyEventHandler;
        private Label lblDisplayText;
        private Button TriggerButton;

        /// <summary>
        /// Occurs before the form is displayed for the first time.
        /// </summary>
        private void ReaderForm_Load(object sender, EventArgs e)
        {
            // If we can initialize the Reader
            if (InitReader())
            {
                // Start a read on the reader
                StartRead();
            }
            else
            {
                // If not, close this form
                Close();
                return;
            }

            // Add MainMenu if Pocket PC
            if (Symbol.Win32.PlatformType == "PocketPC")
            {
                Menu = new MainMenu();
            }

        }

        /// <summary>
        /// Application is closing
        /// </summary>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Terminate reader
            TermReader();
            base.OnClosing(e);
        }

        /// <summary>
        /// Initialize the reader.
        /// </summary>
        private bool InitReader()
        {
            // If reader is already present then fail initialize
            if (MyReader != null) return false;

            // Create new reader, first available reader will be used.
            MyReader = new Reader();

            // Create reader data
            MyReaderData = new ReaderData(ReaderDataTypes.Text, ReaderDataLengths.MaximumLabel);

            // Create event handler delegate
            MyEventHandler = MyReader_ReadNotify;

            try
            {
                // Enable reader, with wait cursor
                MyReader.Actions.Enable();
            }
            catch (Exception)
            {
                return false;
            }

            MyReader.Parameters.Feedback.Success.BeepTime = 125;

            // Attach to activate and deactivate events
            Activated += ReaderForm_Activated;
            Deactivate += (o, e) => StopRead();

            return true;
        }

        /// <summary>
        /// Stop reading and disable/close reader
        /// </summary>
        private void TermReader()
        {
            // If we have a reader
            if (MyReader != null)
            {
                // Disable the reader
                MyReader.Actions.Disable();

                // Free it up
                MyReader.Dispose();

                // Indicate we no longer have one
                MyReader = null;
            }

            // If we have a reader data
            if (MyReaderData == null) return;
            // Free it up
            MyReaderData.Dispose();

            // Indicate we no longer have one
            MyReaderData = null;
        }

        /// <summary>
        /// Start a read on the reader
        /// </summary>
        private void StartRead()
        {
            // If we have both a reader and a reader data
            if (MyReader == null || MyReaderData == null) return;
            // Submit a read
            MyReader.ReadNotify += MyEventHandler;
            MyReader.Actions.Read(MyReaderData);
        }

        /// <summary>
        /// Stop all reads on the reader
        /// </summary>
        private void StopRead()
        {
            // If we have a reader
            if (MyReader == null) return;
            // Flush (Cancel all pending reads)
            MyReader.ReadNotify -= MyEventHandler;
            MyReader.Actions.Flush();
        }

        /// <summary>
        /// Read complete or failure notification
        /// </summary>
        private void MyReader_ReadNotify(object sender, EventArgs e)
        {
            var TheReaderData = MyReader.GetNextReaderData();

            // If it is a successful read (as opposed to a failed one)
            if (TheReaderData.Result != Symbol.Results.SUCCESS) return;
            // Handle the data from this read
            HandleData(TheReaderData);

            // Start the next read
            StartRead();
        }

        /// <summary>
        /// Handle data from the reader
        /// </summary>
        private void HandleData(ReaderData data)
        {
            _scanner.ActionButton.Parameters.Add("Type", data.Type.ToString());
            _scanner.ActionButton.Parameters.Add("Data", data.Text);

            Close();
        }

        private void ReaderForm_Activated(object sender, EventArgs e)
        {
            // If there are no reads pending on MyReader start a new read
            if (!MyReaderData.IsPending)
                StartRead();
        }

        private void TriggerButton_Click(object sender, EventArgs e)
        {
            if (MyReader == null) return;
            MyReader.Actions.ToggleSoftTrigger();
        }

        private void TriggerButton_KeyDown(object sender, KeyEventArgs e)
        {
            // Checks if the key pressed was an enter button (character code 13)
            if (e.KeyValue == (char)13)
                TriggerButton_Click(this, e);
        }
    }
}