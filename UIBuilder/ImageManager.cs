﻿using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace UIBuilder
{
    class ImageManager : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Images";
            var list = new SearchList();
            Items.Add(list);
            ActionButtons.Add(new Button("Add", "") { Action = Button.ActionType.Add });
        }
    }
}