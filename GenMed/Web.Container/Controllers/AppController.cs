using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Web;

namespace KitchenSink.Web.Controllers
{
    public class AppController : Controller
    {
        private bool loading = true;

        // GET: /Render/
        public void Render(string url, string args)
        {
            // set callback handler
            WebFactory.OnOutputLayerComplete += () => { this.OutputLayerComplete(); };

            // navigate to url
            url = url == null ? string.Empty : url;
            iApp.Navigate(url, WebFactory.Instance.GetParameters(this.Request));

            // wait for web factory to output the layer
            while (loading)
                System.Threading.Thread.Sleep(100);
        }

        private void OutputLayerComplete()
        {
            loading = false;
        }
    }
}
