using iFactr.Core.Layers;
using System.Collections.Generic;

namespace GenMed
{
    public class MainTabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = " ";
            LayerStyle.LayerBackgroundImage = "DashboardBG.jpg";

            TabItems.Add("Accounts", "Accounts", "icn_menu_accounts.png");
            TabItems.Add("Regions", "Region", "icn_menu_regions.png");
            TabItems.Add("Products", "Product", "icn_menu_products.png");
            TabItems.Add("Transactions", "Transactions", "icn_menu_transactions.png", true);
            TabItems.Add("Reports", "Reports", "icn_menu_reports.png");
        }
    }
}