using System.Collections.Generic;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.UI;
using Link = iFactr.UI.Link;

using GenMed.Data;

namespace GenMed.Accounts
{
    [PreferredPane(Pane.Master)]
    public class Search : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            // extract parameters from the navigation event to this layer
            string navUri;
            if (parameters.ContainsKey("Type") && parameters.ContainsKey("Model"))
            {
                Name = Name + "ProdOrder";
                navUri = string.Format("Products/{0}/{1}/Sell/{2}/", parameters["Type"], parameters["Model"], parameters["Source"]);
            }
            else { navUri = "Accounts/"; }

            // building the ui
            Title = "Accounts";
            LayerStyle.SectionHeaderColor = new Color("#dddddd");

            var list = new SearchList();
            foreach (Account account in Provider.Accounts)
            {
                string subText = string.Format("{0} - {1}, {2}", account.Id, account.Address.City, account.Address.State);
                var accountItem = new iItem
                {
                    Text = account.Name,
                    Subtext = subText,
                    Icon = new Icon("Headshots/" + account.Id + ".jpg"),
                    Link = new Link(navUri + account.Id),
                };
                list.Add(accountItem);
            }
            Items.Add(list);

            // the About button is only wanted if this layer is on the first tab.
            // in any other case, the layer will be further down the history stack and the button would be inappropriate
            if (iApp.CurrentNavContext.ActiveTab == 0)
                ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("icn_ab_ifactr.png"), });
        }
    }
}