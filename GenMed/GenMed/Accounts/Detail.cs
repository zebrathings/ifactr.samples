using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Accounts
{
    public class Detail : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            string orderId = parameters.ContainsKey("Order") ? parameters["Order"] : null;
            Data.Account account = Data.Provider.GetAccountById(accountId);

            // if the orderId is not null, we are submitting an order
            if (orderId != null)
            {
                Data.Order order = Data.Provider.GetOrderById(orderId);
                Data.Provider.SubmitOrder(order);
                CancelLoadAndNavigate(string.Format("Transactions/Submit/{0}", orderId));
                return;
            }

            Title = account.Name;

            // create the panel
            var panel = new iPanel { Header = "Account Detail", };

            // insert an image on the righthand side
            panel.InsertImageFloatRight("Headshots/" + account.Id + ".jpg", "100", "100");

            // append an image
            panel.AppendSubHeading(account.Name);
            panel.AppendItalicLine("Account Num: " + account.Id);
            panel.AppendLine();
            panel.AppendLine();

            // diplay account detail (in text)
            panel.AppendLine(account.Address.Street1);
            panel.AppendLine(account.Address.City + ", " + account.Address.State + "  " + account.Address.Zip);
            panel.AppendLine();
            if (!string.IsNullOrEmpty(account.Email))
            {
                panel.AppendEmail(account.Email);
                panel.AppendLine();
            }
            if (!string.IsNullOrEmpty(account.Phone))
            {
                panel.AppendTelephone(account.Phone);
            }

            var menu = new iMenu();
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/ShipTo", accountId), "Ship To Addresses") { Icon = new Icon("icn_menu_shiptoaddresses.png") });
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/Inventory", accountId), "Inventory") { Icon = new Icon("icn_menu_inventory.png") });
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/SoldInventory", accountId), "Sold Inventory") { Icon = new Icon("icn_menu_soldinventory.png") });

            Items.Add(panel);
            Items.Add(menu);

            ActionButtons.Add(new Button("Warehouse Sale", string.Format("Transactions/{0}/Sell/Warehouse/New", accountId)) { Image = new Icon("icn_ab_sellwarehouse.png"), });
            ActionButtons.Add(new Button("Trunk Sale", string.Format("Transactions/{0}/Sell/Warehouse/New", accountId)) { Image = new Icon("icn_ab_selltrunk.png"), });
        }
    }
}