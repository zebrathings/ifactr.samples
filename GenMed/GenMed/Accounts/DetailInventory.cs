﻿using System.Collections.Generic;
using iFactr.Core.Layers;

namespace GenMed.Accounts
{
    public class DetailInventory : iLayer 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            Data.Account account = Data.Provider.GetAccountById(accountId);

            Title = "Inventory";

            iList list = new iList();
            foreach (Data.InventoryItem item in account.Inventory)
            {
                list.Items.Add(new iItem(null, item.Quantity + " - " + item.Product.Model, item.Serial + "  (" + item.DaysToExpire + ")" , true));
            }
            if (list.Items.Count == 0)
            {
                list.Items.Add(new iItem(null, "No Inventory", "For this Account"));
            }
            Items.Add(list);
        }
    }
}