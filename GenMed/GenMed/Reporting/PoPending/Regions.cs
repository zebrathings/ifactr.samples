using System;
using System.Collections.Generic;
using iFactr.Core.Layers;

namespace GenMed.Reporting.POPending
{
    public class Regions : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Regions";

            iList list = new iList();
            list.Header = string.Format("{0:C}", Data.Provider.GeographyDataSet.TotalPOs);
            Items.Add(list);

            foreach (Data.Region region in Data.Provider.GeographyDataSet.Regions)
            {
                string uri = "Reports/POPending/" + region.Id;
                list.Add(new RightSubtextItem(uri, region.Name, string.Format("{0:C}", region.TotalPOs)));
            }
        }
    }
}