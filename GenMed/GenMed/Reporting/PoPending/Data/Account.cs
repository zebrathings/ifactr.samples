﻿namespace GenMed.Reporting.POPending.Data
{
    public class Account
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public double AccountPOs { get; set; }
    }
}