﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Reporting.POPending.Data
{
    public class Geography
    {
        public List<Region> Regions { get; set; }

        public double TotalPOs
        {
           get { return  Regions.Sum(region => region.TotalPOs); }
        }

        public Geography()
        {
            Regions = new List<Region>();
        }
    }
}