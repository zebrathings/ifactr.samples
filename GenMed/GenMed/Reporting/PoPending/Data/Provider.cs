﻿using iFactr.Core;
using iFactr.Utilities.Serialization;

namespace GenMed.Reporting.POPending.Data
{
    public static class Provider
    {
        public static Geography GeographyDataSet;

        static Provider()
        {
            string filePOPending = iApp.Factory.ApplicationPath.AppendPath("POPending.xml");
            GeographyDataSet = SerializerFactory.Create<Geography>().DeserializeObjectFromFile(filePOPending);
        }
    }
}