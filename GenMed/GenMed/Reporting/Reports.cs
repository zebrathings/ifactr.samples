using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;

namespace GenMed.Reporting
{
    [PreferredPane(Pane.Master)]
    public class Reports : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Reports";

            var menu = new iMenu { Header = null, };

            menu.Items.Add(new iItem("Reports/POPending", "Daily Sales") { Icon = new Icon("MenuIcons/icn_menu_dailysales.png"), });
            menu.Items.Add(new iItem("Reports/POPending", "Plan vs. Actual") { Icon = new Icon("MenuIcons/icn_menu_planvsactual.png"), });
            menu.Items.Add(new iItem("Reports/POPending", "PO Pending") { Icon = new Icon("MenuIcons/icn_menu_popending.png"), });
            menu.Items.Add(new iItem("Reports/POPending", "Case Tracker") { Icon = new Icon("MenuIcons/icn_menu_case.png"), });

            Items.Add(menu);

            ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("icn_ab_ifactr.png"), });
        }
    }
}