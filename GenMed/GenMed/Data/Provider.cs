using System.Collections.Generic;
using System.Linq;
using iFactr.Core;
using iFactr.Utilities.Serialization;

namespace GenMed.Data
{
    public static class Provider
    {
        public static List<Account> Accounts { get; set; }

        public static readonly List<Account> AccountDetails = new List<Account>();
        public static readonly List<Order> PendingOrders = new List<Order>();
        public static List<ProductType> ProductTypes { get; set; }

        static Provider()
        {
            // we want to load the data from our xml files as soon as they are needed
            string fileAccounts = iApp.Factory.ApplicationPath.AppendPath("Accounts.xml");
            string fileProducts = iApp.Factory.ApplicationPath.AppendPath("Products.xml");

            ProductTypes = SerializerFactory.Create<ProductType>(SerializationFormat.XML)
                .DeserializeListFromFile(fileProducts);

            Accounts = SerializerFactory.Create<Account>(SerializationFormat.XML)
                .DeserializeListFromFile(fileAccounts);
        }

        public static Account GetAccountById(string id)
        {
            Account retval = AccountDetails.FirstOrDefault(account => account.Id == id);

            if (retval == null)
            {
                // lazy-load account info
                string fileAccountDetail = iApp.Factory.ApplicationPath.AppendPath("Accounts").AppendPath(string.Format("{0}.xml", id));

                retval = SerializerFactory.Create<Account>(
                    SerializationFormat.XML).DeserializeObjectFromFile(fileAccountDetail);
            }

            return retval;
        }

        public static string GetDescriptionForProductType(string code)
        {
            ProductType prodType = ProductTypes.FirstOrDefault(type => type.TypeCode == code);
            return prodType == null ? string.Empty : prodType.Description;
        }

        public static IEnumerable<Product> GetProductsForType(string code)
        {
            ProductType prodType = ProductTypes.FirstOrDefault(type => type.TypeCode == code);
            return prodType == null ? new List<Product>() : prodType.Products;
        }

        public static Product GetProductById(string type, string model)
        {
            return string.IsNullOrEmpty(type) ? GetProductById(model) : GetProductsForType(type).FirstOrDefault(product => product.Model == model);
        }

        public static Product GetProductById(string model)
        {
            foreach (var type in ProductTypes)
            {
                var item = type.Products.FirstOrDefault(product => product.Model == model);
                if (item != null) return item;
            }
            return null;
        }

        public static Order GetOrderById(string id)
        {
            return PendingOrders.FirstOrDefault(order => order.Id == id);
        }

        public static void SaveOrder(Order order)
        {
            if (PendingOrders.Contains(order))
                PendingOrders.Remove(order);
            PendingOrders.Add(order);
        }

        public static void SubmitOrder(Order order)
        {
            // for the purposes of this demonstration, we will simply remove the order from the list
            PendingOrders.Remove(order);
        }
    }
}