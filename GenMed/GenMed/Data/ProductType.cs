﻿using System.Collections.Generic;

namespace GenMed.Data
{
    public class ProductType
    {
        public string TypeCode { get; set; }
        public string Description { get; set; }
        public List<Product> Products { get; set; }

        public ProductType()
        {
            Products = new List<Product>();
        }
    }
}