﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Data
{
    public class Region
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<District> Districts { get; set; }

        public IEnumerable<Account> GetAccounts()
        {
            return Districts.SelectMany(district => district.GetAccounts());
        }
        
        public Region()
        {
            Districts = new List<District>();
        }
    }
}