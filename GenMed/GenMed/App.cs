using iFactr.Core;
using iFactr.UI;

namespace GenMed
{
    public class App : iApp
    {
        public override void OnAppLoad()
        {
            Title = "GenMed";

            // when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
            Style.HeaderColor = new Color("0a71a5");
            Style.HeaderTextColor = new Color("FFFFFF");

            // Add navigation mappings.
            // Any part of the URI that is surrounded by curly braces will be a parameter passed along to the layer's Load method.
            // For example, navigating to Accounts/12345 will use the Accounts/{Account} URI and pass in a parameter with the key "Account" and the value "12345"
            NavigationMap.Add("MainTabs", new MainTabs());
            
            NavigationMap.Add("Accounts", new Accounts.Search());
            NavigationMap.Add("Accounts/{Account}", new Accounts.Detail());
            NavigationMap.Add("Accounts/{Account}/ShipTo", new Accounts.DetailShipTo());
            NavigationMap.Add("Accounts/{Account}/Inventory", new Accounts.DetailInventory());
            NavigationMap.Add("Accounts/{Account}/SoldInventory", new Accounts.DetailSoldInventory());

            NavigationMap.Add("Region", new Regions.Regions());
            NavigationMap.Add("Regions/{Region}", new Regions.Districts());
            NavigationMap.Add("Regions/{Region}/All", new Accounts.Search());
            NavigationMap.Add("Regions/{Region}/{District}", new Regions.Territories());
            NavigationMap.Add("Regions/{Region}/{District}/All", new Accounts.Search());
            NavigationMap.Add("Regions/{Region}/{District}/{Territory}", new Accounts.Search());

            NavigationMap.Add("Product", new Products.Types());
            NavigationMap.Add("Products/{Type}", new Products.Search());
            NavigationMap.Add("Products/{Type}/{Model}", new Products.Detail());
            NavigationMap.Add("Products/{Type}/{Model}/Sell/{Source}", new Accounts.Search());
            NavigationMap.Add("Products/{Type}/{Model}/Sell/{Source}/{Account}", new Transactions.Warehouse.Sell());

            NavigationMap.Add("Transactions", new Transactions.PendingOrders());
            NavigationMap.Add("Transactions/Submit/{Order}", new Transactions.PendingOrders());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/New", new Transactions.Warehouse.Sell());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}", new Transactions.Warehouse.Sell());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Detail", new Transactions.Warehouse.SellDetail());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Submit", new Accounts.Detail());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/New", new Products.Types());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}", new Products.Search());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/Detail/{Model}", new Transactions.Warehouse.SellItemDetails());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}/{Model}", new Transactions.Warehouse.SellItemDetails());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/Add/{Type}/{Model}", new Transactions.Warehouse.Sell());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}/{Model}/{OrderItemId}/{Action}", new Transactions.Warehouse.SellItemDetails());

            NavigationMap.Add("Reports", new Reporting.Reports());
            NavigationMap.Add("Reports/POPending", new Reporting.POPending.Regions());
            NavigationMap.Add("Reports/POPending/{Region}", new Reporting.POPending.Districts());
            NavigationMap.Add("Reports/POPending/{Region}/All", new Reporting.POPending.Accounts());
            NavigationMap.Add("Reports/POPending/{Region}/{District}", new Reporting.POPending.Territories());
            NavigationMap.Add("Reports/POPending/{Region}/{District}/All", new Reporting.POPending.Accounts());
            NavigationMap.Add("Reports/POPending/{Region}/{District}/{Territory}", new Reporting.POPending.Accounts());

            NavigationMap.Add("About", new About());

            NavigateOnLoad = "MainTabs";

        }
    }
}