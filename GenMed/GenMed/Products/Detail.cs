using System.Collections.Generic;
using iFactr.Core.Layers;

namespace GenMed.Products
{
    public class Detail : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string typeCode = parameters["Type"];
            string model = parameters["Model"];
			
            Data.Product product = Data.Provider.GetProductById(typeCode, model);

            Title = model;

            iPanel panel = new iPanel();
            panel.Header = "Product Detail";
			panel.InsertImageFloatLeft("Products/" + model.Replace(" ", "") + ".jpg", "100", "100");
            panel.AppendHeading(model);
            panel.AppendLine(product.Description);
            panel.AppendLine();
            
            var menu = new iMenu();
            menu.Items.Add(new iItem(string.Format("Products/{0}/{1}/Sell/Warehouse", typeCode, model), "Warehouse Sale") { Icon = new iFactr.Core.Controls.Icon("icn_menu_warehousesales.png") });
            menu.Items.Add(new iItem(string.Format("Products/{0}/{1}/Sell/Trunk", typeCode, model), "Trunk Sale") { Icon = new iFactr.Core.Controls.Icon("icn_menu_trunksales.png") });

            Items.Add(panel);
            Items.Add(menu);
        }
    }
}