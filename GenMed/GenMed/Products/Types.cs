using System.Collections.Generic;
using GenMed.Data;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;

namespace GenMed.Products
{
    [PreferredPane(Pane.Master)]
    public class Types : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string navUri;
            if (parameters.ContainsKey("Account") && parameters.ContainsKey("Order"))
            {
                navUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/", parameters["Account"], parameters["Order"]);
            }
            else { navUri = "Products/"; }

            // layer equality is determined by Name.
            // it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
            Name = navUri.Replace("/", ".");
            Title = "Products";

            var menu = new iMenu { Header = null };

            foreach (ProductType type in Provider.ProductTypes)
            {
                var item = new iItem(navUri + type.TypeCode, type.Description);
                item.Icon = new Icon("Products/" + type.TypeCode + ".jpg");
                menu.Items.Add(item);
            }
            Items.Add(menu);

            // the About button is only wanted if this layer is on the third tab.
            // in any other case, the layer will be further down the history stack and the button would be inappropriate
            if (iApp.CurrentNavContext.ActiveTab == 2)
                ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("icn_ab_ifactr.png"), });
        }
    }
}