using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;
using iFactr.Core.Controls;

namespace GenMed.Products
{
    [PreferredPane(Pane.Master)]
    public class Search : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            // extract parameters from the navigation event to this layer
            string type = "0";
            parameters.TryGetValue("Type", out type);

            string navUri = string.Empty;
            if (parameters.ContainsKey("Account") && parameters.ContainsKey("Order"))
            {
                navUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/{2}/", parameters["Account"], parameters["Order"], type);
            }
            else { navUri = string.Format("Products/{0}/", type); }

            // when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
            LayerStyle.SectionHeaderColor = new Color("#dddddd");

            // layer equality is determined by Name.
            // it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
            Name = navUri.Replace("/", ".");
            Title = Data.Provider.GetDescriptionForProductType(type);

            SearchList list = new SearchList();
            foreach (Data.Product product in Data.Provider.GetProductsForType(type))
            {
                var item = new iItem(navUri + product.Model, product.Model, product.Description);
                item.Icon = new Icon("Products/" + product.Model.Replace(" ", "") + ".jpg");
                list.Items.Add(item);
            }

            Items.Add(list);
        }
    }
}