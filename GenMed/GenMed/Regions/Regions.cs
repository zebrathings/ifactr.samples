using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.UI;

using GenMed.Reporting.POPending.Data;

namespace GenMed.Regions
{
    [PreferredPane(Pane.Master)]
    public class Regions : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Regions";
       
            var menu = new iMenu { Header = null, };

            foreach (var region in Provider.GeographyDataSet.Regions)
            {
                menu.Items.Add(new iItem("Regions/" + region.Id, region.Name, true)
                {
                    Icon = new Icon("Regions/icn_menu_" + region.Name.ToLower().Replace(" ", string.Empty) + ".png"),
                });
            }

            Items.Add(menu);

            ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("icn_ab_ifactr.png"), });
        }
    }
}