using GenMed.Reporting.POPending.Data;
using iFactr.Core.Layers;
using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.UI;

namespace GenMed.Regions
{
    [PreferredPane(Pane.Master)]
    public class Territories : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string regionId = parameters["Region"];
            string districtId = parameters["District"];
            District currentDistrict = Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId).Districts.FirstOrDefault(district => district.Id == districtId);

            Title = currentDistrict.Name;

            iList list = new iList();
            list.Header = "Territories";

            list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", regionId, districtId), "All Accounts"));
            list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", regionId, districtId), "All Hospitals"));
            list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", regionId, districtId), "All Clinics"));

            foreach (Territory territory in currentDistrict.Territories)
            {
                list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/{2}", regionId, districtId, territory.Id), territory.Name, true));
            }

            Items.Add(list);
        }
    }
}