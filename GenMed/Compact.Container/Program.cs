using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.Core.Layers;

namespace GenMed
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            CompactFactory.Initialize();
            iApp.OnLayerLoadComplete += CompactFactory.Instance.OutputLayer;
            CompactFactory.TheApp = new App();
            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.RootForm);
        }
    }
}