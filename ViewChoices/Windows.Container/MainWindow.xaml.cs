﻿using System.Windows;
using System.Windows.Data;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.UI;
using iFactr.Wpf;
using MonoCross.Navigation;
using ViewChoices;
using Windows.Container.Views;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            WpfFactory.Initialize();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };

            //Instantiate your iFactr application and set the Factory App property
            WpfFactory.TheApp = new ViewChoices.MyApp();

            WpfFactory.TheApp.Style.LayerItemBackgroundColor = Color.White;
            WpfFactory.TheApp.Style.TextColor = Color.Black;

            MXContainer.AddView<string>(new SimpleView(), "NativeApi");

            Content = WpfFactory.Instance.MainWindow;
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
        }
    }
}