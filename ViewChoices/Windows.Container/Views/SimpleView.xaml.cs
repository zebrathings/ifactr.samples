﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MonoCross.Navigation;

namespace Windows.Container.Views
{
    /// <summary>
    /// Interaction logic for SimpleView.xaml
    /// </summary>
    public partial class SimpleView : UserControl, IMXView<string>
    {
        public SimpleView()
        {
            InitializeComponent();
        }

        public Type ModelType { get { return typeof(string); } }

        public object GetModel()
        {
            return Model;
        }

        public void SetModel(object model)
        {
            Model = model as string;
        }

        public void Render()
        {
            ModelTextBlock.Text = Model;
        }

        public string Model { get; set; }
    }
}
