using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.UI;
using MonoCross.Navigation;

namespace Compact.Container
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            CompactFactory.Initialize();
            iApp.OnLayerLoadComplete += CompactFactory.Instance.OutputLayer;
            
            var theApp = new ViewChoices.MyApp();
            theApp.Style.LayerBackgroundColor = Color.White;
            CompactFactory.TheApp = theApp;
            MXContainer.AddView<string>(typeof(NativeView),"NativeApi");

            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.RootForm);
        }
    }
}