﻿using System;
using System.Linq;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.UI;
using MonoCross.Navigation;

namespace Compact.Container
{
    public partial class NativeView : UserControl, IMXView<string>, IHistoryEntry
    {
        public NativeView()
        {
            InitializeComponent();
        }

        public event EventHandler Activated;
        public event EventHandler Deactivated;

        public Link BackLink { get; set; }

        public string StackID { get; set; }

        public Pane OutputPane { get; set; }

        public PopoverPresentationStyle PopoverPresentationStyle { get; set; }

        public ShouldNavigateDelegate ShouldNavigate { get; set; }

        public IHistoryStack Stack
        {
            get
            {
                return PaneManager.Instance.FirstOrDefault(s => s.CurrentView == this);
            }
        }

        #region IMXView<string> Members

        public string Model { get; set; }

        #endregion

        #region IMXView Members

        public Type ModelType
        {
            get { return typeof(string); }
        }

        public object GetModel()
        {
            return Model;
        }

        public void SetModel(object model)
        {
            Model = model.ToString();
        }

        public void Render()
        {
            lblNative.Text = Model;
        }

        #endregion

    }
}
