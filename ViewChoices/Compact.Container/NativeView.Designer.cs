﻿namespace Compact.Container
{
    partial class NativeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNative = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNative
            // 
            this.lblNative.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNative.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblNative.Location = new System.Drawing.Point(0, 66);
            this.lblNative.Name = "lblNative";
            this.lblNative.Size = new System.Drawing.Size(150, 23);
            this.lblNative.Text = "NativeAPI";
            this.lblNative.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // NativeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Red;
            this.Controls.Add(this.lblNative);
            this.Name = "NativeView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNative;
    }
}
