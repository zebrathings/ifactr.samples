
using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

using iFactr.Core;
using iFactr.Touch;
using MonoCross.Navigation;
using Touch.Container.Views;
using ViewChoices;

namespace Touch.Container
{
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            TouchFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => { InvokeOnMainThread(delegate { TouchFactory.Instance.OutputLayer(layer); }); };

            //Instantiate your iFactr application and set the Factory App property
            TouchFactory.TheApp = new ViewChoices.MyApp();

            MXContainer.AddView<string>(typeof(FluctuatingValueViewController), "NativeApi");

            iApp.Navigate(TouchFactory.TheApp.NavigateOnLoad);

            return true;
        }

        public override void OnActivated(UIApplication application) { }
    }

    public class Application { static void Main(string[] args) { UIApplication.Main(args, null, "AppDelegate"); } }
}
