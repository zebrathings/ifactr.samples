using System;
using System.Drawing;

using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoCross.Navigation;

namespace Touch.Container.Views
{
    [Register("SimpleUIViewController")]
    public class SimpleUIView : UIViewController, IMXView
    {
        public SimpleUIView() { View.Frame = new RectangleF(0,20,320,480); }

        public void Render()
        {
            Title = _model;

            View.BackgroundColor = UIColor.Red;
        }

        public object GetModel() { return _model;}
        public Type ModelType { get { return typeof(string); } }
        public void SetModel(object model) { _model = model as string; }
        private string _model;
    }
}