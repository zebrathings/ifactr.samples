using System;
using CoreGraphics;

using CoreFoundation;
using UIKit;
using Foundation;
using MonoCross.Navigation;
using ViewChoices;

namespace Touch.Container.Views
{
    public class HTMLView : UIViewController, IMXView
    {
        UIWebView _webView;
        public HTMLView() { }

        public override void ViewDidLoad()
        {
            _webView = new UIWebView();
            View.Add(_webView);
        }

        public override void ViewWillAppear(bool animated)
        {
            CGRect frame;
            if (View.Superview != null)
                frame = View.Superview.Frame;
            else 
                frame = new CGRect(0,0, 320, 480);
            
            _webView.Frame = frame;
            _webView.LoadHtmlString(ViewTypesController.HtmlMarkup, null);
        }

        public void Render() { Title = _model; }
        public object GetModel() { return _model; }
        public Type ModelType { get { return typeof(string); } }
        public void SetModel(object model) { _model = model as string; }
        private string _model;
    }
}