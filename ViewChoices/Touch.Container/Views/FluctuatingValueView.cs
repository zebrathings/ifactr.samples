using System;
using CoreGraphics;
using System.ComponentModel;

using CoreFoundation;
using UIKit;
using Foundation;
using CoreText;

using iFactr.Touch;
using MonoCross.Navigation;

using ViewChoices;
using System.Threading.Tasks;


namespace Touch.Container.Views
{
    [Register("FluctuatingView")]
    public class FluctuatingValueView : UIView
    {
        public nfloat FluctuatingValue;       

        public FluctuatingValueView()
        {
            Initialize();
        }

        public FluctuatingValueView(CGRect bounds) : base(bounds)
        {
            Initialize();
        }

        void Initialize() { }

        public override void Draw(CGRect rect)
        {

            base.Draw(rect);

            nfloat arc = 100;
            nfloat x = (rect.Width/2);
            nfloat y = (rect.Height - arc)/2;
			MakeGauge (x, y, arc);
        }

		private void MakeGauge(nfloat x, nfloat y, nfloat outerRadius)
		{
			//get graphics context
			using (CGContext g = UIGraphics.GetCurrentContext())
			{

				//centers for all arcs
				//float x = 150;
				//float y = 240;
				//float outerRadius = 125;
				//float innerRadius = 100;
				nfloat innerRadius = outerRadius * .75f; //75%

				//radians starts at east (0), south (90), west(180), north (270)
				//135 to 45 is total arc

				//g.AddArc(x, y, radius, start angle, end angle, clockwise);

				//Outer Zone Arcs
				g.SetLineWidth(outerRadius * .2f); //20%

				//light blue 155,177,191
				g.SetStrokeColor(c(155),c(177),c(191), 1);
				g.AddArc(x, y, outerRadius, DegreesToRaidians(135f), DegreesToRaidians(170f), false);
				g.StrokePath();

				//yellow 247,221,144
				g.SetStrokeColor(c(247),c(221),c(144), 1);
				g.AddArc(x, y, outerRadius, DegreesToRaidians(170f), DegreesToRaidians(270f), false);
				g.StrokePath();

				//orange 232,177,84
				g.SetStrokeColor(c(232),c(177),c(84), 1);
				g.AddArc(x, y, outerRadius, DegreesToRaidians(270f), DegreesToRaidians(330f), false);
				g.StrokePath();

				//dark orange 227,143,67
				g.SetStrokeColor(c(227),c(143),c(67), 1);
				g.AddArc(x, y, outerRadius, DegreesToRaidians(330f), DegreesToRaidians(10f), false);
				g.StrokePath();

				//red 160,33,42
				g.SetStrokeColor(c(160),c(33),c(42), 1);
				g.AddArc(x, y, outerRadius, DegreesToRaidians(10f), DegreesToRaidians(45f), false);
				g.StrokePath();


				//inner arc
				//135f is 60bpm
				//310f is 180bpm
				g.SetLineWidth(outerRadius * .25f); //25% 

				if (FluctuatingValue <= 170f)
				{
					//blue
					g.SetStrokeColor(c(155), c(177), c(191), 1);
				}
				else if (FluctuatingValue > 170 && FluctuatingValue <= 270)
				{
					//yellow 247,221,144
					g.SetStrokeColor(c(247), c(221), c(144), 1);
				}
				else if (FluctuatingValue > 270 && FluctuatingValue <= 330)
				{ 
					//orange 232,177,84
					g.SetStrokeColor(c(232), c(177), c(84), 1);
				}
				else if (FluctuatingValue > 330 && FluctuatingValue <= 360)
				{
					//dark orange 227,143,67
					g.SetStrokeColor(c(227),c(143),c(67), 1);
				}
				else if (FluctuatingValue > 10 && FluctuatingValue <= 45)
				{
					//red 160,33,42
					g.SetStrokeColor(c(160), c(33), c(42), 1);
				}

				g.AddArc(x, y, innerRadius, DegreesToRaidians(135f), DegreesToRaidians(FluctuatingValue), false);
				g.StrokePath();

				//white
				g.SetStrokeColor(1.0f, 1.0f, 1.0f, 1);
				g.AddArc(x, y, innerRadius, DegreesToRaidians(FluctuatingValue), DegreesToRaidians(45f), false);
				g.StrokePath();
			}
		}

        public void SetFluctuatingValue(nfloat newValue)
        {
            this.FluctuatingValue = newValue;
            this.SetNeedsDisplay();
        }

        private nfloat c(int ubyte_color)
        {

            return ubyte_color / 255.0f;
        }

        private nfloat DegreesToRaidians(nfloat degrees)
        {
            return (nfloat)(degrees * (Math.PI / 180));
        }
    }

    [Register("FluctuatingViewController")]
    public class FluctuatingValueViewController : UIViewController, IMXView
    {
        FluctuatingValueView fluctuatingRateView;
        FluctuatingValueModel fluctuatingValueModel;

        public FluctuatingValueViewController() { }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            fluctuatingRateView = new FluctuatingValueView { Frame = UIScreen.MainScreen.Bounds };
            this.View = fluctuatingRateView;

            fluctuatingRateView.SetFluctuatingValue(135f);

            View.Frame = UIScreen.MainScreen.Bounds;
            View.BackgroundColor = UIColor.FromRGBA(0.95f, 0.95f, 0.95f, 1);

            fluctuatingValueModel = new FluctuatingValueModel();
            fluctuatingValueModel.PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
            {
                FluctuateValue();
            };
        }

        public override void ViewWillAppear(bool animated)
        {
            if (NavigationController != null && NavigationController.NavigationBar != null)
            {
                NavigationController.NavigationBar.BarTintColor = iFactr.UI.Color.Gray.ToUIColor();
                NavigationController.NavigationBar.TintColor = UIColor.White;
            }
        }

        private void FluctuateValue()
        {
            fluctuatingRateView.SetFluctuatingValue(fluctuatingValueModel.CurrentValue);
        }

        public void Render() { }
        public object GetModel() { return _model;}
        public Type ModelType { get { return typeof(string); } }
        public void SetModel(object model) { _model = model as string; }
        private string _model;        
    }
    
    public class FluctuatingValueModel : INotifyPropertyChanged
    {
        nfloat currentFluctuatingValue;
        int delay;

        public int DelayMS
        {
            get{return delay;}
            set { delay = value; }
        }

        public nfloat CurrentValue
        {
            get{return currentFluctuatingValue;}
            set { currentFluctuatingValue = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public FluctuatingValueModel()
        {
            this.currentFluctuatingValue = 135f;
            Timer();
        }

        async void Timer()
        {
            bool increment = true;
            while (true)
            {
                await Task.Delay(5);
                
                if(this.currentFluctuatingValue == 360)//360
                {
                    increment = false;
                }
                else if (this.currentFluctuatingValue == 135)//135
                {
                    increment = true;
                }

                if (increment)
                {
                    this.currentFluctuatingValue++;
                }
                else
                {
                    this.currentFluctuatingValue--;
                }

                OnPropertyChanged("HeartRate");

            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }    
}