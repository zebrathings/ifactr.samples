﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace ViewChoices
{
    public class TheExampleLayer : Layer
    {
        public TheExampleLayer() { Title = "Search List Layer"; }

        public override void Load(Dictionary<string, string> parameters)
        {
            LayerStyle.HeaderTextColor = iFactr.UI.Color.White;

            Title = "Employee List";
            var list = new SearchList() { AutoFocus = true, } ;
            int generatedID = 0;
            String [] employeeList = new [] {"Joe", "Sam", "Jon", "Ben", "Sally", 
              "Julie", "Ryan", "Monica", "Brian", "Gary", "Nathan", "Matt",
              "Bevis", "Tom", "Crispin", "Rufus", "Todd", "Steph", "Xanadu",
              "Xavier", "Ozymandius", "Kristin", "Allison", "Captain Spaulding", };
            Random randomStream = new Random(5689);

            foreach (string custName in employeeList)
            {
                generatedID = randomStream.Next(56000, 99000);
                string subText = string.Format("Emp AM{0}", generatedID);
                var accountItem = new iItem
                {
                    Text = custName,
                    Subtext = subText,
                    Link = null,
                };
                list.Add(accountItem);
            }
            Items.Add(list);
        }

        public const string Uri = "TheUriToTheExampleLayer";
    }
}
