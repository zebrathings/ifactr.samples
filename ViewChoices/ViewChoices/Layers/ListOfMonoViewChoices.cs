﻿using System;
using iFactr.Core.Layers;
using System.Collections.Generic;
using iFactr.UI;

namespace ViewChoices
{
    public class ListOfMonoViewChoices : Layer
    {
        public ListOfMonoViewChoices ()
        {
            Title = "MonoView Choices";
        }

        public override void Load(Dictionary<string, string> parameters)
        {
            LayerStyle.HeaderColor = Color.Gray;
            LayerStyle.HeaderTextColor = Color.White;

            LayerStyle.SectionHeaderColor = Color.Black;
            LayerStyle.SectionHeaderTextColor = Color.White;

            var list = new iList();
            list.Header = "MonoView Types";
           
            list.Add(new iItem(ViewTypesController.Uri("BrowserView"), "Browser View"));
            list.Add(new iItem(ViewTypesController.Uri("CanvasView"), "Canvas View"));
            list.Add(new iItem(ViewTypesController.Uri("GridView"), "Grid View"));
            list.Add(new iItem(ViewTypesController.Uri("ListView"), "List View"));
            list.Add(new iItem(ViewTypesController.Uri("ListViewWithCellRecycling"), "List View with Cell Recycling"));
            list.Add(new iItem(ViewTypesController.Uri("TabView"), "TabView"));
           
            Items.Add(list);
        }
        public const string Uri = "ListOfMonoViewChoicesUri";
    }
}

