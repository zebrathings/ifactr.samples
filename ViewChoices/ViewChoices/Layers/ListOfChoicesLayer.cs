﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI;
using iFactr.Core.Layers;
using iFactr.Core;
using iFactr.Core.Controls;

namespace ViewChoices
{
    public class ListOfChoicesLayer : Layer
    {
        public ListOfChoicesLayer() { Title = "Choices"; }
        // Add code to initialize your layer here. For more information, see http://support.ifactr.com/
        public override void Load(Dictionary<string, string> parameters)
        {
            var iconRoot = iApp.Factory.ApplicationPath.AppendPath("images");

            LayerStyle.HeaderColor = Color.Gray;
            LayerStyle.HeaderTextColor = Color.White;
            
            LayerStyle.SectionHeaderColor = Color.Black;
            LayerStyle.SectionHeaderTextColor = Color.White;
            
            var list = new iList() { Header = "View Choices" };

            var iconPath = iconRoot.AppendPath("layer.png");
            list.Add(new iItem(TheExampleLayer.Uri, "Layer") { Icon = new Icon(iconPath) });

            iconPath = iconRoot.AppendPath("monoview.png");
            list.Add(new iItem(ListOfMonoViewChoices.Uri, "MonoView Choices") { Icon = new Icon(iconPath) });

            iconPath = iconRoot.AppendPath("html.png");
            list.Add(new iItem(ViewTypesController.Uri("HTML"), "HTML") { Icon = new Icon(iconPath) });

            iconPath = iconRoot.AppendPath("nativeview.png");
            list.Add(new iItem(ViewTypesController.Uri("NativeApi"), "Native APIs") { Icon = new Icon(iconPath) });

            // Reflected Model is under development.
            //list.Add(new iItem(ReflectedController.Uri, "Reflected from a Model"));

            Items.Add(list);

        }
        
        public const string Uri = "ListOfLayerTypesUri";
    }
}
