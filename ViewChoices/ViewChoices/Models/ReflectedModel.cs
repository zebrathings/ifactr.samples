﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI.Reflection;

namespace ViewChoices.Models
{
    [View(ViewType.List, HeaderColor="Gray", Title="Reflected View" )]   //, BackgroundColor="Black")]
    public class ReflectedModel
    {
        public string InstanceName;

        public string InstanceAddress;
        
        public double InstanceId;

        public DateTime InstanceDate;

        public ReflectedEnum InstanceEnum;
    }

    public enum ReflectedEnum
    {
        Value1,
        Value2,
        ValueN,
    }
}
