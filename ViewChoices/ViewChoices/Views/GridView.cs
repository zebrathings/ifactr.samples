﻿using iFactr.UI;
using iFactr.UI.Controls;

namespace ViewChoices
{
    class GridView : GridView<string>
    {
        protected override void OnRender()
        {
            Title = "Midwest Address Builder";
            HeaderColor = Color.Gray;

            // add 2 columns to the view
            Columns.Add(Column.AutoSized);
            Columns.Add(Column.AutoSized);

            PreferredOrientations = PreferredOrientation.Portrait;
            Padding = new Thickness(Thickness.LeftMargin,
                Thickness.TopMargin,
                Thickness.RightMargin,
                Thickness.BottomMargin);

            var smallItalic = Font.PreferredSmallFont;
            smallItalic.Formatting = FontFormatting.Italic;

            // Name
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Label custNameLabel = new Label("Name:");
            AddChild(custNameLabel);

            TextBox custNameBox = new TextBox()
            {
                Text = "Mr. John Doe",
                Font = smallItalic,
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
            };
            AddChild(custNameBox);

            // Street Address
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Label custStreetAddressLabel = new Label("Street:");
            AddChild(custStreetAddressLabel);
            TextBox custStreetAddress = new TextBox()
            {
                Text = "2520 Lexington Avenue South",
                Font = smallItalic,
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
            };
            AddChild(custStreetAddress);

            // City
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Label cityLabel = new Label()
            {
                Text = "City:",
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
            };
            AddChild(cityLabel);

            TextBox cityName = new TextBox()
            {
                Text = "Mendota Heights",
                Font = smallItalic,
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
            };
            AddChild(cityName);

            // Country and State labels
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Label countriesLabel = new Label("Country:");
            AddChild(countriesLabel);

            Label statesLabel = new Label()
            {
                Text = "State/Province:",
            };
            AddChild(statesLabel);

            // Country and State SelectLists
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            SelectList countries = new SelectList()
            {
                Items = CountryList,
                SelectedIndex = 0,  // Assume United States 
                Font = Font.PreferredSmallFont,
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 10, Thickness.SmallVerticalSpacing),
                HorizontalAlignment = HorizontalAlignment.Left,
            };
            AddChild(countries);

            SelectList states = new SelectList()
            {
                Items = UnitedStates,  // Defaults to U.S. States
                SelectedIndex = 1,  // Defaults to Minnesota
                Font = Font.PreferredSmallFont,
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
                HorizontalAlignment = HorizontalAlignment.Left,
            };
            AddChild(states);

            // Postal Code
            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Label postalCodeLabel = new Label()
            {
                Text = "Postal Code: ",
                HorizontalAlignment = HorizontalAlignment.Left,
            };
            AddChild(postalCodeLabel);

            TextBox postalCode = new TextBox()
            {
                Text = "55120",
                ForegroundColor = Color.Gray,
                Margin = new Thickness(0, 0, 0, Thickness.SmallVerticalSpacing),
                VerticalAlignment = VerticalAlignment.Top,
            };
            AddChild(postalCode);

            // An editable TextArea to hold the full address
            Rows.Add(new Row(Cell.StandardCellHeight * 2, LayoutUnitType.Absolute));
            TextArea fullCustomerAddress = new TextArea()
            {
                Text = "Tap\nBuild Address\nbutton\nto create address\nhere.",
                ColumnSpan = 2,  // fill the row
                Font = smallItalic,
            };
            AddChild(fullCustomerAddress);

            // Handler for the Country SelectList:
            // When country selected, populate states/provinces SelectList
            countries.SelectionChanged += (sender, args) =>
            {
                states.Items = StateMapper[countries.SelectedIndex];
                states.SelectedIndex = 0;
            };

            Rows.Add(new Row(1, LayoutUnitType.Auto));
            Button tapToApprove = new Button("Build Address")
            {
                ColumnSpan = 2,
                ForegroundColor = Color.White,
                BackgroundColor = new Color("205611"),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Top,
                Margin = new Thickness(5),
            };
            AddChild(tapToApprove);

            // When approved, put in TextBox
            tapToApprove.Clicked += (sender, args) =>
            {
                int ci = countries.SelectedIndex;
                if (states.SelectedIndex < 0 || ci < 0 ||
                    states.SelectedIndex > StateMapper[ci].Length)
                    return;  // ignore badness

                fullCustomerAddress.Text = custNameBox.Text + "\n"
                    + custStreetAddress.Text + "\n"
                    + cityName.Text + ", " + StateMapper[ci][states.SelectedIndex] + " " + postalCode.Text + "\n"
                    + CountryList[ci].ToUpper();

            };
        }

        public GridView()
        {
            HeaderColor = Color.Gray;
            TitleColor = Color.White;

            StateMapper = new string[][] { UnitedStates, CanadianProvinces, };
        }
        string[][] StateMapper;
        readonly string[] CountryList = { "United States", "Canada", };
        readonly string[] UnitedStates = { "Iowa", "Minnesota", "Nebraska", "North Dakota", "South Dakota", "Wisconsin", };
        readonly string[] CanadianProvinces = { "Alberta", "Manitoba", "Ontario", "Saskatchewan", };
    }
}
