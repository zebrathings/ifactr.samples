﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.UI.Controls;

namespace ViewChoices
{
    class CanvasView : CanvasView<string>
    {
        private string dirName;
        private string saveFileName;

        public CanvasView()
        {
            dirName = iApp.Factory.DataPath.AppendPath("temp_dir");
            saveFileName = dirName.AppendPath("ScratchPadImage.png");
        }

        protected override void OnRender()
        {
            Title = Model;
            OutputPane = Pane.Popover;
            PopoverPresentationStyle = PopoverPresentationStyle.Normal;

            Toolbar = new Toolbar();
            Toolbar.ForegroundColor = Color.Gray;
            StrokeColor = Color.Purple;
            StrokeThickness = 6;

            var clearButton = new ToolbarButton() { Title = "Clear All", ForegroundColor = Color.Red, };
            clearButton.Clicked += clearButton_Clicked;

            var saveButton = new ToolbarButton() { Title = "Save", ForegroundColor = Color.Green, };
            saveButton.Clicked += saveButton_Clicked;

            var recallButton = new ToolbarButton() { Title = "Recall", ForegroundColor = Color.Black, };
            recallButton.Clicked += recallButton_Clicked;

            var toolBar = new Toolbar();
            toolBar.PrimaryItems = new IToolbarItem[]
            {
                clearButton,
                new ToolbarSeparator(),
                saveButton,
                new ToolbarSeparator(),
                recallButton,
            };
            Toolbar = toolBar;
        }

        void clearButton_Clicked(object sender, EventArgs e)
        {
            this.Clear();
        }

        void saveButton_Clicked(object sender, EventArgs eventArgs)
        {
            try { iFactr.Core.iApp.File.CreateDirectory(dirName); }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error creating directory: {0}", e.ToString());
            }
            this.Save(saveFileName, true);  // Save background too
        }

        void recallButton_Clicked(object sender, EventArgs eventArgs)
        {
            try { iApp.File.Exists(saveFileName); }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Image file does not exist: {0}", e.ToString());
            }
            this.Load(saveFileName);
            // TODO: consider calling Render
        }
    }
}
