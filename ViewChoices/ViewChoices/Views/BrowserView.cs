﻿using iFactr.UI;

namespace ViewChoices
{
    class BrowserView : BrowserView<string>
    {
        bool ownWindow = false;
        protected override void OnRender()
        {
            Title = Model;

            EnableDefaultControls = true;

            if (ownWindow)
            {
                LaunchExternal("https://www.google.com");
                ownWindow = false;
            }
            else
            {
                Load("https://www.google.com");
                ownWindow = true;
            }

            var alertButton = new MenuButton("Alert");
            alertButton.Clicked += (o, e) => { Load("javascript:alert('Hello world!')"); };
            Menu = new Menu(alertButton);
        }

        public BrowserView() 
        {
            HeaderColor = Color.Gray;
            TitleColor = Color.White;
        }
    }
}