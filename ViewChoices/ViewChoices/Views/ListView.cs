﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using iFactr.UI;
using iFactr.UI.Controls;

using iFactr.Core;

namespace ViewChoices
{
    class ListView : ListView<string>
    {
        protected override void OnRender()
        {
            Title = Model;
            Menu = new Menu();
            var menuToggle = new MenuButton("Toggle Columns");
            Menu.Add(menuToggle);

            for (int sect = 0; sect < 10; sect++)
            {
                Sections[sect].ItemCount = 8;
                Sections[sect].Header = new SectionHeader("Header" + sect)
                    {
                        Font = Font.PreferredMessageTitleFont,
                    };
            }

            menuToggle.Clicked += (o, e) =>
            {
                if (ColumnMode == iFactr.UI.ColumnMode.OneColumn)
                {
                    ColumnMode = iFactr.UI.ColumnMode.TwoColumns;
                }
                else
                {
                    ColumnMode = iFactr.UI.ColumnMode.OneColumn;
                }
                ReloadSections();
            };


        }
        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            TextBox custNameBox;
            Thickness margin = new Thickness(0, 8); // handy spacer

            if (section == 0) { custNameBox = new TextBox("Mr. John Doe"); }
            else { custNameBox = new TextBox("Ms. Minnie Mouse"); }

            custNameBox.ID = "custName";
            var cell = new GridCell();

            // Each cell has 3 columns
            cell.Columns.Add(Column.OneStar);
            cell.Columns.Add(new Column(2, LayoutUnitType.Star));
            cell.Columns.Add(new Column(2, LayoutUnitType.Star));

            var cellLabel = new Label("Cell")
            {
                ID = "cellLabel",
                ForegroundColor = Color.Green,
                Margin = margin,
            };
            cell.AddChild(cellLabel);

            cell.Selected += (o, e) =>
            {
                var selectedCell = (GridCell)o;
                var findIt = selectedCell.GetChild<Label>("cellLabel");
                if (findIt.ForegroundColor == Color.Red)
                {
                    findIt.Text = "Still Selected!";
                    findIt.ForegroundColor = Color.Green;
                }
                else
                {
                    findIt.Text = "Cell Selected";
                    findIt.ForegroundColor = Color.Red;
                    findIt.Font = new Font(string.Empty, 18, FontFormatting.BoldItalic);
                    findIt.Margin = new Thickness(0, 20);
                }
            };


            var collapseLabel = new Label("Collapse Entire Next Row")
                {
                    Margin = margin,
                };
            cell.AddChild(collapseLabel);
            var collapseSwitch = new Switch(false)
            {
                ID = "collapseSwitch",
                Margin = margin,
            };
            cell.AddChild(collapseSwitch);


            var mySwitchLabel = new Label("Hide TextBox:") { ID = "switchLabel", };
            cell.AddChild(mySwitchLabel);
            var mySwitch = new Switch(false)
            {
                ID = "mySwitch",
            };
            cell.AddChild(mySwitch);
            // the following TextBox will disappear/reappear when the above switch is toggled
            var myTestText = new TextBox()
            {
                Text = "sect:ind " + section + ":" + index,
                // initially, the TextBox is enabled & visible
            };
            cell.AddChild(myTestText);

            mySwitch.ValueChanged += (o, e) =>
            {
                if (myTestText.Visibility == Visibility.Visible)
                    myTestText.Visibility = Visibility.Hidden;
                else
                    myTestText.Visibility = Visibility.Visible;
            };

            collapseSwitch.ValueChanged += (o, e) =>
            {
                if (collapseSwitch.Value)  // make invisible
                {
                    mySwitchLabel.Visibility = Visibility.Collapsed;
                    mySwitch.Visibility = Visibility.Collapsed;
                    myTestText.Visibility = Visibility.Collapsed;
                }
                else
                {
                    mySwitchLabel.Visibility = Visibility.Visible;
                    mySwitch.Visibility = Visibility.Visible;
                    myTestText.Visibility = Visibility.Visible;
                }
            };

            var myPWLabel = new Label("Password binding (2 way)")
            {
                ColumnIndex = 0,
                Margin = margin,
            };
            myPWLabel.ID = "myPWLabel";

            cell.AddChild(myPWLabel);
            var myText = new TextBox()
                {
                    Margin = margin,
                };
            var myPassword = new PasswordBox()
            {
                ForegroundColor = Color.Red,
                Margin = margin,
            };

            myPassword.SetBinding(new Binding(PasswordBox.PasswordProperty, TextBox.TextProperty)
            {
                Source = myText,
                Mode = BindingMode.TwoWay,
            });
            cell.AddChild(myText);
            cell.AddChild(myPassword);

            var mySliderLabel = new Label("Slider binding (1 way)")
            {
                ColumnIndex = 0,
                Margin = margin,
            };
            cell.AddChild(mySliderLabel);

            var targetSlider = new Slider(0, 10)
            {
                MaximumTrackColor = new Color("8BABD0"),
                MinimumTrackColor = new Color("2F384D"),
                Margin = margin,
            };

            var sourceSlider = new Slider(0, 10, index)
            {
                MaximumTrackColor = Color.Red,
                MinimumTrackColor = Color.Green,
                Margin = margin,
            };

            cell.AddChild(targetSlider);
            cell.AddChild(sourceSlider);

            //targetSlider.SetBinding(new Binding(Slider.ValueProperty, Slider.ValueProperty)
            //{
            //    Source = sourceSlider,
            //    Mode = BindingMode.OneWayToTarget,
            //});

            sourceSlider.ValueChanged += (o, e) =>
                {
                    targetSlider.Value += (e.NewValue - e.OldValue);
                };

            cell.SelectionStyle = SelectionStyle.IndicatorOnly;


            cell.AddChild(new Label("Jump to Cell:"));
            var myJumpIndex = new TextBox()
            {
                Placeholder = "sect:indx",
            };
            myJumpIndex.ID = "myJumpIndex";
            myJumpIndex.ReturnKeyPressed += (o, e) =>
            {
                var findText = (TextBox)o;
                if (findText.Text != null)
                {
                    Regex regex = new Regex(@"\d+:\d+");
                    Match match = regex.Match(findText.Text);
                    if (match.Success)
                    {
                        var splitInts = findText.Text.Split(':');
                        int findSection = int.Parse(splitInts[0]);
                        int findIndex = int.Parse(splitInts[1]);
                        //public ICell ScrollToCell(int section, int index) { }
                        ScrollToCell(findSection, findIndex, true);
                    }
                }
            };
            cell.AddChild(myJumpIndex);



            return cell;
        }

        public ListView()
        {
            HeaderColor = Color.Gray;
            TitleColor = Color.White;
        }
    }
}
