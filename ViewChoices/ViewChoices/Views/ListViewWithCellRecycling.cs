﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI;
using iFactr.UI.Controls;

using iFactr.Core;

namespace ViewChoices
{
    /// <summary>
    /// ListView is virtualized (it only news-up cells when it needs them).
    /// Demonstrate the efficiency of cell-recycling in this ListView by 
    /// allocating many cells in a list and rendering them quickly.
    /// </summary>
    class ListViewWithCellRecycling : ListView<string>
    {
        protected override void OnRender()
        {
            Title = "Cell Recycling";
            int numSections = 2;  // start with 2 sections

            Menu = new Menu();
            var menuAddSection = new MenuButton("Add Section");
            var menuRemoveSection = new MenuButton("Remove Section");
            var menuAddItem = new MenuButton("Add Item to each Section");
            var menuRemoveItem = new MenuButton("Remove Item from each Section");
            Menu.Add(menuAddSection);
            Menu.Add(menuRemoveSection);
            Menu.Add(menuAddItem);
            Menu.Add(menuRemoveItem);

            // numSections Sections, each with x items
            for (int iSec = 0; iSec < numSections; iSec++)
            {
                Sections[iSec].ItemCount = 5;  // each section initially has 5 items
                Sections[iSec].Header = new SectionHeader("Section" + iSec)
                {
                    BackgroundColor = Color.Black,
                    ForegroundColor = Color.White,
                };
            }

            menuAddSection.Clicked += (o, e) =>
            {
                Sections[numSections].ItemCount = 1;
                Sections[numSections].Header = new SectionHeader("Section" + numSections)
                {
                    BackgroundColor = Color.Gray,
                    ForegroundColor = Color.White,
                };
                numSections++;
                SetBackground(Color.Transparent);  // Clear the alert-Red, if needed.
                ReloadSections();
            };
            menuRemoveSection.Clicked += (o, e) =>
            {
                if (numSections > 0)
                {
                    numSections--;
                    Sections.RemoveAt(numSections);
                }
                else
                {
                    SetBackground(Color.Red);
                }
                ReloadSections();
            };

            menuAddItem.Clicked += (o, e) =>
            {
                for (int iSec = 0; iSec < numSections; iSec++)
                    Sections[iSec].ItemCount++;
                SetBackground(Color.Transparent);
                ReloadSections();
            };

            menuRemoveItem.Clicked += (o, e) =>
            {
                for (int iSec = 0; iSec < numSections; iSec++)
                {
                    if (Sections[iSec].ItemCount > 0)
                    {
                        Sections[iSec].ItemCount--;
                    }
                    else
                    {
                        SetBackground(Color.Red);
                    }
                }
                ReloadSections();
            };
        }
        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var cell = recycledCell as GridCell ?? new GridCell();
            var txtBox1 = cell.GetChild<TextBox>("txt1");  // Try to re-use TextBox (search by ID)
            if (txtBox1 == null)
            {
                txtBox1 = new TextBox();
                txtBox1.ID = "txt1";
                cell.AddChild(txtBox1);
            }
            txtBox1.Text = String.Format("Section{0}-Index{1} TextBox1", section, index);

            var txtBox2 = cell.GetChild<TextBox>("txt2");  // Try to re-use TextBox (search by ID)
            if (txtBox2 == null)
            {
                txtBox2 = new TextBox();
                txtBox2.ID = "txt2";
                cell.AddChild(txtBox2);
            }
            txtBox2.Text = String.Format("Section{0}-Index{1} TextBox2", section, index);

            var textArea = cell.GetChild<TextArea>();  // no need to search by ID (since there is only one TextArea on this layer)
            if (textArea == null)
            {
                textArea = new TextArea();
                cell.AddChild(textArea);
            }
            textArea.Text = "put a lot of text in here to demonstrate TextArea wrapping, illustrating how different platforms handle more text than fits in the rendered space.";

            var sly = cell.GetChild<Slider>();  // only one Slider
            if (sly == null)
            {
                sly = new Slider(0, 5);
                sly.MaximumTrackColor = Color.Red;
                sly.MinimumTrackColor = Color.Green;
                cell.AddChild(sly);
            };
            sly.Value = (section % 5);

            // Alternate cell background to create a stripe effect.
            switch (index % 2)
            {
                case 0: cell.BackgroundColor = new Color("EEEEEE");
                    break;
                case 1: cell.BackgroundColor = new Color("CCCCCC");
                    break;
            }

            return cell;
        }

        public ListViewWithCellRecycling()
        {
            HeaderColor = Color.Gray;
            TitleColor = Color.White;
        }
    }
}
