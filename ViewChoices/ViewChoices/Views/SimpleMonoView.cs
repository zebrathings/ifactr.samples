﻿using System;
using System.Collections.Generic;

using MonoCross.Navigation;

using iFactr.UI;
using iFactr.UI.Controls;

using iFactr.Core;
using iFactr.Core.Native;
using iFactr.Core.Targets;
using System.Threading;

namespace ViewChoices
{
    public class SimpleMonoView :  ListView<string>
    {
        
        protected override void OnRender()
        {
            Title = Model;
            Sections[0].ItemCount = 1;
        }
        
        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            int rowIndex = -1;
            var cell = new GridCell();
            cell.SelectionStyle = SelectionStyle.None;

            Title = "Midwest Region Address Builder";
            HeaderColor = Color.Gray;
            cell.MinHeight = 5;
            cell.MaxHeight = 400;

            // 2 columns in the cell
            cell.Columns.Add(new Column(1, LayoutUnitType.Star));
            cell.Columns.Add(new Column(2, LayoutUnitType.Star));
            //cell.Columns.Add(new Column(1, LayoutUnitType.Star));

            var boldItalic = Font.PreferredLabelFont;
            boldItalic.Formatting = FontFormatting.BoldItalic;
            var smallItalic = Font.PreferredSmallFont;
            smallItalic.Formatting = FontFormatting.Italic;

            // Customer Name
            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Label custNameLabel = new Label()
            {
                ColumnIndex = 0,
                RowIndex = rowIndex,
                Text = "Name: ",
                Font = Font.PreferredSmallFont,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(custNameLabel);
            TextBox custNameBox = new TextBox("Mr. John Doe")
            {
                ColumnIndex = 1,
                Font = smallItalic,
                RowIndex = rowIndex,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(custNameBox);

            // Street Address
            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Label custStreetAddressLabel = new Label()
            {
                ColumnIndex = 0,
                RowIndex = rowIndex,
                Text = "Street: ",
                Font = Font.PreferredSmallFont,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(custStreetAddressLabel);
            TextBox custStreetAddress = new TextBox("2520 Lexington Avenue South")
            {
                ColumnIndex = 1,
                Font = smallItalic,
                RowIndex = rowIndex,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(custStreetAddress);

            // City
            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Label cityLabel = new Label()
            {
                ColumnIndex = 0,
                RowIndex = rowIndex,
                Text = "City: ",
                Font = Font.PreferredSmallFont,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(cityLabel);

            TextBox cityName = new TextBox("Mendota Heights")
            {
                ColumnIndex = 1,
                Font = smallItalic,
                RowIndex = rowIndex,
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(cityName);

            // Two Select Lists:  Countries, States/Provinces
            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Label countriesLabel = new Label()
            {
                ColumnIndex = 0,
                RowIndex = rowIndex,
                Font = Font.PreferredSmallFont,
                Text = "Country:",
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(countriesLabel);
            Label statesLabel = new Label()
            {
                ColumnIndex = 1,
                RowIndex = rowIndex,
                Font = Font.PreferredSmallFont,
                Text = "State/Province:",
                VerticalAlignment = VerticalAlignment.Bottom,
            };
            cell.AddChild(statesLabel);

            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            var countryList = new string[] { "United States", "Canada", };
            SelectList countries = new SelectList()
            {
                Items = countryList,
                ColumnIndex = 0,
                Font = Font.PreferredSmallFont,
                RowIndex = rowIndex,
                SelectedIndex = 0,  // Assume United States 
                // HorizontalAlignment = HorizontalAlignment.Left,
                //  VerticalAlignment = VerticalAlignment.Center,
            };
            cell.AddChild(countries);

            var UnitedStates = new string[] { "Iowa", "Minnesota", "Nebraska", "North Dakota", "South Dakota", "Wisconsin", };
            //var MexicanStates = new string[] { "Baja California", "Chiapas", "Jalisco", "Morelos", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", };
            var CanadianProvinces = new string[] { "Alberta", "Manitoba", "Ontario", "Saskatchewan", };
                // { "Alberta", "British Columbia", "Manitoba", "New Brunswick","Newfoundland and Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", };

            var stateMapper = new string[][] { UnitedStates, CanadianProvinces, };
            SelectList states = new SelectList()
            {
                ColumnIndex = 1,
                Font = Font.PreferredSmallFont,
                RowIndex = rowIndex,
                Items = UnitedStates,  // Assume U.S. States
                SelectedIndex = 1,  // Assume Minnesota
                //    HorizontalAlignment = HorizontalAlignment.Left
                //    VerticalAlignment = VerticalAlignment.Center,
            };

            cell.AddChild(states);

            // ZIP or Canadian Postal Code
            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Label postalCodeLabel = new Label()
            {
                ColumnIndex = 0,
                RowIndex = rowIndex,
                Text = "ZIP/Postal Code: ",
                Font = Font.PreferredSmallFont,
                VerticalAlignment = VerticalAlignment.Top,
            };
            cell.AddChild(postalCodeLabel);
            TextBox postalCode = new TextBox("55120")
            {
                ColumnIndex = 1,
                Font = smallItalic,
                RowIndex = rowIndex,
                VerticalAlignment = VerticalAlignment.Top,
            };
            cell.AddChild(postalCode);


            // An editable TextArea to hold the full address
            cell.Rows.Add(new Row(3, LayoutUnitType.Star));
            rowIndex += 1;
            //TextArea fullCustomerAddress = new TextArea("Name\nStreet Address\nCity\nState/Province, Country\nPOSTAL CODE")
            TextArea fullCustomerAddress = new TextArea("Tap\nBuild Address\nbutton\nto create address\nhere.")
            {
                ColumnIndex = 0,
                ColumnSpan = 2,  // fill the row
                Font = smallItalic,
                RowIndex = rowIndex,
            };
            cell.AddChild(fullCustomerAddress);

            // Handlers the Country Select list
            // When country selected, populate states/provinces list.  Will be positioned at first (zeroth) item.
            countries.SelectionChanged += (sender, args) =>
            {
                states.Items = stateMapper[countries.SelectedIndex];
                states.SelectedIndex = 0;
            };

            cell.Rows.Add(new Row(1, LayoutUnitType.Star));
            rowIndex += 1;
            Button tapToApprove = new Button("Build Address")
            {         
                BackgroundColor = Color.Cyan,
                ColumnIndex = 1,
                Font = smallItalic,
                RowIndex = rowIndex,
            };
            cell.AddChild(tapToApprove);

            // When approved, put in TextBox
            tapToApprove.Clicked += (sender, args) =>
            {
                int ci = countries.SelectedIndex;
                if (states.SelectedIndex < 0 || ci < 0 ||
                    states.SelectedIndex > stateMapper[ci].Length) 
                    return;  // ignore badness

                fullCustomerAddress.Text =  custNameBox.Text + "\n"
                    + custStreetAddress.Text + "\n"
                    + cityName.Text + ", " + stateMapper[ci][states.SelectedIndex] + " " + postalCode.Text + "\n"
                    + countryList[ci].ToUpper();

            };
                
            return cell;
        }                
//        public const string RowKey = "SessionRowKey";
//        public const string ColumnKey = "SessionColumnKey";

    }
}

