﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFactr.UI;

namespace ViewChoices
{
    public class HtmlBasedView : BrowserView<string>
    {
        protected override void OnRender()
        {
            // Load HTML into the View
            Title = "HTML code";
            LoadFromString(ViewTypesController.HtmlMarkup);
        }

        public HtmlBasedView()
        {
            HeaderColor = Color.Gray;
            TitleColor = Color.White;
        }
    }
}
