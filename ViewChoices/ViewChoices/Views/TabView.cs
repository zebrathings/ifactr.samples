﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI;
using iFactr.UI.Controls;

namespace ViewChoices
{
    class TabView : TabView<string>
    {
        // View-scoped body 
        protected override void OnRender()
        {
            Title = Model;

            TabItem t1 = new TabItem("MonoView", new Link(ListOfMonoViewChoices.Uri));
            TabItem t2 = new TabItem("Choices", new Link(ListOfChoicesLayer.Uri));

            TabItems = new [] { t1, t2 }  ;

        }
    }
}
