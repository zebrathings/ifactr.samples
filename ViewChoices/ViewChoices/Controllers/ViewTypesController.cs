﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoCross.Navigation;

namespace ViewChoices
{
    public class ViewTypesController : MXController<string>
    {
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            string typeOfView = null;
            if (parameters.TryGetValue(TypeOfViewKey, out typeOfView))
            {
                Model = typeOfView;
            }

            // return a string indicating the view to render
            if (string.IsNullOrEmpty(typeOfView)) throw new Exception("Can't determine view type choosen");

            return Model;
        }

        public static string Uri(string viewType) { return string.Format("{0}/{1}", baseUri, viewType); }
        public static string RegistrationUri = baseUri + "/{" + TypeOfViewKey + "}";
        private const string baseUri = "UriToTheViewTypeController";
        public const string TypeOfViewKey = "ViewTypeToLoad";

        public const string HtmlMarkup = "<html><body background-color:#FF000000><center>My text</center></body></html>";
    }
}
