﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoCross.Navigation;

namespace ViewChoices
{
    public class NativeApiViewController : MXController<string>
    {
        public override string Load(Dictionary<string, string> parameters)
        {
            // add busniss logic, and logic to initialize your model
            // example: Model = "My Model Text:";

            string name;
            if (parameters.TryGetValue(NameKey, out name))
            {
                Model = name;
            }
            else { Model = "not specified"; }

            // return a string indicating the view to render
            if (parameters.ContainsKey(ViewByTypePerspective))
            {
                return ViewByTypePerspective;
            }
            return ViewPerspective.Default;
        }

        public static string Uri(string name) { return string.Format("{0}/{1}", baseUri, name); }
        public static string UriToTypeView(string name) { return string.Format("{0}/{1}/Yes", baseUri, name); }
        public static string RegistrationUri = baseUri + "/{" + NameKey + "}";
        public static string RegistrationUriForTypeView = baseUri + "/{" + NameKey + "}/{" + ViewByTypePerspective + "}";
        private const string baseUri = "UriToTheNativeApiViewController";
        private const string ViewByTypePerspective = "KeyToViewByType";
        public const string NameKey = "Name";

        public const string LoadByTypePerspective = "LoadByType";
    }
}
