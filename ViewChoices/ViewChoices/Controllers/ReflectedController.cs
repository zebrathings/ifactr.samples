﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoCross.Navigation;
using ViewChoices.Models;

namespace ViewChoices
{
    public class ReflectedController : MXController<ReflectedModel>
    {
        public override string Load(string uri, Dictionary<string,string> parameters)
        {
            Model = new ReflectedModel() {
                InstanceDate = DateTime.Now,
                InstanceId = 42,
                InstanceName = Guid.NewGuid().ToString().Substring(0, 10).Trim('-'),
                InstanceEnum = ReflectedEnum.ValueN,
            };

            return ViewPerspective.Default;
        }

        public const string Uri = "UriToTheReflectedView";
    }
}
