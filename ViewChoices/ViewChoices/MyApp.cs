﻿using iFactr.UI;
using iFactr.Core;
using MonoCross.Navigation;

namespace ViewChoices
{
    public class MyApp : iApp
    {
        public override void OnAppLoad()
        {
            // Set the application title
            Title = "View Choices";

            // Add navigation mappings
            NavigationMap.Add(ListOfChoicesLayer.Uri, new ListOfChoicesLayer());
            NavigationMap.Add(ListOfMonoViewChoices.Uri, new ListOfMonoViewChoices());

            NavigationMap.Add(TheExampleLayer.Uri, new TheExampleLayer());
            NavigationMap.Add(ViewTypesController.RegistrationUri, new ViewTypesController());
            NavigationMap.Add(ReflectedController.Uri, new ReflectedController());

            // Add Views to the ViewMap
            MXContainer.AddView<string>(typeof(HtmlBasedView), "HTML");
            MXContainer.AddView<string>(typeof(BrowserView), "BrowserView");
            MXContainer.AddView<string>(typeof(CanvasView), "CanvasView");
            MXContainer.AddView<string>(typeof(GridView), "GridView");
            MXContainer.AddView<string>(typeof(ListView), "ListView");
            MXContainer.AddView<string>(typeof(ListViewWithCellRecycling), "ListViewWithCellRecycling");
            MXContainer.AddView<string>(typeof(TabView), "TabView");

            // Set default navigation URI
            NavigateOnLoad = ListOfChoicesLayer.Uri;

            if (iApp.Factory.Target == MobileTarget.Android)
                Style.HeaderTextColor = Color.Black;
            else
            {
                Style.HeaderColor = Color.Gray;
                Style.HeaderTextColor = Color.White;
            }
        }
    }
}