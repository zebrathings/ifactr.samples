﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MonoCross.Navigation;
using ViewChoices;

using System.Windows.Media;
using iFactr.WinPhone;
using iFactr.UI;

namespace WinPhone.Container.Views
{
    public class WinPhoneBrowserView : BrowserView<string>
    {
        protected override void OnRender()
        {
            LoadFromString(ViewTypesController.HtmlMarkup);
        }
    }
}