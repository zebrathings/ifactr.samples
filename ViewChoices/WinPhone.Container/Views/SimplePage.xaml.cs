﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using MonoCross.Navigation;

namespace WinPhone.Container.Views
{
    public partial class SimplePage : PhoneApplicationPage, IMXView<string>
    {
        public SimplePage()
        {
            InitializeComponent();
        }

        public void Render()
        {
            //PageTitle.Text = Model;
        }

        public object GetModel() { return Model; }
        public Type ModelType { get { return typeof(string); } }
        public void SetModel(object model) { Model = model as string; }
        public string Model { get; set; }
    }
}