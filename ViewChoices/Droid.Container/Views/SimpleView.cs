using System;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using iFactr.Droid;
using MonoCross.Navigation;
using View = Android.Views.View;

namespace ViewChoices
{
    public class SimpleView : BaseFragment, IMXView<string>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = new FrameLayout(Activity);
            view.SetBackgroundColor(Color.Red);
            var label = new TextView(Activity) { Text = "NativeView", Gravity = GravityFlags.Center, };
            label.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
            label.SetTextColor(Color.White);
            view.AddView(label, FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
            return view;
        }

        public string Model { get; set; }

        public override Type ModelType { get { return typeof(string); } }
    }
}