﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.Core.Utilities;
using iFactr.UI;

namespace ControlSampler
{
    // Converters are what allow Bindings to bind two properties of different types.
    // If no converter is specified for a binding, the binding will attempt to convert the value on its own.
    public class DoubleToFontConverter : IValueConverter
    {
        // The Convert method is called when the source property has changed and the target property is being updated.
        public object Convert(object value, Type targetType, object parameter)
        {
            var propertyInfo = Device.Reflector.GetProperty(parameter.GetType(), "Font");
            if (propertyInfo != null)
            {
                string valueString = value.ToString();
                var font = (Font)propertyInfo.GetValue(parameter);
                font.Size = valueString == string.Empty ? Font.PreferredLabelFont.Size : double.Parse(valueString);
                if (font.Size == 0)
                {
                    font.Size = Font.PreferredLabelFont.Size;
                }
                return font;
            }

            return value;
        }

        // The ConvertBack method is called when the target property has changed and the source property is being updated.
        public object ConvertBack(object value, Type targetType, object parameter)
        {
            if (value is Font)
            {
                return ((Font)value).Size;
            }

            return value;
        }
    }
}
