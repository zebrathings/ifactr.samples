﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;

namespace ControlSampler
{
    // Converters are what allow Bindings to bind two properties of different types.
    // If no converter is specified for a binding, the binding will attempt to convert the value on its own.
    public class StringToThicknessConverter : IValueConverter
    {
        // The Convert method is called when the source property has changed and the target property is being updated.
        public object Convert(object value, Type targetType, object parameter)
        {
            var components = value.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s == "-" ? 0 : double.Parse(s)).ToArray();
            switch (components.Length)
            {
                case 0:
                    return new Thickness();
                case 1:
                    return new Thickness(components[0]);
                case 2:
                    return new Thickness(components[0], components[1]);
                case 3:
                    return new Thickness(components[0], components[1], components[2], components[2]);
                default:
                    return new Thickness(components[0], components[1], components[2], components[3]);
            }
        }

        // The ConvertBack method is called when the target property has changed and the source property is being updated.
        public object ConvertBack(object value, Type targetType, object parameter)
        {
            if (value is Thickness)
            {
                var thickness = (Thickness)value;
                return string.Format("{0},{1},{2},{3}", thickness.Left, thickness.Top, thickness.Right, thickness.Bottom);
            }

            return value;
        }
    }
}
