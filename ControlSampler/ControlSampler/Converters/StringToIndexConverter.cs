﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;

namespace ControlSampler
{
    // Converters are what allow Bindings to bind two properties of different types.
    // If no converter is specified for a binding, the binding will attempt to convert the value on its own.
    public class StringToIndexConverter : IValueConverter
    {
        // The Convert method is called when the source property has changed and the target property is being updated.
        public object Convert(object value, Type targetType, object parameter)
        {
            string valueString = value.ToString();
            int index = valueString == string.Empty ? 0 : int.Parse(valueString);
            if (parameter != null)
            {
                string paramString = parameter.ToString();
                if (paramString == "Span")
                {
                    index = Math.Max(index, 1);
                }
                else if (paramString == "Row")
                {
                    index++;
                }
            }
            return index;
        }

        // The ConvertBack method is called when the target property has changed and the source property is being updated.
        public object ConvertBack(object value, Type targetType, object parameter)
        {
            if (value is int)
            {
                int index = (int)value;
                if (parameter != null && parameter.ToString() == "Row")
                {
                    index--;
                }
                return value.ToString();
            }

            return value;
        }
    }
}
