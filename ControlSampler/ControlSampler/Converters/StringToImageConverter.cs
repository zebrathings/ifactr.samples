﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    // Converters are what allow Bindings to bind two properties of different types.
    // If no converter is specified for a binding, the binding will attempt to convert the value on its own.
    public class StringToImageConverter : IValueConverter
    {
        // The Convert method is called when the source property has changed and the target property is being updated.
        public object Convert(object value, Type targetType, object parameter)
        {
            string valueString = value.ToString();
            if (iApp.File.Exists(valueString))
            {
                return new Image(valueString);
            }

            return null;
        }

        // The ConvertBack method is called when the target property has changed and the source property is being updated.
        public object ConvertBack(object value, Type targetType, object parameter)
        {
            var image = value as Image;
            return image == null ? value : image.FilePath;
        }
    }
}
