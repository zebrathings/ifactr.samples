﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.UI.Controls;
using iFactr.UI.Reflection;

namespace ControlSampler
{
    // Models that apply the ViewAttribute will reflect on their properties to generate UI elements.
    // This only happens if there isn't a view mapped to the perspective that is returned by the controller.
    // There are various attributes that can be used to customize the way the view is generated,
    // and most of the attributes have named parameters for even further customization.
    // All of these attributes reside within the iFactr.UI.Reflection namespace.
    [View]

    // The SectionAttribute marks the beginning of a new section within a ListView.
    [Section(HeaderText = "Available Control Types")]
    public class ControlTypes : List<ControlType>
    {
    }

    public class ControlType
    {
        /// <summary>
        /// Gets or sets the name to use when displaying the control type to the user.
        /// </summary>
        // The GroupAttribute gathers all of the properties with the same group ID and places them inside of the same cell.
        // In this example, DisplayName and ShortDescription are in the same group.  Since they are both string properties
        // without a ControlAttribute, they will both be represented by Labels.  The reflective UI builder will choose to
        // use a ContentCell to display these labels; the value of DisplayName will be assigned to the TextLabel and the
        // value of ShortDescription will be assigned to the SubtextLabel.
        [Group("1")]

        // The SelectableAttribute will assign a Link object to the NavigationLink property of the cell that contains the UI element.
        // You may notice the curly braces around 'Type.Name'.  These indicate a property path.  These properties will be evaluated
        // when the view or cell is generated, and the string will be substituted for the property's value.
        // In this case, the Type property in this class will be evaluated followed by the Name property of the Type class.
        // The substring '{Type.Name}' will then be replaced by the value of the Name property.
        // Property paths can be used on any named parameter that takes a string.
        [Selectable("ControlTypes/{Type.Name}")]

        // The ImageAttribute means that you wish to display an image next to the UI element representing this property.
        // Notice the property path pointing to the Image property.  The path to the image will evaluate to the value of the Image property.
        [Image("{Image}")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the path to an image to display alongside the display name.
        /// </summary>
        // The SkipAttribute excludes this property from the UI.
        [Skip]
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets a short version of the control's description.
        /// </summary>
        [Group("1")]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Gets or sets the type of the control.
        /// </summary>
        [Skip]
        public Type Type { get; set; }

        public ControlType(Type type, string displayName)
        {
            Type = type;
            DisplayName = displayName;
        }
    }
}
