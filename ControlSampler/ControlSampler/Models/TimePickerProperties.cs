﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class TimePickerProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Time Picker"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(TimePicker); }
        }

        public TimePickerProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Background Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the background content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });

            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Foreground Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the foreground content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var fontGroup = new PropertyGroup("Font");
            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Name",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToFontConverter),
                Description = "The name of the font that the current time value should be rendered with.  If the specified font is not available on the target platform, the system default will be used.",
                Hint = Font.PreferredDateTimePickerFont.Name
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Size",
                InputType = InputType.String,
                OutputType = typeof(double),
                ConverterType = typeof(DoubleToFontConverter),
                Hint = Font.PreferredDateTimePickerFont.Size.ToString("F1"),
                Description = "The size of the text characters that display the current time value.",
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Formatting",
                InputType = InputType.Enum,
                OutputType = typeof(FontFormatting),
                ConverterType = typeof(EnumToFontConverter),
                Description = "Optional font characteristics such as Bold and Italic.  If the font does not provide a bold or italic variant, it will render with normal formatting."
            });
            Add(fontGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Time",
                InputType = InputType.String,
                OutputType = typeof(DateTime?),
                ConverterType = typeof(StringToDateTimeConverter),
                BindingMode = BindingMode.TwoWay,
                DefaultValue = DateTime.Now,
                Hint = DateTime.Today.ToString("t"),
                Description = "The current time value of the picker."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Time Format",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(EmptyToNullConverter),
                Hint = "hh:mm:ss tt",
                Description = "The format in which the picker should display its current time value."
            });
            Add(valueGroup);
        }
    }
}
