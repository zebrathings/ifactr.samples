﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.Core.Utilities;
using iFactr.UI;
using iFactr.Utilities;

namespace ControlSampler
{
    public abstract class ControlProperties : List<PropertyGroup>
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public abstract Type Type { get; }

        public ControlProperties()
        {
            var layoutGroup = new PropertyGroup("Layout");
            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Column Index",
                InputType = InputType.String,
                OutputType = typeof(int),
                ConverterType = typeof(StringToIndexConverter),
                Description = "The zero-based index of the column that the upper-right corner of the control should reside in.  By default, the framework will attempt to find a suitable column for you; this feature is known as auto-layout."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Column Span",
                InputType = InputType.String,
                OutputType = typeof(int),
                ConverterType = typeof(StringToIndexConverter),
                ConverterParameter = "Span",
                Description = "The number of columns that the control should span across, beginning at the column specified by ColumnIndex and spanning to the right."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Row Index",
                InputType = InputType.String,
                OutputType = typeof(int),
                ConverterType = typeof(StringToIndexConverter),
                ConverterParameter = "Row",
                Description = "The zero-based index of the row that the upper-right corner of the control should reside in.  By default, the framework will attempt to find a suitable row for you; this feature is known as auto-layout."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Row Span",
                InputType = InputType.String,
                OutputType = typeof(int),
                ConverterType = typeof(StringToIndexConverter),
                ConverterParameter = "Span",
                Description = "The number of rows that the control should span across, beginning at the row specified by RowIndex and spanning downward."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Horizontal Alignment",
                InputType = InputType.Enum,
                OutputType = typeof(HorizontalAlignment),
                DefaultValue = GetDefaultValue("Horizontal Alignment"),
                Description = "The method in which the control should align itself within the columns that contain it.  Stretch alignment will only affect controls with stretchable width."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Vertical Alignment",
                InputType = InputType.Enum,
                OutputType = typeof(VerticalAlignment),
                DefaultValue = GetDefaultValue("Vertical Alignment"),
                Description = "The method in which the control should align itself within the rows that contain it.  Stretch alignment will only affect controls with stretchable height."
            });

            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Margin",
                InputType = InputType.String,
                OutputType = typeof(Thickness),
                ConverterType = typeof(StringToThicknessConverter),
                Hint = "Left,Top,Right,Bottom",
                Description = "The amount of spacing between the edges of the control and other UI elements around it.  Positive values move outward from the edges of the control and negative values move inward.  The control may shrink or expand depending on its margin and alignments, as well as the columns and rows that it resides in."
            });
            Add(layoutGroup);
        }

        protected virtual object GetDefaultValue(string propertyName)
        {
            switch (propertyName)
            {
                case "Horizontal Alignment":
                    return "Left";
                case "Vertical Alignment":
                    return "Top";
                default:
                    return null;
            }
        }
    }

    public class PropertyGroup
    {
        /// <summary>
        /// Gets or sets the display name of the property group.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a collection of the properties that are associated with the property group.
        /// </summary>
        public List<ControlProperty> Properties { get; private set; }

        public PropertyGroup(string name)
        {
            Name = name;
            Properties = new List<ControlProperty>();
        }
    }

    public class ControlProperty
    {
        /// <summary>
        /// Gets or sets the mode that should be used when the property is being bound to a UI control.
        /// </summary>
        public BindingMode BindingMode { get; set; }

        /// <summary>
        /// Gets or sets the type of IValueConverter that should be used when the property is being bound to a UI control.
        /// </summary>
        public Type ConverterType { get; set; }

        /// <summary>
        /// Gets or sets the parameter that should be passed to the converter when the property is being bound to a UI control.
        /// </summary>
        public object ConverterParameter { get; set; }

        /// <summary>
        /// Gets or sets the value that the property should begin with.
        /// </summary>
        public object DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets a string describing the function of the property.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the placeholder text to use when the UI control for the property is a TextBox.
        /// </summary>
        public string Hint { get; set; }

        /// <summary>
        /// Gets or sets the type of user input that the property is expecting.
        /// </summary>
        public InputType InputType { get; set; }

        /// <summary>
        /// Gets the input restriction to impose in the UI control for the property when the control is a TextBox.
        /// This is determined by the property's OutputType.
        /// </summary>
        public string InputRestriction { get; private set; }

        /// <summary>
        /// Gets or sets the display name of the property.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the property.
        /// </summary>
        public Type OutputType
        {
            get { return outputType; }
            set
            {
                outputType = value;

                var typeSwitch = new TypeSwitch(outputType);
                typeSwitch.Case<int>(a => InputRestriction = @"^[\d]*$")
                    .Case<double>(a => InputRestriction = @"^[\.\d]*$")
                    .Case<Color>(a => InputRestriction = @"^[\da-fA-F]{0,8}$")
                    .Case<Thickness>(a => InputRestriction = @"^[-\,\.\d]*$");
            }
        }
        private Type outputType;

        public ControlProperty()
        {
            BindingMode = BindingMode.OneWayToTarget;
        }
    }

    /// <summary>
    /// Describes the type of user input a property is expecting.
    /// </summary>
    public enum InputType
    {
        /// <summary>
        /// The property is expecting a string; a TextBox should be used.
        /// </summary>
        String,
        /// <summary>
        /// The property is expecting a enum; a SelectList should be used.
        /// </summary>
        Enum,
        /// <summary>
        /// The property is expecting a boolean value; a Switch should be used.
        /// </summary>
        Boolean
    }
}
