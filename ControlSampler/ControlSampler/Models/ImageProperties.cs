using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class ImageProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Image"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(Image); }
        }

        public ImageProperties()
        {
            var layoutGroup = this.First();
            layoutGroup.Properties.Add(new ControlProperty()
            {
                Name = "Stretch",
                InputType = InputType.Enum,
                OutputType = typeof(ContentStretch),
                Description = "The method in which the image will be stretched when it occupies a space larger than its dimensions.  The Fill option will not maintain aspect ratio, but the other options will.  When occupying a space smaller than its dimensions, the image will always scale down."
            });

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "File Path",
                InputType = InputType.String,
                OutputType = typeof(string),
                DefaultValue = "Monocross.png",
                Hint = "myImage.png",
                Description = "The path to the file containing the image data to render."
            });
            Add(valueGroup);
        }
    }
}
