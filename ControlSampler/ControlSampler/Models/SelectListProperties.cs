﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class SelectListProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Select List"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(SelectList); }
        }

        public SelectListProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Background Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the background content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });

            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Foreground Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the foreground content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var fontGroup = new PropertyGroup("Font");
            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Name",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToFontConverter),
                Description = "The name of the font that the currently selected value should be rendered with.  If the specified font is not available on the target platform, the system default will be used.",
                Hint = Font.PreferredTextBoxFont.Name
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Size",
                InputType = InputType.String,
                OutputType = typeof(double),
                ConverterType = typeof(DoubleToFontConverter),
                Hint = Font.PreferredTextBoxFont.Size.ToString("F1"),
                Description = "The size of the text characters that display the currently selected value.",
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Formatting",
                InputType = InputType.Enum,
                OutputType = typeof(FontFormatting),
                ConverterType = typeof(EnumToFontConverter),
                Description = "Optional font characteristics such as Bold and Italic.  If the font does not provide a bold or italic variant, it will render with normal formatting."
            });
            Add(fontGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Items",
                InputType = InputType.String,
                OutputType = typeof(object[]),
                ConverterType = typeof(StringToArrayConverter),
                DefaultValue = "Item 1,Item 2",
                Hint = "Item 1,Item 2,Item 3",
                Description = "The items from which the user may make a selection.  These items can be any object; the value shown in the list is the string that is returned from the object's ToString method.  In this demonstration, the objects are strings delimited by a comma."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Selected Item",
                InputType = InputType.String,
                OutputType = typeof(object),
                BindingMode = BindingMode.TwoWay,
                Description = "The currently selected item."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Selected Index",
                InputType = InputType.String,
                OutputType = typeof(int),
                ConverterType = typeof(StringToIndexConverter),
                BindingMode = BindingMode.TwoWay,
                Description = "The zero-based index of the currently selected item."
            });
            Add(valueGroup);
        }

        protected override object GetDefaultValue(string propertyName)
        {
            switch (propertyName)
            {
                case "Horizontal Alignment":
                    return "Stretch";
                case "Vertical Alignment":
                    return "Top";
                default:
                    return null;
            }
        }
    }
}
