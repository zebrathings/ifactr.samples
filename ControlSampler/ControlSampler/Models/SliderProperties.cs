﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class SliderProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Slider"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(Slider); }
        }

        public SliderProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Maximum Track Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the track between the thumb and the maximum value as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });

            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Minimum Track Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the track between the thumb and the minimum value as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Value",
                InputType = InputType.String,
                OutputType = typeof(double),
                BindingMode = BindingMode.TwoWay,
                DefaultValue = "0",
                Description = "The current numerical value of the slider represented by the position of the thumb."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Max Value",
                InputType = InputType.String,
                OutputType = typeof(double),
                DefaultValue = 100,
                Description = "The numerical value represented by the end of the track.  The user cannot move the thumb past this point."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Min Value",
                InputType = InputType.String,
                OutputType = typeof(double),
                DefaultValue = 0,
                Description = "The numerical value represented by the beginning of the track.  The user cannot move the thumb past this point."
            });
            Add(valueGroup);
        }

        protected override object GetDefaultValue(string propertyName)
        {
            switch (propertyName)
            {
                case "Horizontal Alignment":
                    return "Stretch";
                case "Vertical Alignment":
                    return "Top";
                default:
                    return null;
            }
        }
    }
}
