﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class ButtonProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Button"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(Button); }
        }

        public ButtonProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Background Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the background content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });

            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Foreground Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the foreground content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var fontGroup = new PropertyGroup("Font");
            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Name",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToFontConverter),
                Description = "The name of the font that the title should be rendered with.  If the specified font is not available on the target platform, the system default will be used.",
                Hint = Font.PreferredButtonFont.Name
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Size",
                InputType = InputType.String,
                OutputType = typeof(double),
                ConverterType = typeof(DoubleToFontConverter),
                Hint = Font.PreferredButtonFont.Size.ToString("F1"),
                Description = "The size of the title characters.",
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Formatting",
                InputType = InputType.Enum,
                OutputType = typeof(FontFormatting),
                ConverterType = typeof(EnumToFontConverter),
                Description = "Optional font characteristics such as Bold and Italic.  If the font does not provide a bold or italic variant, it will render with normal formatting."
            });
            Add(fontGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Title",
                InputType = InputType.String,
                OutputType = typeof(string),
                DefaultValue = "My Button",
                Hint = "My Title",
                Description = "The text that the button should display."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Image",
                InputType = InputType.String,
                OutputType = typeof(Image),
                ConverterType = typeof(StringToImageConverter),
                Hint = "myImage.png",
                Description = "An image to show in place of the title."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Navigation Link",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToLinkConverter),
                Hint = "http://www.google.com/",
                Description = "A navigation URI or web address to navigate to when the button is pressed."
            });
            Add(valueGroup);
        }
    }
}
