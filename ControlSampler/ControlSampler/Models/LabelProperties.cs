﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class LabelProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Label"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(Label); }
        }

        public LabelProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Foreground Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the text as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var fontGroup = new PropertyGroup("Font");
            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Name",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToFontConverter),
                Description = "The name of the font that the text should be rendered with.  If the specified font is not available on the target platform, the system default will be used.",
                Hint = Font.PreferredLabelFont.Name
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Size",
                InputType = InputType.String,
                OutputType = typeof(double),
                ConverterType = typeof(DoubleToFontConverter),
                Hint = Font.PreferredLabelFont.Size.ToString("F1"),
                Description = "The size of the text characters.",
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Formatting",
                InputType = InputType.Enum,
                OutputType = typeof(FontFormatting),
                ConverterType = typeof(EnumToFontConverter),
                Description = "Optional font characteristics such as Bold and Italic.  If the font does not provide a bold or italic variant, it will render with normal formatting."
            });
            Add(fontGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Text",
                InputType = InputType.String,
                OutputType = typeof(string),
                DefaultValue = "My Label",
                Hint = "My Text",
                Description = "The text that the label should display."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Lines",
                InputType = InputType.String,
                OutputType = typeof(int),
                Hint = "0",
                Description = "The maximum number of visible lines of text.  Any text that does not fit will be truncated.  A value of zero (0) will mean that there is no line limit."
            });
            Add(valueGroup);
        }

        protected override object GetDefaultValue(string propertyName)
        {
            switch (propertyName)
            {
                case "Horizontal Alignment":
                    return "Stretch";
                case "Vertical Alignment":
                    return "Stretch";
                default:
                    return null;
            }
        }
    }
}
