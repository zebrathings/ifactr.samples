﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    public class TextBoxProperties : ControlProperties
    {
        /// <summary>
        /// Gets the display name for the control.
        /// </summary>
        public override string Name
        {
            get { return "Text Box"; }
        }

        /// <summary>
        /// Gets the type of the control.
        /// </summary>
        public override Type Type
        {
            get { return typeof(TextBox); }
        }

        public TextBoxProperties()
        {
            var colorGroup = new PropertyGroup("Colors");
            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Background Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the background content as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });

            colorGroup.Properties.Add(new ControlProperty()
            {
                Name = "Foreground Color",
                InputType = InputType.String,
                OutputType = typeof(Color),
                ConverterType = typeof(StringToColorConverter),
                Hint = "FFFFFFFF",
                Description = "The color of the text as an 8-character hex value (FFFFFFFF).  Providing a value of all zeros (00000000) will set the color to the system default."
            });
            Add(colorGroup);

            var fontGroup = new PropertyGroup("Font");
            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Name",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToFontConverter),
                Description = "The name of the font that the text should be rendered with.  If the specified font is not available on the target platform, the system default will be used.",
                Hint = Font.PreferredTextBoxFont.Name
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Size",
                InputType = InputType.String,
                OutputType = typeof(double),
                ConverterType = typeof(DoubleToFontConverter),
                Hint = Font.PreferredTextBoxFont.Size.ToString("F1"),
                Description = "The size of the text characters.",
            });

            fontGroup.Properties.Add(new ControlProperty()
            {
                Name = "Font Formatting",
                InputType = InputType.Enum,
                OutputType = typeof(FontFormatting),
                ConverterType = typeof(EnumToFontConverter),
                Description = "Optional font characteristics such as Bold and Italic.  If the font does not provide a bold or italic variant, it will render with normal formatting."
            });
            Add(fontGroup);

            var valueGroup = new PropertyGroup("Value");
            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Text",
                InputType = InputType.String,
                OutputType = typeof(string),
                BindingMode = BindingMode.TwoWay,
                DefaultValue = "My Text",
                Hint = "My Text",
                Description = "The text within the text box control."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Placeholder",
                InputType = InputType.String,
                OutputType = typeof(string),
                Description = "Optional text to display within the text box when it has no value."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Expression",
                InputType = InputType.String,
                OutputType = typeof(string),
                ConverterType = typeof(StringToRegExConverter),
                Hint = @"^[\d]*$",
                Description = "A regular expression used for limiting the type of characters that the user is able to input."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Text Alignment",
                InputType = InputType.Enum,
                OutputType = typeof(TextAlignment),
                Description = "The manner in which the text is aligned within the text box."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Text Completion",
                InputType = InputType.Enum,
                OutputType = typeof(TextCompletion),
                Description = "Determines the auto-completion behavior of the text box."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Keyboard Type",
                InputType = InputType.Enum,
                OutputType = typeof(KeyboardType),
                Description = "The type of soft keyboard to display when the text box receives focus.  This will not have any affect on platforms that use hardware keyboards."
            });

            valueGroup.Properties.Add(new ControlProperty()
            {
                Name = "Is Enabled",
                InputType = InputType.Boolean,
                OutputType = typeof(bool),
                DefaultValue = true,
                Description = "Whether or not the text box should accept user input."
            });
            Add(valueGroup);
        }

        protected override object GetDefaultValue(string propertyName)
        {
            switch (propertyName)
            {
                case "Horizontal Alignment":
                    return "Stretch";
                case "Vertical Alignment":
                    return "Top";
                default:
                    return null;
            }
        }
    }
}
