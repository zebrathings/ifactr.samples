﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFactr.UI.Controls;
using MonoCross.Navigation;

namespace ControlSampler
{
    // Controllers are responsible for setting the model data with which the view is going to render.
    // This is where you should place any application logic that is not specifically UI logic.
    public class ControlTypesController : MXController<ControlTypes>
    {
        // The Load method gets called when you navigate to the controller.
        // The uri parameter will contain the uri that was navigated to in order to get to this controller.
        // The parameters dictionary will contain any Link parameters and submission values from the previous view.
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            // The most important thing to do in the Load method is to set your Model.
            // This instance will automatically be passed to the view.
            Model = new ControlTypes();

            // We populate our model with any data we think will be relevant to the view when it is being rendered.
            #region Label Control
            var labelControlType = new ControlType(typeof(Label), "Label");
            labelControlType.Image = "icn-label.png";
            labelControlType.ShortDescription = "Displays a single string of read-only text.";

            Model.Add(labelControlType);
            #endregion

            #region Image Control
            var imageControlType = new ControlType(typeof(Image), "Image");
            imageControlType.Image = "icn-image.png";
            imageControlType.ShortDescription = "Renders imaging data from a file.";

            Model.Add(imageControlType);
            #endregion

            #region Button Control
            var buttonControlType = new ControlType(typeof(Button), "Button");
            buttonControlType.Image = "icn-button.png";
            buttonControlType.ShortDescription = "Executes application-defined logic after a click or a tap.";

            Model.Add(buttonControlType);
            #endregion

            #region Date Picker Control
            var dateControlType = new ControlType(typeof(DatePicker), "Date Picker");
            dateControlType.Image = "icn-datepicker.png";
            dateControlType.ShortDescription = "Allows the user to select from a range of day, month, and year values.";

            Model.Add(dateControlType);
            #endregion

            #region Time Picker Control
            var timeControlType = new ControlType(typeof(TimePicker), "Time Picker");
            timeControlType.Image = "icn-timepicker.png";
            timeControlType.ShortDescription = "Allows the user to select from a range of hour and minute values.";

            Model.Add(timeControlType);
            #endregion

            #region Text Box Control
            var textBoxControlType = new ControlType(typeof(TextBox), "Text Box");
            textBoxControlType.Image = "icn-textbox.png";
            textBoxControlType.ShortDescription = "Allows the user to input a single line of text.";

            Model.Add(textBoxControlType);
            #endregion

            #region Password Box Control
            var passwordBoxControlType = new ControlType(typeof(PasswordBox), "Password Box");
            passwordBoxControlType.Image = "icn-passwordbox.png";
            passwordBoxControlType.ShortDescription = "Allows the user to input a single line of hidden text.";

            Model.Add(passwordBoxControlType);
            #endregion

            #region Text Area Control
            var textAreaControlType = new ControlType(typeof(TextArea), "Text Area");
            textAreaControlType.Image = "icn-textarea.png";
            textAreaControlType.ShortDescription = "Allows the user to input multiple lines of text.";

            Model.Add(textAreaControlType);
            #endregion

            #region Select List Control
            var selectListControlType = new ControlType(typeof(SelectList), "Select List");
            selectListControlType.Image = "icn-selectlist.png";
            selectListControlType.ShortDescription = "Allows the user to select from a list of predefined values.";

            Model.Add(selectListControlType);
            #endregion

            #region Slider Control
            var sliderControlType = new ControlType(typeof(Slider), "Slider");
            sliderControlType.Image = "icn-slider.png";
            sliderControlType.ShortDescription = "Allows the user to select from a range of numerical values.";

            Model.Add(sliderControlType);
            #endregion

            #region Switch Control
            var switchControlType = new ControlType(typeof(Switch), "Switch");
            switchControlType.Image = "icn-switch.png";
            switchControlType.ShortDescription = "Allows the user to toggle between a true and a false state.";

            Model.Add(switchControlType);
            #endregion

            // When we are finished populating our model, we return a view perspective.
            // This perspective is used along with the model type to determine which view in our view map we should render.
            // The view perspective can be any string value.  The only important thing to remember is that you
            // must also have a corresponding view map entry.  If a view cannot be found, an exception will be thrown.
            return ViewPerspective.Default;

            // If your model is decorated with a ViewAttribute, it can be used in lieu of an actual view.
            // Whenever a perspective is returned that does not have a corresponding view in the view map,
            // the model will be used to generate a view from scratch.  This is called Reflective UI.
            // To see this in action, return the below perspective instead of the above one.
            // There is no view mapped to the "Reflection" perspective, so the model will be used to generate a view.
            //return "Reflection";
        }
    }
}
