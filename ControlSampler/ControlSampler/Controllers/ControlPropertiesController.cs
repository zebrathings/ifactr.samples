﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;
using MonoCross.Navigation;

namespace ControlSampler
{
    // Controllers are responsible for setting the model data with which the view is going to render.
    // This is where you should place any application logic that is not specifically UI logic.
    public class ControlPropertiesController : MXController<ControlProperties>
    {
        // The Load method gets called when you navigate to the controller.
        // The uri parameter will contain the uri that was navigated to in order to get to this controller.
        // The parameters dictionary will contain any Link parameters and submission values from the previous view.
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            // The most important thing to do in the Load method is to set your Model.
            // This instance will automatically be passed to the view.
            string controlType = parameters.GetValueOrDefault("ControlType");
            switch (controlType)
            {
                case "Label":
                    Model = new LabelProperties();
                    break;
                case "Image":
                    Model = new ImageProperties();
                    break;
                case "Button":
                    Model = new ButtonProperties();
                    break;
                case "DatePicker":
                    Model = new DatePickerProperties();
                    break;
                case "TimePicker":
                    Model = new TimePickerProperties();
                    break;
                case "TextBox":
                    Model = new TextBoxProperties();
                    break;
                case "PasswordBox":
                    Model = new PasswordBoxProperties();
                    break;
                case "TextArea":
                    Model = new TextAreaProperties();
                    break;
                case "SelectList":
                    Model = new SelectListProperties();
                    break;
                case "Slider":
                    Model = new SliderProperties();
                    break;
                case "Switch":
                    Model = new SwitchProperties();
                    break;
                default:
                    throw new ArgumentException(string.Format("Unknown control type '{0}'", controlType));
            }

            // When we are finished populating our model, we return a view perspective.
            // This perspective is used along with the model type to determine which view in our view map we should render.
            // The view perspective can be any string value.  The only important thing to remember is that you
            // must also have a corresponding view map entry.  If a view cannot be found, an exception will be thrown.
            return ViewPerspective.Default;
        }
    }
}
