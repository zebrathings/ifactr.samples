﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    // This a simple demonstration of a GridView.  These views act as full-screen grids for laying out UI elements like labels and buttons.
    // Scrolling can be enabled via the HorizontalScrollingEnabled and VerticalScrollingEnabled properties, but both are turned off by default.
    // Be aware that GridViews are not virtualized and will render all of their content at once.
    public class ControlView : GridView<Control>
    {
        public ControlView()
        {
            Columns.AddRange(new[] { Column.OneStar, Column.OneStar, Column.OneStar });
            Rows.AddRange(new[] { Row.AutoSized, Row.OneStar, Row.OneStar, Row.OneStar });

            // The description that the label provides is a bit inaccurate.
            // The grid is actually 4x3, not 3x3, but the label takes up the first row and we will offset the control's RowIndex by 1 to account for this.
            AddChild(new Label()
            {
                ID = "desc",
                ColumnSpan = 3,
                Margin = new Thickness(0, 0, 0, Thickness.BottomMargin),
                Text = "The control is rendered below in a 3x3 grid.  As you change its properties, the control will update itself to reflect those changes.  Some properties may require the layout of the grid to be invalidated before their changes can be seen.  To force a layout refresh of the grid, press the Invalidate button."
            });

            // Menus provide support functions through clickable buttons.
            // How they render differs for each platform, but they are functionally the same.
            var menuButton = new MenuButton("Invalidate");
            menuButton.Clicked += (o, e) => Render();
            Menu = new Menu(menuButton);
        }

        protected override void OnRender()
        {
            // Our model in this specific case is an actual UI element.  The only thing we need to do is add it to the grid.
            Title = Model.GetType().Name;
            AddChild(Model);
        }
    }
}
