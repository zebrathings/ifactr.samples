﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    // This is a simple example of a ListView displaying several items with a uniform layout.
    // This list is virtualized which means the cells will only be requested when they are ready to be rendered.
    // Because of this, it is important to never assume that a cell will be requested at a particular point in time.
    public class ControlTypesView : ListView<ControlTypes>
    {
        // This method gets called when the view is being presented.
        // This is where you should put any logic specific to the view.
        // Logic specific to an individual cell should go into the OnCellRequested method instead.
        protected override void OnRender()
        {
            Title = "Control Types";

            // Sections can have Headers and Footers
            Sections[0].Header = new SectionHeader("Available Control Types");

            // The ItemCount of a Section determines how many cells will be placed within that section.
            Sections[0].ItemCount = Model.Count;
        }

        // This method is used for cell recycling and gets called before a cell is requested.
        // The section and index parameters describe the position of the cell that is about to be requested.
        // When you return an ID that is shared with a previously created cell that has been moved off-screen,
        // the off-screen cell will be passed to the OnCellRequested method for reuse.  You can then update
        // the reused cell instead of creating a new one.  Recycled cells will maintain their state from
        // before, so remember to update everything that may have changed since the cell was created.
        // In particular, make sure you nullify things that are no longer used and reset any events with the
        // NullifyEvents method.  It is important that you use cell recycling whenver possible.
        // However, only cells that share the same layout should share reuse IDs.  If two cells have
        // different layouts or different controls inside of them, they should use different IDs.
        protected override int OnItemIdRequested(int section, int index)
        {
            // Since all of the cells in this list share the same layout, we will use the same ID for all of them.
            return 1;
        }

        // This method gets called whenever a cell is ready to be rendered and requires you to return a new or recycled cell.
        // The section and index parameters describe the position of the cell within the list.
        // The recycledCell parameter will contain any cell that is ready to be reused.  If there are no qualifying cells
        // to be reused, the parameter will be null.  Platforms that don't support recycling will always pass in null.
        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            // Our model is sorted in the order in which we want the data to appear on screen.
            // Using the index parameter, we can easily retrieve the data for the cell being requested.
            var controlType = Model[index];

            // We first check to see if we have a recycled cell and if it's the correct type.  If not, we make a new one.
            // There are two types of basic cells: GridCells and RichContentCells.
            // GridCells let you place and position any number of controls using columns, rows, and margins.
            // RichContentCells parse HTML/XML into a document that is able to render it.
            // There are also two types of specialized cells: ContentCells and HeaderedControlCells.
            // These cells are grids as well, but they come with predefined controls that may behave differently depending on the platform.
            // This behavior could range from unique styling to differences in layout.  These cells excel in providing an experience tailored
            // to each platform without you having to write conditional code.
            var cell = recycledCell as ContentCell ?? new ContentCell();

            cell.MaxHeight = double.PositiveInfinity;

            // Once we have a cell, whether it be recycled or new, we use the provided labels to show our text.
            cell.TextLabel.Text = controlType.DisplayName;
            cell.SubtextLabel.Text = controlType.ShortDescription;
            cell.SubtextLabel.Lines = 2;
            cell.Image.FilePath = controlType.Image;

            // We also set a navigation link on the cell.  Whenever the user selects the cell, the link will be navigated to.
            cell.NavigationLink = new Link("ControlTypes/" + controlType.Type.Name);

            // When we are finished with the cell, we return it to the framework for rendering.
            return cell;
        }
    }
}
