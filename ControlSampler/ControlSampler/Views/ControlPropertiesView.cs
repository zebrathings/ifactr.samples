﻿using System;
using System.Linq;
using iFactr.Core;
using iFactr.UI;
using iFactr.UI.Controls;

namespace ControlSampler
{
    // This example is a bit more involved than our ControlTypesView class.  Be sure to look at that class for ListView basics.
    // Once again we will be generating a virtualized list of cells from data stored within our model.
    // This time around, our model is an instance of ControlProperties which is set inside of our ControlPropertiesController class.
    [PreferredPane(Pane.Master)]
    public class ControlPropertiesView : ListView<ControlProperties>
    {
        protected override void OnRender()
        {
            Title = Model.Name + " Properties";

            // We are creating an instance of our model type and storing it in our Session dictionary.
            // This is so that the same instance can be shared by both this view and the control view.
            // This makes it possible to bind to the object while displaying it in a different view.
            var control = (Control)Activator.CreateInstance(Model.Type);
            iApp.Session["Control"] = control;

            // If our control type is a Button, we're going to make it do something when the user taps on it.
            var button = control as Button;
            if (button != null)
            {
                button.Clicked += (o, e) => new Alert("You have clicked the button!", "Button Clicked", AlertButtons.OK).Show();
            }

            // A key difference between our OnRender method here and our OnRender method in
            // the ControlTypesView class is that we are now using multiple sections.
            // Our model is a list of property groups with each group containing a certain number of properties.
            // Each group gets its own section complete with a section header displaying the group's name.
            for (int i = 0; i < Model.Count; i++)
            {
                var propertyGroup = Model[i];
                Sections[i].Header = new SectionHeader();
                Sections[i].Header.Text = propertyGroup.Name;
                Sections[i].ItemCount = propertyGroup.Properties.Count;

                // We've given certain properties default values.
                // We want to find those and apply them to our control immediately.
                foreach (var property in propertyGroup.Properties.Where(p => p.DefaultValue != null))
                {
                    string name = property.Name.Replace(" ", "");
                    var converter = property.ConverterType == null ?
                        null : (IValueConverter)Activator.CreateInstance(property.ConverterType);

                    // SetValue is typically used to only set a property on certain platforms.
                    // Here we're using it to set a property for which we only have a name, but we're setting it on every platform.
                    // MobileTarget is a flags enum, so int.MaxValue means set all of the flags to true.
                    control.SetValue(name, property.DefaultValue, converter, (MobileTarget)int.MaxValue);

                    // Bindings can be incredibly useful tools.  When you set one up, the framework will listen for changes to the bound property
                    // and pass on those changes to the target property.  If the two properties cannot be converted, you can define a value
                    // converter that the framework will use to turn the value of the changed property into something that the target property can accept.
                    // This sample application has several converters that you can look at for examples.
                    // Bindings can be either one-directional or two-directional, but they default to one-directional from source to target.
                    control.SetBinding(new Binding(name, "DefaultValue")
                    {
                        Source = property,
                        Mode = BindingMode.OneWayToSource,
                        ValueConverter = converter
                    });
                }
            }

            // We're also initiating a navigation to our control view so that both views will be presented together.
            iApp.Navigate("Control");
        }

        // This method gets called whenever the view is being moved away from, whether it be due to a forward
        // navigation, a back navigation, or a tab switch.  The method expects a boolean to be returned;
        // a value of true means proceed with the navigation and a value of false means stop the navigation.
        // Here we are using the method to pop the control view when the back button is pressed.
        protected override bool ShouldNavigateFrom(Link link, NavigationType type)
        {
            var detail = PaneManager.Instance.FromNavContext(Pane.Detail, 0);
            if (type == NavigationType.Back && detail != null)
            {
                detail.PopToRoot();
            }
            return true;
        }

        protected override int OnItemIdRequested(int section, int index)
        {
            // Not all of our cells are going to be the same in this list.
            // Some will have TextBoxes, others will have SelectLists, and still others will have Switches.
            // We only want cells that will have the same type of control to have the same reuse ID,
            // so we are returning an integer based on the control that is going to go into the cell.
            return (int)Model[section].Properties[index].InputType;
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            // First we check to see if we have a recycled cell that we can use.
            var cell = recycledCell as GridCell;
            if (cell == null)
            {
                // If we don't have a reusable cell, we need to create one.
                cell = new GridCell();

                // We only add columns and rows to our cell if it's a new instance.
                // Recycled cells will already have these columns and rows added to them from when they were first created.
                cell.Columns.AddRange(new[] { Column.AutoSized, Column.OneStar });
                cell.Rows.AddRange(new[] { Row.AutoSized, Row.AutoSized });
            }

            cell.SelectionStyle = SelectionStyle.None;

            // A recycled cell will still have any event handlers attached.
            // Since these handlers more than likely have incorrect references, we will clear these handlers with the NullifyEvents method.
            cell.NullifyEvents();

            // We now extract the data we are going to be using from our model.
            // The section parameter maps to the property group that we want, and the
            // index parameter maps to the individual property that we are dealing with.
            var property = Model[section].Properties[index];

            // Since this may be a recycled cell, we may already have a label waiting to be used.
            // If that's the case, we want to reuse that label instead of creating a new one.
            // In this scenario, we're using the ID 'name' in order to ensure that we're getting the correct label.
            // Only controls with the ID 'name' will be returned by the GetChild method.
            var nameLabel = cell.GetChild("name") as Label;
            if (nameLabel == null)
            {
                // Since GetChild did not return an already existing control for us to use, we must create one.
                nameLabel = new Label()
                {
                    ID = "name",
                    Lines = 1,
                    VerticalAlignment = VerticalAlignment.Center
                };

                // A new control needs to be added to the cell.  We wouldn't need to do this if GetChild returned
                // something for us since that control would already be a part of the cell.
                cell.AddChild(nameLabel);
            }

            // Regardless of whether this is a new label or a recycled one, we want to set the text.
            nameLabel.Text = property.Name + ":";

            // We have two labels we want to set.  The second one is done in the same way as the first.
            // We use a different ID ('desc') so that we don't end up with the wrong label when using GetChild.
            var descriptionLabel = cell.GetChild("desc") as Label;
            if (descriptionLabel == null)
            {
                descriptionLabel = new Label()
                {
                    ID = "desc",
                    Font = Font.PreferredSmallFont,
                    ForegroundColor = Color.Gray,
                    Margin = new Thickness(0, Thickness.SmallVerticalSpacing),
                    RowIndex = 1,
                    ColumnSpan = 2
                };
                cell.AddChild(descriptionLabel);
            }
            descriptionLabel.Text = property.Description;

            Control controlViewModel = (Control)iApp.Session["Control"];

            string propertyName = null;
            // The way we're handling Font in this scenario is a bit different from the other properties.
            // The reason is that Font is a single property but we're representing it with three different inputs (Name, Size, Formatting).
            // When the value of one of these inputs is changed, we will take the existing font from our ControlView control,
            // modify the appropriate field, and set it back to the control.
            if (property.Name.StartsWith("Font"))
            {
                propertyName = "Font";
                property.ConverterParameter = controlViewModel;
            }
            else
            {
                // For all of the other properties, we only need to remove any spaces.
                // Once that is done, we will have a property name that we can bind to.
                propertyName = property.Name.Replace(" ", "");
            }

            // Our input control can be any of three different types: a TextBox, a SelectList, or a Switch.
            // If we get back a control that is not of the type we are expecting, we will want to remove it.
            // Since we made sure that reuse IDs are only shared by cells with the same type of input control,
            // this won't actually happen, but it's still good practice to follow.
            Control control = cell.GetChild<Control>("input");
            switch (property.InputType)
            {
                case InputType.String:
                    var textBox = control as TextBox;
                    if (textBox == null)
                    {
                        if (control != null)
                        {
                            cell.RemoveChild(control);
                        }

                        control = textBox = new TextBox();
                    }
                    else
                    {
                        // Remember that recycled controls maintain their state from before.
                        // Always be sure to reset anything that may have changed during the last time the cell was requested.
                        textBox.ClearAllBindings();
                    }

                    textBox.Expression = property.InputRestriction;
                    textBox.Placeholder = property.Hint;
                    textBox.Text = property.DefaultValue == null ? null : property.DefaultValue.ToString();

                    // By using a binding, changes to the text box's value will automatically
                    // transfer to the appropriate property on our sample control.
                    controlViewModel.SetBinding(new Binding(propertyName, "Text")
                    {
                        Source = textBox,
                        Mode = property.BindingMode,
                        ValueConverter = property.ConverterType == null ? null : (IValueConverter)Activator.CreateInstance(property.ConverterType),
                        ValueConverterParameter = property.ConverterParameter
                    });

                    // Finally, as a means of improving user experience, we'll set focus to the text box if any part of the cell is pressed.
                    // This is ok because it's the only input control that the cell contains, and the cell isn't going to perform any navigations,
                    // so it's pretty safe to assume that the user wants to interact with the text box if they're pressing on the cell.
                    cell.Selected += (sender, e) => textBox.Focus();
                    break;
                case InputType.Enum:
                    var selectList = control as SelectList;
                    if (selectList == null)
                    {
                        if (control != null)
                        {
                            cell.RemoveChild(control);
                        }

                        control = selectList = new SelectList();
                    }
                    else
                    {
                        // Remember that recycled controls maintain their state from before.
                        // Always be sure to reset anything that may have changed during the last time the cell was requested.
                        selectList.ClearAllBindings();
                    }

                    // A SelectList can accept a collection of objects of any type.
                    // When the SelectList is rendered, it displays these objects as the strings that are returned from each object's ToString method.
                    selectList.Items = Enum.GetNames(property.OutputType);

                    // Again, remember that recycled controls maintain their state from before.
                    // We want to ensure that the selected item is the one that we want, not the one that it may have been before being recycled.
                    if (property.DefaultValue == null)
                    {
                        selectList.SelectedIndex = 0;
                    }
                    else
                    {
                        selectList.SelectedItem = property.DefaultValue.ToString();
                    }

                    // By using a binding, changes to the select list's value will automatically
                    // transfer to the appropriate property on our sample control.
                    controlViewModel.SetBinding(new Binding(propertyName, "SelectedIndex")
                    {
                        Source = selectList,
                        Mode = property.BindingMode,
                        ValueConverter = property.ConverterType == null ? null : (IValueConverter)Activator.CreateInstance(property.ConverterType),
                        ValueConverterParameter = property.ConverterParameter
                    });

                    // Finally, as a means of improving user experience, we'll open the select list if any part of the cell is pressed.
                    // This is ok because it's the only input control that the cell contains, and the cell isn't going to perform any navigations,
                    // so it's pretty safe to assume that the user wants to interact with the select list if they're pressing on the cell.
                    cell.Selected += (sender, e) => selectList.ShowList();
                    break;
                case InputType.Boolean:
                    var boolSwitch = control as Switch;
                    if (boolSwitch == null)
                    {
                        if (control != null)
                        {
                            cell.RemoveChild(control);
                        }

                        control = boolSwitch = new Switch();
                    }
                    else
                    {
                        // Remember that recycled controls maintain their state from before.
                        // Always be sure to reset anything that may have changed during the last time the cell was requested.
                        boolSwitch.ClearAllBindings();
                    }

                    boolSwitch.Value = property.DefaultValue == null ? false : (bool)property.DefaultValue;

                    // By using a binding, changes to the switch's value will automatically
                    // transfer to the appropriate property on our sample control.
                    controlViewModel.SetBinding(new Binding(propertyName, "Value")
                    {
                        Source = boolSwitch,
                        Mode = property.BindingMode,
                        ValueConverter = property.ConverterType == null ? null : (IValueConverter)Activator.CreateInstance(property.ConverterType),
                        ValueConverterParameter = property.ConverterParameter
                    });
                    break;
            }

            // Lastly, we will set a few properties that are common to all of the input controls.
            // These include setting the ID, Margin, RowIndex, and anything else we want to apply to every input control.
            // After running the application, you may notice that the control appears to the right of our name label
            // even though we did not set the control's ColumnIndex to 1.  This is due to auto layout.
            // Auto layout will attempt to find an appropriate row and/or column to place the control while making
            // sure that it does not overlap any other children in the cell.  As long as the child does not specify
            // a column index, auto layout will look for a column for you.  The same applies to rows as well.
            // We are setting a RowIndex for this particular control because we want to make sure it goes above our description label.
            // If we didn't set the RowIndex, auto layout would try to place the control below the description label since
            // we've added the input control to the cell after we've added the label.
            if (control != null)
            {
                control.ID = "input";
                control.Margin = new Thickness(Thickness.LargeHorizontalSpacing, 0, 0, 0);
                control.RowIndex = 0;

                if (!cell.Children.Contains(control))
                {
                    cell.AddChild(control);
                }
            }

            // When we are finished with the cell, we return it to the framework for rendering.
            return cell;
        }
    }
}