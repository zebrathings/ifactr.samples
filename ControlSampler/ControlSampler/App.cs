﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFactr.Core;
using iFactr.UI.Controls;
using MonoCross.Navigation;

namespace ControlSampler
{
    public class App : iApp
    {
        // Add code to initialize your application here.  For more information, see http://www.ifactr.com
        public override void OnAppLoad()
        {
            // Set the application title
            Title = "Control Sampler";

            // Add navigation mappings
            NavigationMap.Add("ControlTypes", new ControlTypesController());
            NavigationMap.Add("ControlTypes/{ControlType}", new ControlPropertiesController());
            NavigationMap.Add("Control", new ControlController());

            MXContainer.AddView<ControlTypes>(typeof(ControlTypesView));
            MXContainer.AddView<ControlProperties>(typeof(ControlPropertiesView));
            MXContainer.AddView<Control>(typeof(ControlView));

            // Set default navigation URI
            NavigateOnLoad = "ControlTypes";
        }
    }
}