using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using iFactr.Core;
using iFactr.Touch;

namespace ControlSampler
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
            TouchFactory.Initialize();
            TouchFactory.TheApp = new ControlSampler.App();
            
            iApp.Navigate(App.Instance.NavigateOnLoad);

			return true;
		}

		public override void OnActivated (UIApplication application)
		{
		}      
	}
}
