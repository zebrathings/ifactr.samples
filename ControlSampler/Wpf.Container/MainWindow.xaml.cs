﻿using System.Windows;
using System.Windows.Data;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace Classic.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            WpfFactory.Initialize();
            WpfFactory.TheApp = new ControlSampler.App();
            Content = WpfFactory.Instance.MainWindow;
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
        }
    }
}