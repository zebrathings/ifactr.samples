using Android.App;
using Android.OS;
using Android.Views;
using iFactr.Core;
using iFactr.Droid;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            if (!DroidFactory.IsInitialized)
            {
                DroidFactory.MainActivity = this;
                DroidFactory.TheApp = new AbstractApplication.App();
                iApp.OnLayerLoadComplete += layer => DroidFactory.Instance.OutputLayer(layer);
                iApp.Navigate(iApp.Instance.NavigateOnLoad);
            }
            else DroidFactory.MainActivity = this;
            base.OnCreate(bundle);
        }
    }
}