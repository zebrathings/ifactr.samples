﻿using System.Reflection;
using Android.App;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("TemplateApp")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ITR Mobility")]
[assembly: AssemblyProduct("TemplateApp")]
[assembly: AssemblyCopyright("Copyright © 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


[assembly: Application(Label = "@string/app_name",
                                Icon = "@drawable/icon_launcher",
                                Theme = "@android:style/Theme.Holo.Light.DarkActionBar")]