﻿using iFactr.Webkit;
using System.Web.Mvc;

namespace Webkit.Container.Controllers
{
    /// <summary>
    /// App Controller for Webkit
    /// </summary>
    public class AppController : Controller
    {

        private bool _loading = true;
        // GET: /Render/
        public void Render(string url, string args)
        {
            // set callback handler
            WebkitFactory.OnOutputLayerComplete += () => { OutputLayerComplete(); };

            // navigate to url
            if (url == null)
            {
                url = WebkitFactory.TheApp.NavigateOnLoad;
            }

            // Initiates navigation to the specified layer & performs any validation on POST.
            WebkitFactory.Instance.Navigate(url, this.Request);

            // wait for web factory to output the layer
            while (_loading)
                System.Threading.Thread.Sleep(100);
        }

        private void OutputLayerComplete()
        {
            _loading = false;
        }
    }
}