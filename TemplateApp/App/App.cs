﻿using iFactr.Core;
using AbstractApplication.Layers;

namespace AbstractApplication
{
    public class App : iApp
    {
        // Add code to initialize your application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            //iApp.Encryption.Required = true;
            //iApp.Encryption.Key = "ND5lYPo4czOk5ZT7KNmU2Q==";
            //iApp.Encryption.Salt = System.Text.Encoding.ASCII.GetBytes("test1234");

            // Set the application title
            Title = " ";

            // Optional: Set the application style
            //Style.HeaderColor = new Style.Color("8C0E12");

            // Add navigation mappings
            NavigationMap.Add("", new Tabs());

            NavigationMap.Add("List", new List());

            NavigationMap.Add("Form", new Form());
            NavigationMap.Add("Form/Submit", new FormSubmit());

			NavigationMap.Add("Block", new Block());


            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }
}