﻿using System;
using System.Collections.Generic;

using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace AbstractApplication
{
    class Form : FormLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Form";
            if (iApp.Factory.Target == MobileTarget.WinPhone)
            {
                LayerStyle.HeaderTextColor = iFactr.Core.Styles.Style.Color.Black;
                LayerStyle.TextColor = iFactr.Core.Styles.Style.Color.Black;
            }

			Fieldset fieldset = new Fieldset() {
				Header = "Header"
			};
            fieldset.Fields.Add(new SelectListField("DolorSelectica", "First", "Second", "Third") {
				Label = "Select List"
			});
            fieldset.Fields.Add(new TextField("Text") {
				Label = "Text Field"
			});
            fieldset.Fields.Add(new TextField("Text2") {
				Label = "Text Field"
			});
			fieldset.Fields.Add(new BoolField("Bool") {
				Label = "Boolean"
			});
			fieldset.Fields.Add(new EmailField("Email") {
				Label = "Email"
			});
			fieldset.Fields.Add(new NumericField("Number") {
				Label = "Number"
			});
			fieldset.Fields.Add(new MultiLineTextField("MultiLineTex") {
				Label = "Muti Line Text"
			});
			fieldset.Fields.Add(new NavigationField("Navigation", "Link-Address") {
				Label = "Navigation",
			});
			fieldset.Fields.Add(new DateField("Date") {
				Label = "Date Field",
				Type = DateField.DateType.Date
			});
			fieldset.Fields.Add(new DateField("Time") {
				Label = "Time Field",
				Type = DateField.DateType.Time
			});
			fieldset.Fields.Add(new ImagePickerField("Image") {
				Label = "Image Field",
			});
			fieldset.Fields.Add(new SliderField("Slider") {
				Label = "Slider Field",
			});

			Fieldsets.Add(fieldset);

			fieldset = new Fieldset() {
				Header = "Button"
			};
			fieldset.Fields.Add(new ButtonField("Button", "Form/Submit") {
				Action = Button.ActionType.Submit,
				Label = "Button",
				ConfirmationText = "You tapped the button, are you certain you want to complete the action?"				
			});
			Fieldsets.Add(fieldset);


			string navIconPath = System.IO.Path.Combine(iApp.Factory.ApplicationPath, "Nav");

			ActionButton = new SubmitButton("Submit", "Form/Submit") {
				Image = new Icon(System.IO.Path.Combine(navIconPath, "as-save.png"))
			};
        }
    }
}
