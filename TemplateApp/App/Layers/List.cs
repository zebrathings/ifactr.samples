﻿using System;
using System.IO;
using System.Collections.Generic;

using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace AbstractApplication.Layers
{
    class List : Layer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            // setup up the layer
            Title = "List";
            if (iApp.Factory.Target == MobileTarget.WinPhone)
            {
                LayerStyle.HeaderTextColor = iFactr.Core.Styles.Style.Color.Black;
                LayerStyle.TextColor = iFactr.Core.Styles.Style.Color.Black;
            }

            string iconPath = Path.Combine(iApp.Factory.ApplicationPath, "Icons");

            // add items to an iList
            iList list = new iList();

            // build the first iItem
            var item = new iItem("Block", "An Item and some SubText");
            item.Subtext = "some subtext";
            item.Icon = new Icon(Path.Combine(iconPath, "icn-1.png"));
            list.Add(item);

            // add additional items
            list.Add(new iItem("Block",  "An additional items & subtext", "item 2 subtext") { Icon = new Icon(Path.Combine(iconPath, "icn-3.png")) });
			list.Add(new iItem("Block", "Item 3", "item 3 subtext") { Icon = new Icon(Path.Combine(iconPath, "icn-3.png")) });
            list.Add(new iItem("Block", "Item 4", "subtext for item 4") { Icon = new Icon(Path.Combine(iconPath, "icn-4.png")) });
            list.Add(new iItem("Block", "Item 5", "additional items") { Icon = new Icon(Path.Combine(iconPath, "icn-5.png")) });
            list.Add(new iItem("Block", "Item 6", "a final item") { Icon = new Icon(Path.Combine(iconPath, "icn-6.png")) });

            // add the iList to the layer
            Items.Add(list); 
        }
    }
}
