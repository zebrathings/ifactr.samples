using System;
using System.Collections.Generic;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace AbstractApplication.Layers
{
	public class Block : iLayer, IDetailLayer
	{
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "iBlock";
			
			iBlock block = new iBlock();
			
			block.Header = "Header";
			
			block.AppendHeading("Heading");
			block.AppendSubHeading("Sub Heading");
			block.AppendSecondarySubHeading("Secondary Heading");
			
			block.AppendHorizontalRule();
			block.AppendLine("Append Line");
			block.AppendLine ();
			block.AppendLine("lorem ipsum dolor sit amet, consectetur adipiscing elit.");
			block.AppendItalicLine("Nullam id tristique dolor.");
			block.AppendBoldLine("Curabitur mauris eros.");
			block.AppendBoldItalicLine("Curabitur est metus.");
			block.AppendLabeledTextLine("Label", "Integer diam nibh.");
			
			block.AppendHorizontalRule();
			block.AppendLine("Images");
			block.InsertImageFloatLeft("Images/image.png");
			block.AppendLine ();
			block.InsertImageFloatRight("Images/image.png");
			block.AppendLine ();
			block.AppendLine ();
			block.AppendLine ();
			block.AppendLine ();
			block.AppendLine ();
			block.AppendLine ();
			
			block.AppendHorizontalRule();
			block.AppendLine("Links");
			block.InsertImageLink ("Images/image.png", "http://www.ifactr.com");
			block.AppendLine ();
			block.AppendLine ();
			block.AppendLink (new Link("http://www.ifactr.com") { Text = "ifactr.com" });
			block.AppendLine ();
			block.AppendLine ();
			block.AppendTelephone("(800) 265-1620");
			
			Items.Add(block);
		}
	}
}