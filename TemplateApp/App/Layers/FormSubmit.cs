﻿using System;
using System.Collections.Generic;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace AbstractApplication
{
    class FormSubmit : Layer, IDetailLayer
    {
		public bool IsFullscreen { get; set; } 

        public override void Load(Dictionary<string, string> parameters)
        {        
        	// configure layer
            Title = "Form Details";
            IsFullscreen = false;

			// add a block to the layer
            iBlock block = new iBlock();
            foreach (string key in parameters.Keys)
            {
                block.AppendLine(string.Format("{0}: {1}", key, parameters[key]));
            }
            Items.Add(block);
            
            // add an action button to the layer
			ActionButtons.Add(new Button("Home", "Form"));            
        }
    }
}
