﻿using System;
using System.Collections.Generic;

using iFactr.Core;
using iFactr.Core.Layers;

namespace AbstractApplication.Layers
{
    public class Tabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = " ";

			string navPath = System.IO.Path.Combine(iApp.Factory.ApplicationPath, "Nav");

			TabItems.Add(new Tab("List", "List", System.IO.Path.Combine(navPath, "nav1.png")));
			TabItems.Add(new Tab("Form", "Form", System.IO.Path.Combine(navPath, "nav2.png")));
            TabItems.Add(new Tab("Block", "Block", System.IO.Path.Combine(navPath, "nav3.png")));
        }
    }
}
