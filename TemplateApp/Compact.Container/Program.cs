﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;

namespace Compact.Container
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            iApp.OnLayerLoadComplete += ilayer =>
            {
                CompactFactory.Instance.OutputLayer(ilayer);
            };
            CompactFactory.Initialize();
            CompactFactory.TheApp = new AbstractApplication.App();
            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.Form);
        }
    }
}