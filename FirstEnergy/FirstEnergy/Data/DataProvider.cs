﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using iFactr.Data;
using iFactr.Core.Layers;

using TemplateApp.Shared.Model;

namespace TemplateApp.Data
{
  public class DataProvider : Provider<Informatio>
  {
    // ctor: provide base uri, item endpoint, list endpoint, parameters
    public DataProvider() : base("http://localhost/TemplateApp", "informatio/{id}.xml", "informatio.xml", "id") { }

    // bind data: creates and returns a single list item.
    public override iItem BindData(string baseUri, Informatio item)
    {
      return new SubtextItem(string.Format("{0}/{1}", baseUri, item.ID), item.Text, item.Subtext);
    }

    // map params: maps uri parameters to object properties
    public override Dictionary<string, string> MapParams(Informatio item)
    {
      Dictionary<string,string> retval = new Dictionary<string,string>();
      retval.Add("id", item.ID);
      return retval;
    }

    // on transaction complete: logic to expire cache for refresh
    protected override void OnTransactionComplete(RestfulObject<Informatio> item, string verb)
    {
        this.ExpireList();
        this.ExpireCacheList();
    }
  }
}
