
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;

namespace TemplateApp
{
	class Pledge : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "Pledge";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			iMenu name = new iMenu();
			name.Add (new SubtextBelowAndBesideItem("", "John A. Smith", "1234-123456789", "2012 Pledge = $250"));

			iMenu pledge = new iMenu();
			pledge.Add (new iItem("Pledge/Agency", "Add Agency"));
			pledge.Add (new SubtextBelowAndBesideItem("", "Region Name", "Agency Name", "$250"));
			pledge.Add (new RightSubtextItem("", "Total Pledge", "$250"));

			Fieldset actions = new Fieldset();			
			actions.Fields.Add (new SelectListField("PaymentType", new string[]{ "Payroll Deduction", "Check", "Cash", "I do not wish to contribute" }) { Label = "Payment Type" });
			actions.Fields.Add (new SelectListField("MakeAnonymous", new string[]{ "Yes", "No" }) { Label = "Make Anonymous" });
			actions.Fields.Add (new SelectListField("OptOutAgency", new string[]{ "Agency 1", "Agency 2", "Agency 3" }) { Label = "Opt-Out Agency" });
			actions.Fields.Add(new MultiLineTextField("Comments", 10) { Placeholder = "Enter comments here" });
			actions.Fields.Add (new DrawingField("Signature", "signature", DrawingType.Signature, null, false));

//			actions.Add (new RightSubtextItem("", "Payment Type", "required"));
//			actions.Add (new RightSubtextItem("", "Make Anonymous", "No"));
//			actions.Add (new RightSubtextItem("", "Opt-Out Agency", "optional"));
//			actions.Add (new RightSubtextItem("", "Comments", "optional"));
//			actions.Add (new RightSubtextItem("", "Signature", "optional"));

			Items.Add (name);
			Items.Add (pledge);
			Items.Add (actions);
		}
	}
}

