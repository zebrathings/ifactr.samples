
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.Core.Controls;

namespace TemplateApp
{
	class AgencyEdit : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "AddEditAgency";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			Fieldset agency = new Fieldset();
			agency.Fields.Add (new SelectListField("Region", new string[]{ "Region 1", "Region 2", "Region 3" }) { Label = "Region" });
			agency.Fields.Add (new SelectListField("Agency", new string[]{ "Agency 1", "Agency 2", "Agency 3" }) { Label = "Agency" });
			agency.Fields.Add (new LabelField("AnnualPledge") { Label = "Annual Pledge", Text = "$1" });

			Fieldset delete = new Fieldset();
			delete.Fields.Add(new ButtonField("Delete", "Agency/Delete") { Action = Button.ActionType.Delete, ConfirmationText = "You are removing this agency." });
			
			Items.Add(agency);
			Items.Add(delete);
		}
	}
}

