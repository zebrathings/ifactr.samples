
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace TemplateApp
{
	class Admin : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "Admin";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			iMenu actions = new iMenu();
			actions.Add (new iItem("Admin/Pledges", "Pledges"));
			actions.Add (new iItem("Admin/Message", "Message"));
			actions.Add (new iItem("Admin/Regions", "Regions"));
			actions.Add (new iItem("", "Settings"));

			Items.Add(actions);
		}
	}
}

