
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace TemplateApp
{
	class RegionDetail : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "RegionDetail";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			iMenu region = new iMenu();
			region.Add (new RightSubtextItem(null, "Name", "Region Name"));

			iMenu agencies = new iMenu();
			agencies.Add(new iItem("", "Agency 1"));
			agencies.Add(new iItem("", "Agency 2"));
			agencies.Add(new iItem("", "Agency 3"));
			agencies.Add(new iItem("", "Agency 4"));
			agencies.Add(new iItem("", "Agency 4"));

			Items.Add (region);
			Items.Add (agencies);
		}
	}
}

