
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;

namespace TemplateApp
{
	class Message : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "Message";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			Fieldset email = new Fieldset();
			email.Header = "Email";
			email.Fields.Add(new SelectListField("To", new string[]{"Everyone", "Those that haven't signed", "Those who pledged"}) { Label = "To", Placeholder = "required"});
			email.Fields.Add(new SelectListField("Pledge Period", new string[]{"Open", "Closed"}) { Label = "Pledge Period" });

			Fieldset message = new Fieldset();
			message.Header = "Message";
			message.Fields.Add(new MultiLineTextField("Message", 10) { Placeholder = "Enter message here" });

			Items.Add (email);
			Items.Add (message);
		}
	}
}

