
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace TemplateApp
{
	class RegionList : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "RegionList";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			SearchList regions = new SearchList();
			regions.Add (new iItem("", "Add All"));
			regions.Add (new iItem("", "Remove All"));
			regions.Add (new iItem("", "Region 1") { Button = new Button(string.Empty, "Admin/Regions/Detail") } );
			regions.Add (new iItem("", "Region 2") { Button = new Button(string.Empty, "Admin/Regions/Detail") } );
			regions.Add (new iItem("", "Region 3") { Button = new Button(string.Empty, "Admin/Regions/Detail") } );
			regions.Add (new iItem("", "Region 4") { Button = new Button(string.Empty, "Admin/Regions/Detail") } );
			regions.Add (new iItem("", "Region 5") { Button = new Button(string.Empty, "Admin/Regions/Detail") } );

			Items.Add (regions);
		}
	}
}

