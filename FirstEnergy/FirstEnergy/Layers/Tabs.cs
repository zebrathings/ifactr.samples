﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace TemplateApp.Layers
{
    public class Tabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = " ";
            LayerStyle.LayerItemBackgroundColor = new iFactr.Core.Styles.Style.Color("8C0E12");
            LayerStyle.LayerBackgroundImage = "Background/DashboardBG.jpg";
            TabItems.Add(new Tab("Pledge", "Pledge", "Nav/nav1.png"));
            TabItems.Add(new Tab("Admin", "Admin", "Nav/nav2.png"));
        }
    }
}
