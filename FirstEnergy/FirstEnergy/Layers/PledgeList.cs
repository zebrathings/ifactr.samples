
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace TemplateApp
{
	class PledgeList : Layer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			// TODO: set layer title.  This text will appear on the header of your layer.
			Title = "PledgeList";
            
			// TODO: construct your layer from iFactr.Core controls.
			// For more information, see: http://www.factr.com/documentation.
			iList payable = new iList();
			payable.Header = "$125 - Accounts Payable";
			payable.Add (new SubtextBelowAndBesideItem(null, "Jon Doe", "01.12.2013", "$50"));
			payable.Add (new SubtextBelowAndBesideItem(null, "Jim Smith", "01.12.2013", "$50"));
			payable.Add (new SubtextBelowAndBesideItem(null, "Beth Potter", "01.13.2013", "$25"));

			iList marketing = new iList();
			marketing.Header = "$325 - Marketing";
			marketing.Add (new SubtextBelowAndBesideItem(null, "Jon Doe", "01.12.2013", "$50"));
			marketing.Add (new SubtextBelowAndBesideItem(null, "Jim Smith", "01.12.2013", "$50"));
			marketing.Add (new SubtextBelowAndBesideItem(null, "Beth Potter", "01.13.2013", "$25"));

			Items.Add (payable);
			Items.Add (marketing);
		}
	}
}

