using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Styles;

using TemplateApp.Shared.Model;
using TemplateApp.Data;
using TemplateApp.Layers;

namespace TemplateApp
{
    public class App : iApp
    {
        public static List<Informatio> Data { get { return XmlDataStore.GetData(); } } 
        // Add code to initialize your application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            //iApp.Encryption.Required = true;
            //iApp.Encryption.Key = "ND5lYPo4czOk5ZT7KNmU2Q==";
            //iApp.Encryption.Salt = System.Text.Encoding.ASCII.GetBytes("test1234");

            //iApp.RequestInjectionHeaders.Add("SMSESSION","lqkhjfgoihaspgohldfjkghskldfjhg");

            // Set the application title
            Title = " ";

            // Optional: Set the application style
            //Style.HeaderColor = new Style.Color("8C0E12");

            // Add navigation mappings
            NavigationMap.Add("", new Tabs());

            NavigationMap.Add("Pledge", new Pledge());
			NavigationMap.Add("Pledge/Agency", new AgencyEdit());

			NavigationMap.Add("Admin", new Admin());
			NavigationMap.Add("Admin/Message", new Message());
			NavigationMap.Add("Admin/Pledges", new PledgeList());
			NavigationMap.Add("Admin/Regions", new RegionList());
			NavigationMap.Add("Admin/Regions/Detail", new RegionDetail());

            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }
}

