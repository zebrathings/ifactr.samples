﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;

#if WINDOWS_PHONE
using System.IO.IsolatedStorage;
#endif

#if NETFX_CORE
using Windows.Storage;
#endif

using TemplateApp.Shared.Model;


namespace TemplateApp.Data
{
    public class XmlDataStore
    {
#if DROID
		static string AppPath = "";
		public static Android.Content.Res.AssetManager AssetManager;	
#elif SILVERLIGHT || MONOTOUCH
		// with IsolatedStorage there is no ROOT path, only application files can be accessed
        static string AppPath = "";
#elif NETFX_CORE
        static string AppPath = Windows.ApplicationModel.Package.Current.InstalledLocation.Path + "\\Assets\\";
#else
        static string AppPath = Assembly.GetAssembly(typeof(XmlDataStore)).CodeBase.Substring(0, Assembly.GetAssembly(typeof(XmlDataStore)).CodeBase.LastIndexOf("/")).Replace("file://", "");
#endif

        // Data methods
        public static List<Informatio> GetData()
        {
            return GetData(string.Empty);
        }

        public static List<Informatio> GetData(string filter)
        {
            if (!string.IsNullOrWhiteSpace(filter))
			{
				return (from item in LoadData()
				        where item.Text.Contains(!string.IsNullOrWhiteSpace(filter) ? filter : item.Text) ||
				        item.Subtext.Contains(!string.IsNullOrWhiteSpace(filter) ? filter : item.Subtext) ||
				        item.TopText.Contains(!string.IsNullOrWhiteSpace(filter) ? filter : item.TopText) ||
				        item.BesideText.Contains(!string.IsNullOrWhiteSpace(filter) ? filter : item.BesideText)
				        select new Informatio()).ToList();
			}
			else
				return LoadData();
        }

        public static Informatio GetDatum(string id)
        {
            return LoadData().Where(obj => obj.ID == id).FirstOrDefault();
        }
        
        public static Informatio CreateDatum(Informatio instance)
        {
            List<Informatio> data = LoadData();

            data.Add(instance);
            SaveData(data);
            return instance;
        }

        public static Informatio UpdateDatum(Informatio instance)
        {
            List<Informatio> data = LoadData();
            if (data.Where(obj => obj.ID == instance.ID).Count() != 0)
                data.Remove(data.First(obj => obj.ID == instance.ID));
            data.Add(instance);
            SaveData(data);
            return instance;
        }
        
        public static void DeleteDatum(string id)
        {
            List<Informatio> data = LoadData();
            data.Remove(data.First(obj => obj.ID == id));
            SaveData(data);
        }

        // File system Access 
        static List<Informatio> LoadData()
        {
            //var retval = new List<Informatio>();
            //for (int i = 0; i < 100; i++)
            //{
            //    retval.Add(new Informatio());
            //}
            //SaveData(retval);
            //return retval;
            string dataFilePath = Path.Combine(AppPath, Path.Combine("Xml", "Informatio.xml"));
            XDocument loadedData = null;
#if DROID
            var stream = AssetManager.Open(dataFilePath);
            loadedData = XDocument.Load(stream);
#else
            try { loadedData = XDocument.Load(dataFilePath); }
            catch { loadedData = XDocument.Load("\\\\" + AppPath + "/Xml/Informatio.xml"); }
#endif
            using (var reader = loadedData.Root.CreateReader())
            {
                List<Informatio> list = (List<Informatio>)new XmlSerializer(typeof(List<Informatio>)).Deserialize(reader);
                return list;
            }
        }

#if NETFX_CORE
        static async void SaveData(List<Informatio> data)
#else
        static void SaveData(List<Informatio> data)
#endif
        {
            string dataFilePath = Path.Combine(AppPath, Path.Combine("Xml", "Informatio.xml"));            

#if NETFX_CORE
            var file = await StorageFile.GetFileFromPathAsync(dataFilePath);
            var stream = await file.OpenStreamForWriteAsync();
            new XmlSerializer(typeof(List<Informatio>)).Serialize(stream, data);
#else
            try
            {
                using (StreamWriter writer = new StreamWriter(dataFilePath))
                {
                    var serializer = new XmlSerializer(typeof(List<Informatio>));
                    serializer.Serialize(writer, data);
                }
            }
            catch 
            {
                dataFilePath = "\\\\" + AppPath + "/Xml/Informatio.xml";
                using (StreamWriter writer = new StreamWriter(dataFilePath))
                {
                    var serializer = new XmlSerializer(typeof(List<Informatio>));
                    serializer.Serialize(writer, data);
                } 
            }
#endif

        }
    }
}
