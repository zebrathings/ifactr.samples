﻿using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Utilities.Threading;
using iFactr.Metro;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TemplateApp.Modern
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            MetroFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => iApp.Thread.ExecuteOnMainThread(new ParameterDelegate((o) => MetroFactory.Instance.OutputLayer((iLayer)o)), layer);
            MetroFactory.TheApp = new TemplateApp.App();
            MetroFactory.TheApp.Style.LayerItemBackgroundColor = new iFactr.Core.Styles.Style.Color("8C0E12");
            iApp.Navigate(MetroFactory.TheApp.NavigateOnLoad);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
    }
}