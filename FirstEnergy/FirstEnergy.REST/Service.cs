﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Xml.Serialization;
using System.Web;

using TemplateApp.Data;
using TemplateApp.Shared.Model;

namespace TemplateApp.REST
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Service
    {
        #region XML Services

        [XmlSerializerFormat]
        [WebGet(UriTemplate = "informatio.xml?filter={filter}")]
        public List<Informatio> GetList(string filter)
        {
            return XmlDataStore.GetData(filter);
        }

        [XmlSerializerFormat]
        [WebGet(UriTemplate = "informatio/{id}.xml")]
        public Informatio GetItem(string id)
        {
            return XmlDataStore.GetDatum(id);
        }    
        
        [XmlSerializerFormat]
        [WebInvoke(UriTemplate = "informatio/{id}.xml", Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        public Informatio CreateItem(string id, Informatio instance)
        {
            return XmlDataStore.CreateDatum(instance);
        }

        [XmlSerializerFormat]
        [WebInvoke(UriTemplate = "customers/{id}.xml", Method = "PUT", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        public Informatio UpdateCompany(string id, Informatio instance)
        {
            return XmlDataStore.UpdateDatum(instance);

            // for demos
            //OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            //response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            //response.StatusDescription = "Access Denied.";
            //return null;
        }
        #endregion

        #region JSON Services

        [WebGet(UriTemplate = "informatio.json?filter={filter}", ResponseFormat = WebMessageFormat.Json)]
        public List<Informatio> GetListJson(string filter)
        {
            return XmlDataStore.GetData(filter);
        }

        [WebInvoke(UriTemplate = "informatio/{id}.json", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Informatio CreateItemJson(string id, Informatio instance)
        {
            return XmlDataStore.CreateDatum(instance);
        }

        [WebInvoke(UriTemplate = "informatio/{id}.json", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Informatio UpdateItemJson(string id, Informatio instance)
        {
            return XmlDataStore.UpdateDatum(instance);
        }

        #endregion

        [WebInvoke(UriTemplate = "customers/{id}", Method = "DELETE")]
        public void DeleteItem(string id)
        {
            XmlDataStore.DeleteDatum(id);
        }
    }
}
