using iFactr.Core;
using iFactr.Core.Styles;

namespace GenMed
{
    public class App : iApp
    {
        public override void OnAppLoad()
        {
            Title = "GenMed";

            // when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
            Style.HeaderColor = new Style.Color("#000a71a5");

            // Instantiate objects that will be used more than once during navigations.
            // There are some layers we navigate to with multiple URIs but which don't share the same instance because we want them to take different positions in the history stack
            Layers.Accounts.Search accountSearch = new Layers.Accounts.Search();
            Layers.Accounts.Detail accountDetail = new Layers.Accounts.Detail();

            Layers.Transactions.PendingOrders pendingOrders = new Layers.Transactions.PendingOrders();
            Layers.Transactions.Sell transactionsSell = new Layers.Transactions.Sell();
            Layers.Transactions.SellItemDetails transactionsSellItemDetails = new Layers.Transactions.SellItemDetails();

            Layers.Reporting.PoPending.Accounts reportAccounts = new Layers.Reporting.PoPending.Accounts();

            // Add navigation mappings.
            // Any part of the URI that is surrounded by curly braces will be a parameter passed along to the layer's Load method.
            // For example, navigating to Accounts/12345 will use the Accounts/{Account} URI and pass in a parameter with the key "Account" and the value "12345"
            NavigationMap.Add("MainTabs", new MainTabs());
            
            NavigationMap.Add("Accounts", accountSearch);
            NavigationMap.Add("Accounts/{Account}", accountDetail);
            NavigationMap.Add("Accounts/{Account}/ShipTo", new Layers.Accounts.DetailShipTo());
            NavigationMap.Add("Accounts/{Account}/Inventory", new Layers.Accounts.DetailInventory());
            NavigationMap.Add("Accounts/{Account}/SoldInventory", new Layers.Accounts.DetailSoldInventory());

            NavigationMap.Add("Region", new Layers.Regions.Regions());
            NavigationMap.Add("Regions/{Region}", new Layers.Regions.Districts());
            NavigationMap.Add("Regions/{Region}/All", accountSearch);
            NavigationMap.Add("Regions/{Region}/{District}", new Layers.Regions.Territories());
            NavigationMap.Add("Regions/{Region}/{District}/All", accountSearch);
            NavigationMap.Add("Regions/{Region}/{District}/{Territory}", accountSearch);

            NavigationMap.Add("Product", new Layers.Products.Types());
            NavigationMap.Add("Products/{Type}", new Layers.Products.Search());
            NavigationMap.Add("Products/{Type}/{Model}", new Layers.Products.Detail());
            NavigationMap.Add("Products/{Type}/{Model}/Sell/{Source}", accountSearch);
            NavigationMap.Add("Products/{Type}/{Model}/Sell/{Source}/{Account}", transactionsSell);

            NavigationMap.Add("Transactions", pendingOrders);
            NavigationMap.Add("Transactions/Submit/{Order}", pendingOrders);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/New", transactionsSell);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}", transactionsSell);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Detail", new Layers.Transactions.SellDetail());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Submit", accountDetail);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/New", new Layers.Products.Types());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}", new Layers.Products.Search());
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/Detail/{Model}", transactionsSellItemDetails);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}/{Model}", transactionsSellItemDetails);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/Add/{Type}/{Model}", transactionsSell);
            NavigationMap.Add("Transactions/{Account}/Sell/Warehouse/{Order}/Items/{Type}/{Model}/{OrderItemId}/{Action}", transactionsSellItemDetails);

            NavigationMap.Add("Reports", new Layers.Reporting.Reports());
            NavigationMap.Add("Reports/PoPending", new Layers.Reporting.PoPending.Regions());
            NavigationMap.Add("Reports/PoPending/{Region}", new Layers.Reporting.PoPending.Districts());
            NavigationMap.Add("Reports/PoPending/{Region}/All", reportAccounts);
            NavigationMap.Add("Reports/PoPending/{Region}/{District}", new Layers.Reporting.PoPending.Territories());
            NavigationMap.Add("Reports/PoPending/{Region}/{District}/All", reportAccounts);
            NavigationMap.Add("Reports/PoPending/{Region}/{District}/{Territory}", reportAccounts);

			NavigationMap.Add("About", new Layers.About());

            NavigateOnLoad = "MainTabs";

        }
    }
}