using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Touch;
using iFactr.MVC;
using System.Reflection;

namespace GenMed
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{	
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			TouchFactory.Initialize();
			iApp.OnLayerLoadComplete += (layer) => { InvokeOnMainThread(() => { ProcessLayer(layer); }); };
			TouchFactory.TheApp = new GenMed.App();
			iApp.Navigate(TouchFactory.TheApp.NavigateOnLoad);
			TouchFactory.Instance.InitializeViews();
			return true;
		}

		private void ProcessLayer(iLayer layer)
		{
			// check to see if the layer is a ModelBoundiLayer. Process it approporiately if so.
			ProcessModelBoundLayer(layer);

			// pass the layer to the factory
			TouchFactory.Instance.OutputLayer(layer);
		}

		/// <summary>
		/// Processes the iLayer, checking if it's a ModelBoundiLayer and calling the Render() method if so.
		/// </summary>
		/// <param name="layer">The iLayer.</param>
		private void ProcessModelBoundLayer(iLayer layer)
		{
			// if layer is derived from ModelBoundiLayer
			if (layer.IsSubclassOfRawGeneric(typeof(ModelBoundiLayer<>)))
			{
				// get Renderer property from the ModelBoundiLayer instance
				var renderer = GetRenderer(null, layer);
				
				if (renderer != null)
				{
					// get Render method from the Renderer instance
					var renderMethod = renderer.GetType().GetMethod("Render");
					
					// invoke Render method
					renderMethod.Invoke(renderer, null);
				}
			}
		}
		
		/// <summary>
		/// Gets the layer renderer. It may seem strange that both layer AND it's type are parameters, but both are required in order to be recursive.
		/// </summary>
		/// <returns>The layer renderer.</returns>
		/// <param name="type">Type of the layer. Only pass null when calling this method. This parameter is only used internally for recursion.</param>
		/// <param name="layer">The layer. MUST be of type MobyModelBoundLayer\<ModelLayerModel\>.</param>
		private object GetRenderer(Type type, iLayer layer)
		{
			if (type == null)
			{
				type = layer.GetType();
			}
			
			PropertyInfo rendererProperty = type.GetProperty("Renderer");
			
			if (rendererProperty != null && rendererProperty.GetValue(layer, null) != null)
			{
				return rendererProperty.GetValue(layer, null);
			}
			else
			{
				return GetRenderer(type.BaseType, layer);
			}
		}

		public override void OnActivated (UIApplication application) { }
	}
}
