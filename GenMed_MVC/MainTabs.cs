using iFactr.Core.Layers;
using System.Collections.Generic;

namespace GenMed
{
    public class MainTabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = " ";
            LayerStyle.LayerBackgroundImage = "DashboardBG.jpg";
                
            TabItems.Add("Accounts", "Accounts", "accounts.png");
            TabItems.Add("Regions", "Region", "regions.png");
            TabItems.Add("Products", "Product", "products.png");
            TabItems.Add("Transactions", "Transactions", "transactions.png", true);
            TabItems.Add("Reports", "Reports", "reports.png");
        }
    }
}