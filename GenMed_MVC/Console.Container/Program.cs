﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Console;
using iFactr.Core;
using iFactr.Core.Layers;

namespace GenMed.Console.Container
{
    class Program
    {
        static void Main(string[] args)
        {
            iApp.OnLayerLoadComplete += (iLayer layer) => { ConsoleFactory.Instance.OutputLayer(layer); };
            ConsoleFactory.Initialize();
            ConsoleFactory.TheApp = new GenMed.App();
            iApp.Navigate(ConsoleFactory.TheApp.NavigateOnLoad);
        }
    }
}
