﻿using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Utilities.Threading;
using iFactr.Metro;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Metro.Container
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            MetroFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => iApp.Thread.ExecuteOnMainThread(new ParameterDelegate((o) => MetroFactory.Instance.OutputLayer((iLayer)o)), layer);
            MetroFactory.TheApp = new GenMed.App();

            MetroFactory.TheApp.Style.LayerBackgroundImage = "CategoryBG.jpg";
            MetroFactory.TheApp.Style.LayerItemBackgroundColor = new iFactr.Core.Styles.Style.Color("002035");
            MetroFactory.TheApp.Style.SubTextColor = new iFactr.Core.Styles.Style.Color("DDDDDD");
            MetroFactory.TheApp.Style.HeaderColor = new iFactr.Core.Styles.Style.Color("000000");

            iApp.Navigate(MetroFactory.TheApp.NavigateOnLoad);
        }
    }
}