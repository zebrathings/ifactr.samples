using System;
using iFactr.Core.Layers;
using System.Collections.Generic;

namespace iFactr.MVC
{
	/// <summary>
	/// A <see cref="Layer"/> that is bound to Layer model of type T. T must be a descendant of <see cref="BaseLayerModel"/>
	/// </summary>
	public abstract class ModelBoundiLayer<T> : iLayer where T : BaseLayerModel
	{
		/// <summary>
		/// Gets or sets the renderer. Must be assigned to before Render() is called.
		/// </summary>
		/// <value>The renderer.</value>
		public BaseiLayerRenderer<T> Renderer { protected set; get; }

		/// <summary>
		/// The Layer model.
		/// </summary>
		private BaseLayerModel _Model;

		/// <summary>
		/// Gets or sets the Layer model (the type should be a descendant of <see cref="BaseLayerModel"/>).
		/// </summary>
		/// <value>The Layer model.</value>
		public T Model 
		{
			set { _Model = value; }
			get { return (T)_Model; }
		}

		/// <summary>
		/// Ensures the Model is cleared before each Layer Load().
		/// </summary>
		public override void Clear()
		{
			_Model = null;

			Renderer = null;

			base.Clear();
		}
	}
}

