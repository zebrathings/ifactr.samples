using System;
using System.Runtime.Serialization;

namespace iFactr.MVC
{
	/// <summary>
	/// Thrown when the Model property of a Renderer is null when Render() is called.
	/// </summary>
	public class NullLayerModelException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="iFactr.MVC.NullLayerModelException"/> class.
		/// </summary>
		/// <param name="rendererType">Renderer type.</param>
		/// <param name="layerType">Layer type.</param>
		public NullLayerModelException (Type rendererType, Type layerType) 
			: base(string.Format("The Model instance on renderer {0} is null. Unable to render layer {1}.", rendererType.FullName, layerType.FullName)) { }
	}
}

