using System;
using iFactr.Core;
using iFactr.Core.Layers;

namespace iFactr.MVC
{
	/// <summary>
	/// Base Layer renderer. T specifies the Layer model of the renderer (descedant of <see cref="BaseLayerModel"/>).
	/// </summary>
	public abstract class BaseiLayerRenderer<T> where T : BaseLayerModel
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BaseiLayerRenderer"/> class.
		/// </summary>
		/// <param name="model">The <see cref="BaseLayerModel"/> descendant to render.</param>
		/// <param name="layer">The iFactr <see cref="Layer"/> upon which to render.</param>
		protected BaseiLayerRenderer(ModelBoundiLayer<T> layer)
		{
			Layer = layer;
			_Model = layer.Model;
		}

		/// <summary>
		/// The Layer model;
		/// </summary>
		private BaseLayerModel _Model;

		/// <summary>
		/// Gets or sets the Layer model (descendant of <see cref="BaseLayerModel"/>).
		/// </summary>
		/// <value>The Layer model (descendant of <see cref="BaseLayerModel"/>).</value>
		protected T Model 
		{
			get { return (T)_Model; }
		}

		/// <summary>
		/// Gets the iFactr <see cref="iLayer"/>.
		/// </summary>
		/// <value>The iFactr <see cref="iLayer"/> instance that this renderer acts upon.</value>
		protected iLayer Layer { get; private set; }
		
		/// <summary>
		/// Render the Layer.
		/// </summary>
		public void Render()
		{
			if (Model == null)
			{
				NullLayerModelException ex = new NullLayerModelException(this.GetType(), Layer.GetType());
				iApp.Log.Error(ex.Message);
				throw ex;
			}

			if (_Model.CancelLoadAndNavigateUrl == null)
			{
				BuildLayer();
			}
			else
			{
				Layer.CancelLoad = true;
				iApp.Navigate(_Model.CancelLoadAndNavigateUrl, _Model.CancelLoadAndNavigateParameters);
			}
		}

		/// <summary>
		/// Override this method to implement a Layer's UI. Call base.BuildLayer() to take advantage of properly setting iLayer.Title and iLayer.Name.
		/// </summary>
		public virtual void BuildLayer()
		{
			Layer.Title = ((BaseLayerModel)_Model).Title;
			
			Layer.Name = ((BaseLayerModel)_Model).Name ?? Layer.GetType().FullName;
		}
	}
}

