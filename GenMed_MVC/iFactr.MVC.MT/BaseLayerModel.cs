using System;
using System.Collections.Generic;

namespace iFactr.MVC
{
	public abstract class BaseLayerModel
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BaseLayerModel"/> class.
		/// </summary>
		/// <param name="parameters">Stores an instance of the parameters dictionary from the descendant type.</param>
		public BaseLayerModel(Dictionary<string, string> parameters)
		{
			Parameters = parameters;
		}
		
		/// <summary>
		/// Gets or sets the Layer title.
		/// </summary>
		/// <value>The Layer title.</value>
		public string Title { get; protected set; }
		
		/// <summary>
		/// Gets or sets the Layer name.
		/// </summary>
		/// <value>The Layer name.</value>
		public string Name { get; protected set; }
		
		/// <summary>
		/// Gets the parameters dictionary.
		/// </summary>
		/// <value>The parameters dictionary.</value>
		public Dictionary<string, string> Parameters { get; private set; }
		
		/// <summary>
		/// Gets or sets a value indicating whether this instance has a CancelLoadAndNavigate URL.
		/// </summary>
		/// <value>The CancelLoadAndNavigate URL, if any. May be null.</value>
		public string CancelLoadAndNavigateUrl { get; protected set; }

		/// <summary>
		/// A convenience method that simply sets the CancelLoadAndNavigateUrl value on this instance.
		/// This method exists mainly for helping port existing Layers into the iFactr MVC pattern; it takes the same form as the iLayer CancelLoadAndNavigate() extension method.
		/// </summary>
		/// <param name="cancelLoadAndNavigateUrl">Cancel load and navigate URL.</param>
		public void CancelLoadAndNavigate(string cancelLoadAndNavigateUrl)
		{
			CancelLoadAndNavigateUrl = cancelLoadAndNavigateUrl;
		}
		
		/// <summary>
		/// Gets or sets a value indicating whether this instance has a CancelLoadAndNavigate parameters dictionary.
		/// </summary>
		/// <value>The CancelLoadAndNavigate parameters dictionary, if any. May be null.</value>
		public Dictionary<string, string> CancelLoadAndNavigateParameters { get; protected set; }
	}
}

