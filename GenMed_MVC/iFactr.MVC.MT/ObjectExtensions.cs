using System;
using iFactr.Core.Layers;

namespace iFactr.MVC
{
	public static class ObjectExtensions
	{
		/// <summary>
		/// Determines if specified instance is a subclass of the given generic type
		/// </summary>
		/// <returns><c>true</c> if is subclass of specified genericType; otherwise, <c>false</c>.</returns>
		/// <param name="subclass">Subclass.</param>
		/// <param name="genericType">Generic type.</param>
		public static bool IsSubclassOfRawGeneric(this object subclass, Type genericType)
		{
			Type typeToCheck = subclass.GetType();

			while (typeToCheck != null && typeToCheck != typeof(object)) 
			{
				var cur = typeToCheck.IsGenericType ? typeToCheck.GetGenericTypeDefinition() : typeToCheck;
				if (genericType == cur) 
				{
					return true;
				}
				typeToCheck = typeToCheck.BaseType;
			}
			return false;
		}
	}
}

