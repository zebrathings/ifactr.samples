using System;

namespace GenMed.Data
{
    public class InventoryItem
    {
        public Product Product { get; set; }
        public string Serial { get; set; }
        public DateTime DateSold { get; set; }
        public int DaysToExpire { get; set; }
        public int Quantity { get; set; }

        public InventoryItem()
        {
            Product = new Product();
        }
    }
}