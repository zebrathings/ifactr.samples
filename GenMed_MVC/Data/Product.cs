namespace GenMed.Data
{
    public class Product
    {
        public string Model { get; set; }
        public string Description { get; set; }
        public double ListPrice { get; set; }
    }
}