﻿using System.Collections.Generic;

namespace GenMed.Data
{
    public class Territory
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<Account> Accounts { get; set; }

        public Territory()
        {
            Accounts = new List<Account>();
        }
    }
}