using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;
using GenMed.LayerModels.Regions;
using GenMed.Renderers.Regions;
using iFactr.MVC;

namespace GenMed.Layers.Regions
{
    public class Districts : ModelBoundiLayer<DistrictsViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DistrictsViewModel(parameters);

			this.Renderer = new DistrictsRenderer(this);
        }
    }
}