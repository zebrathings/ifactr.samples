using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using GenMed.LayerModels.Regions;
using GenMed.Renderers.Regions;
using iFactr.MVC;

namespace GenMed.Layers.Regions
{
	public class Regions : ModelBoundiLayer<RegionsViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new RegionsViewModel(parameters);

			this.Renderer = new RegionsRenderer(this);
        }
    }
}