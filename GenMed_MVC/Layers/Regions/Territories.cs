using GenMed.Reporting.PoPending.Data;
using iFactr.Core.Layers;
using System;
using System.Collections.Generic;
using System.Linq;
using GenMed.LayerModels.Regions;
using GenMed.Renderers.Regions;
using iFactr.MVC;

namespace GenMed.Layers.Regions
{
	public class Territories : ModelBoundiLayer<TerritoriesViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new TerritoriesViewModel(parameters);

			this.Renderer = new TerritoriesRenderer(this);
        }
    }
}