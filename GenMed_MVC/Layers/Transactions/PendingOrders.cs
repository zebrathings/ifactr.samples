using System.Collections.Generic;
using GenMed.LayerModels.Transactions;
using GenMed.Renderers.Transactions;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Transactions
{
	public class PendingOrders : ModelBoundiLayer<PendingOrdersViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new PendingOrdersViewModel(parameters, this.Name);

			this.Renderer = new PendingOrdersRenderer(this);
        }
    }
}