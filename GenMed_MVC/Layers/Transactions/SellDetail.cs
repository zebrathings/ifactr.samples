using System.Collections.Generic;
using GenMed.LayerModels.Transactions;
using GenMed.Renderers.Transactions;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Transactions
{
	class SellDetail : ModelBoundiLayer<SellDetailViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SellDetailViewModel(parameters);

			this.Renderer = new SellDetailRenderer(this);
        }
    }
}