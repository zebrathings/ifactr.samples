using System.Collections.Generic;
using GenMed.LayerModels.Transactions;
using GenMed.Renderers.Transactions;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Transactions
{
	class SellItem : ModelBoundiLayer<SellItemViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SellItemViewModel(parameters);

			this.Renderer = new SellItemRenderer(this);
        }
    }
}
