using System.Collections.Generic;
using GenMed.LayerModels.Transactions;
using GenMed.Renderers.Transactions;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Transactions
{
	class SellItemDetails : ModelBoundiLayer<SellItemDetailsViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SellItemDetailsViewModel(parameters);

			this.Renderer = new SellItemDetailsRenderer(this);
        }
    }
}
