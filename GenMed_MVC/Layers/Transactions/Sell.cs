using System.Collections.Generic;
using GenMed.LayerModels.Transactions;
using GenMed.Renderers.Transactions;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Transactions
{
	class Sell : ModelBoundiLayer<SellViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SellViewModel(parameters);

			this.Renderer = new SellRenderer(this);
        }
    }
}
