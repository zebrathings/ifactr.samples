using System.Collections.Generic;
using GenMed.LayerModels.Accounts;
using GenMed.Renderers.Accounts;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Accounts
{
    public class DetailInventory : ModelBoundiLayer<DetailInventoryViewModel> 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DetailInventoryViewModel(parameters);

			this.Renderer = new DetailInventoryRenderer(this);
        }
    }
}