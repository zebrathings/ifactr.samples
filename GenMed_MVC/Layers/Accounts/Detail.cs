using System.Collections.Generic;
using GenMed.LayerModels.Accounts;
using GenMed.Renderers.Accounts;
using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Layers.Accounts
{
	public class Detail : ModelBoundiLayer<DetailLayerModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DetailLayerModel(parameters);

			this.Renderer = new DetailRenderer(this);
        }
    }
}