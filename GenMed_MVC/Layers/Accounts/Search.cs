using System.Collections.Generic;
using GenMed.LayerModels.Accounts;
using GenMed.Renderers.Accounts;
using iFactr.Core.Layers;
using iFactr.Core.Styles;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Layers.Accounts
{
    public class Search : ModelBoundiLayer<SearchViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SearchViewModel(parameters, this.Name);

			this.Renderer = new SearchRenderer(this);
        }
    }
}