using System.Collections.Generic;
using GenMed.LayerModels.Accounts;
using GenMed.Renderers.Accounts;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Accounts
{
    public class DetailSoldInventory : ModelBoundiLayer<DetailSoldInventoryViewModel> 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DetailSoldInventoryViewModel(parameters);

			this.Renderer = new DetailSoldInventoryRenderer(this);
		}
	}
}