using System.Collections.Generic;
using GenMed.LayerModels.Accounts;
using GenMed.Renderers.Accounts;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Accounts
{
    public class DetailShipTo : ModelBoundiLayer<DetailShipToViewModel> 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DetailShipToViewModel(parameters);

			this.Renderer = new DetailShipToRenderer(this);
        }
    }
}