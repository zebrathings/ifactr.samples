using System.Collections.Generic;
using GenMed.LayerModels.Reporting;
using GenMed.Renderers.Reporting;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Reporting
{
	public class Reports : ModelBoundiLayer<ReportsViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new ReportsViewModel(parameters);

			this.Renderer = new ReportsRenderer(this);
        }
    }
}