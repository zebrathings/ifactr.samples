using System.Collections.Generic;
using GenMed.LayerModels.Reporting.PoPending;
using GenMed.Renderers.Reporting.PoPending;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Reporting.PoPending
{
	public class Regions : ModelBoundiLayer<RegionsViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new RegionsViewModel(parameters);

			this.Renderer = new RegionsRenderer(this);
        }
    }
}