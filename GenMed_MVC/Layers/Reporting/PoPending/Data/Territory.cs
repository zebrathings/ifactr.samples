﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Reporting.PoPending.Data
{
    public class Territory
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<Account> Accounts { get; set; }

        public double TotalPOs
        {
            get { return Accounts.Sum(account => account.AccountPOs); }
        }

        public Territory()
        {
            Accounts = new List<Account>();
        }
    }
}