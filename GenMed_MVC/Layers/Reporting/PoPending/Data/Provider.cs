﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using iFactr.Core;

namespace GenMed.Reporting.PoPending.Data
{
    public static class Provider
    {
        public static Geography GeographyDataSet;

        static Provider()
        {
            // load the data from the xml file as soon as it is needed
            string filePoPending = Path.Combine(iApp.Factory.ApplicationPath, "PoPending.xml");

            XmlSerializer xmlPoPending = new XmlSerializer(typeof(Geography),
                new [] { 
        			typeof(Geography), 
        			typeof(List<Region>), 
        			typeof(Region), 
        			typeof(List<Territory>), 
        			typeof(Territory), 
        			typeof(List<District>), 
        			typeof(District), 
        			typeof(List<Account>), 
        			typeof(Account), 
        		});

            // iApp.File.Read will give us a byte array that can be passed to a stream for deserialization
            GeographyDataSet = (Geography)xmlPoPending.Deserialize(new MemoryStream(iApp.File.Read(filePoPending)));
        }
    }
}