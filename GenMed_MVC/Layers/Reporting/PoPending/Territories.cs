using System.Collections.Generic;
using GenMed.LayerModels.Reporting.PoPending;
using GenMed.Renderers.Reporting.PoPending;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Reporting.PoPending
{
	class Territories : ModelBoundiLayer<TerritoriesViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new TerritoriesViewModel(parameters);

			this.Renderer = new TerritoriesRenderer(this);
        }
    }
}