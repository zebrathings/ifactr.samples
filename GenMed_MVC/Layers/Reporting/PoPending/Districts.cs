using GenMed.LayerModels.Reporting.PoPending;
using GenMed.Renderers.Reporting.PoPending;
using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Reporting.PoPending
{
	public class Districts : ModelBoundiLayer<DistrictsViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DistrictsViewModel(parameters);

			this.Renderer = new DistrictsRenderer(this);
        }
    }
}