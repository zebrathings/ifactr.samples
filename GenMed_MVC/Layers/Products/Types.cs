using System.Collections.Generic;
using GenMed.LayerModels.Products;
using GenMed.Renderers.Products;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Products
{
    public class Types : ModelBoundiLayer<TypesViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new TypesViewModel(parameters);

			this.Renderer = new TypesRenderer(this);
        }
    }
}