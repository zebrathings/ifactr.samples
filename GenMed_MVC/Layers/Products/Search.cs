using System.Collections.Generic;
using GenMed.LayerModels.Products;
using GenMed.Renderers.Products;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Products
{
    public class Search : ModelBoundiLayer<SearchViewModel>, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new SearchViewModel(parameters);

			this.Renderer = new SearchRenderer(this);
        }
    }
}