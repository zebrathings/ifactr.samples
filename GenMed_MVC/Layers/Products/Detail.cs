using System.Collections.Generic;
using GenMed.LayerModels.Products;
using GenMed.Renderers.Products;
using iFactr.Core.Layers;
using iFactr.MVC;

namespace GenMed.Layers.Products
{
    public class Detail : ModelBoundiLayer<DetailViewModel>
    {
        public override void Load(Dictionary<string, string> parameters)
        {
			this.Model = new DetailViewModel(parameters);

			this.Renderer = new DetailRenderer(this);
        }
    }
}