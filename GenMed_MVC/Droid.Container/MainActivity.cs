using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;

using iFactr.Droid;

namespace Droid.Container
{
    [Activity(
        MainLauncher = true,
        Theme = "@style/ApplicationTheme", 
        ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation | ConfigChanges.ScreenSize,
        LaunchMode = LaunchMode.SingleTop,
        WindowSoftInputMode = SoftInput.AdjustPan)]

    [IntentFilter(new[] { Intent.ActionSearch }, Categories = new[] { Intent.CategoryDefault })]
    [MetaData("android.app.searchable", Resource = "@xml/searchable")]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            DroidFactory.Instance.MainActivity = this;
            DroidFactory.Instance.Authority = RecentQueryProvider.Authority;
            DroidFactory.TheApp = new GenMed.App();
            base.OnCreate(bundle);
        }
    }
}