using Android.Content;

namespace Droid.Container
{
    public class RecentQueryProvider : SearchRecentSuggestionsProvider
    {
        public const string Authority = "droid.container.RecentQueryProvider.genmed";
        public RecentQueryProvider() : base() { SetupSuggestions(Authority, DatabaseMode.Queries); }
    }
}
