using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Regions;
using iFactr.MVC;

namespace GenMed.Renderers.Regions
{
	public class TerritoriesRenderer : BaseiLayerRenderer<TerritoriesViewModel>
	{
		public TerritoriesRenderer(ModelBoundiLayer<TerritoriesViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();
			list.Header = "Territories";

			list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", this.Model.Region.Id, this.Model.District.Id), "All Accounts"));
			list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", this.Model.Region.Id, this.Model.District.Id), "All Hospitals"));
			list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/All", this.Model.Region.Id, this.Model.District.Id), "All Clinics"));

			foreach (var territory in this.Model.District.Territories)
			{
				list.Items.Add(new iItem(string.Format("Regions/{0}/{1}/{2}", this.Model.Region.Id, this.Model.District.Id, territory.Id), territory.Name, true));
			}
			
			this.Layer.Items.Add(list);
		}
	}
}

