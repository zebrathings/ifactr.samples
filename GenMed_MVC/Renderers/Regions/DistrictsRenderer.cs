using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Regions;
using iFactr.MVC;

namespace GenMed.Renderers.Regions
{
	public class DistrictsRenderer : BaseiLayerRenderer<DistrictsViewModel>
	{
		public DistrictsRenderer(ModelBoundiLayer<DistrictsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();
			list.Header = "Districts";

			list.Items.Add(new iItem(string.Format("Regions/{0}/All", this.Model.Region.Id), "All Accounts"));
			list.Items.Add(new iItem(string.Format("Regions/{0}/All", this.Model.Region.Id), "All Hospitals"));
			list.Items.Add(new iItem(string.Format("Regions/{0}/All", this.Model.Region.Id), "All Clinics"));

			foreach (var district in this.Model.Region.Districts)
			{
				list.Items.Add(new iItem(string.Format("Regions/{0}/{1}", this.Model.Region.Id, district.Id), district.Name, true));
			}
			
			this.Layer.Items.Add(list);
		}
	}
}

