using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Regions;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Regions
{
	public class RegionsRenderer : BaseiLayerRenderer<RegionsViewModel>
	{
		public RegionsRenderer(ModelBoundiLayer<RegionsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			var menu = new iMenu();
			menu.Header = string.Empty;
			
			foreach (var region in this.Model.Regions)
			{
				menu.Items.Add(new iItem("Regions/" + region.Id, region.Name, true)
				               {
					Icon = new iFactr.Core.Controls.Icon("Regions/" + region.Id + ".png")
				});
			}
			
			this.Layer.Items.Add(menu);
			
			this.Layer.ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
		}
	}
}

