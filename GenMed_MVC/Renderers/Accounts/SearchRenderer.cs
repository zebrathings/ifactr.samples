using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Accounts;
using iFactr.Core.Styles;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Accounts
{
	public class SearchRenderer : BaseiLayerRenderer<SearchViewModel>
	{
		public SearchRenderer(ModelBoundiLayer<SearchViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer

		public override void BuildLayer ()
		{
			base.BuildLayer();

			// when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
			this.Layer.LayerStyle.SectionHeaderColor = new Style.Color("#dddddd");
			
			SearchList list = new SearchList();
			
			foreach (var account in this.Model.Accounts)
			{
				list.Items.Add(
					new iItem(this.Model.NavUri + account.Id, account.Name, string.Format("{0} - {1}, {2}", account.Id, account.Address.City, account.Address.State))
				   	{
						Icon = new Icon("Headshots/" + account.Id + ".jpg"),
					});
			}
			this.Layer.Items.Add(list);

			// the About button is only wanted if this layer is on the first tab.
			// in any other case, the layer will be further down the history stack and the button would be inappropriate
			if (this.Layer.NavContext.NavigatedActiveTab == 0)
				this.Layer.ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), } );
		}

		#endregion
	}
}

