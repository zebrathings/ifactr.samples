using System;
using GenMed.LayerModels.Accounts;
using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Accounts
{
	public class DetailRenderer : BaseiLayerRenderer<DetailLayerModel>
	{
		public DetailRenderer(ModelBoundiLayer<DetailLayerModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer
		public override void BuildLayer ()
		{
			base.BuildLayer();

			iPanel panel = new iPanel();
			panel.Header = "Account Detail";
			panel.InsertImageFloatRight("Headshots/" + this.Model.Account.Id + ".jpg", "100", "100");
			panel.AppendSubHeading(this.Model.Account.Name);
			panel.AppendItalicLine("Account Num: " + this.Model.Account.Id);
			panel.AppendLine();
			panel.AppendLine();
			
			panel.AppendLine(this.Model.Account.Address.Street1);
			panel.AppendLine(this.Model.Account.Address.City + ", " + this.Model.Account.Address.State + "  " + this.Model.Account.Address.Zip);
			panel.AppendLine();
			
			if (!string.IsNullOrEmpty(this.Model.Account.Email))
			{
				panel.AppendEmail(this.Model.Account.Email);
				panel.AppendLine();
			}
			
			if (!string.IsNullOrEmpty(this.Model.Account.Phone))
				panel.AppendTelephone(this.Model.Account.Phone);
			
			iMenu menu = new iMenu();
			menu.Items.Add(new iItem(string.Format("Accounts/{0}/ShipTo", this.Model.Account.Id), "Ship To Addresses") { Icon = new Icon("ShipTo.png") });
			menu.Items.Add(new iItem(string.Format("Accounts/{0}/Inventory", this.Model.Account.Id), "Inventory") { Icon = new Icon("Inventory.png") });
			menu.Items.Add(new iItem(string.Format("Accounts/{0}/SoldInventory", this.Model.Account.Id), "Sold Inventory") { Icon = new Icon("SoldInventory.png") });
			
			this.Layer.Items.Add(panel);
			this.Layer.Items.Add(menu);
			
			this.Layer.ActionButtons.Add(new Button("Warehouse Sale", HtmlTextExtensions.VirtualPath(string.Format("Transactions/{0}/Sell/Warehouse/New", this.Model.Account.Id))) { Image = new Icon() { Location = "as-sellwarehouse.png" } });
			this.Layer.ActionButtons.Add(new Button("Trunk Sale", HtmlTextExtensions.VirtualPath(string.Format("Transactions/{0}/Sell/Warehouse/New", this.Model.Account.Id))) { Image = new Icon() { Location = "as-selltrunk.png" } });
		}
		#endregion
	}
}

