using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Accounts;
using iFactr.MVC;

namespace GenMed.Renderers.Accounts
{
	public class DetailInventoryRenderer : BaseiLayerRenderer<DetailInventoryViewModel>
	{
		public DetailInventoryRenderer(ModelBoundiLayer<DetailInventoryViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer

		public override void BuildLayer ()
		{
			base.BuildLayer();

			iList list = new iList();
			foreach (var item in this.Model.Account.Inventory)
			{
				list.Items.Add(new iItem(null, item.Quantity + " - " + item.Product.Model, item.Serial + "  (" + item.DaysToExpire + ")" , true));
			}
			if (list.Items.Count == 0)
			{
				list.Items.Add(new iItem(null, "No Inventory", "For this Account"));
			}
			this.Layer.Items.Add(list);
		}

		#endregion
	}
}

