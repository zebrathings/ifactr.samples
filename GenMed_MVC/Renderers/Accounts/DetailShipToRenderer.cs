using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Accounts;
using iFactr.MVC;

namespace GenMed.Renderers.Accounts
{
	public class DetailShipToRenderer : BaseiLayerRenderer<DetailShipToViewModel>
	{
		public DetailShipToRenderer(ModelBoundiLayer<DetailShipToViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer
		public override void BuildLayer ()
		{
			base.BuildLayer();

			iList list = new iList();
			foreach (var address in this.Model.Account.MailToAddresses)
			{
				list.Items.Add(new iItem(null, address.Street1, address.City + ", " + address.State + "  " + address.Zip, true));
			}
			this.Layer.Items.Add(list);
		}
		#endregion
	}
}

