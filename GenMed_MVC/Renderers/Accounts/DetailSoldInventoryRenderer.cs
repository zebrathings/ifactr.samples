using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Accounts;
using iFactr.MVC;

namespace GenMed.Renderers.Accounts
{
	public class DetailSoldInventoryRenderer : BaseiLayerRenderer<DetailSoldInventoryViewModel>
	{
		public DetailSoldInventoryRenderer(ModelBoundiLayer<DetailSoldInventoryViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer
		public override void BuildLayer ()
		{
			base.BuildLayer();

			iList list = new iList();

			foreach (var item in this.Model.Account.SoldInventory)
			{
				list.Items.Add(new iItem(null, string.Format("{0} {1} (Qty-{2})", item.Product.Model, item.Serial, item.Quantity), 
				                         string.Format ("Sold: {0} DTE: {1} {2}", item.DateSold.ToString("M/d/yyyy"), item.DaysToExpire, item.Product.Description)));
			}
			if (list.Items.Count == 0)
			{
				list.Items.Add(new iItem(null, "No Sold Inventory", "For this Account"));
			}
			this.Layer.Items.Add(list);
		}
		#endregion
	}
}

