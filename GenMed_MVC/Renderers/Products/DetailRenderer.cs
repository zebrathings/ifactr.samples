using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Products;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Products
{
	public class DetailRenderer : BaseiLayerRenderer<DetailViewModel>
	{
		public DetailRenderer(ModelBoundiLayer<DetailViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer

		public override void BuildLayer ()
		{
			base.BuildLayer();

			iPanel panel = new iPanel();
			panel.Header = "Product Detail";
			panel.InsertImageFloatLeft("Products/" + this.Model.Product.Model.Replace(" ", "") + ".jpg", "100", "100");
			panel.AppendHeading(this.Model.Product.Model);
			panel.AppendLine(this.Model.Product.Description);
			panel.AppendLine();

			var menu = new iMenu();
			menu.Items.Add(new iItem(string.Format("Products/{0}/{1}/Sell/Warehouse", this.Model.TypeCode, this.Model.Product.Model), "Warehouse Sale") { Icon = new Icon("sellwarehouse.png") });
			menu.Items.Add(new iItem(string.Format("Products/{0}/{1}/Sell/Trunk", this.Model.TypeCode, this.Model.Product.Model), "Trunk Sale") { Icon = new Icon("selltrunk.png") });
			
			this.Layer.Items.Add(panel);
			this.Layer.Items.Add(menu);
		}

		#endregion
	}
}

