using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Products;
using iFactr.Core.Styles;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Products
{
	public class SearchRenderer : BaseiLayerRenderer<SearchViewModel>
	{
		public SearchRenderer(ModelBoundiLayer<SearchViewModel> layer) : base(layer) { }

		#region implemented abstract members of BaseiLayerRenderer

		public override void BuildLayer ()
		{
			base.BuildLayer();

			// when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
			this.Layer.LayerStyle.SectionHeaderColor = new Style.Color("#dddddd");

			SearchList list = new SearchList();

			foreach (var product in this.Model.Products)
			{
				list.Items.Add(new iItem(this.Model.NavUri + product.Model, product.Model, product.Description) { Icon = new Icon("Products/" + product.Model.Replace(" ", "") + ".jpg") });
			}

			this.Layer.Items.Add(list);
		}

		#endregion
	}
}

