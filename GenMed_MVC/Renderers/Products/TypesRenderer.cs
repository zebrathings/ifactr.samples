using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Products;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Products
{
	public class TypesRenderer : BaseiLayerRenderer<TypesViewModel>
	{
		public TypesRenderer(ModelBoundiLayer<TypesViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			var menu = new iMenu();
			menu.Header = string.Empty;

			foreach (var type in this.Model.ProductTypes)
			{
				menu.Items.Add(new iItem(Model.NavUri + type.TypeCode, type.Description) { Icon = new Icon("Products/" + type.TypeCode + ".jpg") });
			}

			this.Layer.Items.Add(menu);

			// the About button is only wanted if this layer is on the third tab.
			// in any other case, the layer will be further down the history stack and the button would be inappropriate
			if (this.Layer.NavContext.NavigatedActiveTab == 2)
				this.Layer.ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
		}
	}
}

