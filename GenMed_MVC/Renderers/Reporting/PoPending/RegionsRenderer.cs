using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Reporting.PoPending;
using iFactr.MVC;

namespace GenMed.Renderers.Reporting.PoPending
{
	public class RegionsRenderer : BaseiLayerRenderer<RegionsViewModel>
	{
		public RegionsRenderer(ModelBoundiLayer<RegionsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();
			list.Header = string.Format("{0:C}", GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.TotalPOs);

			foreach (var region in this.Model.Regions)
			{
				list.Items.Add(new RightSubtextItem("Reports/PoPending/" + region.Id, region.Name, string.Format("{0:C}", region.TotalPOs)));
			}
			
			this.Layer.Items.Add(list);
		}
	}
}

