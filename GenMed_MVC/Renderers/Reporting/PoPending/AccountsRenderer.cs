using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Reporting.PoPending;
using iFactr.MVC;

namespace GenMed.Renderers.Reporting.PoPending
{
	public class AccountsRenderer : BaseiLayerRenderer<AccountsViewModel>
	{
		public AccountsRenderer(ModelBoundiLayer<AccountsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();

			list.Header = Model.ListHeader;
			
			foreach (var account in this.Model.Accounts)
			{
				list.Items.Add(new RightSubtextItem(null, account.AccountName, string.Format("{0:C}", account.AccountPOs)));
			}
			
			this.Layer.Items.Add(list);
		}
	}
}

