using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Reporting.PoPending;
using iFactr.MVC;

namespace GenMed.Renderers.Reporting.PoPending
{
	public class DistrictsRenderer : BaseiLayerRenderer<DistrictsViewModel>
	{
		public DistrictsRenderer(ModelBoundiLayer<DistrictsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();

			list.Header = string.Format("{0:C}", this.Model.Region.TotalPOs);
			
			list.Items.Add(new iItem(string.Format("Reports/PoPending/{0}/All", this.Model.Region.Id), "All Districts"));
			
			foreach (var district in this.Model.Region.Districts)
			{
				list.Items.Add(new RightSubtextItem(string.Format("Reports/PoPending/{0}/{1}", this.Model.Region.Id, district.Id), district.Name, string.Format("{0:C}", district.TotalPOs)));
			}
			
			this.Layer.Items.Add(list);
		}
	}
}

