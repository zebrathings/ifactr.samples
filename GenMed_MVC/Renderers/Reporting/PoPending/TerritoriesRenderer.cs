using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Reporting.PoPending;
using iFactr.MVC;

namespace GenMed.Renderers.Reporting.PoPending
{
	public class TerritoriesRenderer : BaseiLayerRenderer<TerritoriesViewModel>
	{
		public TerritoriesRenderer(ModelBoundiLayer<TerritoriesViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList list = new iList();
			list.Header = string.Format("{0:C}", Model.District.TotalPOs);
			
			list.Items.Add(new iItem(string.Format("Reports/PoPending/{0}/{1}/All", this.Model.Region.Id, this.Model.District.Id), "All Territories"));

			foreach (var territory in this.Model.District.Territories)
			{
				list.Items.Add(new RightSubtextItem(string.Format("Reports/PoPending/{0}/{1}/{2}", this.Model.Region.Id, this.Model.District.Id, territory.Id), territory.Name, string.Format("{0:C}", territory.TotalPOs)));
			}

			this.Layer.Items.Add(list);
		}
	}
}

