using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Reporting;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Reporting
{
	public class ReportsRenderer : BaseiLayerRenderer<ReportsViewModel>
	{
		public ReportsRenderer(ModelBoundiLayer<ReportsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			var menu = new iMenu();
			menu.Header = string.Empty;
			
			menu.Items.Add(new iItem("Reports/PoPending", "Daily Sales") { Icon = new Icon("MenuIcons/dailysales.png") });
			menu.Items.Add(new iItem("Reports/PoPending", "Plan vs. Actual") { Icon = new Icon("MenuIcons/planactual.png") });
			menu.Items.Add(new iItem("Reports/PoPending", "PO Pending") { Icon = new Icon("MenuIcons/popending.png") });
			menu.Items.Add(new iItem("Reports/PoPending", "Case Tracker") { Icon = new Icon("MenuIcons/casetracking.png") });
			
			this.Layer.Items.Add(menu);
			
			this.Layer.ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
		}
	}
}

