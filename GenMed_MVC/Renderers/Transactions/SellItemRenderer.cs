using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Transactions;
using iFactr.MVC;

namespace GenMed.Renderers.Transactions
{
	public class SellItemRenderer : BaseiLayerRenderer<SellItemViewModel>
	{
		public SellItemRenderer(ModelBoundiLayer<SellItemViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iPanel panel = new iPanel();
			panel.Header = "Item Detail";
			panel.AppendHeading(string.Format("{0} (Qty-{1})", this.Model.Model, this.Model.OrderItem.Quantity.ToString()));
			panel.AppendLine(this.Model.OrderItem.Product.Description);
			panel.AppendLine();
			panel.AppendItalicLine("Note: ");
			panel.AppendLine(this.Model.Order.PONumber == "" ? "N/A" : this.Model.OrderItem.Note);
			
			this.Layer.Items.Add(panel);
		}
	}
}

