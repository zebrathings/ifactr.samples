using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Transactions;
using iFactr.Core.Forms;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Transactions
{
	public class SellDetailRenderer : BaseiLayerRenderer<SellDetailViewModel>
	{
		public SellDetailRenderer(ModelBoundiLayer<SellDetailViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			this.Layer.Items.Add(
				new Fieldset()
				{
				Header = "Order Details",
				Fields =
				{
					new SelectListField("DeliveryMethod", new[] {"FDX - Ground", "FDX - Express", "UPS - Ground", "USPS - First Class", "USPS - Certified", "USPS - Registered"})
					{ 
						SelectedValue = Model.Order.DeliveryMethod,
						Label = "Delivery Method:"
					},
					new TextField("PONumber") { Label = "PO Number:", Text = this.Model.Order != null && this.Model.Order.PONumber != null ? this.Model.Order.PONumber : string.Empty },
				}
			});
			
			this.Layer.ActionButtons.Add(new SubmitButton("Save", string.Format("Transactions/{0}/Sell/Warehouse/{1}", this.Model.Order.AccountId, this.Model.Order.Id)) { Image = new Icon() { Location = "as-save.png" } });
			this.Layer.Layout = LayerLayout.EdgetoEdge;
		}
	}
}

