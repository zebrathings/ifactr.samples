using System;
using iFactr.Core.Layers;
using GenMed.LayerModels.Transactions;
using iFactr.Core.Forms;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Transactions
{
	public class SellItemDetailsRenderer : BaseiLayerRenderer<SellItemDetailsViewModel>
	{
		public SellItemDetailsRenderer(ModelBoundiLayer<SellItemDetailsViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			this.Layer.Items.Add(
				new Fieldset()
				{
				Header = this.Model.Product.Description,
				Fields =
				{
					new SelectListField("Quantity", new[] {"1", "2", "3", "4", "5", "6"})
					{
						SelectedIndex = this.Model.OrderItem != null && this.Model.OrderItem.Quantity > 0 ? this.Model.OrderItem.Quantity - 1 : 0,
						Label = "Quantity:"
					},
					new TextField("Note") { Label = "Note:", Text = this.Model.OrderItem != null ? this.Model.OrderItem.Note : string.Empty },
				}
			});
			
			var submitButton = string.IsNullOrEmpty(this.Model.OrderAction) ? new SubmitButton("Add", this.Model.Action) { Image = new Icon("as-add.png") } : new SubmitButton("Save", this.Model.Action) { Image = new Icon("as-save.png") };
			this.Layer.ActionButtons.Add(submitButton);
			this.Layer.ActionParameters = this.Model.Parameters;
			this.Layer.Layout = LayerLayout.EdgetoEdge;
		}
	}
}

