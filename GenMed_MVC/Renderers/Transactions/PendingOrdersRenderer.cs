using System;
using System.Linq;
using iFactr.Core.Layers;
using GenMed.LayerModels.Transactions;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Transactions
{
	public class PendingOrdersRenderer : BaseiLayerRenderer<PendingOrdersViewModel>
	{
		public PendingOrdersRenderer(ModelBoundiLayer<PendingOrdersViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iList plist = new iList();
			plist.Header = "Pending";
			
			if (this.Model.PendingOrders.Count() == 0)
				plist.Items.Add(new iItem(null, "No Pending Orders", "At this time"));
			else
			{
				foreach (var order in this.Model.PendingOrders)
				{
					plist.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}", order.AccountId, order.Id), order.AccountName, string.Format("{0} {1}, {2} {3}", order.ShipTo.Street1, order.ShipTo.City, order.ShipTo.State, order.ShipTo.Zip))); 
				}
			}

			this.Layer.Items.Add(plist);
			
			iList qlist = new iList();
			qlist.Header = "Awaiting Sync";
			qlist.Items.Add(new iItem(null, "All Orders", "Have been submitted"));
			Layer.Items.Add(qlist);
			
			this.Layer.ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
		}
	}
}

