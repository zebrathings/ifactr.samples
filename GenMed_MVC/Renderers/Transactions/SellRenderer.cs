using System;
using System.Linq;
using iFactr.Core.Layers;
using GenMed.LayerModels.Transactions;
using iFactr.Core.Controls;
using iFactr.MVC;

namespace GenMed.Renderers.Transactions
{
	public class SellRenderer : BaseiLayerRenderer<SellViewModel>
	{
		public SellRenderer(ModelBoundiLayer<SellViewModel> layer) : base(layer) { }

		public override void BuildLayer ()
		{
			base.BuildLayer ();

			iPanel panel = new iPanel();
			panel.Header = "Order Detail";
			panel.AppendSubHeading(this.Model.Account.Name);
			panel.AppendItalicLine("Ship To: ");
			panel.AppendLine(this.Model.Account.Address.Street1);
			panel.AppendLine(this.Model.Account.Address.City + ", " + this.Model.Account.Address.State + "  " + this.Model.Account.Address.Zip);
			panel.AppendLine();
			panel.AppendItalicLine("Delivery Method: ");
			panel.AppendLine(this.Model.Order.DeliveryMethod == "" ? "N/A" : this.Model.Order.DeliveryMethod);
			panel.AppendLine();
			panel.AppendItalicLine("PO Number: ");
			panel.AppendLine(this.Model.Order.PONumber == "" ? "N/A" : this.Model.Order.PONumber);
			
			
			iList list = new iList();
			list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Detail", this.Model.Account.Id, this.Model.Order.Id.ToString()), "Change Details", "For this Order"));
			if (this.Model.Order.Items.Count() == 0)
			{
				list.Items.Add(new iItem(null, "No Items", "For this Order"));
			}
			else
			{
				panel.AppendLine();
				panel.AppendLine("Total line-items: " + this.Model.Order.Items.Count.ToString());
				foreach (var item in this.Model.Order.Items)
				{
					list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/{2}/{3}/{4}/{5}", this.Model.Account.Id, this.Model.Order.Id, this.Model.TypeCode, item.Product.Model, item.Id, "Update"), string.Format("{0} (Qty-{1})", item.Product.Model, item.Quantity), item.Product.Description));
				}
				
				if (this.Model.Order.Id != null)
					list.Items.Add(new iItem(string.Format("Transactions/Submit/{0}", this.Model.Order.Id), "Process", "This Order"));
				else
					list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Submit", this.Model.Account.Id, this.Model.Order.Id), "Process", "This Order"));
			}
			
			this.Layer.Items.Add(panel);
			this.Layer.Items.Add(list);
			
			this.Layer.ActionButtons.Add(new Button("Add", string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/New", this.Model.Order.AccountId,this.Model.Order.Id)) { Action = Button.ActionType.Add, Image = new Icon() { Location = "as-add.png" } });
		}
	}
}

