using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Accounts
{
	public class DetailShipToViewModel : BaseLayerModel
	{
		public DetailShipToViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string accountId = this.Parameters["Account"];

			this.Account = Data.Provider.GetAccountById(accountId);
			
			this.Title = "Ship To";
		}

		public Account Account { get; private set; }
	}
}

