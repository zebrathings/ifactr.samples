using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Accounts
{
	public class DetailInventoryViewModel : BaseLayerModel
	{
		public DetailInventoryViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string accountId = this.Parameters["Account"];

			this.Account = Data.Provider.GetAccountById(accountId);
			
			this.Title = "Inventory";
		}

		public Account Account { get; private set; }
	}
}

