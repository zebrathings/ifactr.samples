using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Accounts
{
	public class SearchViewModel : BaseLayerModel
	{
		public SearchViewModel(Dictionary<string, string> parameters, string layerName) : base(parameters) 
		{
			this.Title = "Accounts";

			if (this.Parameters.ContainsKey("Type") && this.Parameters.ContainsKey("Model"))
			{
				this.Name = layerName + "ProdOrder";
				this.NavUri = string.Format("Products/{0}/{1}/Sell/{2}/", this.Parameters["Type"], this.Parameters["Model"], this.Parameters["Source"]);
			}
			else
			{
				this.NavUri = "Accounts/";
			}

			this.Accounts = GenMed.Data.Provider.Accounts;
		}

		public string NavUri { get; private set; }

		public IEnumerable<Account> Accounts { get; private set; }
	}
}

