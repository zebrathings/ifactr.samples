using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Accounts
{
	public class DetailLayerModel : BaseLayerModel
	{
		public DetailLayerModel(Dictionary<string, string> parameters) : base(parameters)
		{
			string accountId = this.Parameters["Account"];

			string orderId = this.Parameters.ContainsKey("Order") ? this.Parameters["Order"] : null;

			this.Account = Data.Provider.GetAccountById(accountId);
			
			// if the orderId is not null, we are submitting an order
			if (orderId != null)
			{
				var order = Data.Provider.GetOrderById(orderId);
				Data.Provider.SubmitOrder(order);
			}

			this.Title = Account.Name;
		}

		public Account Account { get; private set; }
	}
}

