using System;
using System.Collections.Generic;
using GenMed.Data;
using System.Linq;
using iFactr.MVC;

namespace GenMed.LayerModels.Transactions
{
	public class SellViewModel : BaseLayerModel
	{
		public SellViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string accountId = parameters["Account"];
			string orderId = parameters.Keys.Contains("Order") ? parameters["Order"] : null;
			string typeCode = parameters.Keys.Contains("Type") ? parameters["Type"] : null;
			string model = parameters.Keys.Contains("Model") ? parameters["Model"] : null;
			string delivery = parameters.Keys.Contains("DeliveryMethod") ? parameters["DeliveryMethod"] : null;
			string ponumber = parameters.Keys.Contains("PONumber") ? parameters["PONumber"] : null;
			string quantity = parameters.Keys.Contains("Quantity") ? parameters["Quantity"] : null;
			string note = parameters.Keys.Contains("Note") ? parameters["Note"] : null;
			string orderItemId = parameters.ContainsKey("OrderItemId") ? parameters["OrderItemId"] : null;

			this.Order = null;
			this.Account = Data.Provider.GetAccountById(accountId);
			this.TypeCode = typeCode;

			Title = "Order";

			// if we have an orderId, retrieve that order and update it
			if (orderId != null)
			{
				this.Order = Data.Provider.GetOrderById(orderId);
				if (delivery != null || !string.IsNullOrEmpty(ponumber))
				{
					this.Order.DeliveryMethod = delivery;
					this.Order.PONumber = ponumber;
					Data.Provider.SaveOrder(this.Order);
				}
			}
			// otherwise create a new order
			else
			{
				this.Order = new Data.Order()
				{
					Id = Guid.NewGuid().ToString().Substring(0, 5),
					AccountId = this.Account.Id,
					AccountName = this.Account.Name,
					ShipTo = this.Account.Address,
					DeliveryMethod = string.Empty,
					PONumber = string.Empty,
					Items = new List<OrderItem>()
				};
				Data.Provider.SaveOrder(this.Order);
			}
			
			if (this.TypeCode != null && model != null)
			{
				var product = Data.Provider.GetProductById(this.TypeCode, model);
				
				// if there was no order item id passed, we know this is a new order item
				if (string.IsNullOrEmpty(orderItemId))
				{
					this.Order.Items.Add(
						new Data.OrderItem()
						{
						Id = Guid.NewGuid().ToString().Substring(0, 5),
						Note = note ?? string.Empty,
						Product = product,
						Quantity = quantity != null ? int.Parse(quantity) : 1
					});
				}
				// else this is an existing order item and we need to update it accordingly
				else
				{
					this.OrderItem = this.Order.Items.SingleOrDefault(item => item.Id == orderItemId);
					if (this.OrderItem != null)
					{
						this.OrderItem.Note = note ?? string.Empty;
						this.OrderItem.Quantity = quantity != null ? int.Parse(quantity) : 1;
					}
				}
				
				Data.Provider.SaveOrder(this.Order);
			}
		}

		public Account Account { get; private set; }

		public Order Order { get; private set; }

		public OrderItem OrderItem { get; private set; }

		public string TypeCode { get; private set; }
	}
}

