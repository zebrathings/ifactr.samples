using System;
using System.Linq;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Transactions
{
	public class PendingOrdersViewModel : BaseLayerModel
	{
		public PendingOrdersViewModel(Dictionary<string, string> parameters, string layerName) : base(parameters) 
		{
			string orderId = this.Parameters.Keys.Contains("Order") ? this.Parameters["Order"] : null;
			
			// if the orderId is not null, we are submitting an order
			if (orderId != null)
			{
				var order = Data.Provider.GetOrderById(orderId);
				Data.Provider.SubmitOrder(order);
			}
			
			this.Title = "Transactions";
			
			if (this.Parameters.ContainsKey("Type") && this.Parameters.ContainsKey("Model"))
			{
				this.Name = layerName + "ProdOrder";
			}

			this.PendingOrders = Data.Provider.PendingOrders;
		}

		public IEnumerable<Order> PendingOrders { get; private set; }
	}
}

