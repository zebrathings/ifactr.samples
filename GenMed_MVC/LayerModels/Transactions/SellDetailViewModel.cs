using System;
using System.Collections.Generic;
using GenMed.Data;
using System.Linq;
using iFactr.MVC;

namespace GenMed.LayerModels.Transactions
{
	public class SellDetailViewModel : BaseLayerModel
	{
		public SellDetailViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string accountId = this.Parameters["Account"];

			string orderId = this.Parameters.Keys.Contains("Order") ? this.Parameters["Order"] : null;

			this.Order = Data.Provider.GetOrderById(orderId);

			this.Account = Data.Provider.GetAccountById(accountId);
			
			this.Title = this.Account.Name;
		}

		public Order Order { get; private set; }

		public Account Account { get; private set; }
	}
}

