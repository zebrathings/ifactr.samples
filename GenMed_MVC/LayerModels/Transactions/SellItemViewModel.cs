using System;
using System.Collections.Generic;
using GenMed.Data;
using System.Linq;
using iFactr.MVC;

namespace GenMed.LayerModels.Transactions
{
	public class SellItemViewModel : BaseLayerModel
	{
		public SellItemViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string orderId = this.Parameters.Keys.Contains("Order") ? this.Parameters["Order"] : null;

			string model = this.Parameters.Keys.Contains("Model") ? this.Parameters["Model"] : null;

			this.Order = Data.Provider.GetOrderById(orderId);

			this.OrderItem = this.Order.Items.FirstOrDefault(item => item.Product.Model == model);

			this.Title = this.Order.AccountName;
		}

		public Order Order { get; private set; }

		public OrderItem OrderItem { get; private set; }

		public string Model { get; private set; }
	}
}

