using System;
using System.Collections.Generic;
using GenMed.Data;
using System.Linq;
using iFactr.MVC;

namespace GenMed.LayerModels.Transactions
{
	public class SellItemDetailsViewModel : BaseLayerModel
	{
		public SellItemDetailsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string accountId = this.Parameters["Account"];
			string orderId = this.Parameters["Order"];
			string model = this.Parameters["Model"];
			string orderAction = this.Parameters.ContainsKey("Action") ? this.Parameters["Action"]: null;

			this.OrderAction = orderAction;

			string typeCode = null;
			
			if (!string.IsNullOrEmpty(this.OrderAction) && this.OrderAction.Equals("Update"))
			{
				this.Action = string.Format("Transactions/{0}/Sell/Warehouse/{1}", accountId, orderId);
			}
			else if (this.Parameters.ContainsKey("Type"))
			{
				typeCode = this.Parameters["Type"];
				this.Action = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/Add/{2}/{3}", accountId, orderId, typeCode, model);
			}
			else
			{
				this.Action = string.Format("Transactions/{0}/Sell/Warehouse/{1}", accountId, orderId, model);
			}
			
			
			this.Product = Data.Provider.GetProductById(typeCode, model);
			this.Order = Data.Provider.GetOrderById(orderId);
			this.OrderItem = null;
			
			if (this.Order != null)
			{
				this.OrderItem = this.Order.Items.FirstOrDefault(i => i.Product == this.Product);
			}
			
			this.Title = model;
		}

		public Product Product { get; private set; }

		public Order Order { get; private set; }

		public OrderItem OrderItem { get; private set; }

		public string Action { get; private set; }

		public string OrderAction { get; private set; }
	}
}

