using System;
using System.Linq;
using System.Collections.Generic;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Reporting.PoPending
{
	public class DistrictsViewModel : BaseLayerModel
	{
		public DistrictsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string regionId = this.Parameters["Region"];

			this.Region = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(r => r.Id == regionId);
			
			this.Title = this.Region.Name;
		}

		public Region Region { get; private set; }
	}
}

