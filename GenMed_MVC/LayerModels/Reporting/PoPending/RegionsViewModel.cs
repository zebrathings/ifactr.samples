using System;
using System.Collections.Generic;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Reporting.PoPending
{
	public class RegionsViewModel : BaseLayerModel
	{
		public RegionsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			this.Title = "Regions";

			this.ListHeader = string.Format("{0:C}", GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.TotalPOs);

			this.Regions = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions;
		}

		public string ListHeader { get; private set; }

		public IEnumerable<Region> Regions { get; private set; }
	}
}

