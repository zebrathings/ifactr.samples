using System;
using System.Linq;
using System.Collections.Generic;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Reporting.PoPending
{
	public class AccountsViewModel : BaseLayerModel
	{
		public AccountsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string regionId = null;
			string districtId = null;
			string territoryId = null;
			
			if (this.Parameters.ContainsKey("Region"))
				regionId = this.Parameters["Region"];
			
			if (this.Parameters.ContainsKey("District"))
				districtId = this.Parameters["District"];
			
			if (this.Parameters.ContainsKey("Territory"))
				territoryId = this.Parameters["Territory"];

			var currentRegion = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);
			
			if (districtId != null)
			{
				var currentDistrict = currentRegion.Districts.FirstOrDefault(district => district.Id == districtId);
				
				if (territoryId != null)
				{
					var currentTerritory = currentDistrict.Territories.FirstOrDefault(territory => territory.Id == territoryId);
					this.Accounts = currentTerritory.Accounts;
					this.Title = currentTerritory.Name;
					this.ListHeader = string.Format("{0:C}", currentTerritory.TotalPOs);
				}
				else
				{
					this.Accounts = currentDistrict.GetAccounts();
					this.Title = currentDistrict.Name;
					this.ListHeader = string.Format("{0:C}", currentDistrict.TotalPOs);
				}
			}
			else
			{
				this.Accounts = currentRegion.GetAccounts();
				this.Title = currentRegion.Name;
				this.ListHeader = string.Format("{0:C}", currentRegion.TotalPOs);
			}
		}

		public string ListHeader { get; private set; }

		public IEnumerable<Account> Accounts { get; private set; }
	}
}

