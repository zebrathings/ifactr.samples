using System;
using System.Collections.Generic;
using iFactr.MVC;

namespace GenMed.LayerModels.Reporting
{
	public class ReportsViewModel : BaseLayerModel
	{
		public ReportsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			this.Title = "Reports";
		}
	}
}

