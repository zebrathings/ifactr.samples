using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Products
{
	public class SearchViewModel : BaseLayerModel
	{
		public SearchViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string type = "0";

			this.Parameters.TryGetValue("Type", out type);
			
			this.NavUri = string.Empty;

			if (this.Parameters.ContainsKey("Account") && this.Parameters.ContainsKey("Order"))
			{
				this.NavUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/{2}/", this.Parameters["Account"], this.Parameters["Order"], type);
			}
			else
			{
				this.NavUri = string.Format("Products/{0}/", type);
			}
			
			// layer equality is determined by Name.
			// it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
			this.Name = this.NavUri.Replace("/", ".");

			this.Title = GenMed.Data.Provider.GetDescriptionForProductType(type);

			this.Products = GenMed.Data.Provider.GetProductsForType(type);
		}

		public string NavUri { get; private set; }

		public IEnumerable<Product> Products { get; private set; }
	}
}

