using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Products
{
	public class TypesViewModel : BaseLayerModel
	{
		public TypesViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			if (this.Parameters.ContainsKey("Account") && this.Parameters.ContainsKey("Order"))
			{
				this.NavUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/", this.Parameters["Account"], this.Parameters["Order"]);
			}
			else
			{
				this.NavUri = "Products/";
			}

			// layer equality is determined by Name.
			// it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
			this.Name = this.NavUri.Replace("/", ".");
			this.Title = "Products";

			this.ProductTypes = Data.Provider.ProductTypes;
		}

		public string NavUri { get; private set; }

		public IEnumerable<ProductType> ProductTypes { get; private set; }
	}
}

