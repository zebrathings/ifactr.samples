using System;
using System.Collections.Generic;
using GenMed.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Products
{
	public class DetailViewModel : BaseLayerModel
	{
		public DetailViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string typeCode = this.Parameters["Type"];

			string model = parameters["Model"];

			this.TypeCode = typeCode;

			this.Product = Data.Provider.GetProductById(typeCode, model);
			
			this.Title = this.Product.Model;
		}

		public string TypeCode { get; private set; }

		public Product Product { get; private set; }
	}
}

