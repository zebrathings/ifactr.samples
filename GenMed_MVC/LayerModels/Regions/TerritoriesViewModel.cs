using System;
using System.Linq;
using System.Collections.Generic;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Regions
{
	public class TerritoriesViewModel : BaseLayerModel
	{
		public TerritoriesViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string regionId = parameters["Region"];

			string districtId = parameters["District"];

			this.Region = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);

			this.District = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId).Districts.FirstOrDefault(district => district.Id == districtId);
			
			this.Title = District.Name;
		}

		public Region Region { get; private set; }

		public District District { get; private set; }
	}
}

