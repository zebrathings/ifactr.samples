using System;
using System.Collections.Generic;
using System.Linq;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Regions
{
	public class DistrictsViewModel : BaseLayerModel
	{
		public DistrictsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			string regionId = parameters["Region"];

			this.Region = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);
			
			this.Title = Region.Name;
		}

		public Region Region { get; private set; }
	}
}

