using System;
using System.Collections.Generic;
using GenMed.Reporting.PoPending.Data;
using iFactr.MVC;

namespace GenMed.LayerModels.Regions
{
	public class RegionsViewModel : BaseLayerModel
	{
		public RegionsViewModel(Dictionary<string, string> parameters) : base(parameters) 
		{
			this.Title = "Regions";

			this.Regions = GenMed.Reporting.PoPending.Data.Provider.GeographyDataSet.Regions;
		}

		public IEnumerable<Region> Regions { get; private set; }
	}
}

