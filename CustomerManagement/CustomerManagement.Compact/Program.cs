using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;

namespace CustomerManagement.Compact
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
#if (NETCF)
        [MTAThread]
#else
        [STAThread]
#endif
        static void Main()
        {
            iApp.OnLayerLoadComplete += ilayer =>
            {
                CompactFactory.Instance.OutputLayer(ilayer);
            };
            CompactFactory.Initialize();
            CompactFactory.TheApp = new App();
            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.RootForm);
        }
    }
}