﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.Core.Controls;
using iFactr.UI;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    [PreferredPane(Pane.Detail)]
    class CustomerDetails : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Customer Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            // build detail block
            if (!string.IsNullOrEmpty(customer))
            {
                iBlock block = new iBlock();

                // get customer by uri
                Customer cust = Providers.Customers.Get(customer).Object;        

                if (cust != null)
                {
					block.InsertImageFloatLeft(DetailImagePath);
                    block.AppendBoldLine(cust.Name);
                    block.AppendLine(cust.Website);
                    block.AppendLine(cust.PrimaryPhone);
                    block.AppendLine();
                    block.AppendLine(string.Format("Pending Transactions: {0}", Providers.Customers.QueueList.Count(c => c.ID == cust.ID)));
                }
                else
                {
                    block.AppendLine("No Customer Data Available.\r\n\r\nIs this a newly added customer?  Try refreshing the Customer List from the menu.");
                }
                Items.Add(block);

                // add edit and delete buttons
                Fieldset actions = new Fieldset();
                if (cust != null && CustomerProvider.CanChange)
                    actions.Add(new ButtonField("Edit", string.Format("Customers/{0}/Load", cust.ID)) { Label = "Edit Customer", Action = Button.ActionType.Edit });
                if (cust != null && CustomerProvider.CanDelete)
                    actions.Add(new ButtonField("Delete", string.Format("Customers/{0}/Delete", cust.ID)) { Label = "Delete Customer", Action = Button.ActionType.Delete, ConfirmationText = "Are you sure you want to delete this customer?" });
                if (actions.Count > 0)
                    Items.Add(actions);
            }
        }
    	
		internal const string DetailImagePath = "DetailPane_Cust.png";
	}
}
