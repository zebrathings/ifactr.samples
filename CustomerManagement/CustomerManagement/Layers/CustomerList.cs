﻿using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;
using CustomerManagement.Data;
using CustomerManagement.Shared.Model;
using MonoCross.Navigation;
using System;
using iFactr.Core;
using System.Linq;
using iFactr.Core.Utilities;

namespace CustomerManagement.Controllers
{
    [PreferredPane(Pane.Master)]
    public class CustomerListController : MXController<CustomerListViewModel>
    {
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            var custList = CustomerProvider.CustomerList;

            if (!customer.IsNullOrEmptyOrWhiteSpace())
            {
                if (customer == "Refresh")
                {
                    CustomerProvider.RefreshCustomerList();
                    MXContainer.Instance.Redirect("Customers");
                    return null;
                }
            }
            
            Model = new CustomerListViewModel(custList, customer);     
            return ViewPerspective.Default;
        }

        internal const string CustImagePath = "customer.png";
    }

    public class CustomerListView : ListView<CustomerListViewModel>
    {
        string selectedCustomerId = string.Empty;
        List<Customer> _searchResults;

        protected override void OnRender()
        {
            if (Model == null)
            {
                throw new NullReferenceException("Model passed to ListView cannot be null");
            };
            _searchResults = Model.Customers.ToList();


            Title = "Customer List";

            // add menu buttons
            Menu = new Menu();
            if (CustomerProvider.CanAdd) Menu.Add(new MenuButton("Add") { NavigationLink = new Link("Customers/0/Load"), });
            Menu.Add(new MenuButton("Dashboard") { NavigationLink = new Link("Dashboard"), });
            Menu.Add(new MenuButton("Refresh List") { NavigationLink = new Link("Customers/Refresh"), });
            Menu.Add(new MenuButton("Clear Cache") { NavigationLink = new Link("Dashboard/Remove"), });
            Menu.Add(new MenuButton("Prefetch Customers") { NavigationLink = new Link("Dashboard/Prefetch"), });

            Sections[0].ItemCount = _searchResults.Count;
            Sections[0].CellRequested += CellRequestedHandler;

            Activated += OnActivated;

            var mySearch = new SearchBox();
            mySearch.SearchPerformed += (o, e) =>
            {
                if (!e.SearchText.IsNullOrEmptyOrWhiteSpace())
                {
                    string searchText = e.SearchText;  // case insenstive
                    _searchResults = Model.Customers.Where(c => c.Name.StartsWith(searchText, StringComparison.OrdinalIgnoreCase)).ToList();
                }
                else
                {
                    _searchResults = Model.Customers.ToList();
                }
                Sections[0].ItemCount = _searchResults.Count;
                ReloadSections();
            };
            SearchBox = mySearch;

        }

        protected ICell CellRequestedHandler(int index, ICell recycledCell)
        {
            // On platforms that support cell recycling (Android and iOS), recycledCell is passed-in, otherwise is null.
            // In any case, a cell must be returned.

            ContentCell cell = recycledCell as ContentCell ?? new ContentCell();

            Customer cust = null;
            if ((_searchResults.Count > 0) && (index >= 0)) cust = _searchResults[index];
            if (cust == null)
            {
                throw new NullReferenceException("_searchResults[" + index + "] in ListView cannot be null");
            };

            selectedCustomerId = cust.ID;
            
            cell.TextLabel.Text = cust.Name;
            cell.SubtextLabel.Text = cust.Website;
            cell.Image.FilePath = CustomerListController.CustImagePath;

            // get link for list item
            var cellNavLink = new Link();
            if (iApp.Factory.LargeFormFactor)
                cellNavLink.Address = string.Format("Customers/{0}", cust.ID);
            else
                cellNavLink.Address = string.Format("Customers/{0}/Detail", cust.ID);
            cell.NavigationLink = cellNavLink;            
            string accessoryLinkUri = CustomerProvider.IsCached(cust.ID) ||
                                      cust.ID == Model.NavigatedCustomerId && CustomerProvider.IsCaching && !CustomerProvider.IsQueued(cust) ?
                                    string.Format("Indexed/{0}/LoadRefresh", cust.ID) : null;

            bool isCached = CustomerProvider.IsCached(cust.ID);
            bool isCaching =  CustomerProvider.IsCaching;
            bool isNotInQueue = !CustomerProvider.IsQueued(cust);

            if (!string.IsNullOrEmpty(accessoryLinkUri))
                cell.AccessoryLink = new Link(accessoryLinkUri);

            return cell;
        }


        /// <summary>
        /// Called when a reuse identifier is needed for a cell or tile.  Return the identifier that should be used
        /// to determine which cells or tiles may be reused for the item at the given index within the given section.
        /// This is only called on platforms that support recycling.  See Remarks for details.
        /// </summary>
        /// <param name="section">The section that will contain the item.</param>
        /// <param name="index">The index at which the item will be placed in the section.</param>
        protected override int OnItemIdRequested(int section, int index)
        {
            return 1;   // it's the same cell type always
        }


        public void OnActivated(object o, EventArgs e)
        {
            Device.Log.Debug("Automatically Navigate to Detail if on LFF.  Otherwise do nothing.");
            if (iApp.Factory.LargeFormFactor)
            {

                int index = _searchResults.IndexOf(c => c.ID == Model.NavigatedCustomerId);
                if (Model.NavigatedCustomerId.IsNullOrEmptyOrWhiteSpace() || index < 0)
                {
                    Device.Log.Debug("Navigate to overall Dashboard (because no specific customer has been selected)");
                    iApp.Navigate("Dashboard");
                }
                else
                {
                    Device.Log.Debug("Scroll into view and navigate to Customer Detail for:" + Model.NavigatedCustomerId);
                    iApp.Navigate(string.Format("Customers/{0}/Detail", Model.NavigatedCustomerId));
                    ScrollToCell(0, index, false);  // scroll list, if necessary, to bring selected customer into view (false means no scroll animation, i.e. snap into place)
                }
            }
        }
    }

    public class CustomerListViewModel
    {
        public List<Customer> Customers { get; private set; }

        public string NavigatedCustomerId { get; set; }

        public CustomerListViewModel(List<Customer> customers, string custId)
        {
            if (customers == null)
                throw new ArgumentNullException("customers");

            Customers = customers;
            NavigatedCustomerId = custId;
        }
    }
}
