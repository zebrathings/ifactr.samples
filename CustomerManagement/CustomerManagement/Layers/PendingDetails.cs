﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.Core.Controls;
using iFactr.Data;
using iFactr.UI;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    [PreferredPane(Pane.Detail)]
    class PendingDetails : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Queued Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;
            
            // build detail block
            if (!string.IsNullOrEmpty(customer))
            {
                // get customer by id
                CacheIndexItem item = CustomerProvider.GetCustomerIndex(customer);
                Customer qcust = Providers.Customers.QueueList.Where(q => q.ID == customer).First();
                Customer ccust = Providers.Customers.GetByUri(string.Format("customers/{0}.xml", customer)).Object;

                // get customer by uri
                //Customer cust = Providers.Customers.GetByUri(string.Format("customers/{0}.xml", customer)).Object;
                iBlock queued = new iBlock();
                iBlock cached = new iBlock();

                queued.Header = "Queued Customer";
                if (qcust != null)
                {
                    queued.InsertImageFloatLeft(CustImagePath);
                    queued.AppendBoldLine(qcust.Name);
                    queued.AppendLine(qcust.Website);
                    queued.AppendLine(qcust.PrimaryPhone);
                    queued.AppendLine();
                }
                else
                {
                    queued.AppendLine("No Customer Data Available");
                }

                Items.Add(queued);

                cached.Header = "Cached Customer";                    
                if (ccust != null)
                {
                    cached.InsertImageFloatLeft(item.IsExpired ? CustExpireIconPath : CustCacheIconPath);
                    cached.AppendBoldLine(ccust.Name);
                    cached.AppendLine(ccust.Website);
                    cached.AppendLine(ccust.PrimaryPhone);
                    cached.AppendLine();
                }
                else
                {
                    cached.AppendLine("No Customer Data Available");
                }
                Items.Add(cached);
            }
        }

        internal const string CustImagePath = "cust.png"; 
        internal const string CustExpireIconPath = "cust-exp.png";
        internal const string CustCacheIconPath = "cust-cache.png";
	}
}
