﻿using System.Collections.Generic;
using System.Linq;

using iFactr.Core.Layers;
using iFactr.UI;

using CustomerManagement.Data;

namespace CustomerManagement.Layers
{
    [PreferredPane(Pane.Detail)]
    class Dashboard : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Provider Dashboard";

            string action = parameters.ContainsKey("Action") ? parameters["Action"] : string.Empty;
            switch (action)
            {
                case "Remove":
                    CustomerProvider.RemoveCache();
                    this.CancelLoadAndNavigate("Dashboard");
                    break;
                case "Prefetch":
                    CustomerProvider.PrefetchCustomers();
                    this.CancelLoadAndNavigate("Dashboard");
                    break;
                default:
                    break;
            }

            Items.Add(Providers.Customers.GetDashboardBlock());
            Items.AddRange(Providers.Customers.GetDashboardMenus().Cast<iLayerItem>());
        }
    }
}