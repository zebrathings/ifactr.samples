﻿using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.UI;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    [PreferredPane(Pane.Master)]
    class IndexedCustomers : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Indexed Customers";

            string customer;
            parameters.TryGetValue("Customer", out customer);

            if (!string.IsNullOrEmpty(customer))
                this.DetailLink = new Link(string.Format("Indexed/{0}/Load", customer));
            else
                this.DetailLink = new Link("Dashboard");

            // build cached customer list
            iList list = new iList();
            foreach (CacheIndexItem item in CustomerProvider.IndexedCustomers)
            {
                Customer cust = Providers.Customers.GetByUri(item.RelativeUri).Object;
                //Customer cust = Providers.Customers.Get(App.GetCustomerIdFromIndex(item)).Object;
                list.Items.Add(new SubtextItem(string.Format("Indexed/{0}/Load", cust.ID), cust.Name, item.RelativeUri) { Icon = new iFactr.Core.Controls.Icon(item.IsExpired ? CustExpireIconPath : CustCacheIconPath) });
            }
            if (list.Items.Count == 0)
                list.Items.Add(new SubtextItem(null, "No Customers", "have been cached"));

            list.Items.Sort((x, y) => string.Compare(x.Text, y.Text, System.StringComparison.Ordinal));
            Items.Add(list);

            ActionButtons.Add(new iFactr.Core.Controls.Button(string.Empty, "Dashboard") { Text = "Dashboard" });
        }

        internal const string CustExpireIconPath = "cust-exp.png";
        internal const string CustCacheIconPath = "cust-cache.png";
    }
}