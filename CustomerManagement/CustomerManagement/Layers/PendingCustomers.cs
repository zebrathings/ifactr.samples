﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;
using CustomerManagement.Controllers;

namespace CustomerManagement.Layers
{
  class PendingCustomers : Layer
  {
    public override void Load(Dictionary<string, string> parameters)
    {
      Title = "Queued Customers";

      // enable queue to trigger processing of pending items
      Providers.Customers.QueueEnabled = true;

      // build pending transacion list
      iList list = new iList();
      foreach (Customer cust in Providers.Customers.QueueList)
      {
          list.Items.Add(new SubtextItem(string.Format("Pending/{0}", cust.ID), cust.Name, cust.Website) 
                            { Icon = new Icon(CustomerListController.CustImagePath), Button = new Button(string.Empty, string.Format("Indexed/{0}", cust.ID)) });
      }
      if (list.Items.Count == 0)
        list.Items.Add(new SubtextItem(null, "All Pending Transactions", "have been submitted"));

      Items.Add(list);
    }
  }
}
