﻿using iFactr.Core;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;
using iFactr.Core.Utilities.Logging;
using CustomerManagement.Layers;
using MonoCross.Navigation;
using CustomerManagement.Controllers;

namespace CustomerManagement
{
    public class App : iApp
    {
        // Add code to initialize application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            // set the application title
            Title = "Customer Management";

            iApp.Log.LoggingLevel = LoggingLevel.Debug;

            // set cache serialization interval to immediate
            CacheIndex.SerializationInterval = 0;

            // Add navigation mappings
            NavigationMap.Add("", new MainTabs());

            NavigationMap.Add("Dashboard", new Dashboard());
            NavigationMap.Add("Dashboard/{Action}", new Dashboard());

            NavigationMap.Add("Customers", new CustomerListController());
            NavigationMap.Add("Customers/{Customer}", new CustomerListController());
            NavigationMap.Add("Customers/{Customer}/Detail", new CustomerDetails());
            NavigationMap.Add("Customers/{Customer}/{Action}", new CustomerEdit());

            NavigationMap.Add("Indexed", new IndexedCustomers());
            NavigationMap.Add("Indexed/{Customer}", new IndexedCustomers());
            NavigationMap.Add("Indexed/{Customer}/{Action}", new IndexedDetails());

            NavigationMap.Add("Stored", new StoredCustomers());
            NavigationMap.Add("Stored/{Customer}", new StoredDetails());

            NavigationMap.Add("Pending", new PendingCustomers());
            NavigationMap.Add("Pending/{Customer}", new PendingDetails());
            
            // Add Views to ViewMap
            MXContainer.AddView<CustomerListViewModel>(typeof(CustomerListView));

            // set default navigation uri
            NavigateOnLoad = string.Empty;
        }
    }
}