﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Data;
using iFactr.Core.Layers;
using iFactr.Utilities.NetworkResource.ResourceStrategy;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Shared.Model;

namespace CustomerManagement.Data
{
    partial class CustomerProvider
    {
        #region Initialization Helpers

        enum ServiceMode
        {
            RemoteXml,
            RemoteJson,
            LocalXml,
            LocalJson,
            LocalOdata,
            Offline
        }

        static string ServiceURI
        {
            get
            {
#pragma warning disable 162
                switch (DemoMode)
                {
                    case ServiceMode.LocalJson:
                    case ServiceMode.LocalXml:
                        return "http://localhost/MXDemo";
                    case ServiceMode.LocalOdata:
                        return "http://localhost/ODataDemo";
                    case ServiceMode.Offline:
                        return "LocalData";
                    default:
                        return "http://ifactrcustomers-dev.azurewebsites.net";
#pragma warning restore 162
                }
            }
        }

        static string ObjectURI
        {
            get
            {
#pragma warning disable 162
                switch (DemoMode)
                {
                    case ServiceMode.LocalJson:
                    case ServiceMode.RemoteJson:
                        return "customers/{customer}.json";
                    case ServiceMode.LocalXml:
                    case ServiceMode.RemoteXml:
                    case ServiceMode.Offline:
                        return "customers/{customer}.xml";
                    case ServiceMode.LocalOdata:
                        return "Customers({customer})";
                    default:
                        return string.Empty;
                }
#pragma warning restore 162
            }
        }

        static string ListURI
        {
            get
            {
#pragma warning disable 162
                switch (DemoMode)
                {
                    case ServiceMode.LocalJson:
                    case ServiceMode.RemoteJson:
                        return "customers.json";
                    case ServiceMode.LocalXml:
                    case ServiceMode.RemoteXml:
                    case ServiceMode.Offline:
                        return "customers.xml";
                    case ServiceMode.LocalOdata:
                        return "Customers";
                    default:
                        return string.Empty;
                }
#pragma warning restore 162
            }
        }

        void SetFormatForDemo()
        {
#pragma warning disable 162
            switch (DemoMode)
            {
                case ServiceMode.LocalJson:
                case ServiceMode.RemoteJson:
                    Format = iFactr.Utilities.Serialization.SerializationFormat.JSON;
                    break;
                case ServiceMode.LocalXml:
                case ServiceMode.RemoteXml:
                case ServiceMode.Offline:
                    Format = iFactr.Utilities.Serialization.SerializationFormat.XML;
                    break;
                case ServiceMode.LocalOdata:
                    Format = iFactr.Utilities.Serialization.SerializationFormat.ODATA;
                    break;
            }
#pragma warning restore 162
        }

        private const string KeyParameters = "customer";

        #endregion

        #region Dashboard Helpers

        public iBlock GetDashboardBlock()
        {
            var block = new iBlock();
            block.InsertImageFloatLeft("provider.png");

            block.AppendLine();
            block.AppendHeading("Customer Provider");
            block.AppendLine(BaseUri);
            block.AppendLine();
            block.AppendLabeledTextLine("Data Format", Format.ToString());
            block.AppendLine();
            block.AppendLabeledTextLine("Provider Methods", ProviderMethods.ToString());
            block.AppendLine();
            block.AppendLabeledTextLine("Resource Strategy", ResourceStrategy.ToString());

            return block;
        }

        public List<iMenu> GetDashboardMenus()
        {
            if (IsCaching)
            {
                // get cache metrics
                var memcount = Providers.Customers.CacheList.Count.ToString(CultureInfo.InvariantCulture);
                var indexcount = IndexedCustomers.Count.ToString(CultureInfo.InvariantCulture);
                var filecount = StoredCustomers.Count.ToString(CultureInfo.InvariantCulture);
                return new List<iMenu>
            {
                new iMenu
                {
                    Header = "Cache Statistics",
                    Items =
                    {
                        new SubtextItem(null, "Cache Method:", CacheMethod.ToString()),
                        new SubtextItem(null, "Stale Method:", CacheStaleMethod.ToString()),
                        new SubtextItem(null, "Default Expiration:", FormattedExpiration),
                    },
                },
                new iMenu
                {
                    new SubtextItem(null, "Items in Memory:", memcount),
                    new SubtextItem(null, "Items in Index:", indexcount),
                    new SubtextItem(null, "Items in Storage:", filecount),
                },
                new iMenu
                {
                    Header = "Queue Statistics",
                    Items =
                    {
                        new SubtextItem(null, FormattedEnabled, null),
                        new SubtextItem(null, "Response Timeout:", FormattedTimeout),
                        new SubtextItem(null, "Pending Transactions:", Providers.Customers.QueueList.Count.ToString(CultureInfo.InvariantCulture)),
                    },
                },
            };
            }
            else
            {
                return new List<iMenu>()
                {
                    new iMenu
                    {
                        Header = "Cache Statistics",
                        Items =
                        {
                            new SubtextItem(null, "No Cache Statistics Available", string.Empty)
                        },
                    }
                };
            }            
        }

        string FormattedExpiration
        {
            get
            {
                var strb = new StringBuilder();

                strb.Append(DefaultExpiration.Days > 0 ? string.Format("{0} day{1} ", DefaultExpiration.Days, DefaultExpiration.Days > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Hours > 0 ? string.Format("{0} hour{1} ", DefaultExpiration.Hours, DefaultExpiration.Hours > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Minutes > 0 ? string.Format("{0} minute{1} ", DefaultExpiration.Minutes, DefaultExpiration.Minutes > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Seconds > 0 ? string.Format("{0} second{1} ", DefaultExpiration.Seconds, DefaultExpiration.Seconds > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Milliseconds > 0 ? string.Format("{0} millisecond{1}", DefaultExpiration.Milliseconds, DefaultExpiration.Milliseconds > 1 ? "s" : string.Empty) : string.Empty);

                string retval = strb.ToString();
                return string.IsNullOrEmpty(retval) ? "Immediate" : retval;
            }
        }
        string FormattedTimeout
        {
            get { return string.Format("{0} seconds", (ResponseTimeout / 1000)); }
        }
        string FormattedEnabled { get { return QueueEnabled ? "Queue is Enabled" : "Queue is Disabled"; } }

        #endregion

        #region Cache Helpers

        static List<Customer> _localCustomers;
        public static List<Customer> CustomerList
        {
            get
            {
                if (_localCustomers == null)
                    _localCustomers = Providers.Customers.GetList().Object;
                var retval = IsCachingList ? Providers.Customers.CacheList : _localCustomers;
                retval.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
                return retval;
            }
        }

        static CacheIndex CustomerIndex
        {
            get
            {
                return CacheIndexMap.GetFromUri(ServiceURI);
            }
        }

        static bool CacheInStorage
        {
            get
            {
                return Providers.Customers.ResourceStrategy == ResourceStrategyType.Cache;
            }
        }

        static bool CacheInMemory
        {
            get
            {
                return Providers.Customers.ResourceStrategy != ResourceStrategyType.LocalFile &&
                       Providers.Customers.CacheMethod != CacheMethodType.None;
            }
        }

        static bool IsCachingList
        {
            get
            {
                return Providers.Customers.ResourceStrategy == ResourceStrategyType.Cache &&
                              Providers.Customers.CacheMethod != CacheMethodType.None &&
                              Providers.Customers.CacheMethod != CacheMethodType.PersistOnly;
            }
        }

        public static bool IsCaching
        {
            get
            {
                return Providers.Customers.ResourceStrategy == ResourceStrategyType.Cache && (CacheInStorage || CacheInMemory);
            }
        }

        public static List<CacheIndexItem> IndexedCustomers
        {
            get
            {
                return CustomerIndex.Where(item => item.RelativeUri != "customers.xml" &&
                    item.RelativeUri != "customers.json" &&
                    item.RelativeUri != "Customers" &&
                    (item.RelativeUri.EndsWith(".xml") ||
                      item.RelativeUri.EndsWith(".json"))).ToList();
            }
        }

        public static List<CacheIndexItem> StoredCustomers
        {
            get
            {
                return IndexedCustomers.Where(item => iApp.File.Exists(CustomerIndex.GetCachePath(item))).ToList();
            }
        }

        public static void RefreshCustomerList()
        {
            Providers.Customers.ExpireList();
            _localCustomers = Providers.Customers.GetList().Object;
        }

        public static CacheIndexItem GetCustomerIndex(string customer)
        {
            return CustomerIndex.Get(Providers.Customers.ObjectAbsoluteUri.Replace("{customer}", customer), false);
        }

        public static string GetCachePath(CacheIndexItem item)
        {
            return CustomerIndex.GetCachePath(item).Replace(CustomerIndex.CachePath, string.Empty);
        }

        public static bool IsCached(string customer)
        {
            if (Providers.Customers.ResourceStrategy == ResourceStrategyType.DirectStream ||
                Providers.Customers.ResourceStrategy == ResourceStrategyType.LocalFile)
                return false;

            var item = GetCustomerIndex(customer);
            if (item == null) return false;

            bool cacheResult = false;
            switch (Providers.Customers.CacheMethod)
            {
                case CacheMethodType.Direct:
                case CacheMethodType.PersistOnly:
                    cacheResult = true;
                    break;
                default:
                    cacheResult = CacheInStorage && item.IsDownloaded;
                    break;
            }
            return cacheResult;
        }

        public static void RemoveCache()
        {
            CustomerIndex.KillIndex();
        }

        public static void PrefetchCustomers()
        {
            RemoveCache();
            var index = CacheIndexMap.GetFromUri(ServiceURI);
            var manifest = CacheManifest.CreateFromUri(ServiceURI.AppendPath("test.manifest"));
            index.UpdateIndex(manifest);
            index.PreFetchItems();
        }

        public void PrefetchComplete(string indexUri, CacheIndexItem[] items)
        {
            iApp.Log.Info(string.Format("Cache Prefetch for index at {0} complete.", indexUri));
        }
        #endregion

        #region Queue Helpers

        public static bool IsQueued(Customer cust)
        {
            return Providers.Customers.QueueList.Contains(cust);
        }
        public static bool CanAdd { get { return Providers.Customers.ProviderMethods.HasFlag(ProviderMethod.POST); } }
        public static bool CanChange { get { return Providers.Customers.ProviderMethods.HasFlag(ProviderMethod.PUT); } }
        public static bool CanDelete { get { return Providers.Customers.ProviderMethods.HasFlag(ProviderMethod.DELETE); } }

        #endregion
    }
}
