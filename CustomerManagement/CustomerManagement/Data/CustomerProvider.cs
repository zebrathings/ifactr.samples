﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Data;
using iFactr.Core.Layers;
using iFactr.Utilities.NetworkResource.ResourceStrategy;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Shared.Model;
using iFactr.Utilities.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CustomerManagement.Data
{
    public partial class CustomerProvider : Provider<Customer>
    {
        readonly object _syncLock = new object();

        // set service mode - to vary data services used for demonstration.
        const ServiceMode DemoMode = ServiceMode.RemoteXml;
 
        // Possible service mode values:
        //  RemoteXml = (default) XML formatted data from our Azure service (http://ifactrcustomers-dev.azurewebsites.net/)
        //  RemoteJson = JSON formatted data from our Azure service (http://ifactrcustomers-dev.azurewebsites.net/)
        //
        //  ** Local demonstration values require inclusion of the IIS services projects included in the bundle **
        //  ** Recommended for advanced users who have experience deploying IIS web services only **
        //  LocalXml = XML formatted data from a local IIS deployed service (http://localhost/MXDemo)
        //  LocalJson = XML formatted data from a local IIS deployed service (http://localhost/MXDemo)
        //  LocalOdata = ODATA light formatted data from a local IIS deployed service (http://localhost/OdataDemo)

        // ctor: provide base uri, item endpoint, list endpoint, parameters

        public CustomerProvider()
            : base(ServiceURI, ObjectURI, ListURI, KeyParameters)
        {
            SetFormatForDemo(); // set demo mode
            CustomerIndex.OnPrefetchComplete += this.PrefetchComplete; // prefetch logging
        }
        public override Dictionary<string, string> MapParams(Customer item)
        {
            return new Dictionary<string, string> { { "customer", item.ID }, };
        }
        protected override void OnTransactionComplete(RestfulObject<Customer> item, string verb)
        {
            lock (_syncLock)
            {
                switch (verb)
                {
                    case "POST":
                        // remove new customer placeholder from cache
                        Expire(GetUri(item.Object.TempID), ExpireMethodType.RemoveAll);
                        break;
                    case "PUT":
                        // expire customer cache
                        Expire(item.Object, ExpireMethodType.RemoveAll);
                        break;
                    case "DELETE":
                        break;
                }
                // expire the cache list and refresh
                ExpireCacheList();
                RefreshCustomerList();
            }
        }
        protected override void OnTransactionError(RestfulObject<Customer> item, string verb, System.Net.HttpStatusCode error)
        {
            base.OnTransactionError(item, verb, error);
        }
        protected override void OnTransactionFailed(RestfulObject<Customer> item, string verb, Exception ex)
        {
            base.OnTransactionFailed(item, verb, ex);
        }
    }
}