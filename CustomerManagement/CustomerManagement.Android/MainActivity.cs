using Android.App;
using Android.OS;
using Android.Views;
using CustomerManagement;
using iFactr.Core;
using iFactr.Core.Targets;
using iFactr.Droid;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            if (!DroidFactory.IsInitialized)
            {
                DroidFactory.MainActivity = this;
                TargetFactory.TheApp = new App();
                iApp.OnLayerLoadComplete += DroidFactory.Instance.OutputLayer;
                iApp.Navigate(iApp.Instance.NavigateOnLoad);
            }
            else DroidFactory.MainActivity = this;
            base.OnCreate(bundle);
        }
    }
}