﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData;
using System.Web.Mvc;

using CustomerManagement.Shared.Model;
using CustomerManagement.Data;


namespace CustomerManagment.OData.Controllers
{
    public class CustomersController : EntitySetController<Customer, string>
    {
        protected override string GetKey(Customer entity)
        {
            return entity.ID;
        }
        public override IQueryable<Customer> Get()
        {
            return XmlDataStore.GetCustomers().AsQueryable();
        }

        protected override Customer GetEntityByKey(string key)
        {
            return XmlDataStore.GetCustomer(key);
        }

        protected override Customer CreateEntity(Customer entity)
        {
            return XmlDataStore.CreateCustomer(entity);
        }

        protected override Customer UpdateEntity(string key, Customer update)
        {
            return XmlDataStore.UpdateCustomer(update);
        }

        public override void Delete(string key)
        {
            XmlDataStore.DeleteCustomer(key);
        }
    }
}
