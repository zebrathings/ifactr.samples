﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

#if (MonoDevelop) 
using NUnit.Framework;
#else
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endif

namespace BestSellers.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
#if (MonoDevelop)
    [TestFixture]
#else
    [TestClass]
#endif
    public class UnitTests
    {
        public UnitTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

#if (!MonoDevelop)
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
#endif

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestMethod1()
        {
            //
            // TODO: Add test logic	here
            //
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestAgain()
        {
            //
            // TODO: Add test logic	here
            //
        }
    }
}
