﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Test;

#if (MonoDevelop) 
using NUnit.Framework;
#else  
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endif


namespace BestSellers.Tests
{

#if (MonoDevelop) 
    [TestFixture] 
#else
    [TestClass]
#endif
    public class iFactrTests
    {
        public iFactrTests()
        {
            TestFactory.Initialize();

            // specify iFactr targets for this app
            TestFactory.Instance.Targets.Add(MobileTarget.Touch);
            TestFactory.Instance.Targets.Add(MobileTarget.WebKit);

            // configure and instantiate application instance
            iApp.OnLayerLoadComplete += (iLayer layer) => { TestFactory.Instance.OutputLayer(layer); };
            TestFactory.TheApp = new BestSellers.App();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestApp()
        {
            TestFactory.Instance.TestApp();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestNavigation()
        {
            TestFactory.Instance.TestNavigation();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestLayers()
        {
            TestFactory.Instance.TestLayers();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestPanels()
        {
            TestFactory.Instance.TestPanels();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestBlocks()
        {
            TestFactory.Instance.TestBlocks();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestTabs()
        {
            TestFactory.Instance.TestTabs();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestLists()
        {
            TestFactory.Instance.TestLists();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestMenus()
        {
            TestFactory.Instance.TestMenus();
        }

#if (MonoDevelop) 
        [Test] 
#else
        [TestMethod]
#endif
        public void TestForms()
        {
            TestFactory.Instance.TestForms();
        }
    }
}
