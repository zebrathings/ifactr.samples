﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Console;

/// <summary>
/// This namespace contains the Console container for the BestSellers sample
/// </summary>
namespace Console.Container
{
    /// <summary>
    /// This is the Console container for the BestSellers Sample
    /// </summary>
    /// <includesource>yes</includesource>
    class Program
    {
        static void Main(string[] args)
        {
            iApp.OnLayerLoadComplete += (iLayer layer) => { ConsoleFactory.Instance.OutputLayer(layer); };
            ConsoleFactory.Initialize();
            ConsoleFactory.TheApp = new BestSellers.App();
            iApp.Navigate(ConsoleFactory.TheApp.NavigateOnLoad);
        }
    }
}
