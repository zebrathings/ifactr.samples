using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using iFactr.Core;
using iFactr.WinPhone;

namespace WinPhone.Container
{
    /// <summary>
    /// The application entry point for the BestSellers application on the Windows Phone
    /// mobile target.
    /// </summary>
    /// <remarks>
    /// The <c>App </c>class inherits from the <c>System.Windows.Application</c> class and implements the base Silverlight application used on the Windows Phone mobile target.  The App class represents the entry point of the BestSellers iFactr application running on Windows Phone.  Initialization of iFactr components occurs in the <c>App </c>class constructor.
    /// </remarks>
    public partial class App : Application
    {
        /// <summary>
        /// Gets the root frame of the Windows Phone application.
        /// </summary>
        /// <remarks>
        /// The RootFrame property provides access to the root frame of the Windows Phone
        /// application.
        /// </remarks>
        /// <value>
        /// A PhoneApplicationFrame instance.
        /// </value>
        public PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BestSellers.Phone.App">App</see>
        /// class.
        /// </summary>
        /// <remarks>
        /// The App class constructor initializes the BestSellers app on the Windows Phone
        /// mobile target.  The constructor performs the following initialization steps:
        /// <para></para>
        /// <list type="bullet">
        /// <item>
        /// <description>Sets the Application.UnhandledException event
        /// delegate.</description></item>
        /// <item>
        /// <description>Initializes the Application component.</description></item>
        /// <item>
        /// <description>Initializes the Windows Phone application.</description></item>
        /// <item>
        /// <description>Initializes the iFactr WinPhoneFactory for use, passing the RootFrame
        /// of the application.</description></item>
        /// <item>
        /// <description>Sets the iFactr application instance on the
        /// WinPhoneFactory.</description></item></list>
        /// <para></para>
        /// <para>Additional intialization steps should be placed in the App class
        /// constructor as needed. </para>
        /// </remarks>
        public App()
        {
            // Global handler for uncaught exceptions. 
            // Note that exceptions thrown by ApplicationBarItem.Click will not get caught here.
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            WinPhoneFactory.Initialize(this.RootFrame);
            WinPhoneFactory.TheApp = new BestSellers.App();
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}
