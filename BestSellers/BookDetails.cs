using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using iFactr.Core;
using iFactr.Core.Layers;

namespace BestSellers
{
    /// <summary>
    /// Defines the details of a New York Times Best Sellers book in an iFactr block.
    /// </summary>
    /// <remarks>
    /// The <c>BookDetails </c>class defines the abstract iFactr user interface for display book information in the BestSellers application. This sample class demonstrates how a simple RESTful request can be parsed via LINQ to create a list of items using the iFactr <c>iBlock </c>class.
    /// </remarks>
   public class BookDetails : Layer, IDetailLayer
    {
        /// <summary>
        /// Loads the New York Times Best Sellers book information.
        /// </summary>
        /// <remarks>
        /// The <c>Load() </c>method contains the logic necessary to load book information from the New York Times RESTful API. The sample uses LINQ and the <c>XDocument </c>class to parse the HTTP GET response to populate an iFactr <c>iList </c>class for display of book summary information. 
        /// <para> </para>
        /// <para>The category and book id, (ISBN), selected is passed to the <c>Load() </c>method on the parameters argument, and is used to construct the RESTful call to retrieve book information, the information is then parsed and iFactr <c>iBlock </c>is created to display book details.</para>
        /// </remarks>
        /// <param name="parameters">A dictionary of parameters used to load the
        /// layer.</param>
        /// <includesource>yes</includesource>
        public override void Load(Dictionary<string, string> parameters)
        {
            string category = parameters["Category"];
            string bookId = parameters["Book"];

            Title = category;
            Name = category + bookId;

            iBlock block = new iBlock();

            iBlock missingContentBlock = new iBlock();
            missingContentBlock.AppendBoldLine("Missing Content");
            missingContentBlock.AppendLine("Sorry, we couldn't find any details for this book!");

            string urlBooks = "http://api.nytimes.com/svc/books/v2/lists.xml?list={0}&isbn={1}&api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697";

            urlBooks = string.Format(urlBooks, category.Replace(" ", "-"), bookId);

            try
            {
                XDocument loaded = XDocument.Parse(iApp.Network.Get(urlBooks));

                //TODO: Check the validity of the loaded XDocument. XDocument.Parse() never returns null.
                if (loaded != null)
                {
                    var books = loaded.Descendants("book").Select(item =>
                    new
                    {
                        Category = item.Element("display_name").Value,
                        BestSellersDate = item.Element("bestsellers_date").Value,
                        Rank = item.Element("rank").Value,
                        RankLastWeek = item.Element("rank_last_week").Value,
                        WeeksOnList = item.Element("weeks_on_list").Value,
                        PublishedDate = item.Element("published_date").Value,

                        Title = item.Descendants("book_detail").Elements("title").First().Value.ToTitleCase(),
                        Contributor = item.Descendants("book_detail").Elements("contributor").First().Value,
                        ContributorNote = item.Descendants("book_detail").Elements("contributor_note").First().Value,
                        Author = item.Descendants("book_detail").Elements("author").First().Value,
                        ISBN13 = item.Descendants("book_detail").Elements("primary_isbn13").First().Value,
                        ISBN10 = item.Descendants("book_detail").Elements("primary_isbn10").First().Value,
                        Publisher = item.Descendants("book_detail").Elements("publisher").First().Value,
                        AgeGroup = item.Descendants("book_detail").Elements("age_group").First().Value,
                        Price = item.Descendants("book_detail").Elements("price").First().Value,
                        Description = item.Descendants("book_detail").Elements("description").First().Value,

                        BookReviewLink = item.Descendants("review").Elements("book_review_link").First().Value,
                        FirstChapterLink = item.Descendants("review").Elements("first_chapter_link").First().Value,
                        SundayReviewLink = item.Descendants("review").Elements("sunday_review_link").First().Value,
                        ArticleChapterLink = item.Descendants("review").Elements("article_chapter_link").First().Value
                    });

                    var book = books.FirstOrDefault();

                    if (book != null)
                    {
                        block.InsertImageFloatLeft(String.Format("http://images.amazon.com/images/P/{0}.01.ZTZZZZZZ.jpg", book.ISBN10), "125px", "75px");
                        block.AppendLine(book.Contributor);
                        block.AppendLine();
                        block.AppendLabeledTextLine("Publisher", book.Publisher);
                        block.AppendLine(book.Description);
                        block.AppendLine();
                        block.AppendLabeledTextLine("List Price", "$" + book.Price);
                        block.AppendLine();

                        Items.Add(block);

                        if (book.BookReviewLink != "")
                        {
                            Items.Add(new iMenu
                            {
                                Items = { new iItem(book.BookReviewLink, "View Book Review ...", false) },
                            });
                        }

                        if (book.FirstChapterLink != "")
                        {
                            Items.Add(new iMenu
                            {
                                Items = { 
			                      new iItem(book.FirstChapterLink, "View First Chapter ...", false),
			                  }
                            });
                        }

                        if (book.SundayReviewLink != "")
                        {
                            Items.Add(new iMenu
                            {
                                Items = { 
			                      new iItem(book.SundayReviewLink, "View Sunday Review ...", false),
			                  }
                            });
                        }

                        if (book.ArticleChapterLink != "")
                        {
                            Items.Add(new iMenu
                            {
                                Items = { 
			                      new iItem(book.ArticleChapterLink, "View Article Chapter ...", false),
			                  }
                            });
                        }
                    }
                    else
                    {
                        Items.Add(missingContentBlock);
                    }
                }
                else
                {
                    Items.Add(missingContentBlock);
                }
            }
            catch (Exception ex)
            {
                if (ex is WebException)
                {
                    string s = ex.ToString();
                    Console.WriteLine("Web Exception occurred:\r\n" + s);
                }

                Items.Add(missingContentBlock);
            }
        }
    }
}