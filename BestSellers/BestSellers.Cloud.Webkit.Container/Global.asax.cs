﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Webkit;

namespace BestSellers.Cloud.Webkit.Container
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",                                              // Route name
                "{*url}",                                               // URL with parameters
                new { controller = "App", action = "Render", id = "" }  // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            // initialize web factory
            WebkitFactory.Initialize();

            // initialize iFactr application
            iApp.OnLayerLoadComplete += (iLayer ilayer) => { WebkitFactory.Instance.OutputLayer(ilayer); };
        }
        protected void Session_Start()
        {
            WebkitFactory.TheApp = new BestSellers.App();
        }
    }
}