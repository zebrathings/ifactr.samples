﻿using System.Windows;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;
using System.Windows.Media.Imaging;
using System;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <remarks>
    /// The <c>MainWindow </c>class represents the entry point for the BestSellers shared application on the Windows target. MainWindow inherits from the <c>System.Windows.Window</c> class, and provides the code behind interaction logic for the <c>MainWindow.xam</c>l markup file. 
    /// <para> </para>
    /// <para>Initialization of the BestSellers applicaiton on the Windows target is accomplished in the <c>MainWindow </c>constructor.</para>
    /// </remarks>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see
        /// cref="T:BestSellers.Windows.MainWindow">MainWindow</see> class.
        /// </summary>
        /// <remarks>
        /// The MainWindow constructor initializes the BestSellers shared application on the
        /// Windows target.  By default the MainWindow construcor performs the following
        /// initialization steps:
        /// <para></para>
        /// <list type="bullet">
        /// <item>
        /// <description>Calls the base Window.InitializeComponent()
        /// method.</description></item>
        /// <item>
        /// <description>Initializes the WpfFactory.</description></item>
        /// <item>
        /// <description>Sets the iApp.OnLayerLoadComplete event
        /// delegate.</description></item>
        /// <item>
        /// <description>Sets the application instance on the
        /// WpfFactory.</description></item>
        /// <item>
        /// <description>Sets the Window.Content property to the MainWindow instance on the
        /// WpfFactory.</description></item>
        /// <item>
        /// <description>Navigates to the default application
        /// endpoint.</description></item></list>
        /// <para></para>
        /// <para>Additional steps to initialize the shared applicaiton on Windows should be
        /// added to the MainWindow constructor.</para>
        /// </remarks>
        /// <includesource>yes</includesource>
        public MainWindow()
        {
            InitializeComponent();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };
            WpfFactory.Initialize();
            WpfFactory.TheApp = new BestSellers.App();
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
            Content = WpfFactory.Instance.MainWindow;
            //Icon = new BitmapImage(new System.Uri("icon.ico", UriKind.Relative));
        }
    }
}