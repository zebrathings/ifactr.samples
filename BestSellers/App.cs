using iFactr.Core;
using iFactr.Core.Styles;

/// <summary>
/// This sample represents an implementation of the iFactr abstract application
/// object model that accesses the New York Times' data for Bestselling books, and
/// allows users to view book details organized by category.
/// <para></para>
/// <para><img src="Samples\BestSellers\App.cd"/></para>
/// </summary>
namespace BestSellers
{
    /// <summary>
    /// The BestSellers shared iFactr application.
    /// </summary>
    /// <remarks>
    /// The <c>MyApp </c>class represents the iFactr shared application for the BestSellers application. MyApp inherits from the<c> iFactr.Core.iApp</c> class to provide the base implementation for application workflow, navigation, and application initialization. 
    /// <para> </para>
    /// <para>The <c>MyApp </c>class will be shared by all your cross-platform targets as you build out your platform-specific implementations.</para>
    /// </remarks>
    public class App : iApp
    {
        /// <summary>
        /// Called when the application instance is loaded. This event is meant to be
        /// overridden in consuming applications for application-level initialization code.
        /// </summary>
        /// <remarks>
        /// To initialize your iFactr shared application you will override the <c>OnAppLoad()</c> method to perform the following tasks: 
        /// <para> </para>
        /// <para><b>Setting the application Title</b></para>
        /// <para> </para>
        /// <para>You can give you applicaton a title by simply setting the Title property
        /// on your <b>iApp</b> instance:</para>
        /// <code lang="C#">
        /// // Set the application title
        /// Title = &quot;Best Sellers&quot;;</code>
        /// <para> </para>
        /// <para><b>Setting the Application Style</b></para>
        /// <para> </para>
        /// <para>You can apply application-level styles using the <c>Style</c> property. For example, to set the header color to black set the <c>HeaderColor</c> property on your <b>iApp</b> instance's <c>Style</c> to a new <b>Color</b> object containing appropriate information:</para>
        /// <code lang="C#">
        /// // Set the application style
        /// Style.HeaderColor = new Style.Color(0,0,0);</code>
        /// <para> </para>
        /// <para><b>Establishing a Navigation Map</b></para>
        /// <para> </para>
        /// <para>The Navigation Map defines your application structure by associating a specific URI template with a specific layer instance. Establish a navigation map by adding these associations in your <c>OnAppLoad()</c> override:</para>
        /// <code lang="C#">
        /// 
        /// // Add navigation mappings
        /// NavigationMap.Add(&quot;&quot;, new CategoryList());
        /// NavigationMap.Add(&quot;{Category}&quot;, new BookList());
        /// NavigationMap.Add(&quot;{Category}/{Book}&quot;, new BookDetails());</code>
        /// <para> </para>
        /// <para><b>Setting the Default Navigation URI</b></para>
        /// <para> </para>
        /// <para>The default navigation URI represents the starting layer for your application. Use the URI value from your Navigation Map that represents the starting point for your app, and set the <c>NavigateOnLoad</c> property:</para>
        /// <code lang="C#">
        /// 
        /// // Set default navigation URI</code>
        /// <para><code lang="C#">NavigateOnLoad = &quot;&quot;;</code>  </para>
        /// </remarks>
        /// <includesource>yes</includesource>
        public override void OnAppLoad()
        {
            // Set data providers
            //this.SetProviders(new Data.Providers());

            // Set the application title
            Title = "Best Sellers";

            #region Application Style
            // Set the application style
            switch (iApp.Factory.Target)
            {
                case MobileTarget.Touch:
                    Style.MasterTransitionAnimation = Transition.None;
                    Style.LayerItemBackgroundColor = new Style.Color(0, 1, 1, 1);
                    Style.HeaderColor = new Style.Color(125, 62, 30);
                    Style.SeparatorColor = Style.Color.Transparent;
                    Style.LayerDynamicMasterImage = new DynamicImage("MasterBG.9.png", DynamicResizeMethod.Tile);
                    Style.LayerDynamicDetailImage = new DynamicImage("DetailBG.9.png", DynamicResizeMethod.Tile);
                    break;
                case MobileTarget.WebKit:
                    break;
                case MobileTarget.Windows:
                case MobileTarget.Silverlight:
                    Style.HeaderColor = new Style.Color("#C2C2C2");
                    Style.HeaderTextColor = new Style.Color("#FFFFFF");
                    Style.LayerBackgroundColor = Style.Color.Transparent;
                    Style.SeparatorColor = Style.Color.Transparent;
                    Style.LayerItemBackgroundColor = Style.Color.Transparent;
                    Style.HeaderColor = new Style.Color(125, 62, 30);
                    Style.LayerDynamicMasterImage = new DynamicImage("MasterBG.9.png", DynamicResizeMethod.Tile);
                    Style.LayerDynamicDetailImage = new DynamicImage("DetailBG.9.png", DynamicResizeMethod.Tile);
                    break;
            }
            #endregion

            // Add navigation mappings
            NavigationMap.Add("", new CategoryList());
            NavigationMap.Add("{Category}", new BookList());
            NavigationMap.Add("{Category}/{Book}", new BookDetails());

            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }
}