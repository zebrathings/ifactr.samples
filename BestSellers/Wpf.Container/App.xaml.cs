﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace Schwarzkopf.Wpf.Container
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };
            WpfFactory.Initialize();
            WpfFactory.TheApp = new BestSellers.App();
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
            Run(WpfFactory.Instance.MainWindow);
            Environment.Exit(0);
        }
    }
}