using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using iFactr.Droid;

namespace Droid.Container
{
    /// <summary>
    /// The startup activity for the BestSellers android container.
    /// </summary>
    /// <remarks>
    /// The <c>MainActivity </c>class implements the startup logic for binding the BestSellers shared application to the Android mobile target.  MainActivity inherits from the <c>iFactrActivity </c>class, which implements an Android Activity for initialization of the application.
    /// </remarks>
    [Activity(ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation | ConfigChanges.ScreenSize, Theme = "@style/ApplicationTheme", LaunchMode = LaunchMode.SingleTop, WindowSoftInputMode = SoftInput.AdjustPan)]
    [IntentFilter(new[] { Intent.ActionSearch }, Categories = new[] { Intent.CategoryDefault })]
    [MetaData("android.app.searchable", Resource = "@xml/searchable")]
    public class MainActivity : iFactrActivity
    {
        /// <summary>
        /// The initialization method for the BestSellers android container.
        /// </summary>
        /// <remarks>
        /// The <c>OnCreate()</c> method performs the initialization steps necessary to bind the BestSellers shared application to the Android mobile target.  By default, the <c>OnCreate()</c> method performs the following initialization steps:
        /// <para></para>
        /// <list type="bullet">
        /// <item>
        /// <description>Sets the <c>MainActivity </c>property on the <c>DroidFactory </c>instance to the current activity instance.</description></item>
        /// <item>
        /// <description>Sets the <c>Authority </c>property on the <c>DroidFactory </c>instance to the <c>RecentQueryProvider.Authority</c> of the container.</description></item>
        /// <item>
        /// <description>Sets the <c>DroidFactory.TheApp </c>to a new instance of the shared BestSellers application.</description></item>
        /// <item>
        /// <description>Calls <c>Activity.OnCreate() </c>on the base class, passing the bundle provided.</description></item></list>
        /// <para></para>
        /// <para>Additional application initialization steps should be added to this method, and should be called before the base <c>OnCreate() </c>call.</para>
        /// </remarks>
        /// <param name="bundle">The <c>Android.OS.Bundle</c> instance to be initialized.</param>
        /// <includesource>yes</includesource>
        protected override void OnCreate(Bundle bundle)
        {
            DroidFactory.Instance.MainActivity = this;
            DroidFactory.Instance.Authority = RecentQueryProvider.Authority;
            DroidFactory.TheApp = new BestSellers.App();
            base.OnCreate(bundle);
        }
    }
}