using Android.Content;

namespace Droid.Container
{
    /// <summary>
    /// The Android search recent suggestions provider for the BestSellers android
    /// container.
    /// </summary>
    /// <remarks>
    /// The <c>RecentQueryProvider </c>class represents a search suggestion provider for the Android application.  This class is used by the MainActivity to set the <c>DroidFactory.Instance.Authority </c>property at application initialization.
    /// </remarks>
    /// <includesource>yes</includesource>
    public class RecentQueryProvider : SearchRecentSuggestionsProvider
    {
        public const string Authority = "droid.container.RecentQueryProvider.bestsellers";
        public RecentQueryProvider() : base() { SetupSuggestions(Authority, DatabaseMode.Queries); }
    }
}
