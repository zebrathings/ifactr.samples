﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Webkit;

namespace Webkit.Container.Controllers
{
    /// <summary>
    /// The ASP.NET MVC controller for the WebKit application target.
    /// </summary>
    /// <remarks>
    /// The AppController class implements the ASP.NET MVC controller for all navigation
    /// in the iFactr WebKit container.
    /// </remarks>
    public class AppController : Controller
    {
        private bool loading = true;

        /// <summary>
        /// Processes all iFactr navigation in the WebKit mobile-platform container.
        /// </summary>
        /// <remarks>
        /// The Render() method processes all navigation actions on the WebKit target.  The
        /// URL is passed passed to the iApp.Navigate() method, and processed by the
        /// WebkitFactory which loads the requested resource.
        /// </remarks>
        /// <param name="url">The URL of the resource being requested.</param>
        /// <includesource>yes</includesource>
        public void Render(string url, string args)
        {
            // set callback handler
            WebkitFactory.OnOutputLayerComplete += () =>
            {
                this.OutputLayerComplete();
            };

            // navigate to url
            url = url == null ? WebkitFactory.TheApp.NavigateOnLoad : url;

            // iApp.Navigate(url, WebkitFactory.Instance.GetParameters(this.Request));
            // Initiates navigation to the specified layer & performs any validation on POST.
            WebkitFactory.Instance.Navigate(url, this.Request);            

            // wait for web factory to output the layer
            while ( loading )
                System.Threading.Thread.Sleep( 100 );
        }

        private void OutputLayerComplete()
        {
            loading = false;
        }
    }
}
