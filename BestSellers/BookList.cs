using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace BestSellers
{
    /// <summary>
    /// Defines the list of New York Times Best Sellers books in an iFactr list.
    /// </summary>
    /// <remarks>
    /// The <c>BookList </c>class defines the abstract iFactr user interface for display a list of books for selection in the BestSellers application. This sample class demonstrates how a simple RESTful request can be parsed via LINQ to create a list of items using the iFactr <c>iList </c>class.
    /// </remarks>
    public class BookList : Layer, IMasterLayer
    {
        /// <summary>
        /// Loads the New York Times Best Sellers book list for a specific category.
        /// </summary>
        /// <remarks>
        /// The <c>Load() </c>method contains the logic necessary to load a list of books for a specific category from the New York Times RESTful API. The sample uses LINQ and the <c>XDocument </c>class to parse the HTTP GET response to populate an iFactr <c>iList </c>class for display of book summary information. 
        /// <para></para>
        /// <para>The category selected is passed to the <c>Load() </c>method on the parameters argument, and is used to construct the RESTful call to retrieve book information, the information is then parsed and iFactr <c>iItems </c>are created for the list.</para>
        /// </remarks>
        /// <includesource>yes</includesource>
        public override void Load(Dictionary<string, string> parameters)
        {
            string category = parameters["Category"];

            Title = category;

            iList list = new iList();

            iItem networkErrorItem = new iItem("localhost", "FAILED TO RECEIVE DATA", "Please check your network connection.");

            string urlBooks = string.Format("http://api.nytimes.com/svc/books/v2/lists/{0}.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697", category.Replace(" ", "-"));

            XDocument loaded = XDocument.Parse(iApp.Network.Get(urlBooks));

            //TODO: Check the validity of the loaded XDocument. XDocument.Parse() never returns null.
            if (loaded != null)
            {
                var elements = loaded.Descendants("book").ToList();

                if (elements.Any())
                {
                    var books = elements.Select(item =>
                    new
                    {
                        CategoryEncoded = category,
                        Category = item.Element("display_name").Value,
                        BestSellersDate = item.Element("bestsellers_date").Value,
                        Rank = item.Element("rank").Value,
                        RankLastWeek = item.Element("rank_last_week").Value,
                        WeeksOnList = item.Element("weeks_on_list").Value,
                        PublishedDate = item.Element("published_date").Value,

                        Title = item.Descendants("book_detail").Elements("title").First().Value.ToTitleCase(),
                        Contributor = item.Descendants("book_detail").Elements("contributor").First().Value,
                        ContributorNote = item.Descendants("book_detail").Elements("contributor_note").First().Value,
                        Author = item.Descendants("book_detail").Elements("author").First().Value,
                        ISBN13 = item.Descendants("book_detail").Elements("primary_isbn13").First().Value,
                        ISBN10 = item.Descendants("book_detail").Elements("primary_isbn10").First().Value,
                        Publisher = item.Descendants("book_detail").Elements("publisher").First().Value,
                        AgeGroup = item.Descendants("book_detail").Elements("age_group").First().Value,
                        Price = item.Descendants("book_detail").Elements("price").First().Value,
                        Description = item.Descendants("book_detail").Elements("description").First().Value,

                        BookReviewLink = item.Descendants("review").Elements("book_review_link").First().Value,
                        FirstChapterLink = item.Descendants("review").Elements("first_chapter_link").First().Value,
                        SundayReviewLink = item.Descendants("review").Elements("sunday_review_link").First().Value,
                        ArticleChapterLink = item.Descendants("review").Elements("article_chapter_link").First().Value
                    });

                    foreach (var book in books)
                    {
                        list.Items.Add(new iItem(category + "/" + book.ISBN10.Trim('X'), book.Title, book.Contributor, true)
                        {
                            Icon = new Icon { Location = String.Format("http://images.amazon.com/images/P/{0}.01.THUMBZZZ.png", book.ISBN10) }
                        });
                    }
                }
                else
                {
                    list.Items.Add(networkErrorItem);
                }
            }
            else
            {
                list.Items.Add(networkErrorItem);
            }
            Items.Add(list);
        }
    }
}