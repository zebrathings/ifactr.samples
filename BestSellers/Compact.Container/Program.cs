﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;


using iFactr.Core;
using iFactr.Compact;
using iFactr.Core.Layers;

namespace BestSellers
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
#if (NETCF)
        [MTAThread]
#else
        [STAThread]
#endif
        static void Main()
        {
            iApp.OnLayerLoadComplete += (iLayer layer) => { CompactFactory.Instance.OutputLayer(layer); };

            CompactFactory.TheApp = new App();

            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.Form);
        }
    }
}