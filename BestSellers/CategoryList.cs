using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace BestSellers
{
    /// <summary>
    /// Defines the list of New York Times Best Sellers categories in an iFactr list.
    /// </summary>
    /// <remarks>
    /// The <c>CategoryList </c>class defines the abstract iFactr user interface for display a list of book categories for selection in the BestSellers application. This sample class demonstrates how a simple RESTful request can be parsed via LINQ to create a list of items using the iFactr <c>iList </c>class.
    /// </remarks>
    public class CategoryList : Layer, IMasterLayer
    {
        /// <summary>
        /// Loads the New York Times Best Sellers book category list.
        /// </summary>
        /// <remarks>
        /// The <c>Load() </c>method contains the logic necessary to load a list of book categories from the New York Times RESTful API. The sample uses LINQ and the <c>XDocument </c>class to parse the HTTP GET response to populate an iFactr <c>iList </c>class for display of category information. 
        /// <para></para>
        /// </remarks>
        /// <param name="parameters">A dictionary of parameters passed to the layer by the
        /// iFactr framework.</param>
        /// <includesource>yes</includesource>
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Categories";

            iList list = new iList();
            list.Text = "New York Times Best Sellers";

            const string urlCategories = "http://api.nytimes.com/svc/books/v2/lists/names.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697";

            try
            {
                XDocument loaded = XDocument.Parse(iApp.Network.Get(urlCategories));

                //TODO: Check the validity of the loaded XDocument. XDocument.Parse() never returns null.
                if (loaded != null)
                {
                    var categories = loaded.Descendants("result").Select(item =>
                    new
                    {
                        ListName = item.Element("list_name").Value,
                        DisplayName = item.Element("display_name").Value
                    });

                    foreach (var category in categories)
                    {
                        list.Items.Add(new iItem(category.ListName, category.ListName.Replace("-", " "), true) { Icon = new Icon { Location = "http://ifactr.com/webapps/nytbestsellers/NytIcon.png" } });
                    }
                }
                else { list.Items.Add(new iItem("localhost", "FAILED TO RECEIVE LIST", "Please check your network connection.")); }

            }
            catch (Exception)
            {
                list.Items.Add(new iItem(string.Empty, "No List retrieved"));
            }
            Items.Add(list);
        }
    }
}