using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

using iFactr.Data;

namespace BestSellers
{
    /// <summary>
    /// This sample class demonstrates how a simple RESTful request can be parsed via
    /// LINQ to create a list of items.
    /// </summary>
    /// <includesource>yes</includesource>
    class CategoryList : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Categories";
            iList list = new iList();
            list.Text = "New York Times Best Sellers";

            Data.CategoryList clist = Data.Providers.Categories.Get(parameters);
            foreach (Data.Category category in clist)
            {
                list.Items.Add(new iListItem(category.Name, category.UriEndpoint, true) { Icon = new Icon { Location = "http://ifactr.com/webapps/nytbestsellers/NytIcon.png" } });
            }
            list.OnItemSelection += (iFactr.Core.Layers.iItem listItem) => { iApp.Navigate(listItem.Link.Address); };

            Items.Add(list);
        }

    }
}
