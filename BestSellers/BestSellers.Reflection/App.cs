using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Styles;
using iFactr.Data;
using iFactr.Reflection;

/// <summary>
/// This sample represents an implementation of the iFactr abstract application
/// object model that accesses the New York Times' data for Bestselling books, and
/// allows users to view book details organized by category.
/// <para></para>
/// <para><img src="Samples\BestSellers\App.cd"/></para>
/// </summary>
namespace BestSellers
{
    /// <summary>
    /// This class represents a sample implementation of the iApp class to create an
    /// application.
    /// </summary>
    /// <remarks>
    /// <b>iApp </b>is the base container for your business logic when using the iFactr
    /// framework. Within your own implementation of <b>iApp</b> you will define a
    /// navigation map that establishes the heirarchy of <b>iLayer</b> objects. These
    /// layers will be consumed by the target bindings for your mobile platform, and
    /// translated to an optimized user-experience on your target, or targets of choice.
    /// <para> </para>
    /// <para>When implementing your own<b> iApp</b> you will override the <c>OnAppLoad()</c>method to perform the following tasks: </para>
    /// <para> </para>
    /// <para><b>Setting the application Title</b></para>
    /// <para> </para>
    /// <para>You can give you applicaton a title by simply setting the Title property
    /// on your <b>iApp</b> instance:</para>
    /// <code lang="C#">
    /// // Set the application title
    /// Title = &quot;Best Sellers&quot;;</code>
    /// <para> </para>
    /// <para><b>Establishing a Navigation Map</b></para>
    /// <para> </para>
    /// <para>The Navigation Map defines your application structure by associating a specific URI template with a specific layer instance. Establish a navigation map by adding these associations in your <c>OnAppLoad()</c> override:</para>
    /// <code lang="C#">
    /// 
    /// // Add navigation mappings
    /// NavigationMap.Add(&quot;&quot;, new CategoryList());
    /// NavigationMap.Add(&quot;{Category}&quot;, new BookList());
    /// NavigationMap.Add(&quot;{Category}/{Book}&quot;, new BookDetails());</code>
    /// <para> </para>
    /// <para><b>Setting the Default Navigation URI</b></para>
    /// <para> </para>
    /// <para>The default navigation URI represents the starting layer for your application. Use the URI value from your Navigation Map that represents the starting point for your app, and set the <c>NavigateOnLoad</c> property:</para>
    /// <code lang="C#">
    /// 
    /// // Set default navigation URI
    /// NavigateOnLoad = &quot;&quot;;</code>
    /// <para> </para>
    /// <para><b>Setting the Application Style</b></para>
    /// <para> </para>
    /// <para>Finally, you can apply application-level styles using the <c>Style</c> class. For example, to set the header color to black set the <c>Style</c> property on your <b>iApp</b> instance to a new <b>Style</b> object containing appropriate information:</para>
    /// <code lang="C#">
    /// 
    /// // Set the application style
    /// Style = new Style()
    /// {
    /// 	HeaderColor = new Style.Color(0,0,0)
    /// };</code>
    /// <para> </para>
    /// <para>Now that you've implemented your own <b>iApp</b>, and initialized it by overriding the <c>OnAppLoad() </c>method, you're ready to bind your application to an iFactr target. </para>
    /// </remarks>
    /// <includesource>yes</includesource>
    public class App : iApp 
    {
        public override void OnAppLoad()
        {
            // Set data providers
            this.SetProviders(new Data.Providers());

            // Set the application title
            Title = "Best Sellers";

            // Set the application style
			Style.HeaderColor = new Style.Color(0,0,0);	

            // Add navigation mappings
            NavigationMap.Add("", new DerivedLayer<Data.CategoryList>());
            NavigationMap.Add("{Category}", new DerivedLayer<Data.BookList>());
            NavigationMap.Add("{Category}/{Book}", new DerivedLayer<Data.Book>());

            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }
}