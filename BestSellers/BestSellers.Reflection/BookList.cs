using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace BestSellers
{
    /// <summary>
    /// This sample class demonstrates how a simple RESTful request can be parsed via
    /// LINQ to create a list of items.
    /// </summary>
    /// <includesource>yes</includesource>
    class BookList : iLayer, IMasterLayer 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string category = parameters["Category"];
            
            Title = category;

            iList list = new iList();

            string urlBooks = String.Format("http://api.nytimes.com/svc/books/v2/lists/{0}.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697", category.Replace(" ","-"));

            Data.BookList books = Data.Providers.BookLists.Get(parameters);

            foreach (Data.Book book in books)
            {
                list.Items.Add(new iListItem(category + "/" + book.ISBN10, book.Title, book.Contributor, true) { Icon = new Icon { Location = String.Format("http://images.amazon.com/images/P/{0}.01.THUMBZZZ.png", book.ISBN10) } });
            }

            // When list item is selected, navigate to the location specified in the selected item's link address
            list.OnItemSelection += (iFactr.Core.Layers.iItem listItem) => { iApp.Navigate(listItem.Link.Address); };

            Items.Add(list);

        }
        
    }
}
