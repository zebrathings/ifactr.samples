﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Data;
using iFactr.Reflection;

namespace BestSellers.Data
{
    [ListItem]
    public class Category : RestfulObject
    {
        [LinkAddress] public string Id { get { return Name.Replace(" ", "-");} }
        [ListText] public string Name { get; set; }
        [IconAddress] public string IconLink { get { return "http://ifactr.com/webapps/nytbestsellers/NytIcon.png"; } }
    }
}
