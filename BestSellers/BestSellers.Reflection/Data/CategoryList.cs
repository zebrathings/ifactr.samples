﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Data;
using iFactr.Reflection;

namespace BestSellers.Data
{
    [NavigationList(Name="New York Times Best Sellers", Title="Categories")]
    public class CategoryList : RestfulList<Category>, IRestfulObject { }
}
