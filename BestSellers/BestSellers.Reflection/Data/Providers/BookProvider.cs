﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using iFactr.Data;

namespace BestSellers.Data
{
    public class BookProvider : Provider<Book>, IDataProvider, IDataProvider<Book>
    {
        public BookProvider() : base("http://api.nytimes.com/svc/books/v2/lists.xml?list={Category}&isbn={Book}&api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697", string.Empty, string.Empty) { }
        public override Book Get(Dictionary<string, string> parameters)
        {
            string uri = GetObjectUri(parameters);
            Book retval = cache.ContainsKey(uri) ? cache[uri] : null;

            if (retval == null)
            {
                XDocument loaded = XDocument.Load(uri);
                var books = from item in loaded.Descendants("book")
                            select new
                            {
                                rank = (string)item.Element("rank"),
                                bestSellersDate = (string)item.Element("bestsellers_date"),
                                publishedDate = (string)item.Element("published_date"),
                                weeksOnList = (string)item.Element("weeks_on_list"),
                                rankLastWeek = (string)item.Element("rank_last_week"),
                                title = (string)item.Descendants("book_detail").Elements("title").FirstOrDefault(),
                                description = (string)item.Descendants("book_detail").Elements("description").FirstOrDefault(),
                                contributor = (string)item.Descendants("book_detail").Elements("contributor").FirstOrDefault(),
                                author = (string)item.Descendants("book_detail").Elements("author").FirstOrDefault(),
                                contributorNote = (string)item.Descendants("book_detail").Elements("contributor_note").FirstOrDefault(),
                                price = (string)item.Descendants("book_detail").Elements("price").FirstOrDefault(),
                                ageGroup = (string)item.Descendants("book_detail").Elements("age_group").FirstOrDefault(),
                                publisher = (string)item.Descendants("book_detail").Elements("publisher").FirstOrDefault(),
                                isbn10 = (string)item.Descendants("isbn").Elements("isbn10").FirstOrDefault(),
                                isbn13 = (string)item.Descendants("isbn").Elements("isbn13").FirstOrDefault(),
                                bookReviewLink = (string)item.Descendants("review").Elements("book_review_link").FirstOrDefault(),
                                firstChapterLink = (string)item.Descendants("review").Elements("first_chapter_link").FirstOrDefault(),
                                sundayReviewLink = (string)item.Descendants("review").Elements("sunday_review_link").FirstOrDefault(),
                                articleChapterLink = (string)item.Descendants("review").Elements("article_chapter_link").FirstOrDefault()
                            };
                var bookDetails = books.FirstOrDefault();
                if (bookDetails != null)
                {
                    retval = new Book()
                    {
                        Description = bookDetails.description,
                        Contributor = bookDetails.contributor,
                        ISBN10 = bookDetails.isbn10,
                        //BookReviewLink = bookDetails.bookReviewLink,
                        Author = bookDetails.author,
                        Price = bookDetails.price,
                        Publisher = bookDetails.publisher,
                        //SundayReviewLink = bookDetails.sundayReviewLink,
                        Title = bookDetails.title, 
                        ActionLinks = new List<BookActionLink>()
                    };
                    if (bookDetails.bookReviewLink != null) retval.ActionLinks.Add(new BookActionLink() { Title = "Read Book Review", Link = bookDetails.bookReviewLink});
                    if (bookDetails.firstChapterLink != null) retval.ActionLinks.Add(new BookActionLink() { Title = "Read First Chapter", Link = bookDetails.firstChapterLink });
                    if (bookDetails.sundayReviewLink != null) retval.ActionLinks.Add(new BookActionLink() { Title = "Read Sunday Review", Link = bookDetails.sundayReviewLink });
                    if (bookDetails.articleChapterLink != null) retval.ActionLinks.Add(new BookActionLink() { Title = "Read Article Chapter", Link = bookDetails.articleChapterLink });

                    cache.Add(uri, retval);
                }
            }
            return retval;
        }
    }
}
