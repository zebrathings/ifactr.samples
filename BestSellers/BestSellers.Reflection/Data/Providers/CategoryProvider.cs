﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using iFactr.Data;

namespace BestSellers.Data
{
    public class CategoryProvider : Provider<Category>, IDataProvider, IDataProvider<Category>
    {
        public CategoryProvider() : base("http://api.nytimes.com/svc/books/v2/lists/{0}.xml?api-key=d8ad3be01d98001865e96ee55c1044db:8:57889697", string.Empty, string.Empty) { }
    }
}
