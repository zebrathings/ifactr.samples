﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wap;

namespace BestSellers.Wap.Controllers
{
    public class AppController : Controller
    {
        private bool loading = true;

        // GET: /Render/
        public void Render(string url, string args)
        {
            // set callback handler
            WapFactory.OnOutputLayerComplete += () => { this.OutputLayerComplete(); };

            // navigate to url
            url = url == null ? string.Empty : url;
            iApp.Navigate(url, WapFactory.Instance.GetParameters(this.Request));

            // wait for web factory to output the layer
            while (loading)
                System.Threading.Thread.Sleep(100);
        }

        private void OutputLayerComplete()
        {
            loading = false;
        }
    }
}
