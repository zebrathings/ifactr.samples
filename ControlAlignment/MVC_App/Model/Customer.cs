﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVC_App
{
    public class Character
    {
        public string Name;
        public int ID;
        public string Location;
        public string Role;
        public int FameIndex;  // 0-9
    }
}
