﻿using System;
using System.Collections.Generic;
using System.Text;

using MonoCross.Navigation;
using MVC_App.Controllers;
using MVC_App.Views;

using iFactr.Core;

namespace MVC_App
{
    public class App : iApp
    {
        public override void OnAppLoad()
        {
            Title = "Control Alignment and ScratchPad Demo";

            MXContainer.AddView<string>(typeof(CustomerView),"Align");
            MXContainer.AddView<string>(typeof(ScratchPad),"Scratch");
            MXContainer.AddView<string>(typeof(MainPanel),"Main");

            // add controllers to navigation map
            NavigationMap.Add("Alignment", new Controllers.MessageController() { Model = "Align", });
            NavigationMap.Add("ScratchPad", new Controllers.MessageController() { Model = "Scratch", });
            NavigationMap.Add("Main", new Controllers.MessageController() { Model = "Main", });

            // set navigate on load endpoint
            NavigateOnLoad = "Main";
        }
    }
}
