﻿using System;
using System.Collections.Generic;
using System.Text;

using MonoCross.Navigation;

namespace MVC_App.Controllers
{
    public class MessageController : MXController<string>
    {
        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            return Model;  // Align, Scratch or Main
        }
    }
}