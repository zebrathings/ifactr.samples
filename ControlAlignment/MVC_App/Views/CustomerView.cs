﻿using System;
using System.Collections.Generic;
using System.Text;
using iFactr.UI;
using iFactr.Core;
using iFactr.UI.Controls;

namespace MVC_App.Views
{
    [PreferredPane(Pane.Detail)]
    public class CustomerView : ListView<string>
    {
        Random myRand = new Random();

        DateTime? myDate { get; set; }
        DateTime? myTime { get; set; }

        public CustomerView()
        {
            myDate = System.DateTime.Now;
            myTime = System.DateTime.Now.AddMinutes(-60);
        }
        protected override int OnItemIdRequested(int section, int index)
        {
            return 1;  // every cell in this simplistic example has the same identifier
        }
        protected override void OnRender()
        {
            Title = "Control Alignment";
            TitleColor = Color.Purple;
            HeaderColor = Color.White;
            // The following statement did NOT work as I had intended (it was ignored).
            OutputPane = Pane.Detail;  // force to rhs on Windows (because PreferredPane isn't enough)

            var sectionFont = new Font("Arial", 22, FontFormatting.Bold);
            if (iApp.Factory.Platform == MobilePlatform.Android)
            {
                sectionFont.Name = "Roboto";
            }

            // Each section iterates through 5 variations on alignment:
            //   Default, left, right, center, stretch
            //         or
            //   Default, top, bottom, center, stretch
            Sections[0] = new Section(5, "Horizontal Alignment")
            {
                Header = { Font = sectionFont },
                Footer = new SectionFooter("End Horizontal") { Font = sectionFont, }
            };

            Sections[1] = new Section(5, "Vertical Alignment")
            {
                Header = { Font = sectionFont },
                Footer = new SectionFooter("End Vertical") { Font = sectionFont, }
            };

            Sections[2] = new Section(5, "H & V diagonal")
            {
                Header = { Font = sectionFont },
                Footer = new SectionFooter("End H & V") { Font = sectionFont, }
            };
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            // Use recycled cell, if provided.
            var myCell = recycledCell as GridCell;
            if (myCell == null)
            {
                myCell = new GridCell();

                // Two columns: a label, and the control
                myCell.Columns.Add(Column.OneStar);
                myCell.Columns.Add(2, LayoutUnitType.Star);

                // Add rows, one for each Control being displayed
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(.9, LayoutUnitType.Star);
                myCell.Rows.Add(5, LayoutUnitType.Star);  // The image
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(1.5, LayoutUnitType.Star);
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(1, LayoutUnitType.Star);
                myCell.Rows.Add(2, LayoutUnitType.Star);
                myCell.Rows.Add(2, LayoutUnitType.Star);
                myCell.Rows.Add(1, LayoutUnitType.Star);
            }

            myCell.NullifyEvents(true);  // clears all child-controls' events

            //myCell.Selected += (o, e) => { ReloadSections(); };

            int nxtRow = 0;

            bool doHorizontal = (section == 0 || section == 2);
            bool doVertical = (section == 1 || section == 2);

            string hAlign = "Default";
            string vAlign = "Default";
            HorizontalAlignment hAlignCode = HorizontalAlignment.Left;
            VerticalAlignment vAlignCode = VerticalAlignment.Top;
            switch (index)
            {
                case 1:
                    hAlign = "Left";
                    vAlign = "Top";
                    hAlignCode = HorizontalAlignment.Left;
                    vAlignCode = VerticalAlignment.Top;
                    break;
                case 2:
                    hAlign = "Right";
                    vAlign = "Bottom";
                    hAlignCode = HorizontalAlignment.Right;
                    vAlignCode = VerticalAlignment.Bottom;
                    break;
                case 3:
                    hAlign = "Center";
                    vAlign = hAlign;
                    hAlignCode = HorizontalAlignment.Center;
                    vAlignCode = VerticalAlignment.Center;
                    break;
                case 4:
                    hAlign = "Stretch";
                    vAlign = hAlign;
                    hAlignCode = HorizontalAlignment.Stretch;
                    vAlignCode = VerticalAlignment.Stretch;
                    break;
                default:
                    break;
            }
            var descrLabel = new Label[50];

            // Each control gets its own label which also states the kind of alignment
            string alignDesc;
            if (doHorizontal) { alignDesc = "(" + hAlign + ")"; }
            else { alignDesc = "(" + vAlign + ")"; }
            // 3rd and final section exercises both horizontal and vertical alignment
            if (doHorizontal && doVertical && index > 0) { alignDesc = "(" + hAlign + "&" + vAlign + ")"; }

            //  Each Control gets its own Row in the cell.
            //  Rows and Column numbering is 0-based.
            //  Control in row 0 is a Label
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl0");
            descrLabel[nxtRow].Text = "Label" + alignDesc;
            var myLabel = myCell.GetOrCreateChild<Label>("control0");
            myLabel.ColumnIndex = 1;
            myLabel.RowIndex = nxtRow;
            myLabel.Text = "Next Cell Label";
            myLabel.Font = Font.PreferredHeaderFont;
            if (index != 0)
            {
                if (doHorizontal) myLabel.HorizontalAlignment = hAlignCode;
                if (doVertical) myLabel.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            //  Next control is a Color-changing Button
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl1");
            descrLabel[nxtRow].Text = "Button" + alignDesc;
            var myButton = myCell.GetOrCreateChild<Button>("control1");
            myButton.ColumnIndex = 1;
            myButton.RowIndex = nxtRow;
            myButton.Title = "Change Color";
            myButton.BackgroundColor = Color.Blue;
            myButton.Margin = new Thickness(0, 5);
            // myButton.NullifyEvents();  // prevent Clicked event handler pile-up (use global NullifyEvents instead
            myButton.Clicked += ToggleButtonColor;

            myButton.Font = Font.PreferredLabelFont;
            if (index != 0)
            {
                if (doHorizontal) myButton.HorizontalAlignment = hAlignCode;
                if (doVertical) myButton.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            //  Next control is another Button, only this one Navigates to ScratchPad
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl2");
            descrLabel[nxtRow].Text = "NavButton" + alignDesc;
            var myNavButton = myCell.GetOrCreateChild<Button>("control2");
            myNavButton.ColumnIndex = 1;
            myNavButton.RowIndex = nxtRow;
            myNavButton.Title = "ScratchPad";
            myNavButton.BackgroundColor = Color.Brown;
            myNavButton.Margin = new Thickness(0, 5);
            myNavButton.NavigationLink = new iFactr.UI.Link("ScratchPad");
            myNavButton.Font = Font.PreferredLabelFont;
            if (index != 0)
            {
                if (doHorizontal) myNavButton.HorizontalAlignment = hAlignCode;
                if (doVertical) myNavButton.VerticalAlignment = vAlignCode;
            }

            nxtRow++;
            // Next control is a DatePicker
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl3");
            descrLabel[nxtRow].Text = "DatePicker" + alignDesc;
            var myDatePicker = myCell.GetOrCreateChild<DatePicker>("control3");
            myDatePicker.ColumnIndex = 1;
            myDatePicker.RowIndex = nxtRow;
            myDatePicker.Margin = new Thickness(0, Thickness.LargeVerticalSpacing);

            // 12Nov2014: there's a bug in WinPhone Binding.  The workaround is to use the property "Value" instead the "Date"
            if (iApp.Factory.Platform == MobilePlatform.WinPhone)
                myDatePicker.SetBinding(new Binding("Value", "myDate") { Source = this, Mode = BindingMode.TwoWay, });
            else
                myDatePicker.SetBinding(new Binding("Date", "myDate") { Source = this, Mode = BindingMode.TwoWay, });


            //myDatePicker.Date = myDate;
            //myDatePicker.DateChanged += (o, e) =>
            //    {
            //        myDate = e.NewValue;
            //    };

            //myDatePicker.Date = System.DateTime.Now.AddDays(index);
            if (index != 0)
            {
                if (doHorizontal) myDatePicker.HorizontalAlignment = hAlignCode;
                if (doVertical) myDatePicker.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next control is an Image
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl4");
            descrLabel[nxtRow].Text = "Image" + alignDesc;
            var myImage = myCell.GetOrCreateChild<Image>("control4");
            myImage.ColumnIndex = 1;
            myImage.Margin = new Thickness(0, Thickness.LargeVerticalSpacing);
            myImage.RowIndex = nxtRow;
            myImage.FilePath = iApp.Factory.ApplicationPath.AppendPath("Kenya.png");

            if (index != 0)
            {
                if (doHorizontal) myImage.HorizontalAlignment = hAlignCode;
                if (doVertical) myImage.VerticalAlignment = vAlignCode;
                if (index == 4)  // 4 means Stretch
                { myImage.Stretch = ContentStretch.Fill; }
                else
                { myImage.Stretch = ContentStretch.None; }
                //myImage.Stretch = (ContentStretch)((index - 1) % 4);
                //   ContentStretch iterates through:  None = 0, Fill = 1,Uniform = 2, UniformToFill = 3,
            }
            else { myImage.Stretch = ContentStretch.None; }

            nxtRow++;

            // Next control is a (numeric only) PasswordBox
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl5");
            descrLabel[nxtRow].Text = "PasswordBox/PIN" + alignDesc;
            var myPasswordBox = myCell.GetOrCreateChild<PasswordBox>("control5");
            myPasswordBox.ColumnIndex = 1;
            myPasswordBox.Margin = new Thickness(0, Thickness.LargeVerticalSpacing);
            myPasswordBox.RowIndex = nxtRow;
            myPasswordBox.Font = Font.PreferredSmallFont;
            myPasswordBox.Expression = "[0-9]";
            if (index != 0)
            {
                if (doHorizontal) myPasswordBox.HorizontalAlignment = hAlignCode;
                if (doVertical) myPasswordBox.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next control is a SelectList
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl6");
            descrLabel[nxtRow].Text = "SelectList" + alignDesc;
            var mySelectList = myCell.GetOrCreateChild<SelectList>("control6");
            mySelectList.ColumnIndex = 1;
            mySelectList.Margin = new Thickness(0, 10);
            mySelectList.RowIndex = nxtRow;
            mySelectList.Items = new string[] {
                "one", "two", "three" };
            mySelectList.SelectedIndex = index % 3;
            if (index != 0)
            {
                if (doHorizontal) mySelectList.HorizontalAlignment = hAlignCode;
                if (doVertical) mySelectList.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next control is a Slider
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl7");
            descrLabel[nxtRow].Text = "Slider" + alignDesc;
            var mySlider = myCell.GetOrCreateChild<Slider>("control7");
            mySlider.ColumnIndex = 1;
            mySlider.Margin = new Thickness(0, 10);
            mySlider.RowIndex = nxtRow;
            mySlider.MaximumTrackColor = Color.Red;
            mySlider.MinimumTrackColor = Color.Green;
            mySlider.Value = myRand.Next(0, 10);
            if (index != 0)
            {
                if (doHorizontal) mySlider.HorizontalAlignment = hAlignCode;
                if (doVertical) mySlider.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next control is a Switch
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl8");
            descrLabel[nxtRow].Text = "Switch" + alignDesc;
            var mySwitch = myCell.GetOrCreateChild<Switch>("control8");
            mySwitch.ColumnIndex = 1;
            mySwitch.Margin = new Thickness(0, 10);
            mySwitch.RowIndex = nxtRow;
            mySwitch.Value = (index % 2 == 0);
            mySwitch.FalseColor = Color.Red;
            mySwitch.TrueColor = Color.Green;
            if (index != 0)
            {
                if (doHorizontal) mySwitch.HorizontalAlignment = hAlignCode;
                if (doVertical) mySwitch.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next: TextArea
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl9");
            descrLabel[nxtRow].Text = "TextArea" + alignDesc;
            var myTextArea = myCell.GetOrCreateChild<TextArea>("control9");
            myTextArea.ColumnIndex = 1;
            myTextArea.Margin = new Thickness(0, 10);
            myTextArea.RowIndex = nxtRow;
            myTextArea.Text = "Multi-line TextArea";
            myTextArea.Font = Font.PreferredTextBoxFont;
            if (index != 0)
            {
                if (doHorizontal) myTextArea.HorizontalAlignment = hAlignCode;
                if (doVertical) myTextArea.VerticalAlignment = vAlignCode;
            }

            nxtRow++;

            // Next: TextBox (one line only, vs. multi-line TextArea)
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl10");
            descrLabel[nxtRow].Text = "TextBox" + alignDesc;
            var myTextBox = myCell.GetOrCreateChild<TextBox>("control10");
            myTextBox.ColumnIndex = 1;
            myTextBox.Margin = new Thickness(0, 10);
            myTextBox.RowIndex = nxtRow;
            myTextBox.Text = "1-line TextBox";
            myTextBox.BackgroundColor = Color.Orange;
            myTextBox.Font = Font.PreferredTextBoxFont;
            if (index != 0)
            {
                if (doHorizontal) myTextBox.HorizontalAlignment = hAlignCode;
                if (doVertical) myTextBox.VerticalAlignment = vAlignCode;
            }
            nxtRow++;

            // Next: TimePicker
            NextDescLabel(descrLabel, nxtRow, myCell, "lbl11");
            descrLabel[nxtRow].Text = "TimePicker" + alignDesc;
            var myTimePicker = myCell.GetOrCreateChild<TimePicker>("control11");

            myTimePicker.ColumnIndex = 1;
            myTimePicker.Margin = new Thickness(0, 5);
            myTimePicker.TimeFormat = "H:mm:ss";
            myTimePicker.RowIndex = nxtRow;
            // myTimePicker.Time = System.DateTime.Now.AddMinutes((double)myRand.Next(100));
            myTimePicker.SetBinding(new Binding("Time", "myTime") { Source = this, Mode = BindingMode.TwoWay, });
            if (index != 0)
            {
                if (doHorizontal) myTimePicker.HorizontalAlignment = hAlignCode;
                if (doVertical) myTimePicker.VerticalAlignment = vAlignCode;
            }

            nxtRow++;
            // Room for future expansion

            return myCell;
        }

        // Allocates and positions the Description Label associated with each Control.
        void NextDescLabel(Label[] descrLabel, int nxtRow, GridCell myCell, string elementID)
        {
            descrLabel[nxtRow] = myCell.GetOrCreateChild<Label>(elementID);
            descrLabel[nxtRow].ColumnIndex = 0;
            descrLabel[nxtRow].RowIndex = nxtRow;
            descrLabel[nxtRow].Font = Font.PreferredLabelFont;
        }

        void ToggleButtonColor(object o, EventArgs e)
        {
            Button myButton = (Button)o;
            System.Diagnostics.Debug.WriteLine("ToggleButtonColor, currently: " + myButton.BackgroundColor);
            // Toggle color
            if (myButton.BackgroundColor == Color.Blue)
            { myButton.BackgroundColor = Color.Yellow; }
            else { myButton.BackgroundColor = Color.Blue; }
        }
    }
}
