﻿using iFactr.UI;
using iFactr.UI.Controls;

namespace MVC_App.Views
{
    [PreferredPane(Pane.Master)]
    public class MainPanel : GridView<string>
    {
        protected override void OnRender()
        {
            Title = "Control Alignment";
            // Following caused crash on Android (due to infinite maximum height
            // in a GridView).  Sam fixed in LH2104
            VerticalScrollingEnabled = true;  // makes infinite Max Ht/Wdth on the View

            var but1 = new Button("Show Alignments")
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                NavigationLink = new Link("Alignment"),
            };
            AddChild(but1);

            var but2 = new Button("Show ScratchPad")
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                NavigationLink = new Link("ScratchPad"),
            };
            AddChild(but2);
        }
    }
}