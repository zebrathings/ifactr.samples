﻿using System;
using iFactr.UI;
using iFactr.Core;

namespace MVC_App.Views
{
    public class ScratchPad : CanvasView<string>
    {
        // put the drawing in this file:
        private string saveFileName;

        public ScratchPad()
        {
            saveFileName = iFactr.Core.StringExtensions.AppendPath(iApp.Factory.TempPath, "ScratchPadImage.png");
            StrokeThickness = 5;
            SetBackground(Color.White);
        }
        protected override void OnRender()
        {
            OutputPane = Pane.Detail;
            StrokeColor = Color.Red;
            //StrokeThickness = 14;
            //SetBackground(Color.White);  // background is not necessarily white, so set it.

			BackLink = new Link ("Main") { Text = "Go Back", };

            var drawButton = new ToolbarButton() { Title = "Draw", ForegroundColor = Color.Gray, };
            drawButton.Clicked += drawButton_Clicked;
            var clearButton = new ToolbarButton() { Title = "Clear All", ForegroundColor = Color.Gray, };
            clearButton.Clicked += clearButton_Clicked;
            var eraseButton = new ToolbarButton() { Title = "Erase", ForegroundColor = Color.Gray, };
            eraseButton.Clicked += eraseButton_Clicked;
            var saveButton = new ToolbarButton() { Title = "Save", ForegroundColor = Color.Cyan, };
            saveButton.Clicked += saveButton_Clicked;
            var recallButton = new ToolbarButton() { Title = "Recall", ForegroundColor = Color.Green, };
            recallButton.Clicked += recallButton_Clicked;

            var toolBar = new Toolbar();
            toolBar.PrimaryItems = new IToolbarItem[]
            {
                drawButton, clearButton, eraseButton,
                new ToolbarSeparator(),
                saveButton,
                recallButton,
            };
            toolBar.ForegroundColor = Color.Purple;
            Toolbar = toolBar;
        }
        void drawButton_Clicked(object sender, EventArgs e)
        {
            StrokeColor = Color.Red;
            //StrokeThickness = 14;
        }
        void eraseButton_Clicked(object sender, EventArgs e)
        {
            StrokeColor = Color.White;   // then change pen color to white to "erase"
            //StrokeThickness = 20;
        }
        void clearButton_Clicked(object sender, EventArgs e)
        {
            Clear();
        }
        void saveButton_Clicked(object sender, EventArgs eventArgs)
        {
            try { iApp.File.EnsureDirectoryExistsForFile(saveFileName); }
            catch (Exception e)
            {
                iApp.Log.Debug("Error ensuring directory exists for: {0}", e);
            }
            Save(saveFileName, true);  // Save background too
        }
        void recallButton_Clicked(object sender, EventArgs eventArgs)
        {

            try { iApp.File.Exists(saveFileName); }
            catch (Exception e)
            {
                iApp.Log.Debug("Image file does not exist: {0}", e);
            }
            Load(saveFileName);
        }
    }
}