using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.UI;
using MVC_App;

namespace Compact.Container
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            CompactFactory.Initialize();
            iApp.OnLayerLoadComplete += CompactFactory.Instance.OutputLayer;

            var theApp = new App { Style = { LayerBackgroundColor = Color.White } };
            CompactFactory.TheApp = theApp;

            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.RootForm);
        }
    }
}