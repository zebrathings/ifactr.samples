﻿using System.Windows;
using System.Windows.Data;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace Windows.Container
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            WpfFactory.Initialize();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };

            //Instantiate your iFactr application and set the Factory App property
            // Example: WpfFactory.TheApp = new HelloWorld.MyApp();
            WpfFactory.TheApp = new MVC_App.App();

            Content = WpfFactory.Instance.MainWindow;
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
        }
    }
}