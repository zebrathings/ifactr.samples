using Android.Content;
using Android.Content.PM;
using iFactr.Droid;
using MonkeySpace;
using MonoCross.Navigation;
using System;
using System.Collections.Generic;

namespace Droid.Container
{
	public class TweetView : ComposeTweetView<Speaker>
	{
		public TweetView (Speaker speaker) : base(speaker) { }

		public override void Render()
		{
			TweetIntent.PutExtra (Intent.ExtraText, string.Format ("{0}  #MonkeySpace", Model.FormattedTwitterHandle));
			DroidFactory.Instance.MainActivity.StartActivity (ResolveTweetHandler () ? TweetIntent : Intent.CreateChooser(TweetIntent, "Choose one..."));
			DroidFactory.Instance.IsBusy = false;
		}
	}
	
	public abstract class ComposeTweetView<T>: IMXView
	{
		public ComposeTweetView(T model)
		{
			Model = model;
			TweetIntent = new Intent(Intent.ActionSend);
			TweetIntent.SetType ("text/plain");
		}
		public Intent TweetIntent { get; private set;}
		
		public T Model { get; set; }
		public Type ModelType { get { return typeof(T); } }
		public abstract void Render();
		public void SetModel(object model)
		{
			Model = (T)model;
		}
		public bool ResolveTweetHandler()
		{
			var pm = DroidFactory.Instance.MainActivity.PackageManager;
			IList<ResolveInfo> lract 
				= pm.QueryIntentActivities(TweetIntent, PackageInfoFlags.MatchDefaultOnly);

			foreach(ResolveInfo ri in lract)
			{
				if(ri.ActivityInfo.Name.EndsWith(".SendTweet") || ri.ActivityInfo.Name.EndsWith(".PostActivity"))
				{
					TweetIntent.SetClassName(ri.ActivityInfo.PackageName,
					                         ri.ActivityInfo.Name);
					return true;
				}
			}
			return false;
		}
	}
}

