using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using iFactr.Core;
using iFactr.Droid;

using System.Collections.Generic;

namespace Droid.Container
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            DroidFactory.Instance.MainActivity = this;
            DroidFactory.TheApp = new MonkeySpace.MyApp();
			iApp.OnLayerLoadComplete += (layer) =>
            {
			    if (layer.NavContext.NavigatedUrl.StartsWith("Tweet"))
                {
                    int id = int.Parse (layer.NavContext.NavigatedUrl.Split (char.Parse ("/"))[1]);
                    TweetView tv = new TweetView(MonkeySpace.Data.Speakers.Find(s => s.Id == id));
                    tv.Render ();
                }
                else DroidFactory.Instance.OutputLayer(layer);
            };
            base.OnCreate(bundle);
        }
    }
}