using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Touch;

namespace MonkeySpace.Touch
{
	public class Application
	{
		static void Main (string[] args)
		{
			UIApplication.Main (args, null, "AppDelegate");
		}
	}

	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
            TouchFactory.Initialize();
			TouchFactory.TheApp = new MonkeySpace.MyApp();
			//iApp.OnLayerLoadComplete -= TouchFactory.Instance.OutputLayer;
			iApp.OnLayerLoadComplete += this.OutputLayerOrView;
			iApp.Navigate (TouchFactory.TheApp.NavigateOnLoad);
			TouchFactory.Instance.InitializeViews ();
			return true;

		}

		public void OutputLayerOrView(iLayer layerorview)
		{
			if (layerorview.NavContext.NavigatedUrl.StartsWith("Tweet"))
			{
				int id = int.Parse (layerorview.NavContext.NavigatedUrl.Split (char.Parse ("/"))[1]);
				TweetView tv = new TweetView(MonkeySpace.Data.Speakers.FirstOrDefault(s => s.Id == id));
				tv.Render ();
			}
			else
				TouchFactory.Instance.OutputLayer (layerorview);
		}
		public override void OnActivated (UIApplication application)
		{
		}
	}
}
