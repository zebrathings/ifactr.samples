
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.Twitter;
using MonoTouch.UIKit;
			
using MonoCross.Navigation;
//using MonoCross.Touch;

using iFactr.Touch;

using MonkeySpace;

namespace MonkeySpace.Touch
{
	// TODO: Change the "ModelClassNameHere" below to be the class of the Model for this View
	public class TweetView : ComposeTweetViewController<Speaker>
	{
		public TweetView(Speaker speaker) : base(speaker) { }
		public override void Render ()
		{
			controller.Title = "MonkeyTweet";
			controller.SetInitialText(string.Format ("@{0}  #MonkeySpace", Model.TwitterHandle));

			controller.SetCompletionHandler((result) => {
				if (TouchFactory.Instance.Platform == iFactr.Core.MobilePlatform.iPad)
				{
					var parent = TouchFactory.Instance.TopViewController as ISplitViewController;
					var nav = parent.DetailViewController as UINavigationController;
					var table = nav.TopViewController as iFactr.Touch.TableViewController;
					table.TableView.DeselectRow(NSIndexPath.FromRowSection (0, 1), false);
				}
				else
				{
					var parent = TouchFactory.Instance.TopViewController as UITabBarController;
					var nav = parent.SelectedViewController as UINavigationController;
					var table = nav.TopViewController as iFactr.Touch.TableViewController;
					table.TableView.DeselectRow(NSIndexPath.FromRowSection (0, 1), false);
				}
				controller.DismissViewController (true, null);
			});
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, HandleKeyboardWillShow);			
			TouchFactory.Instance.TopViewController.PresentViewController (controller, true, null);
			TouchFactory.Instance.IsLoading = false;
		}
		void HandleKeyboardWillShow(NSNotification notification)
		{
			UIView fr = FindFirstResponderInView (controller.View);
			if (fr is UITextView)
			{
				var tv = fr as UITextView;
				tv.SelectedRange = new NSRange(Model.TwitterHandle.Length + 2, 0);
			}
		}
		UIView FindFirstResponderInView(UIView view)
		{
			if (view.IsFirstResponder)
				return view;
			foreach (UIView subview in view.Subviews)
			{
				UIView result = this.FindFirstResponderInView (subview);
				if (result != null)
					return result;
			}
			return null;
		}
	}

	public abstract class ComposeTweetViewController<T>: IMXView
	{
		public ComposeTweetViewController(T model) : base()
		{
			Model = model;
			controller = new TWTweetComposeViewController();
		}
		protected TWTweetComposeViewController controller;

		public T Model { get; set; }
		public Type ModelType { get { return typeof(T); } }
		public abstract void Render();
		public void SetModel(object model)
		{
			Model = (T)model;
		}
	}
}

