using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class ITRMobility : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "ITR Mobility";
			
			iPanel photo = new iPanel();
			photo.Title = "ITR Mobility";
			photo.InsertImageFloatLeft("ITRm.png");
			
			iList info = new iList();
			info.Text = "About ITR Mobility";
			info.Add (new SubtextItem("http://www.itr-mobility.com", "Sponsor Website", "itr-mobility.com")
			          { Icon = new Icon("icon.png") });
			info.Add (new SubtextItem("http://www.monocross.net", "MonoCross Website", "monocross.net")
			          { Icon = new Icon("icon.png") });
			info.Add (new SubtextItem("http://www.ifactr.com", "iFactr Website", "ifactr.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "ITR Mobility is a management and IT consulting firm with " +
				"hundreds of staff nationwide, devoted to creating mobile strategies and enterprise architectures " +
				"for Fortune 500 companies. Our clients include 3M, Ameriprise Financial, Best Buy, Boston Scientific, " +
				"Ecolab, General Mills, Medtronic, St. Jude Medical, Target, Thomson Reuters, UnitedHealth Group, " +
				"and Wells Fargo. As the creator of the iFactr mobile development framework, \r\n \r\nITR Mobility has " +
				"deployed enterprise applications in some of the largest retail, healthcare and financial companies " +
			    "in North America, as well as powered the largest iPad deployment in 2010.  ITR mobility is a proud " +
			    "sponsor of the open source Monocross project. Learn more about the framework, download the Monocross " +
			    "Utilities, and contribute to the community at the Monocross.net site. \r\n \r\n" +
			    "The iFactr enterprise cross-platform framework was featured by Microsoft Corp. at the launch of Visual " +
			    "Studio 2012. iFactr gives software developers the ability to create enterprise mobile applications for " +
			    "any mobile platform using the Microsoft .NET Framework. Because iFactr is an extension of Visual Studio 2012, " +
			    "developers can create and deliver cross-platform mobile applications easily, without the large learning curve " +
			    "involved with a new development language or platform-specific nuances. Organizations with significant Microsoft " +
			    "application development investments can leverage code, infrastructure and technical expertise to quickly deliver " +
			    "state-of-the-art mobile applications, fully integrated with back-end data and systems."));
			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
