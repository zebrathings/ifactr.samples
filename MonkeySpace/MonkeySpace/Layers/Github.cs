using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Github : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Github";
			
			iPanel photo = new iPanel();
			photo.Title = "Github";
			photo.InsertImageFloatLeft("Github.png");
			
			iList info = new iList();
			info.Text = "About Github";
			info.Add (new SubtextItem("http://github.com", "Sponsor Website", "github.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "GitHub is the best place to share code with friends, co-workers, " +
			                                 "classmates, and complete strangers. Over two million people use GitHub to " +
			                                 "build amazing things together.  With the collaborative features of GitHub.com, " +
			                                 "our desktop and mobile apps, and GitHub Enterprise, it has never been easier " +
			                                 "for individuals and teams to write better code, faster.  Originally founded by " +
			                                 "Tom Preston-Werner, Chris Wanstrath, and PJ Hyett to simplify sharing code, " +
			                                 "GitHub has grown into the largest code host in the world."));			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
