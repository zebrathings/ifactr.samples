using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class SessionDetail : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			//Title = "Session";
			
			int id = parameters.ContainsKey("id") ? int.Parse(parameters["id"]) : -1;
			Session sess = Data.Sessions.FirstOrDefault(s => s.Id == id);

			Title = sess.Title;
			//var firstName = sess.Name.Substring(0, sess.Name.IndexOf(" ") + 1);
			
			iPanel title = new iPanel();
			title.InsertImageFloatLeft("icon.png", "110", "110");
			title.AppendSubHeading(sess.Title);
			title.AppendLine (sess.FormattedBegins);

			
			iList spkinfo = new iList();
			foreach (Speaker spkr in sess.Speakers)
			{				
				spkinfo.Text = sess.Speakers.Count > 1 ? "About the Speakers" : "About the Speaker";
				spkinfo.Add(new ShopItem(string.Format("Speakers/{0}", spkr.Id.ToString()),
				                         spkr.Name, 
				                         spkr.Bio,
				                         spkr.FormattedTwitterHandle) { 
					Icon = new Icon(spkr.LocalHeadshotUrl) });
			}
			
			iPanel abstr = new iPanel();
			abstr.Title = "Abstract";
			abstr.Append(sess.Abstract);
			
			Items.Add(title);
			Items.Add(spkinfo);
			Items.Add(abstr);
		}
	}
}
