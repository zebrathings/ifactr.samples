using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Couchbase : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Couchbase";
			
			iPanel photo = new iPanel();
			photo.Title = "Couchbase";
			photo.InsertImageFloatLeft("Couchbase.png");
			
			iList info = new iList();
			info.Text = "About Couchbaes";
			info.Add (new SubtextItem("http://couchbase.com", "Sponsor Website", "github.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "Couchbase is open source NoSQL for mission-critical systems. " +
				"Discover why developers around the world trust Couchbase Server to deliver the flexibility, " +
				"scalability, and performance they need for their interactive web applications. Join us."));			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
