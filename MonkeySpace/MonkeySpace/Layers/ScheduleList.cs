using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	public class ScheduleList : Layer, IMasterLayer
	{
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Schedule";
			
			var schedule = Data.Schedule;
			foreach (Day day in schedule.Days)
			{
				iList list = new iList();
				list.Text = day.Date.ToString ("D");

				var sessions = day.Sessions;
				sessions.Sort((x, y) => { return x.Begins.CompareTo(y.Begins); });
				
				foreach (Session sess in sessions)
				{
					var speakerName = string.Empty;
					if (sess.Speakers.Count > 0)
					{
						if (sess.Speakers.Count == 1)
						{
							speakerName = sess.Speakers[0].Name;
						}
						else
						{
							for(int i = 0; i < sess.Speakers.Count; i++)
							{
								var spkr = sess.Speakers[i];
								if (i == 0)
									speakerName += spkr.Name;
								else if (i + 1 < sess.Speakers.Count)
									speakerName += string.Format (", {0}", spkr.Name);
								else
									speakerName += string.Format (" and {0}", spkr.Name);
							}
						}
					}
					list.Add(new MessageItem(string.Format("Sessions/{0}", sess.Id.ToString()),
					                         sess.Title, 
					                         string.Format ("{0} - {1}", sess.Begins.ToString("dddd h:mm tt"), sess.Location),
					                         "",
					                         speakerName) { 
						Icon = new Icon("icon.png") });
				}
				Items.Add(list);
			}
		}
	}
}
