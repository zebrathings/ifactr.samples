using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Venue : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Microsoft Nerd Center";
			
			iPanel photo = new iPanel();
			photo.Title = "Microsoft NERD Center";
			photo.InsertImageFloatLeft("venue-image.jpg");

			iList info = new iList();
				info.Text = "About the Venue";
			info.Add (new SubtextItem("https://maps.google.com/maps?ie=UTF8&cid=6363975811005716971&q=Microsoft+New+England+Research+and+Development+Center&iwloc=A&gl=US&hl=en", "Venue Map", "@ Google Maps")
			          { Icon = new Icon("map.png") });
			info.Add (new SubtextItem("http://microsoftcambridge.com/", "Venue Website", "www.microsoftcambridge.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "MonkeySpace will be held at the Microsoft NERD Center " +
					"located in Boston, MA one of the most historic cities in the United States. " +
					"We also negotiated a group rate at the Boston Marriott Cambridge to allow everybody who attends to stay at the same hotel. " +
					"You can find more info about the venue and hotel accommodations below."));

			Items.Add(photo);
			Items.Add(info);
		}
	}
}
