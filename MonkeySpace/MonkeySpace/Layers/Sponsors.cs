using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Sponsors : Layer, IMasterLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Sponsors";

			iList list = new iList();
			list.Text = "Brought to you by...";
			list.Add (new SubtextItem("Sponsors/Couchbase", "Couchbase", "Simple, Fast, Elastic"){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/Github", "Github", "Social Coding"){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/ITRMobility", "ITR Mobility", "Think. Work. Mobile."){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/LessFilms", "Less Films", "Rediculously good at crafting videos."){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/Microsoft", "Microsoft", "Realize your Potential"){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/Moncai", "Moncaí", "Moncaí - Irish for monkey."){ Icon = new Icon("icon.png") });
			list.Add (new SubtextItem("Sponsors/Xamarin", "Xamarin", "Cross-Platform Apps. No Compromises."){ Icon = new Icon("icon.png") });

			Items.Add(list);
		}
	}
}
