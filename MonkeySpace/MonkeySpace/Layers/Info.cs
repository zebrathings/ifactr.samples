
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	public class Info : Layer, IMasterLayer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			Title = "Info";

			iList list = new iList();
			list.Text = "More Monkeyspace Info";
			list.Items.Add (new SubtextItem("Venue", "Venue", "Microsoft NERD Center") { Icon = new Icon("icon.png") });
			list.Items.Add (new SubtextItem("Hotel", "Hotel", "Boston Marriott Cambridge") { Icon = new Icon("icon.png") });
			list.Items.Add (new SubtextItem("Sponsors", "Sponsors", "Making Monkeyspace Possible") { Icon = new Icon("icon.png") });
			Items.Add (list);
		}
	}
}

