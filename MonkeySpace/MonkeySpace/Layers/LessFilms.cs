using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class LessFilms : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Less Films";
			
			iPanel photo = new iPanel();
			photo.Title = "Less Films";
			photo.InsertImageFloatLeft("LessFilms.png");
			
			iList info = new iList();
			info.Text = "About Less Films";
			info.Add (new SubtextItem("http://lessfilms.com", "Sponsor Website", "lessfilms.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "Forged in the fiery underground film-wrestling rings of Florida, " +
				"LessFilms brings it's unique brand of humor and delight to your project. Not to mention, we're super " +
				"easy to work with as long as you go along with all of our ideas and never give us any lip."));
			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
