﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
    class SpeakerDetail : Layer, IDetailLayer
    {
        // Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
        public override void Load(Dictionary<string, string> parameters)
        {
            //Title = "Speaker";

            int id = parameters.ContainsKey("id") ? int.Parse(parameters["id"]) : -1;
            Speaker spkr = Data.Speakers.FirstOrDefault(s => s.Id == id);

			Title = spkr.Name;

			List<Session> sessions = Data.Sessions.Where (s => s.Speakers.Any(p => p.Id == id)).ToList ();

            iPanel headshot = new iPanel();
            headshot.InsertImageFloatLeft(spkr.LocalHeadshotUrl, "110", "110");
            headshot.AppendLine();
            headshot.AppendSubHeading(spkr.Name);

            iList list = new iList();
            list.Text = string.Format("Connect with {0}", spkr.FirstName);
            list.Add(new SubtextItem(string.Format("Tweet/{0}", spkr.Id),
                                      "Tweet", spkr.FormattedTwitterHandle) 
                                 { Icon = new Icon("http://aux.iconpedia.net/uploads/1061260918.png") });
			list.Add(new SubtextItem(sessions.Count > 1 ? string.Format ("Speakers/{0}/Sessions", spkr.Id) : string.Format ("Sessions/{0}", sessions[0].Id),
                                      "Sessions", string.Format("with {0}", spkr.FirstName)) 
                                      { Icon = new Icon("icon.png") });

            iPanel bio = new iPanel();
			bio.Title = string.Format("About {0}", spkr.FirstName);
            bio.Append(spkr.Bio);

            Items.Add(headshot);
            Items.Add(list);
            Items.Add(bio);
        }
    }
}
