﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
    public class SpeakerList : Layer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Speakers";

			Data.Speakers.Sort ((x,y) => { return x.LastName.CompareTo (y.LastName); });
			SpeakerSearchList list = new SpeakerSearchList();
            foreach (Speaker spkr in Data.Speakers)
            {
				list.Add(new ShopItem(string.Format("Speakers/{0}", spkr.Id.ToString()),
                                                      spkr.Name, 
                                                      spkr.Bio,
				                      			      spkr.FormattedTwitterHandle) 
				         							  { Icon = new Icon(spkr.LocalHeadshotUrl) });
            }
            Items.Add(list);
        }
    }
	public class SpeakerSearchList : SearchList
	{
		public List<MessageItem> MessageItemsSource { get; set; }
		public override void PerformSearch (string navigationUri, string searchTerm)
		{		
			if (ItemsSource == null)
				ItemsSource = Items;
			if (searchTerm != null)
				searchTerm = searchTerm.Trim();
			if (string.IsNullOrEmpty(searchTerm))
			{
				this.Items = ItemsSource;
				return;
			}
			
			string[] delim = new string[] { " " };
			string[] terms = searchTerm.ToLower().Split(delim, StringSplitOptions.RemoveEmptyEntries);

			List<iItem> results = this.ItemsSource.Where(item => item.Text.Clean().Contains(terms[0])).ToList ();
			
			//filter the result set by each subsequent keyword
			for (int i = 1; i < terms.Length; i++)
			{
				results = results.Where(item => item.Text.Clean().Contains(terms[i])).ToList ();
			}
			this.Items = results;
		}
	}
}
