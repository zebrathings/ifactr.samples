using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Msft : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Microsoft";
			
			iPanel photo = new iPanel();
			photo.Title = "Microsoft";
			photo.InsertImageFloatLeft("Microsoft.png");
			
			iList info = new iList();
			info.Text = "About Microsoft";
			info.Add (new SubtextItem("http://microsoft.com", "Sponsor Website", "microsoft.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "At Microsoft, we're motivated and inspired every day by how our customers" +
				"use our software to find creative solutions to business problems, develop breakthrough ideas, and stay " +
			                                 "connected to what's most important to them.\r\n\r\nWe are committed long term " +
			                                 "to the mission of helping our customers realize their full potential. Just as " +
			                                 "we constantly update and improve our products, we want to continually evolve our " +
			                                 "company to be in the best position to accelerate new technologies as they emerge " +
			                                 "and to better serve our customers."));			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
