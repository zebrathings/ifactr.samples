using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Moncai : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Moncaí";
			
			iPanel photo = new iPanel();
			photo.Title = "Moncaí";
			photo.InsertImageFloatLeft("Moncai.png");
			
			iList info = new iList();
			info.Text = "About Moncaí";
			info.Add (new SubtextItem("http://moncai.com", "Sponsor Website", "moncai.com")
			          { Icon = new Icon("icon.png") });
			info.Add (new SubtextItem("http://sinesignal.com/blog/archives/2010/11/11/introducing-moncai/", "Sponsor Blog", "@ sinsignal.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "Moncaí - Irish for monkey, pronounced monkey. \r\n\r\n" +
				"Coming soon, the easiest way to deploy and scale your .NET/Mono web apps, by pushing " +
				"to the cloud using Git or Mercurial. Once deployed, you can scale your app up and down " +
			    "based on load and pay for only what you use. \r\n\r\nThe best way " +
			    "we can think of rolling this service out and having you pummel it, is by creating a " +
			    "waiting list. Therefore, if you're interested in participating in our private beta, please " +
			    "feel free to sign up. We will select a certain number of users on a first come first serve " +
			    "basis and add more and more when we can. You can also read more about the announcement in our blog post."));
			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
