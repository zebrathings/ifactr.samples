using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Xamarin : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Xamarin";
			
			iPanel photo = new iPanel();
			photo.Title = "Xamarin";
			photo.InsertImageFloatLeft("Xamarin.png");
			
			iList info = new iList();
			info.Text = "About Xamarin";
			info.Add (new SubtextItem("http://xamarin.com", "Sponsor Website", "xamarin.com")
			          { Icon = new Icon("icon.png") });
			info.Add (new SubtextItem("http://xamarin.com/monotouch", "MonoTouch Website", "xamarin.com/monotouch")
			          { Icon = new Icon("icon.png") });
			info.Add (new SubtextItem("http://xamarin.com/monoforandroid", "Mono for Android Website", "xamarin.com/monoforandroid")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "Our mission is to produce the best software development " +
			                                 "tools in the world, and to make it fast, easy and fun to build great mobile apps. " +
			                                 "\r\n\r\nXamarin was founded in May 2011 and is headquartered in San Francisco, with " +
			                                 "an engineering office in Cambridge, MA."));
			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
