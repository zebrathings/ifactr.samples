using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	public class SessionList : Layer, IMasterLayer
	{
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Sessions";
			var id = parameters.ContainsKey ("id") ? int.Parse (parameters["id"]) : -1;

			iList list;
			List<Session> sessions;
			if (id > -1)
			{
				list = new iList();
				sessions = Data.Sessions.Where (s => s.Speakers.Any(p => p.Id == id)).ToList ();
			}
			else
			{
				list = new SessionSearchList();
				sessions = Data.Sessions;
			}

			sessions.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
			foreach (Session sess in sessions)
			{
				var speakerName = string.Empty;
				if (sess.Speakers.Count > 0)
				{
					if (sess.Speakers.Count == 1)
					{
						speakerName = sess.Speakers[0].Name;
					}
					else
					{
						for(int i = 0; i < sess.Speakers.Count; i++)
						{
							var spkr = sess.Speakers[i];
							if (i == 0)
								speakerName += spkr.Name;
							else if (i + 1 < sess.Speakers.Count)
								speakerName += string.Format (", {0}", spkr.Name);
							else
								speakerName += string.Format (" and {0}", spkr.Name);
						}
					}
				}
				list.Add(new MessageItem(string.Format("Sessions/{0}", sess.Id.ToString()),
				                      sess.Title, 
				                      sess.FormattedBegins,
				                         "",
				                      speakerName) { 
					Icon = new Icon("icon.png") });
			}
			Items.Add(list);
		}
	}
	public class SessionSearchList : SearchList
	{
		public List<MessageItem> MessageItemsSource { get; set; }
		public override void PerformSearch (string navigationUri, string searchTerm)
		{		
			if (MessageItemsSource == null)
			{
				MessageItemsSource = new List<MessageItem>();
				foreach(iItem item in Items)
				{					
					MessageItemsSource.Add (item as MessageItem);
				}
			}
			if (searchTerm != null)
				searchTerm = searchTerm.Trim();
			if (string.IsNullOrEmpty(searchTerm))
			{
				foreach (MessageItem item in MessageItemsSource)
				{
					this.Items.Add (item);
				}
				return;
			}

			string[] delim = new string[] { " " };
			string[] terms = searchTerm.ToLower().Split(delim, StringSplitOptions.RemoveEmptyEntries);
			List<MessageItem> results = MessageItemsSource.Where(item =>
			                                             item.Text.Clean().Contains(terms[0]) ||
			                                             item.MessageText.Clean().Contains(terms[0]) ||			                                
			                                             item.Subtext.Clean().Contains(terms[0])).ToList();
			
			//filter the result set by each subsequent keyword
			for (int i = 1; i < terms.Length; i++)
			{
				results = results.Where(item =>
				                        item.Text.Clean().Contains(terms[i]) ||
				                        item.MessageText.Clean().Contains(terms[i]) ||	
				                        item.Subtext.Clean().Contains(terms[i])).ToList();
			}
			this.Items.Clear ();			
			foreach (iItem item in results)
			{
				this.Items.Add (item);
			}
		}
	}
}
