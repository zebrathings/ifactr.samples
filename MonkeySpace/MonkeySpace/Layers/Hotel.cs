using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace MonkeySpace.Layers
{
	class Hotel : Layer, IDetailLayer
	{
		// Add code to initialize your layer here.  For more information, see http://www.factr.com/documentation
		public override void Load(Dictionary<string, string> parameters)
		{
			Title = "Boston Marriot Cambridge";
			
			iPanel photo = new iPanel();
			photo.Title = "Boston Marriot Cambridge";
			photo.InsertImageFloatLeft("Hotel.png");

			iList info = new iList();
				info.Text = "About the Hotel";
			info.Add (new SubtextItem("https://maps.google.com/maps?ie=UTF-8&q=Boston+Marriott+Cambridge&iwloc=A&gl=US&hl=en", "Hotel Map", "@ Google Maps")
			          { Icon = new Icon("map.png") });
			info.Add (new SubtextItem("http://www.marriott.com/hotels/travel/boscb-boston-marriott-cambridge/", "Hotel Website", "@ marriott.com")
			          { Icon = new Icon("icon.png") });
			info.Add(new VariableContentItem(null, "As a top choice among Cambridge hotels, the Boston Marriott Cambridge " +
				"is situated across the Charles River from downtown Boston and is the perfect location for your next visit " +
				"to the city. This Cambridge hotel is positioned in Kendall Square near popular Boston area attractions. " +
				"Top universities such as MIT, Harvard, and Boston University are just minutes away, as well as " +
				"Massachusetts General Hospital & the Children's Hospital. Guests of this hotel in Cambridge will " +
				"enjoy the close proximity to Harvard Square, Fenway Park, TD Banknorth Garden, the North End and the " +
				"Museum of Science. Enjoy casual American fare at our Cambridge, MA hotel’s new restaurant, Champions. " +
				"The Boston Marriott Cambridge hotel is also an ideal event and meeting facility. With ten breakout " +
				"rooms and 12,000 sq ft of event space, this hotel can accommodate groups of any size. With easy access " +
				"to Logan Airport, I-93 and the subway, the Boston Marriott Cambridge is the ideal lodging choice for business " +
				"trips and vacations in Boston, MA."));
			
			Items.Add(photo);
			Items.Add(info);
		}
	}
}
