using System;
using System.Collections.Generic;

namespace MonkeySpace
{
	public class Schedule
	{
		public List<Day> Days { get; set; }
	}
	public class Day
	{
		DateTime _date;
		public DateTime Date { get { return _date.AddDays (1); } set { _date = value; } }
//		public DateTime Date { get; set; }
		public List<Session> Sessions { get; set; }
	}
}

