using System;
using System.Collections.Generic;

namespace MonkeySpace
{
	public class Session
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Location { get; set; }
		public DateTime Begins { get; set; }
		public DateTime Ends { get; set; }
		public string Abstract { get; set; }
		public List<Speaker> Speakers { get; set; }

		public string FormattedBegins { get { return string.Format ("{0} - {1}", Begins.ToString("dddd h:mm tt"), Location); } }
	}
}

