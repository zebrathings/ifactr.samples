using System;

namespace MonkeySpace
{
	public class Speaker
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string TwitterHandle { get; set; }
		public string HeadshotUrl { get; set; }
		public string Bio { get; set; }

		public string FirstName { get { return Name.Substring(0, Name.IndexOf(" ")); } }
		public string LastName { get { return Name.Substring(Name.IndexOf(" ") + 1, Name.Length - Name.IndexOf (" ") - 1); } }

		public string FormattedTwitterHandle { get { return TwitterHandle.StartsWith ("@") ? TwitterHandle : string.Format ("@{0}", TwitterHandle); } }
		public string LocalHeadshotUrl { get { return HeadshotUrl.Replace ("/images/speakers/", string.Empty); } }
	}
}

