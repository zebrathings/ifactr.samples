using System;

using iFactr.Core.Layers;

namespace MonkeySpace
{
	public class Tabs : NavigationTabs
	{
		public override void Load (System.Collections.Generic.Dictionary<string, string> parameters)
		{
			TabItems.Add (new Tab("Schedule", "Schedule", "schedule.png"));
			TabItems.Add (new Tab("Speakers", "Speakers", "speakers.png"));
			TabItems.Add (new Tab("Sessions", "Sessions", "sessions.png"));
			TabItems.Add (new Tab("Info", "Info", "info.png"));
		}
	}
}

