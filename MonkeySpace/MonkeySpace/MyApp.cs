﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;

using iFactr.Utilities;
using iFactr.Utilities.Serialization;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using MonkeySpace.Layers;

namespace MonkeySpace
{
    public class MyApp : iApp
    {
        // Add code to initialize your application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            // Set the application title
            Title = "Monkeyspace";

            // Optional: Set the application style
            // Example Style.HeaderColor = new Style.Color(0, 0, 0);

            // Add navigation mappings
			NavigationMap.Add ("", new Tabs());
            NavigationMap.Add("Speakers", new SpeakerList());
            NavigationMap.Add("Speakers/{id}", new SpeakerDetail());			
			NavigationMap.Add("Speakers/{id}/Sessions", new SessionList());

			NavigationMap.Add ("Sessions", new SessionList());
			NavigationMap.Add("Sessions/{id}", new SessionDetail());	

			NavigationMap.Add ("Schedule", new ScheduleList());

			NavigationMap.Add ("Info", new Info());			
			NavigationMap.Add ("Venue", new Venue());			
			NavigationMap.Add ("Hotel", new Hotel());

			NavigationMap.Add ("Sponsors", new Sponsors());			
			NavigationMap.Add ("Sponsors/Couchbase", new Couchbase());			
			NavigationMap.Add ("Sponsors/Github", new Github());			
			NavigationMap.Add ("Sponsors/ITRMobility", new ITRMobility());		
			NavigationMap.Add ("Sponsors/LessFilms", new LessFilms());		
			NavigationMap.Add ("Sponsors/Microsoft", new Msft());				
			NavigationMap.Add ("Sponsors/Moncai", new Moncai());		
			NavigationMap.Add ("Sponsors/Xamarin", new Xamarin());

            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }

    public static class Data
    {
        public static List<Speaker> Speakers { get; set; }
        public static List<Session> Sessions { get; set; }
        public static Schedule Schedule { get; set; }

        static Data()
        {
            // retrieve data from server
			Data.Schedule = SerializerFactory.Create<Schedule>
				(SerializationFormat.JSON).DeserializeObject
					(Network.Get("http://monkeyspace.org/data/schedule.json"));
			Data.Speakers = SerializerFactory.Create<Speaker>
                (SerializationFormat.JSON).DeserializeList
                (Network.Get("http://monkeyspace.org/data/speakers.json"));
            Data.Sessions = SerializerFactory.Create<Session>
                (SerializationFormat.JSON).DeserializeList
                (Network.Get("http://monkeyspace.org/data/sessions.json"));

//			CacheIndex cache = CacheIndex.Create ("http://monkeyspace.org");
//
//			foreach (Speaker spkr in Speakers)
//			{
//				cache.EnsureCurrentCache (cache.Get ( string.Format ("{0}{1}", cache.BaseUri, spkr.HeadshotUrl)));
//			}
        }
    }
}
